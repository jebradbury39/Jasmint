package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.SandboxModeInfo;
import type_env.TypeEnvironment.TypeStatus;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class MemlimitExpression extends AbstractExpression {

  public final Type returnType;
  public final Expression limitValue;
  public final BlockStatement body;
  public final CatchExcSection catchSection;

  public MemlimitExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Type returnType, Expression limitValue, BlockStatement body,
      CatchExcSection catchSection) {
    super(lineNum, columnNum, AstType.MEMLIMIT_EXPRESSION, preComments, postComments);
    this.returnType = returnType;
    this.limitValue = limitValue;
    this.body = body;
    this.catchSection = catchSection;

    determinedType = returnType;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType.renameIds(renamings, msgState), limitValue.renameIds(renamings, msgState),
        (BlockStatement) body.renameIds(renamings, msgState),
        (CatchExcSection) catchSection.renameIds(renamings, msgState));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType.replaceType(mapFromTo), (Expression) limitValue.replaceType(mapFromTo),
        (BlockStatement) body.replaceType(mapFromTo),
        (CatchExcSection) catchSection.replaceType(mapFromTo));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType.basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        limitValue.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        (BlockStatement) body
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get(),
        (CatchExcSection) catchSection
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(val));
  }

  @Override
  public String toString(String indent) {
    return "memlimit " + returnType + " (" + limitValue.toString(indent) + ") "
        + body.toString(indent) + " " + catchSection.toString(indent);
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Nullable<SandboxModeInfo> sandboxModeInfo = tenv.inSandboxMode();
    if (sandboxModeInfo.isNotNull()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.SANDBOX,
          "memlimit expressions are not allowed in sandbox mode (" + sandboxModeInfo + ")", lineNum,
          columnNum);
      return Optional.empty();
    }

    // first check returnType is valid
    Optional<TypeStatus> optTyStatus = tenv.lookupType(returnType, lineNum, columnNum);
    if (!optTyStatus.isPresent()) {
      return Optional.empty();
    }
    TypeStatus tyStatus = optTyStatus.get();
    if (tyStatus == TypeStatus.UNDEFINED) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "memlimit block cannot return '" + returnType + "' (this type is undefined)", lineNum,
          columnNum);
      return Optional.empty();
    }
    
    determinedType = determinedType.normalize(tenv, lineNum, columnNum).get();

    // now ensure that limitValue returns a uint32
    Optional<TypecheckRet> retOpt = limitValue.typecheck(tenv, Nullable.empty());
    if (!retOpt.isPresent()) {
      return Optional.empty();
    }
    Type limitType = retOpt.get().type;
    if (!tenv.canSafeCast(limitType, IntType.Create(false, 32))) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "memlimit limit must be uint32 type, not: " + limitType, lineNum, columnNum);
      return Optional.empty();
    }

    // ensure that the body will return a value of returnType
    TypeEnvironment bodyTenv = tenv.extend();
    Optional<Type> typeOpt = body.typecheckAsFunctionBody(bodyTenv, returnType);
    if (!typeOpt.isPresent()) {
      return Optional.empty();
    }

    // ensure that the catch section will return a value of returnType
    typeOpt = catchSection.typecheckAsFunctionBody(tenv, returnType);
    if (!typeOpt.isPresent()) {
      return Optional.empty();
    }

    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.MemlimitExpression.Builder subDoc = AstProto.MemlimitExpression.newBuilder();

    subDoc.setReturnType(returnType.serialize());
    subDoc.setLimitValue(limitValue.serialize());
    subDoc.setBody(body.serialize());
    subDoc.setCatchSection(catchSection.serialize());

    AstProto.Ast.Builder document = preSerialize();
    document.setMemlimitExpression(subDoc);
    return document;
  }

  public static MemlimitExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.MemlimitExpression document) throws SerializationError {

    return new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        AbstractType.deserialize(document.getReturnType()),
        AbstractExpression.deserialize(document.getLimitValue()),
        (BlockStatement) AbstractStatement.deserialize(document.getBody()),
        (CatchExcSection) AbstractExpression.deserialize(document.getCatchSection()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = catchSection.findFreeVariables();
    freeVars.putAll(body.findFreeVariables());
    freeVars.putAll(limitValue.findFreeVariables());
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof RecursionExpression)) {
      return false;
    }
    MemlimitExpression ourOther = (MemlimitExpression) other;

    if (!returnType.equals(ourOther.returnType)) {
      return false;
    }
    if (!limitValue.compare(ourOther.limitValue)) {
      return false;
    }
    if (!body.compare(ourOther.body)) {
      return false;
    }
    return catchSection.compare(ourOther.catchSection);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType, (Expression) limitValue.replace(target, with),
        (BlockStatement) body.replace(target, with),
        (CatchExcSection) catchSection.replace(target, with));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = limitValue.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    tmp = body.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }

    return catchSection.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    Pair<List<DeclarationStatement>, Ast> tmpPair = limitValue.toAnf(tuning);
    insert.addAll(tmpPair.a);
    final Expression newLimitValue = (Expression) tmpPair.b;

    tmpPair = body.toAnf(tuning);
    insert.addAll(tmpPair.a);
    final BlockStatement newBody = (BlockStatement) tmpPair.b;

    tmpPair = catchSection.toAnf(tuning);
    insert.addAll(tmpPair.a);
    final CatchExcSection newCatchSection = (CatchExcSection) tmpPair.b;

    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType, newLimitValue, newBody, newCatchSection);
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType, (Expression) limitValue.replaceVtableAccess(project, msgState).get(),
        (BlockStatement) body.replaceVtableAccess(project, msgState).get(),
        (CatchExcSection) catchSection.replaceVtableAccess(project, msgState).get());
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    returnType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    limitValue.addDependency(modDepGraph, ourModNode, tenv);
    body.addDependency(modDepGraph, ourModNode, tenv);
    catchSection.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (returnType.containsType(types)) {
      return true;
    }
    return limitValue.containsType(types) || body.containsType(types)
        || catchSection.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return limitValue.containsAst(asts) || body.containsAst(asts) || catchSection.containsAst(asts);
  }

  private static NewExpression createNewMemtrackerConstructor(
      RenamesOfInterest jsmntGlobalRenamings, Expression limitValue) {
    // new jsmnt_global.MemTracker(nullptr, limit)
    List<Expression> excConstructorArgs = new LinkedList<>();
    excConstructorArgs.add(new NullExpression());
    excConstructorArgs.add(limitValue);
    List<Type> excConstructorParams = new LinkedList<>();
    excConstructorParams.add(JsmntGlobal.fnCtxRefType);
    excConstructorParams.add(IntType.Create(false, 32));
    FunctionCallExpression excConstructorFn = new FunctionCallExpression(Nullable.empty(),
        new IdentifierExpression(jsmntGlobalRenamings.memtrackerConstructor), excConstructorArgs,
        "");
    excConstructorFn.calledType = Nullable.of(new FunctionType(JsmntGlobal.memtrackerClassType,
        VoidType.Create(), excConstructorParams, LambdaStatus.NOT_LAMBDA));
    NewExpression newExcExpr = new NewExpression(new NewClassInstanceExpression(
        JsmntGlobal.memtrackerClassType, JsmntGlobal.memtrackerClassType.toString(),
        excConstructorFn));
    newExcExpr.determinedType = VoidType.Create();

    return newExcExpr;
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    hitExcId = "tmp_hit_exc_" + getTemp();

    Optional<ResolveExcRet> tmp = catchSection.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn, withinFnStatement, false, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    final List<Statement> catchStatements = ((BlockStatement) tmp.get().ast.get()).statements;

    tmp = body.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings, withinFn,
        withinFnStatement, true, false, hitExcId, catchStatements, 0, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    BlockStatement tmpBody = (BlockStatement) tmp.get().ast.get();
    List<Statement> newStatements = new LinkedList<>();
    newStatements.add(new DeclarationStatement(JsmntGlobal.exceptionRefType, hitExcId,
        Nullable.of(new NullExpression()), false, false, 8));
    newStatements.addAll(tmpBody.statements);
    final BlockStatement newBody = new BlockStatement(newStatements);

    final String timeoutLambdaFnCtx = "tmp_" + JsmntGlobal.fnCtxId + "_" + getTemp();
    List<Statement> preStatements = new LinkedList<>();
    tmp = limitValue.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    Expression newLimitValue = (Expression) tmp.get().ast.get();

    preStatements.add(new DeclarationStatement(JsmntGlobal.fnCtxClassType, timeoutLambdaFnCtx,
        Nullable.of(new DereferenceExpression(new IdentifierExpression(JsmntGlobal.fnCtxId))),
        false, false, JsmntGlobal.fnCtxClassTypeSizeof));
    // set tmp_fn_ctx_0.memtracker = new jsmnt_global.MemTracker(nullptr, limit)
    preStatements.add(new AssignmentStatement(
        new LvalueDot(new IdentifierExpression(timeoutLambdaFnCtx),
            jsmntGlobalRenamings.memtracker),
        createNewMemtrackerConstructor(jsmntGlobalRenamings, newLimitValue)));

    List<DeclarationStatement> timeoutLambdaParams = new LinkedList<>();
    timeoutLambdaParams.add(new DeclarationStatement(JsmntGlobal.fnCtxRefType, JsmntGlobal.fnCtxId,
        Nullable.empty(), false, false, 8));
    LambdaExpression timeoutLambda = new LambdaExpression(returnType, timeoutLambdaParams, newBody);

    List<Expression> timeoutLambdaCallArgs = new LinkedList<>();
    timeoutLambdaCallArgs
        .add(new ReferenceExpression(new IdentifierExpression(timeoutLambdaFnCtx)));
    FunctionCallExpression timeoutLambdaCall = new FunctionCallExpression(Nullable.empty(),
        timeoutLambda, timeoutLambdaCallArgs, "");

    return Optional.of(new ResolveExcRet(preStatements, timeoutLambdaCall));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {

    ScopedIdentifiers ourScope = scopedIds.extend(ScopeFilter.FUNCTION);

    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType, limitValue,
        (BlockStatement) body.insertDestructorCalls(ourScope, destructorRenamings).statements
            .get(0),
        (CatchExcSection) catchSection.insertDestructorCalls(ourScope, destructorRenamings));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType, limitValue.liftClassDeclarations(liftedClasses, replacements).get(),
        (BlockStatement) body.liftClassDeclarations(liftedClasses, replacements).get(),
        (CatchExcSection) catchSection.liftClassDeclarations(liftedClasses, replacements).get());
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        determinedType, limitValue.resolveUserTypes(msgState).get(),
        (BlockStatement) body.resolveUserTypes(msgState).get(),
        (CatchExcSection) catchSection.resolveUserTypes(msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    MemlimitExpression val = new MemlimitExpression(lineNum, columnNum, preComments, postComments,
        returnType, limitValue.addThisExpression(classHeader),
        (BlockStatement) body.addThisExpression(classHeader),
        (CatchExcSection) catchSection.addThisExpression(classHeader));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
