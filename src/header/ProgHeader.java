package header;

import ast.AbstractAst;
import ast.Program;
import ast.Program.RenamesOfInterest;
import ast.SerializationError;
import astproto.AstProto;
import audit.AuditEntry;
import audit.AuditEntry.AuditEntryType;
import audit.AuditEntryLite;
import audit.AuditLiftedClasses;
import audit.AuditRenameIds;
import audit.AuditRenameTypes;
import audit.AuditRenameTypesLite;
import audit.AuditResolveExceptions;
import audit.AuditTrailLite;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader.StaticFilter;
import header.RegisteredGenerics.Registration;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImports;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import mbo.ModuleBuildObj;
import mbo.Renamings;
import multifile.JsmntGlobal;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import type_env.BasicTypeBox;
import type_env.ClassTypeBox;
import type_env.EnumTypeBox;
import type_env.ModuleTypeBox;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.BoolType;
import typecheck.ClassDeclType;
import typecheck.EnumDeclType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ImportType;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.ResolvedImportType;
import typecheck.Type;
import typecheck.UserDeclType;
import typecheck.VoidType;
import typegraph.TypeGraph;
import util.Pair;

/*
 * This is a psuedo-header created from a module for import by other modules
 * It is saved into the built file, making the built file look like:
 * 
 * 0) File Hash - needed to check for rebuilding
 * 1) AST - The parsed form (aka the binary). This may have transformations applied (e.g. renamings)
 * 2) ProgHeader - Function declarations, Class declarations (with parent class linked in). (aka the API)
 * 2.5) AuditTrail - List of enums giving the order and number of transformations on the AST
 *  e.g.: TYPECHECK,RENAME_VARS,RENAME_TYPES,LIFT_INNER
 * 2.6) ProgressStatus - START,COMPLETE //applies to latest audit field, and lets us track if progress
 *  was interrupted
 * Optionals
 * 3) Renamings - Classes/modules/functions and class fields. Only for this ast, and these have already been applied to the AST
 *  X -> Foo_X
 *  fn -> fn1_2
 *  So on import, we know to apply these renamings on the importing module's usage of the API
 *  module b
 *  import a
 *  a.X -> a.Foo_X
 *  Even if b goes through multiple rounds of renamings, it will still use the same renamings for a
 * 4) Vtables - Could be stored in ProgHeader under each class. Vtable accesses cannot be renamed
 *  We'll need the mapping of a.fn to a.vtable[0] to be present.
 * 5) The built C++/Python/ect code (could be any data in string form, like a file path)
 * 
 * When another src file is changed, no need to re-typecheck this file, just pull in the header+renamings
 * But bear in mind that the built file could be made after any stage (say to save memory during large build).
 * 
 * To handle generics, a second build file is stored locally with:
 * 1) AST - only solid versions of generic classes for this module. This gets (partially?)
 *  rebuilt every time a new instance of the template is registered. There will be a function to
 *  combine this AST with the full module AST.
 *   e.g. a.X<T> -> X_int, X_string
 *   
 * This is for several goals: multithreading, partial builds, generics as templates (don't want to keep rebuilding std lib classes)
 * 1) Each stage will feed into a worker thread pool
 * 2) After the first stage of each module publishing their header (or pulling it from the built files)
 *  we wait, then order the modules so the ones with the most dependencies go first. When linking A
 *  into B, we must wait for the signal that A is done, then we can add B to the work queue
 *  
 * Typecheck Stages
 * 1) Create/Find headers. Organize these into the moduleGraph
 * 2) Link in parent classes
 * 3) Typecheck all unbuilt modules fully, but for built modules, just look at header. Then write
 *  unbuilt modules to file with determinedTypes
 *  
 * For each project transformation, give it a thread pool. The transformation takes care of it.
 */

//this header needs to say: when someone imports me, what do they get access to?
//all actual logic is kept in the AST, which will get linked and trusted to work based on the header
public class ProgHeader {
  public final String srcFileName;
  // module name (abs path)
  public final ModuleType moduleType;
  public final Set<ModuleType> submods; // abs paths
  // no submods since those have their own AST+header
  // no imports since we already linked in parent class, and our AST already will
  // have that
  // function declarations
  public final List<FunctionHeader> functions;
  // class declarations
  /*
   * Generic classes are just saved as templates. If a.X<int> is used in multiple
   * places, it should still be seen as the same type, even though it will have
   * multiple declarations. The solid decl will be stored in the importing AST.
   * However, we still need a single declaration.
   * 
   * Order of classes matters (parents have to be done first)
   */
  public final List<ClassHeader> classes;

  public final List<EnumHeader> enums;

  public ProgHeader(String srcFileName, ModuleType moduleType, Set<ModuleType> submods,
      List<FunctionHeader> functions, List<ClassHeader> classes, List<EnumHeader> enums) {
    this.srcFileName = srcFileName;
    this.moduleType = moduleType;
    this.submods = submods;
    this.functions = functions;
    this.classes = classes;
    this.enums = enums;
  }

  @Override
  public String toString() {
    String res = "";

    res += "ProgHeader @ " + srcFileName + " MODULE: " + moduleType + "\n";
    res += "Submods: " + submods + "\n";
    res += "Functions: " + functions + "\n";
    res += "Classes: " + classes + "\n";

    return res;
  }

  public final List<PsuedoClassHeader> psuedoClasses() {
    List<PsuedoClassHeader> items = new LinkedList<>();
    items.addAll(classes);
    items.addAll(enums);
    return items;
  }

  /*
   * Parent mods do not automatically link in submods, nor do submods
   * automatically link in parent mods. Why should std.list care about all of std?
   * If it needs something, it should import it explicitly (or it can do an import
   * std if it really wants to). In the same way, why should std care about
   * std.list? It can import it explicitly if it needs it.
   * 
   * Bear in mind that this function ONLY serves to update our class
   * members/vtable. It does not add functions/classes from the modules we are
   * importing.
   * 
   * imports are mapped AbsModPathType->ProgHeader for that mod. Furthermore,
   * these mods have already finished linking in the mods they are dependent on,
   * so their member lists are static at this point
   */
  public void linkImports(Map<ModuleType, ProgHeader> importedMods,
      Map<String, ClassHeader> importedClasses, MsgState msgState) throws FatalMessageException {
    // if any classes extend a sibling class, they will go in correct order (keep
    // order from Program)
    for (ClassHeader ch : classes) {
      ch.linkImports(importedMods, importedClasses, this, msgState);
    }
  }

  public AstProto.ProgHeader.Builder serialize() {
    AstProto.ProgHeader.Builder document = AstProto.ProgHeader.newBuilder();

    document.setSrcFilename(srcFileName);
    document.setModuleType(moduleType.serialize());
    for (ModuleType submod : submods) {
      document.addSubmods(submod.serialize());
    }
    for (FunctionHeader fn : functions) {
      document.addFunctions(fn.serialize());
    }
    for (ClassHeader ch : classes) {
      document.addClasses(ch.serialize());
    }
    for (EnumHeader en : enums) {
      document.addEnums(en.serialize());
    }

    return document;
  }

  public static ProgHeader deserialize(AstProto.ProgHeader document, MsgState msgState)
      throws SerializationError, FatalMessageException {
    Set<ModuleType> submods = new HashSet<>();
    for (AstProto.Type bvSubmod : document.getSubmodsList()) {
      submods.add((ModuleType) AbstractType.deserialize(bvSubmod));
    }

    List<FunctionHeader> functions = new LinkedList<>();
    for (AstProto.FunctionHeader bvFunc : document.getFunctionsList()) {
      functions.add(FunctionHeader.deserialize(bvFunc));
    }

    List<ClassHeader> classes = new LinkedList<>();
    for (AstProto.ClassHeader bvClass : document.getClassesList()) {
      ClassHeader ch = ClassHeader.deserialize(bvClass, msgState);
      classes.add(ch);
    }

    List<EnumHeader> enums = new LinkedList<>();
    for (AstProto.EnumHeader bvEnum : document.getEnumsList()) {
      enums.add(EnumHeader.deserialize(bvEnum));
    }

    return new ProgHeader(document.getSrcFilename(),
        (ModuleType) AbstractType.deserialize(document.getModuleType()), submods, functions,
        classes, enums);
  }

  public void populateRenameIds(Project project, Renamings renamings,
      RegisteredGenerics registeredGenerics, MsgState msgState) throws FatalMessageException {
    renamings.unlock();
    for (EnumHeader en : enums) {
      en.populateRenameIds(renamings);
    }

    // populate renamings for this module
    for (FunctionHeader function : functions) {
      function.populateRenameIds(renamings);
    }

    // take into account generic classes - look at all registered usages of generic
    // classes
    // declared by this module
    // e.g. A<U>.function(U t)
    // also save A<int>.function(int t)
    for (ClassHeader ch : classes) {
      ch.populateRenameIds(project, renamings, registeredGenerics, msgState);
    }

    renamings.lock();
  }

  private ProgHeader renameIds(Renamings renamings, MsgState msgState, boolean noError)
      throws FatalMessageException {
    List<FunctionHeader> newFunctions = new LinkedList<>();
    List<ClassHeader> newClasses = new LinkedList<>();
    List<EnumHeader> newEnums = new LinkedList<>();

    for (FunctionHeader function : functions) {
      newFunctions.add(function.renameIds(renamings, msgState));
    }

    for (ClassHeader ch : classes) {
      newClasses.add(ch.renameIds(renamings, msgState, noError));
    }

    for (EnumHeader en : enums) {
      newEnums.add(en.renameIds(renamings, msgState));
    }

    return new ProgHeader(srcFileName, moduleType, submods, newFunctions, newClasses, newEnums);
  }

  private ProgHeader resolveGenerics(AuditTrailLite projectAuditTrailSoFar, Project project,
      ModuleBuildObj mbo, RegisteredGenerics registeredGenerics, MsgState msgState)
      throws FatalMessageException {
    List<ClassHeader> newClasses = new LinkedList<>();

    Map<Type, Type> genericToRealMapping = mbo.indexGenericToRealMapping(project,
        projectAuditTrailSoFar, registeredGenerics, msgState);

    for (ClassHeader ch : classes) {
      ClassHeader tmp = null;
      if (!ch.innerTypes.isEmpty()) {
        // generic classes should only copy over static stuff, with type replacement
        tmp = ch.copy(true, StaticFilter.ONLY_STATICS, msgState);
      } else {
        // copy over all non-generic classes as is, but do type replacement
        tmp = ch.copy(true, StaticFilter.NO_FILTER, msgState);
      }
      newClasses.add(tmp.replaceTypes(genericToRealMapping, false, msgState));
    }

    // now add new resolved classes as headers
    Set<String> siblingClasses = new HashSet<>();
    for (ClassHeader ch : classes) {
      siblingClasses.add(ch.name);
    }

    for (Registration reg : registeredGenerics.getRegistrations()) {
      siblingClasses.add(reg.resolvedDeclaration.get().name);
    }

    for (Registration reg : registeredGenerics.getRegistrations()) {
      // find corresponding template class header
      Nullable<PsuedoClassHeader> templateHeaderTmp = getClassHeader(reg.realType.name);
      if (templateHeaderTmp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "templateHeader is null");
      }
      ClassHeader templateHeader = (ClassHeader) templateHeaderTmp.get();

      // do type replacements
      ClassHeader ch = (ClassHeader) templateHeader.replaceGenerics(projectAuditTrailSoFar, project,
          Nullable.empty(), reg.realType.innerTypes, true, genericToRealMapping, msgState, -1, -1)
          .get();
      ch = ch.copy(true, StaticFilter.NO_STATICS, msgState);
      ch = ch.replaceTypes(genericToRealMapping, false, msgState);

      ClassDeclType completed = ch.absName;
      if (project.getAuditTrail().contains(AuditEntryType.RENAME_TYPES)) {
        completed = new ClassDeclType(completed.outerType,
            completed.name + "_GCT_" + AbstractAst.getTemp());
      }
      project.addCompletedClass(completed);
      ch.isLinked = true;
      newClasses.add(ch);
    }

    ProgHeader newProg = new ProgHeader(srcFileName, moduleType, submods, functions, newClasses,
        enums);

    // reorder classes by dependency (parent classes must go before children)
    ClassGraph classGraph = new ClassGraph(Nullable.of(mbo.moduleType), msgState);
    newProg.populateClassGraph(classGraph,
        mbo.effectiveImports.applyTransforms(projectAuditTrailSoFar, msgState), msgState);
    newProg = classGraph.reorder(newProg, msgState).get();

    return newProg;
  }

  public Nullable<PsuedoClassHeader> getClassHeader(String name) {
    for (PsuedoClassHeader ch : psuedoClasses()) {
      if (ch.name.equals(name)) {
        return Nullable.of(ch);
      }
    }
    return Nullable.empty();
  }

  public void populateClassGraph(ClassGraph classGraph, EffectiveImports effectiveImports,
      MsgState msgState) throws FatalMessageException {
    for (ClassHeader ch : classes) {
      ch.populateClassGraph(classGraph, effectiveImports, msgState);
    }
  }

  /*
   * typeRenamings should also contain moduleRenamings merged in
   */
  private ProgHeader replaceTypes(AuditRenameTypesLite moduleRenamings,
      Map<Type, Type> typeRenamings, MsgState msgState) throws FatalMessageException {
    List<ClassHeader> newClasses = new LinkedList<>();
    for (ClassHeader ch : classes) {
      ClassHeader newCH = ch.replaceTypes(typeRenamings, true, msgState);
      newClasses.add(newCH);
    }

    List<FunctionHeader> newFunctions = new LinkedList<>();
    for (FunctionHeader function : functions) {
      newFunctions.add(function.replaceTypes(typeRenamings));
    }

    Set<ModuleType> newSubmods = new HashSet<>();
    for (ModuleType submod : submods) {
      Nullable<ModuleType> tmp = moduleRenamings.getNewType(submod);
      if (tmp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "unable to find module renaming for submod: " + submod);
      }
      newSubmods.add(tmp.get());
    }

    List<EnumHeader> newEnums = new LinkedList<>();
    for (EnumHeader en : enums) {
      newEnums.add(en.replaceTypes(typeRenamings));
    }

    return new ProgHeader(srcFileName, (ModuleType) moduleType.replaceType(typeRenamings),
        newSubmods, newFunctions, newClasses, newEnums);
  }

  private ProgHeader resolveExceptions(Project project, AuditResolveExceptions auditEntry,
      Renamings renamings, MsgState msgState) throws FatalMessageException {

    RenamesOfInterest jsmntGlobalRenamings = Program.findRenamesOfInterest(renamings, msgState);

    List<ClassHeader> newClasses = new LinkedList<>();
    for (ClassHeader ch : classes) {
      ClassHeader newCH = ch.resolveExceptions(project, moduleType, auditEntry,
          jsmntGlobalRenamings, false, "", msgState);
      newClasses.add(newCH);
    }

    List<FunctionHeader> newFunctions = new LinkedList<>();

    if (moduleType.equals(JsmntGlobal.modType)) {
      List<Type> params = new LinkedList<>();

      // sanityCheckTimeout(fn_ctx)
      params.add(JsmntGlobal.fnCtxRefType);
      newFunctions.add(new FunctionHeader(-1, -1, AbstractAst.getNextLabel(), true,
          JsmntGlobal.modType, Program.sanityCheckTimeoutFnName,
          JsmntGlobal.modType,
          new FunctionType(moduleType, VoidType.Create(), params, LambdaStatus.NOT_LAMBDA)));
      
      // sanityCheckRecursion(fn_ctx)
      params = new LinkedList<>();
      params.add(JsmntGlobal.fnCtxRefType);
      newFunctions.add(new FunctionHeader(-1, -1, AbstractAst.getNextLabel(), true,
          JsmntGlobal.modType, Program.sanityCheckRecursionFnName,
          JsmntGlobal.modType,
          new FunctionType(moduleType, VoidType.Create(), params, LambdaStatus.NOT_LAMBDA)));
      
      // sanityCheckMemtrack(fn_ctx, num_bytes, to_free)
      params = new LinkedList<>();
      params.add(JsmntGlobal.fnCtxRefType);
      params.add(IntType.Create(false, 32));
      params.add(BoolType.Create());
      newFunctions.add(new FunctionHeader(-1, -1, AbstractAst.getNextLabel(), true,
          JsmntGlobal.modType, Program.sanityCheckMemtrackFnName,
          JsmntGlobal.modType,
          new FunctionType(moduleType, VoidType.Create(), params, LambdaStatus.NOT_LAMBDA)));
    }

    for (FunctionHeader function : functions) {
      Pair<Nullable<FunctionHeader>, FunctionHeader> tmp = function.resolveExceptions(auditEntry);
      if (tmp.a.isNotNull()) {
        newFunctions.add(tmp.a.get());
      }
      newFunctions.add(tmp.b);
    }

    return new ProgHeader(srcFileName, moduleType, submods, newFunctions, newClasses, enums);
  }
  
  private ProgHeader moveInitsToConstructors(Project project, AuditTrailLite projectAuditTrailSoFar,
      MsgState msgState)
      throws FatalMessageException {
    List<ClassHeader> newClasses = new LinkedList<>();
    for (ClassHeader ch : classes) {
      ClassHeader newCH = ch.moveInitsToConstructors(project, projectAuditTrailSoFar, msgState);
      newClasses.add(newCH);
    }
    
    return new ProgHeader(srcFileName, moduleType, submods, functions, newClasses, enums);
  }

  /*
   * mbo: Which holds this progheader project: the calling project
   * registeredGenerics: for the progheader module
   */
  public ProgHeader applyTransforms(Project project, AuditTrailLite projectAuditTrail,
      ModuleBuildObj mbo,
      RegisteredGenerics registeredGenerics, MsgState msgState) throws FatalMessageException {

    ProgHeader newProg = this;

    // go only as far as Project.auditTrail allows, but take data from relevant mbo
    // for this prog
    Iterator<AuditEntryLite> iterProjectAudit = projectAuditTrail.iterator();
    Iterator<AuditEntry> iterMboAudit = mbo.auditTrail.iterator();

    AuditTrailLite projectAuditTrailSoFar = new AuditTrailLite();
    while (iterProjectAudit.hasNext()) {
      AuditEntryLite projectTransform = iterProjectAudit.next();
      projectAuditTrailSoFar.add(projectTransform);
      if (!iterMboAudit.hasNext()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Module audit trail (" + mbo.auditTrail
            + ") is shorter than project audit trail (" + projectAuditTrail + ")");
      }
      AuditEntry mboTransform = iterMboAudit.next();
      if (projectTransform.getType() != mboTransform.getType()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Audit trails do not match");
      }
      switch (mboTransform.getType()) {
        case MOVE_INITS_TO_CONSTRUCTORS:
          AuditTrailLite tmp = new AuditTrailLite(projectAuditTrailSoFar);
          tmp.pop();
          newProg = newProg.moveInitsToConstructors(project, tmp, msgState);
          break;
        case RENAME_TYPES:
          mboTransform = mbo.auditTrail.combineWithImported(moduleType, mboTransform, project,
              mbo.effectiveImports.applyTransforms(project.getAuditTrail(), msgState),
              projectAuditTrailSoFar, msgState);
          AuditRenameTypesLite projectRenameTypes = (AuditRenameTypesLite) projectTransform;
          newProg = newProg.replaceTypes(projectRenameTypes,
              ((AuditRenameTypes) mboTransform).typeRenamings, msgState);
          break;
        case RESOLVE_GENERICS:
          newProg = newProg.resolveGenerics(projectAuditTrailSoFar, project, mbo,
              registeredGenerics, msgState);
          break;
        case RENAME_IDS:
          newProg = newProg.renameIds(((AuditRenameIds) mboTransform).renamings, msgState, false);
          break;
        case RESOLVE_EXCEPTIONS:
          newProg = newProg.resolveExceptions(project, (AuditResolveExceptions) mboTransform,
              project.getJsmntGlobalRenamings(projectAuditTrailSoFar), msgState);
          break;
        case LIFT_CLASSES:
          newProg = newProg.liftClassDeclarations((AuditLiftedClasses) mboTransform, msgState);
          break;
        default:
          // skip
          break;
      }
    }

    return newProg;
  }

  private ProgHeader liftClassDeclarations(AuditLiftedClasses auditEntry, MsgState msgState) {
    List<ClassHeader> newClasses = new LinkedList<>();
    newClasses.addAll(classes);
    newClasses.addAll(auditEntry.liftedClasses);
    
    return new ProgHeader(srcFileName, moduleType, submods, functions, newClasses, enums);
  }

  public void populateTypeGraph(LoadedModuleHandle moduleHandle, Project project,
      TypeGraph typeGraph, boolean submodsOnly, MsgState msgState) throws FatalMessageException {

    final Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
    filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.INHERITED_IMPORT);

    // add abs path classes/enums
    for (PsuedoClassHeader ch : this.psuedoClasses()) {
      ch.populateTypeGraph(typeGraph);
    }
    
    ModuleType currentModTy = moduleHandle.getModuleType(project.getAuditTrail(), msgState);
    ResolvedImportType currentImpModTy = (ResolvedImportType) currentModTy.asImportType();

    // add our imports to the typegraph
    for (Pair<ImportType, Nullable<ProjectNameVersion>> imp
        : moduleHandle.getImports(project.getAuditTrail(), msgState)
        .getImportList(true, filterImpTypes))
    {
      if (imp.a.equals(currentImpModTy)) {
        continue;
      }
      if (submodsOnly
          && !moduleHandle.getSubmods(project.getAuditTrail(), msgState).contains(imp.a)) {
        continue;
      }
      Nullable<ModuleEndpoint> modEp = project.lookupModule(imp.a, project.getAuditTrail());
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "Failed to find module: " + imp.a);
      }
      modEp.get().load();
      LoadedModuleHandle impModuleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      ProgHeader newImpProgHeader = impModuleHandle.moduleBuildObj.progHeader.applyTransforms(
          project, project.getAuditTrail(),
          impModuleHandle.moduleBuildObj, impModuleHandle.registeredGenerics, msgState);
      newImpProgHeader.populateTypeGraph(impModuleHandle, project, typeGraph, true, msgState);
    }
  }

  public void populateVtables(Project project, ClassGraph classGraph,
      List<UserDeclType> classOrdering, ModuleBuildObj mbo, MsgState msgState)
      throws FatalMessageException {
    for (UserDeclType classTy : classOrdering) {
      // find the class header
      Nullable<PsuedoClassHeader> chTmp = getClassHeader(classTy.name);
      if (chTmp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
            "Failed to find class header for: " + classTy);
      }
      if (chTmp.get() instanceof EnumHeader) {
        continue;
      }
      ClassHeader ch = (ClassHeader) chTmp.get();

      // find the class node
      Nullable<Node> node = classGraph.findNode(classTy);
      if (node.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
            "Failed to fine class node for: " + classTy);
      }

      // populate vtable for that class
      ch.populateVtable(project, classGraph, node.get(), mbo, msgState);

      // stored in moduleBuildObj with other transforms
    }
  }

  public ProgHeader reorderClassWriteOrder(List<UserDeclType> classOrder, MsgState msgState)
      throws FatalMessageException {
    Map<ClassDeclType, ClassHeader> regClasses = new HashMap<>();
    for (ClassHeader ch : classes) {
      regClasses.put(ch.absName, ch);
    }

    List<ClassHeader> newClassOrder = new LinkedList<>();
    for (UserDeclType userTy : classOrder) {
      if (userTy instanceof EnumDeclType) {
        continue;
      }
      ClassDeclType classTy = (ClassDeclType) userTy;
      ClassHeader ch = regClasses.get(classTy);
      if (ch == null) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "Did not find " + classTy + " for class order");
      }
      newClassOrder.add(ch);
    }

    return new ProgHeader(srcFileName, moduleType, submods, functions, newClassOrder, enums);
  }

  public List<String> getClassNames() {
    List<String> names = new LinkedList<>();

    for (ClassHeader ch : classes) {
      names.add(ch.name);
    }

    return names;
  }

  public void determineIsOverrideOf(Project project, TypeEnvironment tenv, MsgState msgState)
      throws FatalMessageException {
    for (ClassHeader ch : classes) {
      ch.determineIsOverrideOf(project, tenv, msgState);
    }
  }

  public Optional<TypeEnvironment> createTenv(Project project, TypeGraph typeGraph,
      MsgState msgState)
      throws FatalMessageException {
    return createTenv(project, typeGraph, msgState, false, true, false);
  }
  
  // only include inheritedImports when resolving class headers
  public Optional<TypeEnvironment> createTenv(Project project, TypeGraph typeGraph,
      MsgState msgState,
      boolean isImported, boolean addMembers, boolean includeInheritedImports)
      throws FatalMessageException {
    
    Nullable<ModuleEndpoint> modEp = project.lookupModule(moduleType, project.getAuditTrail());
    if (modEp.isNull()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "Did not find module: " + moduleType);
      return Optional.empty();
    }
    modEp.get().load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
    
    EffectiveImports effectiveImports = moduleHandle.getImports(project.getAuditTrail(), msgState);
    
    TypeEnvironment tenv = TypeEnvironment.createEmptyUserTypeEnv(project, moduleType,
        typeGraph, effectiveImports, msgState);
    
    if (addMembers) {
      // add classes and functions (if they are exported, unless this is not an import)
      for (ClassHeader ch : classes) {
        tenv.defineVar(ch.name, new ClassTypeBox(ch, !isImported || ch.export, VariableScoping.GLOBAL));
      }
      for (EnumHeader eh : enums) {
        tenv.defineVar(eh.name, new EnumTypeBox(eh, !isImported || eh.export, VariableScoping.GLOBAL));
      }
      for (FunctionHeader fn : functions) {
        tenv.defineVar(fn.name, new BasicTypeBox(fn.getType(), !isImported || fn.export,
            true, VariableScoping.GLOBAL));
      }
      
      // add submods, if this is a terminal module import (or the original module)
      for (ModuleType submod : submods) {
        tenv.defineUserType(new ModuleType(Nullable.empty(), submod.name), submod,
            Nullable.empty());
      }
    }
    
    // add self reference if not imported
    TypeEnvironment oldTenv = tenv;
    if (!isImported) {
      // if this is not root-level (e.g. a.b), then we only want to store our name
      // make a full copy so that we store the self-ref as if it was an import
      tenv = tenv.fullCopy();
    }
    
    // add imports if not imported (ALL imports)
    if (!isImported) {
      final Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
      filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
      filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
      if (includeInheritedImports) {
        filterImpTypes.add(EffectiveImport.EffectiveImportType.INHERITED_IMPORT);
      }
      
      Map<ImportType, EffectiveImport> importMap = moduleHandle
          .getImports(project.getAuditTrail(), msgState).getImportMap(false, filterImpTypes);

      for (Entry<ImportType, EffectiveImport> impEntry : importMap.entrySet()) {
        // say import is 'a.b.c', then we want to define 'a' and 'c' in our tenv
        // in order to do this, we have to first create the tenv for 'a' (but limit it to
        // not import everything, otherwise we could do 'a.x', but we did not import 'a' or 'a.x')
        ImportType impTy = impEntry.getKey();
        tenv.defineUserType(impTy, impEntry.getValue().absPath, Nullable.empty());
        if (impTy.outerType.isNotNull()) {
          tenv.defineUserType(new ModuleType(Nullable.empty(), impTy.name),
              impEntry.getValue().absPath, Nullable.empty());
        }
      }
    }
    
    if (!isImported) {
      tenv.defineUserType(moduleType, moduleType,
          Nullable.of(new ModuleTypeBox(moduleType, true, oldTenv)));
    }
    
    // add builtins if not imported
    if (!isImported) {
      tenv.addBuiltins(moduleType);
    }
    
    return Optional.of(tenv);
  }

  public void resolveUserTypes(TypeEnvironment tenv) throws FatalMessageException {
    for (ClassHeader ch : classes) {
      ch.resolveUserTypes(tenv);
    }
    
    for (FunctionHeader fn : functions) {
      fn.resolveUserTypes(tenv);
    }
  }
  
  public List<ProgItemHeader> allItems() {
    List<ProgItemHeader> items = new LinkedList<>();
    items.addAll(enums);
    items.addAll(classes);
    items.addAll(functions);
    return items;
  }

  public Nullable<ProgItemHeader> lookupItem(String name) {
    List<ProgItemHeader> items = allItems();
    
    for (ProgItemHeader item : items) {
      if (item.getName().equals(name)) {
        return Nullable.of(item);
      }
    }
    
    return Nullable.empty();
  }

  // return true on success
  public boolean linkResolvedExceptionsAuditEntries(Project project,
      AuditResolveExceptions auditEntry, MsgState msgState) throws FatalMessageException {
    boolean hitErr = false;
    for (ClassHeader ch : classes) {
      if (!ch.linkResolvedExceptionsAuditEntries(project, auditEntry, msgState)) {
        hitErr = true;
      }
    }
    return !hitErr;
  }

}
