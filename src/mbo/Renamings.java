package mbo;

import ast.SerializationError;
import astproto.AstProto;
import audit.AuditEntry;
import audit.AuditRenameIds;
import audit.AuditRenameTypes;
import audit.AuditTrail;
import errors.Nullable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import multifile.Project;
import typecheck.AbstractType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;

public class Renamings {
  public final Map<RenamingIdKey, String> renamings;
  private Nullable<Renamings> parent = Nullable.empty();
  private boolean locked = false;
  public final Project project;
  public final ModuleType moduleType;
  
  public void lock() {
    locked = true;
  }
  
  public void unlock() {
    locked = false;
  }
  
  public Renamings(Project project, ModuleType moduleType, Map<RenamingIdKey, String> renamings) {
    this.project = project;
    this.renamings = renamings;
    this.moduleType = moduleType;
  }
  
  public Renamings(Project project, ModuleType moduleType) {
    this.project = project;
    this.renamings = new HashMap<>();
    this.moduleType = moduleType;
  }
  
  public Renamings extend() {
    Renamings newRenamings = new Renamings(project, moduleType, new HashMap<>());
    newRenamings.parent = Nullable.of(this);
    return newRenamings;
  }
  
  public Renamings create() {
    Renamings newRenamings = new Renamings(project, moduleType, new HashMap<>());
    return newRenamings;
  }
  
  @Override
  public String toString() {
    String res = "";
    for (Entry<RenamingIdKey, String> entry : renamings.entrySet()) {
      res += entry + "\n";
    }
    if (parent.isNotNull()) {
      res += "\n EXTENDS\n" + parent.get();
    }
    return res;
  }
  
  public Nullable<String> lookup(String id, Type fnType,
      Nullable<DotAccessType> within) {
    return lookup(id, Nullable.of(fnType), within);
  }
  
  public Nullable<String> lookup(String id, Nullable<Type> fnType,
      Nullable<DotAccessType> within) {
    if (fnType.instanceOf(FunctionType.class)) {
      return lookup(new RenamingIdKey(within, Nullable.of((FunctionType) fnType.get()), id));
    }
    return lookup(new RenamingIdKey(within, Nullable.empty(), id));
  }
  
  public Nullable<String> lookup(RenamingIdKey tmp) {
    String res = renamings.get(tmp);
    if (res != null) {
      return Nullable.of(res);
    }
    
    if (parent.isNotNull()) {
      return parent.get().lookup(tmp);
    }
    
    return Nullable.empty();
  }

  public String add(String name, Type fnType, Nullable<DotAccessType> within, long label) {
    return add(name, fnType, within, "", label);
  }
  
  public String add(String name, Type fnType, Nullable<DotAccessType> within, String newId, long label) {
    if (locked) {
      throw new IllegalArgumentException("Cannot modify this renamings");
    }
    if (newId.isEmpty()) {
      newId = name + "_" + label;
    }
    renamings.put(new RenamingIdKey(within,
        fnType instanceof FunctionType
          ? Nullable.of((FunctionType) fnType) : Nullable.empty(), name),
        newId);
    return newId;
  }

  public void updateWithImport(Renamings impRenamings) {
    for (Entry<RenamingIdKey, String> impRename : impRenamings.renamings.entrySet()) {
      renamings.put(impRename.getKey(), impRename.getValue());
    }
  }

  public boolean isEmpty() {
    return renamings.isEmpty();
  }
  
  public static Renamings deserialize(Project project, ModuleType moduleType,
      AstProto.Renamings document)
      throws SerializationError {
    Map<RenamingIdKey, String> renamings = new HashMap<>();
    Iterator<AstProto.RenamingIdKey> iterKeys = document.getKeysList().iterator();
    Iterator<String> iterVals = document.getValsList().iterator();
    while (iterKeys.hasNext()) {
      renamings.put(RenamingIdKey.deserialize(iterKeys.next()), iterVals.next());
    }
    Renamings newRenamings = new Renamings(project, moduleType, renamings);
    return newRenamings;
  }
  
  public AstProto.Renamings.Builder serialize() {
    AstProto.Renamings.Builder document = AstProto.Renamings.newBuilder();
    
    for (Entry<RenamingIdKey, String> entry : renamings.entrySet()) {
      document.addKeys(entry.getKey().serialize());
      document.addVals(entry.getValue());
    }
    
    return document;
  }
  
  public static class RenamingIdKey {
    //only non-null if id is a field in a class (or function in a module)
    //abs path, may be inherited field. May be ModuleType or ClassDeclType
    public final Nullable<DotAccessType> within;
    public final Nullable<FunctionType> fnType; //only non-null if id is a fn, used for overloads
    public final String id;
    
    public RenamingIdKey(Nullable<DotAccessType> within, Nullable<FunctionType> fnType,
        String id) {
      if (within.isNotNull()) {
        if (within.get() instanceof ClassType) {
          ClassType ty = (ClassType) within.get();
          within = Nullable.of(new ClassDeclType(ty.outerType, ty.name));
        } else if (!(within.get() instanceof DotAccessType)) {
          throw new IllegalArgumentException();
        }
      }
      
      this.within = within;
      if (fnType.isNotNull()) {
        fnType = Nullable.of((FunctionType) fnType.get().setConst(false));
      }
      this.fnType = fnType;
      this.id = id;
    }
    
    public AstProto.RenamingIdKey.Builder serialize() {
      AstProto.RenamingIdKey.Builder document = AstProto.RenamingIdKey.newBuilder();
      
      if (within.isNotNull()) {
        document.addWithin(within.get().serialize());
      }
      
      if (fnType.isNotNull()) {
        document.addFnType(fnType.get().serialize());
      }
      
      document.setId(id);
      
      return document;
    }

    public static RenamingIdKey deserialize(AstProto.RenamingIdKey document)
        throws SerializationError {
      Nullable<DotAccessType> within = Nullable.empty();
      Nullable<FunctionType> fnType = Nullable.empty();
      
      if (document.getWithinCount() > 0) {
        within = Nullable.of((DotAccessType) AbstractType.deserialize(document.getWithin(0)));
      }
      
      if (document.getFnTypeCount() > 0) {
        fnType = Nullable.of((FunctionType) AbstractType.deserialize(document.getFnType(0)));
      }
      
      return new RenamingIdKey(within, fnType, document.getId());
    }

    @Override
    public String toString() {
      return "<" + within + ", " + fnType + ", " + id + ">";
    }
    
    @Override
    public int hashCode() {
      return within.hashCode() + fnType.hashCode() + id.hashCode();
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof RenamingIdKey)) {
        return false;
      }
      RenamingIdKey otherKey = (RenamingIdKey) other;
      return within.equals(otherKey.within) && fnType.equals(otherKey.fnType)
          && id.equals(otherKey.id);
    }
    
    public RenamingIdKey applyTransforms(AuditTrail auditTrail) {
      Iterator<AuditEntry> iterTrail = auditTrail.iterator();
      RenamingIdKey newIdKey = this;
      while (iterTrail.hasNext()) {
        AuditEntry entry = iterTrail.next();
        switch (entry.getType()) {
          case RENAME_IDS:
            newIdKey = newIdKey.renameIds((AuditRenameIds) entry);
            break;
          case RENAME_TYPES:
            newIdKey = newIdKey.replaceTypes((AuditRenameTypes) entry);
            break;
          default:
            break;
        }
      }
      return newIdKey;
    }

    private RenamingIdKey replaceTypes(AuditRenameTypes entry) {
      Nullable<DotAccessType> newWithin = within;
      if (within.isNotNull()) {
        newWithin = Nullable.of((DotAccessType) within.get().replaceType(entry.typeRenamings));
      }
      Nullable<FunctionType> newFnType = fnType;
      if (fnType.isNotNull()) {
        newFnType = Nullable.of((FunctionType) fnType.get().replaceType(entry.typeRenamings));
      }
      
      return new RenamingIdKey(newWithin, newFnType, id);
    }

    private RenamingIdKey renameIds(AuditRenameIds entry) {
      Nullable<String> newId = entry.renamings.lookup(this);
      if (newId.isNull()) {
        return this;
      }
      return new RenamingIdKey(within, fnType, newId.get());
    }
  }

  // combining sequential renameids does not really work, since renametypes can come in between
  // if we have (X, a) -> a_1, and other has (X_1, a_1) -> a_1_1, then we want (X, a) -> a_1_1
  
}
