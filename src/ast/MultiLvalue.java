package ast;

import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.MultiValue;
import interp.Value;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.MultiType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;

public class MultiLvalue extends AbstractLvalue {

  public final List<Lvalue> lvalues; // must contain at least two

  public MultiLvalue(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, List<Lvalue> lvalues) {
    super(lineNum, columnNum, AstType.MULTI_LVALUE, preComments, postComments);
    this.lvalues = lvalues;
  }

  @Override
  public Expression toExpression() {
    // used to desugar binops. This is not currently supported for this node type
    throw new UnsupportedOperationException(
        "Binop modifiers like += are not currently supported for multiple lvalues");
  }

  @Override
  public Lvalue renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      newLvalues.add(lvalue.renameIds(renamings, msgState));
    }
    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }

  @Override
  public Lvalue normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      newLvalues.add(
          lvalue.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState));
    }
    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }

  @Override
  public String toString(String indent) {
    String res = preToString();
    boolean first = true;
    for (Lvalue lvalue : lvalues) {
      if (!first) {
        res += ", ";
      }
      first = false;
      res += lvalue.toString();
    }
    return res + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {

    boolean hitErr = false;
    List<Type> types = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      Optional<TypecheckRet> tyOpt = lvalue.typecheck(tenv, Nullable.empty());
      if (!tyOpt.isPresent()) {
        hitErr = true;
        continue;
      }
      types.add(tyOpt.get().type);
    }
    if (hitErr) {
      return Optional.empty();
    }
    determinedType = new MultiType(types);
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    List<Value> values = new LinkedList<>(); // these will all be MemValues
    for (Lvalue lvalue : lvalues) {
      values.add(lvalue.interp(env));
    }
    return new MultiValue(determinedType, values);
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.MultiLvalue.Builder subDoc = AstProto.MultiLvalue.newBuilder();

    for (Lvalue lvalue : lvalues) {
      subDoc.addLvalues(lvalue.serialize());
    }

    document.setMultiLvalue(subDoc);
    return document;
  }

  public static MultiLvalue deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.MultiLvalue document) throws SerializationError {
    List<Lvalue> values = new LinkedList<>();
    for (AstProto.Ast deVal : document.getLvaluesList()) {
      values.add(AbstractLvalue.deserialize(deVal));
    }
    return new MultiLvalue(lineNum, columnNum, preComments, postComments, values);
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<>();
    for (Lvalue lvalue : lvalues) {
      freeVars.putAll(lvalue.findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof MultiLvalue)) {
      return false;
    }
    MultiLvalue ourOther = (MultiLvalue) other;
    if (lvalues.size() != ourOther.lvalues.size()) {
      return false;
    }
    Iterator<Lvalue> iterUs = lvalues.iterator();
    Iterator<Lvalue> iterOther = ourOther.lvalues.iterator();
    while (iterUs.hasNext() && iterOther.hasNext()) {
      if (!iterUs.next().compare(iterOther.next())) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue val : lvalues) {
      newLvalues.add((Lvalue) val.replace(target, with));
    }

    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (Lvalue val : lvalues) {
      Nullable<Ast> tmp = val.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      Pair<List<DeclarationStatement>, Ast> tmp = lvalue.toAnf(tuning);
      insert.addAll(tmp.a);
      newLvalues.add((Lvalue) tmp.b);
    }

    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Lvalue replaceType(Map<Type, Type> mapFromTo) {
    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      newLvalues.add((Lvalue) lvalue.replaceType(mapFromTo));
    }
    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      newLvalues.add((Lvalue) lvalue.replaceVtableAccess(project, msgState).get());
    }
    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.setOriginalLabel(val.getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    for (Lvalue lvalue : lvalues) {
      lvalue.addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    for (Lvalue val : lvalues) {
      if (val.containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (Lvalue val : lvalues) {
      if (val.containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();
    List<Lvalue> newLvalues = new LinkedList<>();

    for (Lvalue lvalue : lvalues) {
      Optional<ResolveExcRet> tmp = lvalue.resolveExceptions(project, currentModule,
          auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution,
          hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newLvalues.add((Lvalue) tmp.get().ast.get());
    }

    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(val)));
  }

  @Override
  public Nullable<Lvalue> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      newLvalues.add(lvalue.liftClassDeclarations(liftedClasses, replacements).get());
    }
    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.setOriginalLabel(val.getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Lvalue> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      newLvalues.add(lvalue.resolveUserTypes(msgState).get());
    }
    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.setOriginalLabel(val.getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Lvalue addThisExpression(ClassHeader classHeader) {
    List<Lvalue> newLvalues = new LinkedList<>();
    for (Lvalue lvalue : lvalues) {
      newLvalues.add(lvalue.addThisExpression(classHeader));
    }
    MultiLvalue val = new MultiLvalue(lineNum, columnNum, preComments, postComments, newLvalues);
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }

}
