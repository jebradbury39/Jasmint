package interp;

import java.util.Map;

import errors.FatalMessageException;
import typecheck.FunctionType;
import typecheck.Type;
import util.Pair;

public interface IdToValue {

  //this actually just adds to function overload list, but does overwrite 
  public abstract IdToValue write(String name, ValueBox valBox);
  
  //type is only needed when hunting for function overload. Function return type is never checked
  public abstract Pair<ValueBox, Integer> read(Environment env, String name,
      FunctionType functionType) throws InterpError, FatalMessageException;
  
  public abstract IdToValue fullCopy(String name, Environment copyEnv) throws InterpError;
  
  public abstract IdToValue replaceType(String name, Map<Type, Type> mapFromTo,
      String moduleAppend) throws InterpError;
}
