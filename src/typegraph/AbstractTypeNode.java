package typegraph;

import errors.FatalMessageException;
import errors.Nullable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import typecheck.Type;
import typegraph.TypeGraph.CastDirection;
import util.DotGen;

public abstract class AbstractTypeNode implements TypeNode {

  //casting up. B extends A. Can still cast A$ to B$ (explicit), or store B$ in A$ (no explicit)
  //Every B$ must be an A$, but not every A$ must be a B$
  //Going up the cast chain ALWAYS results in truncation of stack memory (less info),
  //but you are not prohibited from casting down (more info)
  protected Set<TypeNodeLink> links = new HashSet<>();
  
  public AbstractTypeNode() {
  }
  
  @Override
  public void updateDotGraph(DotGen dotGraph) {
    for (TypeNodeLink link : links) {
      DotGen.Edge edge = new DotGen.Edge(getType().toString(), link.toNode.getType().toString());
      switch (link.linkType) {
        case UP:
          edge.color = DotGen.Color.BLUE;
          break;
        case DOWN:
          edge.color = DotGen.Color.BLACK;
          break;
        case SIBLING:
          edge.color = DotGen.Color.RED;
          break;
        default:
          throw new IllegalArgumentException();
      }
      
      dotGraph.addEdge(edge);
    }
  }
  
  @Override
  public void addLink(CastDirection linkType, TypeNode node) {
    links.add(new TypeNodeLink(linkType, node));
  }
  
  public Set<TypeNodeLink> filterLinks(CastDirection direction) {
    Set<TypeNodeLink> filteredLinks = new HashSet<>();
    
    for (TypeNodeLink link : links) {
      if (direction == link.linkType) {
        filteredLinks.add(link);
      }
    }
    
    return filteredLinks;
  }
  
  /*
   * If fromTy is null, then search all links. Otherwise, find that specific node in the links
   */
  boolean stepPathTo(Nullable<Type> fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph, Set<TypeNode> visited)
          throws FatalMessageException {
    Set<TypeNodeLink> searchLinks = filterLinks(direction);
    
    for (TypeNodeLink link : searchLinks) {
      if (fromTy.isNull()) {
        //then any type is acceptable, as long as a path exists
        Type nodeTy = link.toNode.getType();
        if (nodeTy == null) {
          continue;
        }
        if (link.toNode.findPathTo(nodeTy, direction, toTy, mapGenericNameToRealType, graph,
            visited)) {
          return true;
        }
      } else if (link.toNode.typeEquals(fromTy.get(), true)) {
        return link.toNode.findPathTo(fromTy.get(), direction, toTy, mapGenericNameToRealType, graph,
            visited);
      }
    }
    
    return false;
  }
  
  // return true if we were already here
  boolean checkVisited(Set<TypeNode> visited) {
    if (visited.contains(this)) {
      return true;
    }
    visited.add(this);
    return false;
  }
}
