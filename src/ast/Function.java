package ast;

import ast.ClassDeclarationSection.Visibility;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import callgraph.CallNode;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.FunctionHeader;
import header.PsuedoClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.Renamings.RenamingIdKey;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.BasicTypeBox;
import type_env.ClassTypeBox;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.SandboxModeInfo;
import type_env.TypeEnvironment.TypeStatus;
import type_env.UserDeclTypeBox;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserDeclType;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class Function extends AbstractStatement implements Declaration {
  public static enum MetaType {
    NORMAL, PSEUDO_LAMBDA, MAIN, START, CONSTRUCTOR, DESTRUCTOR, HASH, CMP
  }

  public final String name;

  // abs module and/or class path, before any renamings. Does not contain our name
  public final UserDeclType originalWithin;
  public final FunctionType fullType; // contains within+returnType info

  public final List<DeclarationStatement> params;
  private Nullable<BlockStatement> body;
  private boolean isStatic;
  // can be null, used to call parent constructor with args first
  public final Nullable<FunctionCallExpression> parentCall;
  public final MetaType metaType;
  public final boolean isOverride;
  public final boolean export;
  public final boolean required; // if true, body will be null until linked

  private boolean isOverridden = false; // set during typechecking

  /* only determined by typechecker (set before typecheck() call) */
  private VariableScoping scoping = VariableScoping.UNKNOWN;
  private FunctionType determinedFullType;

  public Nullable<BlockStatement> getBody() {
    return body;
  }

  public void setBody(BlockStatement b) {
    body = Nullable.of(b);
    b.setParentAst(this);
  }

  public boolean getIsStatic() {
    return isStatic;
  }

  public void setIsStatic(boolean s) {
    isStatic = s;
  }

  public Function(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, boolean export, boolean required, boolean isOverride,
      String name, UserDeclType originalWithin, FunctionType fullType,
      List<DeclarationStatement> params, Nullable<BlockStatement> body, boolean isStatic,
      Nullable<FunctionCallExpression> parentCall, MetaType metaType) {
    super(lineNum, columnNum, AstType.FUNCTION, preComments, postComments);

    this.export = export;
    this.required = required; // actual ffi implementation provided by a provider module
    this.isOverride = isOverride;
    this.fullType = fullType;
    this.name = name;
    this.originalWithin = originalWithin;
    this.params = params;
    this.body = body;
    this.isStatic = isStatic;
    this.parentCall = parentCall;
    this.metaType = metaType;
    this.determinedFullType = fullType;

    if (name == null) {
      throw new IllegalArgumentException("name may not be null");
    }

    for (DeclarationStatement param : params) {
      param.setParentAst(this);
    }

    if (body.isNotNull()) {
      body.get().setParentAst(this);
    }
    if (parentCall.isNotNull()) {
      parentCall.get().setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  public Function(boolean export, boolean required, boolean isOverride, String name,
      UserDeclType originalWithin, FunctionType fullType, List<DeclarationStatement> params,
      Nullable<BlockStatement> body, boolean isStatic, Nullable<FunctionCallExpression> parentCall,
      MetaType metaType) {
    super(AstType.FUNCTION);

    this.export = export;
    this.required = required; // actual ffi implementation provided by a provider module
    this.isOverride = isOverride;
    this.fullType = fullType;
    this.name = name;
    this.originalWithin = originalWithin;
    this.params = params;
    this.body = body;
    this.isStatic = isStatic;
    this.parentCall = parentCall;
    this.metaType = metaType;
    this.determinedFullType = fullType;

    if (name == null) {
      throw new IllegalArgumentException("name may not be null");
    }

    for (DeclarationStatement param : params) {
      param.setParentAst(this);
    }

    if (body.isNotNull()) {
      body.get().setParentAst(this);
    }
    if (parentCall.isNotNull()) {
      parentCall.get().setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    String result = preToString();
    if (isStatic) {
      result += "static ";
    }
    if (isOverride) {
      result += "override ";
    }

    if (false && metaType == MetaType.CONSTRUCTOR) {
      result += "init";
    } else if (false && metaType == MetaType.DESTRUCTOR) {
      result += "end";
    } else {
      result += "fun " + fullType.returnType + " " + name;
    }

    result += "(";
    boolean first = true;
    for (DeclarationStatement param : params) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += param.toString(indent);
    }
    result += ") ";
    if (parentCall.isNotNull()) {
      result += parentCall.get().toString(indent) + " ";
    }
    if (body.isNotNull()) {
      result += body.get().toString(indent);
    }
    return result + postToString();
  }

  @Override
  public void setScoping(VariableScoping scoping) {
    this.scoping = scoping;
  }

  @Override
  public VariableScoping getScoping() {
    return scoping;
  }

  /*
   * Declare the function and validate all param types and return type
   */
  public Optional<Type> typecheckDeclare(TypeEnvironment globalTenv)
      throws FatalMessageException {

    boolean hitErr = false;
    Optional<TypeStatus> tyStatusOpt = globalTenv.lookupType(fullType.returnType, lineNum,
        columnNum);
    if (tyStatusOpt.isPresent()) {
      String prefix = "Function '" + name + "' return type '" + fullType.returnType + "' is ";
      if (tyStatusOpt.get() == TypeStatus.INCOMPLETE
          || tyStatusOpt.get() == TypeStatus.INCOMPLETE_AND_GENERIC
          || tyStatusOpt.get() == TypeStatus.UNDEFINED) {
        hitErr = true;
        globalTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            prefix + tyStatusOpt.get(), lineNum, columnNum);
      }
    }

    // check params for duplicate names
    for (DeclarationStatement pname : params) {
      for (DeclarationStatement cmp : params) {
        if (pname == cmp) {
          continue;
        }
        if (pname.name.equals(cmp.name)) {
          globalTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "duplicate parameter names [" + pname.name + "] in function [" + name + "]", lineNum,
              columnNum);
          return Optional.empty();
        }
      }
    }

    for (DeclarationStatement param : params) {
      tyStatusOpt = globalTenv.lookupType(param.type, param.lineNum, param.columnNum);
      if (tyStatusOpt.isPresent()) {
        String prefix = "Function '" + name + "' param '" + param.name + "' type '" + param.type
            + "' is ";
        if (tyStatusOpt.get() == TypeStatus.INCOMPLETE
            || tyStatusOpt.get() == TypeStatus.INCOMPLETE_AND_GENERIC
            || tyStatusOpt.get() == TypeStatus.UNDEFINED) {
          hitErr = true;
          globalTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              prefix + tyStatusOpt.get(), param.lineNum, param.columnNum);
        }
      }
    }

    if (hitErr) {
      return Optional.empty();
    }
    
    determinedFullType = (FunctionType) fullType.normalize(globalTenv, lineNum, columnNum).get();

    // add to the call graph
    Nullable<SandboxModeInfo> sandboxModeInfo = globalTenv.inSandboxMode();
    globalTenv.callGraph.addNode(name, determinedFullType, sandboxModeInfo.isNotNull());
    
    globalTenv.defineVar(name, new BasicTypeBox(determinedFullType, true, true, scoping));
    return Optional.of(determinedFullType);
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {

    if (scoping == VariableScoping.NORMAL) {
      /*
       * Since this is normal scope, we need to declare first
       */
      Optional<Type> declType = typecheckDeclare(tenv);
      if (!declType.isPresent()) {
        return Optional.empty();
      }
    }

    Optional<TypeBox> tyOpt = typecheckAsField(tenv, tenv.ofDeclType, Nullable.empty(), false);
    if (!tyOpt.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new TypecheckRet(determinedType));
  }

  /*
   * classTenv might be a one of (not necessarily a class scope): GLOBAL GLOBAL ->
   * NORMAL ... GLOBAL -> STATIC ... GLOBAL -> STATIC -> INSTANCE ...
   * 
   * realTypes: when defined
   */
  public Optional<TypeBox> typecheckAsField(TypeEnvironment classTenv, DotAccessType definedIn,
      Nullable<Visibility> visibility, boolean resolveVtable) throws FatalMessageException {

    Nullable<ClassDeclType> definedInClass = Nullable.empty();
    List<Type> realInnerTypes = new LinkedList<>();
    if (definedIn instanceof ClassDeclType) {
      definedInClass = Nullable.of((ClassDeclType) definedIn);
    } else if (definedIn instanceof ClassType) {
      ClassType tmp = (ClassType) definedIn;
      definedInClass = Nullable.of(new ClassDeclType(tmp.outerType, tmp.name));
      realInnerTypes = tmp.innerTypes;
    }

    // statics cannot be overrides
    if (isOverride && isStatic) {
      classTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "static functions cannot be overrides", lineNum, columnNum);
      return Optional.empty();
    }

    // lookup the type (outer type and name) to see if it is complete and/or real
    Optional<TypeStatus> tyStatus = classTenv.lookupType(fullType, lineNum, columnNum);
    if (isStatic && tyStatus.get().isGeneric()) {
      classTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "The static function [" + name + "] cannot have a generic type: " + fullType, lineNum,
          columnNum);
      return Optional.empty();
    }

    if (scoping == VariableScoping.UNKNOWN) {
      classTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "scoping undetermined for Function", lineNum, columnNum);
      return Optional.empty();
    }

    Nullable<ClassHeader> withinClassHeader = Nullable.empty();
    if (definedInClass.isNotNull()) {
      // check if we can even find the class we are defined in
      Optional<UserDeclTypeBox> userTyBox = classTenv.lookupUserDeclType(definedInClass.get(),
          lineNum, columnNum);
      if (!userTyBox.isPresent()) {
        return Optional.empty();
      }
      if (!(userTyBox.get() instanceof ClassTypeBox)) {
        classTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "not within a class: " + userTyBox, lineNum, columnNum);
        return Optional.empty();
      }
      ClassTypeBox classTyBox = (ClassTypeBox) userTyBox.get();

      withinClassHeader = Nullable.of(classTyBox.classHeader);

      // replace generics if within class with real types
      if (!realInnerTypes.isEmpty()) {
        Optional<PsuedoClassHeader> repl = withinClassHeader.get().replaceGenerics(
            classTenv.project.getAuditTrail(), classTenv.project, Nullable.of(classTenv),
            realInnerTypes, true, new HashMap<>(), classTenv.msgState, lineNum, columnNum);
        if (!repl.isPresent()) {
          classTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
              "replace generics failed", lineNum, columnNum);
        }
        withinClassHeader = Nullable.of((ClassHeader) repl.get());
      }
    }

    // check for parent constructor
    if (parentCall.isNotNull()) {
      if (definedInClass.isNull()) {
        classTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Only a constructor within a class may call a parent constructor", lineNum, columnNum);
        return Optional.empty();
      }

      if (withinClassHeader.get().getParent().isNull()) {
        classTenv.msgState
            .addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
                "constructor cannot call parent constructor since class ["
                    + withinClassHeader.get().absName + "] does not have a parent",
                lineNum, columnNum);
        return Optional.empty();
      }
    }

    TypeEnvironment bodyTenv = classTenv.extend();
    Nullable<TypeBox> box = bodyTenv.lookup(name, fullType, lineNum, columnNum);
    if (box.isNull()) {
      bodyTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Failed to find function decl box: " + name + " with type " + fullType,
          lineNum, columnNum);
    }
    box.get().setIsInit(true);

    // Populate newTenv with params
    boolean hitErr = false;
    for (DeclarationStatement param : params) {
      param.setScoping(VariableScoping.NORMAL);
      Optional<TypecheckRet> tmp = param.typecheck(bodyTenv, Nullable.empty());
      if (!tmp.isPresent()) {
        hitErr = true;
        continue;
      }

      // function parameters MUST have a value, ofc
      bodyTenv.lookup(param.name, lineNum, columnNum).get().setIsInit(true);
    }
    if (hitErr) {
      return Optional.empty();
    }

    if (parentCall.isNotNull()) {
      if (withinClassHeader.isNull()) {
        bodyTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "parent is not null but within class is", lineNum, columnNum);
      }
      if (withinClassHeader.get().getParent().isNull()) {
        bodyTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Using parent call but class has no parent", lineNum, columnNum);
        return Optional.empty();
      }

      // TODO
      // typecheck the args we are passing
      // and check if parent has a constructor with the correct signature
      ClassType parentClassInst = withinClassHeader.get().getParent().get();
      /*
       * A<X, Z> { constructor (X m, Z n) }
       * 
       * B<Y> extends A<Y, int> { constructor (Y m) parent(m, 5) {
       * 
       * }
       * 
       * 'parent' must be A<Y, int>.constructor(m, 5)
       * 
       */

      // just get the parent type tenv and find a matching constructor
      Optional<TypeEnvironment> parentClassTenv = parentClassInst.getTenv(classTenv, lineNum,
          columnNum);
      if (!parentClassTenv.isPresent()) {
        return Optional.empty();
      }
      if (!parentCall.get().typecheck(bodyTenv, parentClassTenv.get(), parentClassInst)
          .isPresent()) {
        return Optional.empty();
      }
    }

    // for ffi, once if the body is linked, we want to typecheck in here
    if (body.isNotNull()) {
      if (!body.get().typecheckAsFunctionBody(bodyTenv, fullType.returnType).isPresent()) {
        return Optional.empty();
      }
    }
    
    determinedFullType = (FunctionType) fullType.normalize(classTenv, lineNum, columnNum).get();

    return Optional.of(box.get());
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    if (required) {
      throw new InterpError("Cannot interp a required/provided function", lineNum, columnNum);
    }
    // env.defineVar(name, VariableScoping.GLOBAL,
    // new ClosureValue(fullType, params, body, env.makeClosureCopy(), parentCall,
    // withinClass.get()),
    // true); //creates the function box with clo linking to global
    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.Function.Builder subDoc = AstProto.Function.newBuilder();

    subDoc.setExport(export);
    subDoc.setRequired(required);
    subDoc.setIsOverride(isOverride);
    subDoc.setName(name);
    subDoc.setOriginalWithin(originalWithin.serialize());
    subDoc.setFullType(fullType.serialize());

    for (DeclarationStatement param : params) {
      subDoc.addParams(param.serialize());
    }

    if (body.isNotNull()) {
      subDoc.addBody(body.get().serialize());
    }
    subDoc.setIsStatic(isStatic);
    subDoc.setMetaType(metaType.ordinal());

    if (parentCall.isNotNull()) {
      subDoc.addParentCall(parentCall.get().serialize());
    }

    subDoc.setScoping(scoping.serialize());
    subDoc.setIsOverride(isOverride);

    document.setFunction(subDoc);
    return document;
  }

  public static Function deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, long ordering, AstProto.Function document)
      throws SerializationError {
    List<DeclarationStatement> params = new LinkedList<DeclarationStatement>();
    for (AstProto.Ast bvParam : document.getParamsList()) {
      params.add(DeclarationStatement.deserialize(bvParam));
    }

    Nullable<FunctionCallExpression> parentCall = Nullable.empty();
    if (document.getParentCallCount() > 0) {
      parentCall = Nullable
          .of((FunctionCallExpression) AbstractExpression.deserialize(document.getParentCall(0)));
    }

    Nullable<BlockStatement> body = Nullable.empty();
    if (document.getBodyCount() > 0) {
      body = Nullable.of((BlockStatement) AbstractStatement.deserialize(document.getBody(0)));
    }

    Function result = new Function(lineNum, columnNum, preComments, postComments,
        document.getExport(), document.getRequired(), document.getIsOverride(), document.getName(),
        (UserDeclType) AbstractType.deserialize(document.getOriginalWithin()),
        (FunctionType) AbstractType.deserialize(document.getFullType()), params, body,
        document.getIsStatic(), parentCall, MetaType.values()[document.getMetaType()]);
    result.setScoping(VariableScoping.deserialize(document.getScoping()));
    result.setOrdering(ordering);
    return result;
  }

  public static Function deserialize(AstProto.Ast document) throws SerializationError {
    int deLineNum = document.getLineNum();
    int deColumnNum = document.getColNum();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();
    Pair<AstType, Long> tmp = AbstractAst.preDeserialize(document, preComments, postComments);
    long ordering = tmp.b;
    Type deDeterminedType = AbstractType.deserialize(document.getDeterminedType());
    long deOriginalLabel = document.getLabel();

    Function value = deserialize(deLineNum, deColumnNum, preComments, postComments, ordering,
        document.getFunction());
    value.determinedType = deDeterminedType;
    value.setOriginalLabel(deOriginalLabel);

    return value;
  }

  /*
   * This ensures unique names and also takes care of shadowing. This is a utility
   * for the users of the ast
   */
  public Function renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Nullable<String> newName = Nullable.empty();
    Renamings scopedRenamings = renamings.extend();

    if (metaType == MetaType.START) {
      newName = Nullable.of(name);
    } else if (metaType == MetaType.PSEUDO_LAMBDA) {
      newName = Nullable.of(name + "_" + label);
      renamings.renamings.put(new RenamingIdKey(Nullable.of(renamings.moduleType),
          Nullable.of((FunctionType) fullType.setConst(false)), name), newName.get());
    } else {
      newName = renamings.lookup(name, fullType.setConst(false), fullType.within);
      if (newName.isNull()) {
        newName = renamings.lookup(name, fullType.setConst(true), fullType.within);
      }
      if (newName.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "did not find function name in renamings: " + name + " with type " + fullType,
            lineNum, columnNum);
      }
    }

    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.renameIds(scopedRenamings, msgState));
    }

    Nullable<BlockStatement> newBody = body;
    if (body.isNotNull()) {
      newBody = Nullable.of((BlockStatement) body.get().renameIds(scopedRenamings, msgState));
    }

    Nullable<FunctionCallExpression> newParentCall = parentCall;
    if (parentCall.isNotNull()) {
      newParentCall = Nullable
          .of((FunctionCallExpression) parentCall.get().renameIds(scopedRenamings, msgState));
    }

    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, newName.get(), originalWithin,
        (FunctionType) fullType.renameIds(scopedRenamings, msgState), newParams, newBody, isStatic,
        newParentCall, metaType);
    value.determinedType = determinedType.renameIds(scopedRenamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    value.setOrdering(ordering);
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<>();
    if (body.isNotNull()) {
      freeVars = body.get().findFreeVariables();
    }
    // remove params from freeVars. no need to check params for freeVars since no
    // values
    for (DeclarationStatement param : params) {
      freeVars.remove(param.name);
    }
    // remove fn name from freeVars
    freeVars.remove(name);
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof Function)) {
      return false;
    }
    Function ourOther = (Function) other;
    if (!ourOther.name.equals(name)) {
      return false;
    }
    if (!ourOther.fullType.returnType.equals(fullType.returnType)) {
      return false;
    }

    if (ourOther.params.size() != params.size()) {
      return false;
    }
    Iterator<DeclarationStatement> iterOtherP = ourOther.params.iterator();
    Iterator<DeclarationStatement> iterUsP = params.iterator();
    while (iterOtherP.hasNext() && iterUsP.hasNext()) {
      if (!iterOtherP.next().compare(iterUsP.next())) {
        return false;
      }
    }
    if (ourOther.body.isNull()) {
      return body.isNull();
    } else if (body.isNull()) {
      return false;
    }
    return ourOther.body.get().compare(body.get());
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.replace(target, with));
    }

    Nullable<BlockStatement> newBody = body;
    if (body.isNotNull()) {
      newBody = Nullable.of((BlockStatement) body.get().replace(target, with));
    }

    Nullable<FunctionCallExpression> newParentCall = parentCall;
    if (parentCall.isNotNull()) {
      newParentCall = Nullable.of((FunctionCallExpression) parentCall.get().replace(target, with));
    }

    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, fullType, newParams, newBody, isStatic, newParentCall,
        metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (DeclarationStatement param : params) {
      Nullable<Ast> tmp = param.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    if (body.isNotNull()) {
      return body.get().findByOriginalLabel(findLabel);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      Pair<List<DeclarationStatement>, Ast> tmp = param.toAnf(tuning);
      newParams.add((DeclarationStatement) tmp.b);
      insert.addAll(tmp.a);
    }
    Pair<List<DeclarationStatement>, Ast> parentCallPair = new Pair<>(new LinkedList<>(),
        parentCall.isNull() ? null : parentCall.get());
    if (parentCall.isNotNull()) {
      parentCallPair = parentCall.get().toAnf(tuning);
      insert.addAll(parentCallPair.a);
    }
    Pair<List<DeclarationStatement>, Ast> bodyPair = new Pair<>(null, null);
    if (body.isNotNull()) {
      bodyPair = body.get().toAnf(tuning);
      insert.addAll(bodyPair.a);
    }

    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, fullType, newParams,
        bodyPair.b == null ? Nullable.empty() : Nullable.of((BlockStatement) bodyPair.b), isStatic,
        parentCall.isNull() ? Nullable.empty()
            : Nullable.of((FunctionCallExpression) parentCallPair.b),
        metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.setOrdering(ordering);
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  public Function replaceType(Map<Type, Type> mapFromTo) {
    return replaceType(mapFromTo, (UserDeclType) fullType.within.get().replaceType(mapFromTo));
  }

  public Function replaceType(Map<Type, Type> mapFromTo, UserDeclType newWithin) {
    List<DeclarationStatement> newParams = new LinkedList<>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.replaceType(mapFromTo));
    }

    Nullable<BlockStatement> newBody = body;
    if (body.isNotNull()) {
      newBody = Nullable.of((BlockStatement) body.get().replaceType(mapFromTo));
    }

    Nullable<FunctionCallExpression> newParentCall = parentCall;
    if (parentCall.isNotNull()) {
      newParentCall = Nullable.of((FunctionCallExpression) parentCall.get().replaceType(mapFromTo));
    }

    FunctionType newFullType = (FunctionType) fullType.replaceType(mapFromTo);
    newFullType = new FunctionType(newWithin, newFullType.returnType, newFullType.argTypes,
        fullType.lambdaStatus);

    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, newFullType, newParams, newBody, isStatic, newParentCall,
        metaType);
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.replaceVtableAccess(project, msgState).get());
    }

    Nullable<BlockStatement> newBody = body;
    if (body.isNotNull()) {
      Optional<Ast> tmpBody = body.get().replaceVtableAccess(project, msgState);
      if (!tmpBody.isPresent()) {
        return Optional.empty();
      }
      newBody = Nullable.of((BlockStatement) tmpBody.get());
    }

    Nullable<FunctionCallExpression> newParentCall = parentCall;
    if (parentCall.isNotNull()) {
      Optional<Ast> tmpPC = parentCall.get().replaceVtableAccess(project, msgState);
      if (!tmpPC.isPresent()) {
        return Optional.empty();
      }
      newParentCall = Nullable.of((FunctionCallExpression) tmpPC.get());
    }

    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, fullType, newParams, newBody, isStatic, newParentCall,
        metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return Optional.of(value);
  }

  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param
          .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get());
    }

    Nullable<BlockStatement> newBody = body;
    if (body.isNotNull()) {
      Optional<Statement> tmpBody = body.get().normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmpBody.isPresent()) {
        return Optional.empty();
      }
      newBody = Nullable.of((BlockStatement) tmpBody.get());
    }

    Nullable<FunctionCallExpression> newParentCall = parentCall;
    if (parentCall.isNotNull()) {
      Optional<Nullable<Expression>> tmpPC = parentCall.get().normalizeType(keepCurrentModRelative,
          currentModule, effectiveImports, msgState);
      if (!tmpPC.isPresent()) {
        return Optional.empty();
      }
      newParentCall = Nullable.of((FunctionCallExpression) tmpPC.get().get());
    }

    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin,
        (FunctionType) fullType
            .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        newParams, newBody, isStatic, newParentCall, metaType);
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return Optional.of(value);
  }

  public void buildModuleDependencyGraph(ClassGraph modDepGraph, Node ourModNode,
      TypeEnvironment tenv) throws FatalMessageException {
    /*
     * This can be a hard or soft dependency, meaning that just because we use an
     * other_mod.class*, does not mean they MUST be compiled before us, or that they
     * cannot then have a dependency on us. Basically, we know the size of the
     * pointer, and so we are not concerned about the details.
     *
     * We need to recursively inspect all types used in both params and body. We do
     * NOT add a dependency link on a module for soft reasons, since adding a link
     * forces that module to be compiled before us which prevents them from having
     * any dependency on us.
     */

    for (DeclarationStatement param : params) {
      param.addDependency(modDepGraph, ourModNode, tenv);
    }

    if (body.isNotNull()) {
      body.get().addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  /*
   * Only a function from a provider module calls this, so 'program' in this case
   * is the 'require' module. The exported class is also from require mod, but
   * must match. E.g. if you have: require: export class A; provide: class A;
   * 
   * Then this is an error.
   */
  public Nullable<Function> linkRequiredProvidedFunctions(Program program, MsgState msgState)
      throws FatalMessageException {
    Nullable<Function> reqFnAst = program.lookupFunction((UserDeclType) fullType.within.get(), name,
        fullType);

    if (reqFnAst.isNull()) {
      if (!export) {
        // just add line (private function)
        return Nullable.of(this);
      }
      msgState.addMessage(MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN,
          "Unable to find function:" + this, lineNum, columnNum);
      return Nullable.empty();
    }

    /*
     * If not inside class, then verify that export levels match.
     */
    if (!(fullType.within.get() instanceof ClassDeclType)) {
      if (export != reqFnAst.get().export) {
        msgState.addMessage(
            MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN, "Require function " + name + " to be export("
                + reqFnAst.get().export + "), but found provide class to be export(" + export + ")",
            lineNum, columnNum);
        return Nullable.empty();
      }
    }

    if (reqFnAst.isNull()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN,
          "Unable to find function:" + this, lineNum, columnNum);
      return Nullable.empty();
    }

    Function reqFn = reqFnAst.get();
    if (!reqFn.required) {
      msgState.addMessage(MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN,
          "Cannot provide ffi for a non-req function: " + this, lineNum, columnNum);
      return Nullable.empty();
    }
    // finish linking
    reqFn.body = body;
    // make sure our next non-exported line gets added after where we put this
    // (relative positioning)
    return reqFnAst;
  }

  public boolean findUnlinkedReqFun(MsgState msgState) throws FatalMessageException {
    if (body.isNull()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN,
          "Failed to link required function: " + this, lineNum, columnNum);
      return true;
    }
    return false;
  }

  public Function prependInitStatements(List<AssignmentStatement> newInits) {
    if (body.isNull()) {
      throw new IllegalArgumentException("internal: function body is null");
    }
    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, fullType, params,
        Nullable.of(body.get().prependInitStatements(newInits)), isStatic, parentCall, metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return value;
  }

  public FunctionHeader generateHeader(ModuleType moduleType, EffectiveImports effectiveImports,
      MsgState msgState) throws FatalMessageException {
    return new FunctionHeader(lineNum, columnNum, label,
        export, moduleType, name, originalWithin, (FunctionType) fullType
        .basicNormalize(false, moduleType, effectiveImports, msgState).get().get());
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (body.isNotNull()) {
      if (body.get().containsType(types)) {
        return true;
      }
    }
    return fullType.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    if (body.isNotNull()) {
      if (body.get().containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Optional<Set<ModuleType>> findExportedTypes(ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Set<ModuleType> exportedTypes = new HashSet<>();
    // if this function is not exported, then it cannot export any types
    if (!export) {
      return Optional.of(exportedTypes);
    }

    if (!fullType.getNormalizedModType(currentModule, effectiveImports, msgState, exportedTypes,
        false)) {
      return Optional.empty();
    }

    return Optional.of(exportedTypes);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    //this function itself may not be sandboxed, but it might contain a 'sandbox' expression
    if (!enableResolution) {
      // can only turn on sandboxed mode...cannot turn it off
      CallNode tmp = new CallNode(name, fullType, false);
      enableResolution = project.getSandboxedFunctions().contains(tmp);
    }

    if (enableResolution) {
      auditEntry.addFunction(name, fullType, msgState);
    }

    List<Statement> statements = new LinkedList<>();
    if (enableResolution) {
      // timeout check
      List<Type> sanityChecksTypes = new LinkedList<>();
      sanityChecksTypes.add(JsmntGlobal.fnCtxRefType);
      
      List<Expression> sanityChecksArgs = new LinkedList<>();
      //sanityChecksArgs.add(new ReferenceExpression(new IdentifierExpression(JsmntGlobal.fnCtxId)));
      
      FunctionCallExpression sanityChecksCall = new FunctionCallExpression(
          Nullable.of(new StaticClassIdentifierExpression(JsmntGlobal.modType)),
          new IdentifierExpression(Program.sanityCheckTimeoutFnName), sanityChecksArgs,
          Program.sanityCheckTimeoutFnName);
      sanityChecksCall.calledType = Nullable.of(new FunctionType(JsmntGlobal.modType,
          VoidType.Create(), sanityChecksTypes, LambdaStatus.NOT_LAMBDA));

      statements.add(new FunctionCallStatement(sanityChecksCall));
    }
    if (enableResolution) {
      // recursion check
      List<Type> sanityChecksTypes = new LinkedList<>();
      sanityChecksTypes.add(JsmntGlobal.fnCtxRefType);
      
      List<Expression> sanityChecksArgs = new LinkedList<>();
      //sanityChecksArgs.add(new ReferenceExpression(new IdentifierExpression(JsmntGlobal.fnCtxId)));
      
      FunctionCallExpression sanityChecksCall = new FunctionCallExpression(
          Nullable.of(new StaticClassIdentifierExpression(JsmntGlobal.modType)),
          new IdentifierExpression(Program.sanityCheckRecursionFnName), sanityChecksArgs,
          Program.sanityCheckRecursionFnName);
      sanityChecksCall.calledType = Nullable.of(new FunctionType(JsmntGlobal.modType,
          VoidType.Create(), sanityChecksTypes, LambdaStatus.NOT_LAMBDA));

      statements.add(new FunctionCallStatement(sanityChecksCall));
    }

    // don't do memlimit check on constructor/destructor since that complicates
    // stack vs heap
    /*
     * if (isConstructor || isDestructor) { List<Type> sanityChecksTypes = new
     * LinkedList<>(); sanityChecksTypes.add(JsmntGlobal.fnCtxRefType);
     * sanityChecksTypes.add(IntType.Create(false, 32));
     * sanityChecksTypes.add(BoolType.Create()); List<Expression> sanityChecksArgs =
     * new LinkedList<>(); sanityChecksArgs.add(new ReferenceExpression(new
     * IdentifierExpression(JsmntGlobal.fnCtxId))); sanityChecksArgs.add(new
     * SizeofExpression(fullType.within.get())); //must run typecheck after
     * sanityChecksArgs.add(new BoolExpression(isDestructor)); //isFree ==
     * isDestructor FunctionCallExpression sanityChecksCall = new
     * FunctionCallExpression( Nullable.of(new
     * IdentifierExpression(JsmntGlobal.modType.name)), new
     * IdentifierExpression(Program.sanityCheckMemtrackFnName), sanityChecksArgs,
     * Program.sanityCheckRecursionFnName); sanityChecksCall.calledType =
     * Nullable.of(new FunctionType(JsmntGlobal.modType, VoidType.Create(),
     * sanityChecksTypes, LambdaStatus.NOT_LAMBDA));
     * 
     * statements.add(new FunctionCallStatement(sanityChecksCall)); }
     */

    statements.addAll(body.get().statements);

    Optional<ResolveExcRet> bodyResOpt = new BlockStatement(statements).resolveExceptions(project,
        currentModule, auditEntry, jsmntGlobalRenamings, Nullable.of(fullType), false,
        enableResolution, metaType == MetaType.START, "", new LinkedList<>(), 0, msgState);
    if (!bodyResOpt.isPresent()) {
      return Optional.empty();
    }

    List<DeclarationStatement> newParams = new LinkedList<>();
    FunctionType newFullType = fullType;
    if (enableResolution) {
      // update params
      newParams.add(new DeclarationStatement(-1, -1, JsmntGlobal.fnCtxRefType, JsmntGlobal.fnCtxId,
          Nullable.empty(), false, false));
      
      
      
      if (fullType.lambdaStatus == LambdaStatus.SANDBOX_NOT_LAMBDA) {
        newFullType = fullType.prependFnArgType(JsmntGlobal.fnCtxRefType);
        //newParams.add(new DeclarationStatement(-1, -1, null, name, Nullable.empty(), false, false));
      } else {
        newFullType = fullType.prependFnArgType(JsmntGlobal.fnCtxRefType);
      }
    }
    newParams.addAll(params);
    
    // If this is a sandboxed function, then we want to keep the OG and generate a sandboxed copy
    // The exception to this is when the function is defined inside of the sandbox, in which case
    // we just want to replace the function (with a lambda, which is more transpile-friendly).
    // We also replace any non-sandboxed function.
    BlockStatement newBody = (BlockStatement) bodyResOpt.get().ast.get();
    if (fullType.lambdaStatus == LambdaStatus.SANDBOX_NOT_LAMBDA) {
      LambdaExpression lambdaExpr = new LambdaExpression(lineNum, columnNum,
          preComments, postComments,
          newFullType.returnType, newParams, newBody);
      lambdaExpr.setOriginalLabel(lambdaExpr.label);
      
      FunctionType nonSandboxFnTy = new FunctionType(newFullType.within, newFullType.returnType,
          newFullType.argTypes, LambdaStatus.LAMBDA);
      lambdaExpr.setDeterminedType(nonSandboxFnTy);
      
      DeclarationStatement declStatement = new DeclarationStatement(lineNum, columnNum,
          preComments, postComments, nonSandboxFnTy, name, Nullable.of(lambdaExpr), false, false);
      declStatement.setOriginalLabel(declStatement.label);
      declStatement.setScoping(VariableScoping.NORMAL);
      
      return Optional.of(new ResolveExcRet(declStatement));
    }
    
    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, newFullType, newParams,
        Nullable.of(newBody), isStatic, parentCall, metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);

    // must not be a sandbox-defined function at this point
    if (enableResolution) {
      List<Statement> preStatements = new LinkedList<>();
      preStatements.add(value);
      return Optional.of(new ResolveExcRet(preStatements, this));
    }
    
    return Optional.of(new ResolveExcRet(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  public Function insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings, Nullable<ClassType> parentType)
      throws FatalMessageException {

    ScopedIdentifiers ourScope = scopedIds.extend(ScopeFilter.FUNCTION);

    List<Statement> statements = new LinkedList<>();
    if (metaType == MetaType.DESTRUCTOR) {
      if (parentType.isNotNull()) {
        // insert call to parent destructor (if we have a parent)
        statements.add(parentType.get().createDestructorCall("",
            destructorRenamings).get());
      }

      // insert destructor calls for all our specific class members
      PsuedoClassHeader classHeader = destructorRenamings.project.lookupClassHeader(
          (ClassDeclType) fullType.within.get(), Nullable.empty(), true,
          destructorRenamings.project.getAuditTrail());

      // add fields to ourScope
      classHeader.insertDestructorCalls(ourScope, destructorRenamings);
    }

    // we do not add params to scope, since we do not want to call destructors on
    // our params when the function exits

    Nullable<BlockStatement> newBody = body;
    if (body.isNotNull()) {
      newBody = Nullable.of((BlockStatement) body.get().insertDestructorCalls(ourScope,
          destructorRenamings).statements.get(0));
      statements.addAll(newBody.get().statements);
      statements.addAll(ourScope.getDestructors(ScopeFilter.BLOCK, destructorRenamings));
      newBody = Nullable.of(new BlockStatement(body.get().lineNum, body.get().columnNum,
          body.get().preComments, body.get().postComments, statements));
      newBody.get().setOriginalLabel(body.get().getOriginalLabel());
    }

    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, (FunctionType) fullType, params, newBody, isStatic,
        parentCall, metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return value;
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return new InsertDestructorsRet(this);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    if (body.isNull()) {
      replacements.msgState.addMessage(MsgType.FATAL, MsgClass.LIFT_CLASS_DECLARATIONS,
          "function body is null (failed to link req-provide)", lineNum, columnNum);
    }
    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, fullType, params,
        Nullable.of((BlockStatement) body.get().liftClassDeclarations(liftedClasses,
            replacements).get()),
        isStatic, parentCall, metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return Nullable.of(value);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<DeclarationStatement> newParams = new LinkedList<>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.resolveUserTypes(msgState).get());
    }

    Nullable<BlockStatement> newBody = body;
    if (body.isNotNull()) {
      newBody = Nullable.of((BlockStatement) body.get().resolveUserTypes(msgState).get());
    }

    Nullable<FunctionCallExpression> newParentCall = parentCall;
    if (parentCall.isNotNull()) {
      newParentCall = Nullable.of((FunctionCallExpression) parentCall.get().resolveUserTypes(msgState).get());
    }
    
    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, determinedFullType, newParams,
        newBody,
        isStatic, newParentCall, metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    if (isStatic) {
      return this;
    }
    List<DeclarationStatement> newParams = new LinkedList<>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.addThisExpression(classHeader));
    }

    Nullable<BlockStatement> newBody = body;
    if (body.isNotNull()) {
      newBody = Nullable.of((BlockStatement) body.get().addThisExpression(classHeader));
    }

    Nullable<FunctionCallExpression> newParentCall = parentCall;
    if (parentCall.isNotNull()) {
      newParentCall = Nullable.of((FunctionCallExpression) parentCall.get().addThisExpression(classHeader));
    }
    
    Function value = new Function(lineNum, columnNum, preComments, postComments, export, required,
        isOverride, name, originalWithin, determinedFullType, newParams,
        newBody,
        isStatic, newParentCall, metaType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.scoping = scoping;
    value.setOrdering(ordering);
    return value;
  }

}
