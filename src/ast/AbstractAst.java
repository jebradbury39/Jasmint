package ast;

import ast.ClassDeclarationSection.Visibility;
import ast.Function.MetaType;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import type_env.BasicTypeBox;
import type_env.TypeEnvironment;
import typecheck.ClassDeclType;
import typecheck.EnumDeclType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.ReferencedType;
import typecheck.Type;
import typecheck.UndeterminedType;
import typecheck.UserDeclType;
import util.Pair;

public abstract class AbstractAst implements Ast {
  private static long nextLabel = 0;
  private static long nextTemp = 0;
  
  static final List<CommentAttr> EMPTY_COMMENTS = new LinkedList<>();
  
  public static final String incIndent = "  ";
  
  public static void resetLabelGen() {
    nextLabel = 0;
  }
  
  public static long peekNextLabel() {
    return nextLabel;
  }
  
  public static void resetNextTemp() {
    nextTemp = 0;
  }
  
  public static synchronized long getNextLabel() {
    return nextLabel++;
  }
  
  public static long getTemp() {
    return nextTemp++;
  }
  
  public static enum AstType {
    NONE,
    ARRAY_INIT_EXPRESSION,
    ASSIGNMENT_STATEMENT,
    BINARY_EXPRESSION,
    BLOCK_STATEMENT,
    BOOL_EXPRESSION,
    BRACKET_EXPRESSION,
    BREAK_STATEMENT,
    CAST_EXPRESSION,
    CHAR_EXPRESSION,
    CLASS_DECLARATION,
    CLASS_DECLARATION_SECTION,
    CONDITIONAL_STATEMENT,
    CONTINUE_STATEMENT,
    DECLARATION_STATEMENT,
    DELETE_STATEMENT,
    DOT_EXPRESSION,
    DOUBLE_EXPRESSION,
    FFI_EXPRESSION,
    FFI_STATEMENT,
    FOR_STATEMENT,
    FUNCTION,
    FUNCTION_CALL_EXPRESSION,
    FUNCTION_CALL_STATEMENT,
    IDENTIFIER_EXPRESSION,
    IMPORT_STATEMENT,
    INTEGER_EXPRESSION,
    LAMBDA_EXPRESSION,
    LEFT_UNARY_OP_EXPRESSION,
    BRACKET_LVALUE,
    DEREFERENCE_LVALUE,
    DOT_LVALUE,
    FFI_LVALUE,
    ID_LVALUE,
    MAP_INIT_EXPRESSION,
    MODULE_STATEMENT,
    NEW_CLASS_INSTANCE_EXPRESSION,
    NEW_EXPRESSION,
    NULL_EXPRESSION,
    PARENT_CALL_EXPRESSION,
    PROGRAM,
    RETURN_STATEMENT,
    STRING_EXPRESSION,
    SUBMOD_STATEMENT,
    SWITCH_CASE_STATEMENT,
    SWITCH_STATEMENT,
    THIS_EXPRESSION,
    VTABLE_ACCESS_EXPRESSION,
    WHILE_STATEMENT,
    REFERENCE_EXPRESSION,
    DEREFERENCE_EXPRESSION,
    EXPRESSION_STATEMENT,
    MULTI_LVALUE,
    MULTI_EXPRESSION,
    STATIC_CLASS_IDENTIFIER_EXPRESSION,
    COMMENT_STATEMENT,
    SANDBOX_EXPRESSION,
    CATCH_EXC_SECTION,
    TIMEOUT_EXPRESSION,
    ENUM_DECLARATION,
    RECURSION_EXPRESSION,
    SIZEOF_EXPRESSION,
    MEMLIMIT_EXPRESSION,
    INSTANCEOF_EXPRESSION
  }
  
  public static class SortByOrdering implements Comparator<Ast> {

    @Override
    public int compare(Ast a, Ast b) {
      return (int) (a.getOrdering() - b.getOrdering());
    }

  }
  
  public final int lineNum;
  public final int columnNum;
  public final AstType astType;
  
  public final List<CommentAttr> preComments; //only block comments
  public final List<CommentAttr> postComments; //one line comment at most, at end of list + block?
  
  public final long label; //unique label for this ast node
  long ordering; //used by Program (and maybe others)
  //functions as a uid from the first generation of the ast before serialization
  private long originalLabel = -1;
  private Ast parentAst = null; //should be set by parent creator
  
  protected Type determinedType = UndeterminedType.Create();
  
  public AbstractAst(int lineNum, int columnNum, AstType aType, List<CommentAttr> preComments,
      List<CommentAttr> postComments) {
    this.lineNum = lineNum;
    this.columnNum = columnNum;
    this.astType = aType;
    this.label = getNextLabel();
    this.preComments = preComments;
    this.postComments = postComments;
  }
  
  public AbstractAst(int lineNum, int columnNum, AstType aType) {
    this.lineNum = lineNum;
    this.columnNum = columnNum;
    this.astType = aType;
    this.label = getNextLabel();
    this.preComments = EMPTY_COMMENTS;
    this.postComments = EMPTY_COMMENTS;
    
    setOriginalLabel(this.label);
  }
  
  public AbstractAst(AstType aType) {
    this.lineNum = -1;
    this.columnNum = -1;
    this.astType = aType;
    this.label = getNextLabel();
    this.preComments = EMPTY_COMMENTS;
    this.postComments = EMPTY_COMMENTS;
    
    setOriginalLabel(this.label);
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Ast)) {
      return false;
    }
    return compare((Ast) other);
  }
  
  @Override
  public int hashCode() {
    return astType.hashCode();
  }
  
  String preToString() {
    String res = "";
    for (CommentAttr p : preComments) {
      res += p.toString();
    }
    return res;
  }
  
  @Override
  public String toString() {
    return toString("");
  }
  
  String postToString() {
    String res = "";
    for (CommentAttr p : postComments) {
      res += p.toString();
    }
    return res;
  }
  
  @Override
  public long getOrdering() {
    return ordering;
  }
  
  public void setOrdering(long newOrder) {
    ordering = newOrder;
  }
  
  @Override
  public long getLabel() {
    return label;
  }
  
  @Override
  public long getOriginalLabel() {
    return originalLabel;
  }
  
  public void setOriginalLabel(long val) {
    originalLabel = val;
  }
  
  @Override
  public String strLbl() {
    if (label < 0) {
      return "_" + (-label);
    }
    return "" + label;
  }
  
  @Override
  public final Type getDeterminedType() {
    return determinedType;
  }
  
  @Override
  public void setDeterminedType(Type determinedType) {
    this.determinedType = determinedType;
  }
  
  public int getLine() {
    return lineNum;
  }
  
  public int getColumn() {
    return columnNum;
  }
  
  @Override
  public void addComments(List<CommentAttr> comments, boolean isPre) {
    if (isPre) {
      preComments.addAll(comments);
    } else {
      postComments.addAll(comments);
    }
  }
  
  protected AstProto.Ast.Builder preSerialize() {
    AstProto.Ast.Builder document = AstProto.Ast.newBuilder();

    document.setAstType(astType.ordinal());
    document.setLineNum(lineNum);
    document.setColNum(columnNum);
    document.setDeterminedType(determinedType.serialize().build());
    document.setLabel(label); //used for function calls to overloaded functions
    document.setOrdering(ordering);

    for (CommentAttr pre : preComments) {
      document.addPreComments(pre.serialize());
    }
    
    for (CommentAttr post : postComments) {
      document.addPostComments(post.serialize());
    }
    
    return document;
  }

  public static Pair<AstType, Long> preDeserialize(AstProto.Ast document,
      List<CommentAttr> preComments, List<CommentAttr> postComments) {
    
    for (AstProto.CommentAttr pre : document.getPreCommentsList()) {
      preComments.add(CommentAttr.deserialize(pre));
    }
    
    for (AstProto.CommentAttr post : document.getPostCommentsList()) {
      postComments.add(CommentAttr.deserialize(post));
    }
    
    int astTypeIndex = document.getAstType();
    return new Pair<>(AstType.values()[astTypeIndex], document.getOrdering());
  }
  
  @Override
  public Ast getParentAst() {
    return parentAst;
  }
  
  @Override
  public void setParentAst(Ast parent) {
    parentAst = parent;
  }
  
  /**
   * Checks parent to see if within function def with name 'constructor'
   * @return
   */
  @Override
  public boolean withinConstructor() {
    if (astType == AstType.FUNCTION) {
      if (((Function) this).metaType == MetaType.CONSTRUCTOR) {
        return true;
      }
    }
    if (parentAst == null) {
      return false;
    }
    return parentAst.withinConstructor();
  }
  
  @Override
  public Nullable<Function> withinFunction() {
    if (astType == AstType.LAMBDA_EXPRESSION) {
      return Nullable.empty();
    }
    if (astType == AstType.FUNCTION) {
      return Nullable.of((Function) this);
    }
    if (parentAst == null) {
      return Nullable.empty();
    }
    return parentAst.withinFunction();
  }
  
  /**
   * Not used currently
   * @return
   */
  /*
  @Override
  public ClassDeclaration withinClassDeclaration() {
    if (astType == AstType.CLASS_DECLARATION) {
      return (ClassDeclaration) this;
    }
    if (parentAst == null) {
      return null;
    }
    return parentAst.withinClassDeclaration();
  }
  */
  
  protected boolean checkVisibilityLevel(TypeEnvironment tenv, String id, BasicTypeBox tyBox,
      MsgState msgState)
      throws FatalMessageException {
    if (tyBox.visibilityLevel.isNotNull() && tyBox.visibilityLevel.get() != Visibility.PUBLIC) {
      UserDeclType enclosing = tenv.ofDeclType.asUserDeclType();
      if (!(enclosing instanceof ClassDeclType || enclosing instanceof EnumDeclType)) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Not within class [" + tyBox.definedInClass
            + "], cannot access field: " + id, lineNum, columnNum);
        return false;
      }
      
      if (tyBox.visibilityLevel.get() == Visibility.PRIVATE) {
        if (!enclosing.equals(tyBox.definedInClass.get())) {
          msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "Cannot access private member in class [" 
              + tyBox.definedInClass + "] from class [" 
              + enclosing + "]: " + id, lineNum, columnNum);
          return false;
        }
      } else if (tyBox.visibilityLevel.get() == Visibility.PROTECTED) {
        if (tyBox.definedInClass.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "definedInClass is null");
        }
        if (!tenv.canSafeCast(new ReferenceType((ReferencedType) enclosing),
            new ReferenceType((ReferencedType) tyBox.definedInClass.get()))) {
          msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "Cannot access protected member in class [" 
              + tyBox.definedInClass + "] from class [" 
              + enclosing + "]: " + id, lineNum, columnNum);
          return false;
        }
      }
    }
    return true;
  }
  
  @Override
  public Optional<Set<ModuleType>> findExportedTypes(ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(new HashSet<ModuleType>());
  }
  
  /*
  @Override
  public Pair<BlockStatement, Integer> findOurStatementLine() throws AstError {
    Ast containingBlock = this;
    //the line in the block statement which eventually will contain us (through some path)
    Ast ourLine = null;
    while (!(containingBlock instanceof BlockStatement) && containingBlock.getParent() != null) {
      ourLine = containingBlock;
      containingBlock = containingBlock.getParent();
    }
    if (!(containingBlock instanceof BlockStatement)) {
      throw new AstError("Expected node with BlockStatement parent", lineNum, columnNum);
    }
    
    int lineIndex = 0; //find which line index we are on
    for (Statement statement : ((BlockStatement) containingBlock).statements) {
      if (statement == ourLine) {
        break;
      }
      lineIndex++;
    }
    if (lineIndex == ((BlockStatement) containingBlock).statements.size()) {
      throw new AstError("Did not find ourLine", lineNum, columnNum);
    }
    return new Pair<BlockStatement, Integer>((BlockStatement) containingBlock, lineIndex);
  }
  */
}
