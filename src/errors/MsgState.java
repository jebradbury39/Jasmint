package errors;

import errors.Message.MsgClass;
import errors.Message.MsgType;
import java.util.LinkedList;
import java.util.List;

/*
 * Hold all the messages we have found. Each error has a classification
 */

public class MsgState {
  private List<Message> messages = new LinkedList<>();
  public final Nullable<String> filename; //the current file we are looking at
  private boolean foundError = false;
  private boolean foundWarning = false;
  
  public MsgState(Nullable<String> srcFileName) {
    this.filename = srcFileName;
  }
  
  public boolean hitError() {
    return foundError || foundWarning;
  }
  
  public synchronized void addMessage(MsgType msgType, MsgClass msgClass, String content)
      throws FatalMessageException {
    addMessage(msgType, msgClass, content, -1, -1);
  }
  
  public synchronized void addMessage(MsgType msgType, MsgClass msgClass, String content,
      int line, int column)
      throws FatalMessageException {
    messages.add(new Message(msgType, msgClass, content, filename, line, column));
    
    switch (msgType) {
    case FATAL:
    case INTERNAL:
      //stop
      throw new FatalMessageException(this);
    case ERROR:
      foundError = true;
      break;
    default:
      foundWarning = true;
      break;
    }
  }
  
  @Override
  public String toString() {
    String res = "";
    for (Message msg : messages) {
      res += msg + "\n";
    }
    return res;
  }
}
