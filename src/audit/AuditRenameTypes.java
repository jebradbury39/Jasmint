package audit;

import ast.SerializationError;
import astproto.AstProto;
import astproto.AstProto.AuditEntry.Builder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import typecheck.AbstractType;
import typecheck.Type;

public class AuditRenameTypes implements AuditEntry {
  
  public final Map<Type, Type> typeRenamings; //only abs class names e.g. a.X -> a_1.X_2
  public final Map<Type, Type> relativeTypeRenamings; //only relative class names e.g. X -> X2
  
  public AuditRenameTypes(Map<Type, Type> typeRenamings, Map<Type, Type> relativeTypeRenamings) {
    this.typeRenamings = typeRenamings;
    this.relativeTypeRenamings = relativeTypeRenamings;
  }

  public AuditRenameTypes() {
    this.typeRenamings = new HashMap<>();
    this.relativeTypeRenamings = new HashMap<>();
  }
  
  @Override
  public String toString() {
    return getType().toString();
  }

  @Override
  public AuditEntryType getType() {
    return AuditEntryType.RENAME_TYPES;
  }

  public static AuditEntry deserialize(AstProto.AuditRenameTypes document)
      throws SerializationError {
    AuditRenameTypes newRenamings = new AuditRenameTypes();
    
    Iterator<AstProto.Type> iterKeys = document.getKeysList().iterator();
    Iterator<AstProto.Type> iterVals = document.getValsList().iterator();
    while (iterKeys.hasNext()) {
      newRenamings.typeRenamings.put(AbstractType.deserialize(iterKeys.next()),
          AbstractType.deserialize(iterVals.next()));
    }
    
    return newRenamings;
  }

  @Override
  public Builder serialize() {
    AstProto.AuditEntry.Builder document = AstProto.AuditEntry.newBuilder();
    document.setAuditEntryType(getType().ordinal());
    
    AstProto.AuditRenameTypes.Builder subDoc = AstProto.AuditRenameTypes.newBuilder();
    
    for (Entry<Type, Type> entry : typeRenamings.entrySet()) {
      subDoc.addKeys(entry.getKey().serialize());
      subDoc.addVals(entry.getValue().serialize());
    }
    
    document.setAuditRenameTypes(subDoc);
    return document;
  }

  @Override
  public AuditEntry combineWith(AuditEntry other) {
    if (other.getType() != getType()) {
      throw new IllegalArgumentException();
    }
    AuditRenameTypes otherTypes = (AuditRenameTypes) other;
    Map<Type, Type> combinedRenamings = new HashMap<>();
    combinedRenamings.putAll(otherTypes.typeRenamings);
    combinedRenamings.putAll(typeRenamings);
    return new AuditRenameTypes(combinedRenamings, relativeTypeRenamings);
  }

  @Override
  public AuditEntry withRelativeEntries() {
    Map<Type, Type> newTypeRenamings = new HashMap<>(typeRenamings);
    newTypeRenamings.putAll(relativeTypeRenamings);
    
    return new AuditRenameTypes(newTypeRenamings, relativeTypeRenamings);
  }
}
