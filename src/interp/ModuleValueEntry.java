package interp;

import multifile.Project;
import typecheck.UserInstanceType;

public class ModuleValueEntry extends AbstractValue {

  public final UserInstanceType fullType; //this is the type of the module (e.g. std.Lists)
  public final Environment modEnv;
  
  public ModuleValueEntry(UserInstanceType fullType, Environment modEnv) {
    super(MetaValue.NORMAL, fullType);
    this.fullType = fullType;
    this.modEnv = modEnv;
  }
  
  public Environment getEnv(Project project, int lineNum, int columnNum) throws InterpError {
    return modEnv;
  }
  
  @Override
  public String toString() {
    return "<ModuleValue:" + fullType + ">";
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    throw new InterpError("May not convert module to string: " + this.toString(), -1, -1);
  }

  @Override
  public Value copy(Environment copyEnv) throws InterpError {
    //this is immutable, but new value for new box
    return new ModuleValueEntry(fullType, modEnv);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return this.toString().equals(other.toString());
  }

}
