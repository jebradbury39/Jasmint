package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.ClassTypeBox;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.ClassDeclType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class BlockStatement extends AbstractStatement {

  public final List<Statement> statements;

  public BlockStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, List<Statement> statements) {
    super(lineNum, columnNum, AstType.BLOCK_STATEMENT, preComments, postComments);
    this.statements = statements;

    for (Statement statement : this.statements) {
      statement.setParentAst(this);
    }
  }

  public BlockStatement(int lineNum, int columnNum, List<Statement> statements) {
    super(lineNum, columnNum, AstType.BLOCK_STATEMENT);
    this.statements = statements;

    for (Statement statement : this.statements) {
      statement.setParentAst(this);
    }
  }

  public BlockStatement(List<Statement> statements) {
    super(AstType.BLOCK_STATEMENT);
    this.statements = statements;

    for (Statement statement : this.statements) {
      statement.setParentAst(this);
    }
  }

  @Override
  public String toString(String indent) {
    String result = preToString() + "{\n";
    String save = indent;
    indent += incIndent;

    for (Statement statement : statements) {
      result += indent + statement.toString(indent);
      if (result.charAt(result.length() - 1) != '}') {
        result += ";";
      }
      result += "\n";
    }

    indent = save;
    return result + indent + "}" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {

    Nullable<Type> actualReturnType = Nullable.empty();
    boolean foundReturnStatement = false;

    TypeEnvironment newTenv = tenv.extend();
    
    // find all the classes defined within this block, generate headers for them, and add to tenv
    // class table. We need this so that the classes can reference each other. Also
    // ensure valid class names.
    Set<String> siblingClasses = tenv.getSiblingClasses();
    List<ClassDeclaration> sortedClasses = new ArrayList<>();
    for (Statement statement : statements) {
      if (statement instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) statement;
        
        sortedClasses.add(cdecl);
        
        if (siblingClasses.contains(cdecl.name)) {
          tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "Cannot declare same module class twice: " + cdecl.name,
              cdecl.lineNum, cdecl.columnNum);
          continue;
        }
        siblingClasses.add(cdecl.name); // can only extend a class which is previously defined
        
        // check if the tenv already has this class name, or if it is in siblingClasses
        Optional<ClassHeader> classHeader = cdecl.generateHeader(new LinkedList<>(), siblingClasses,
            newTenv.ofDeclType.getModuleType(),
            false, newTenv.effectiveImports, newTenv.msgState);
        
        // now add the class header to the tenv
        newTenv.defineVar(cdecl.name, new ClassTypeBox(classHeader.get(), true, VariableScoping.NORMAL));
      }
    }
    
    boolean hitErr = false;
    Map<ClassDeclType, TypeEnvironment> classTenvs = new HashMap<>();
    for (ClassDeclaration cdecl : sortedClasses) {
      Optional<TypeEnvironment> classTenv = cdecl.typecheckDeclare(tenv);
      if (!classTenv.isPresent()) {
        hitErr = true;
      } else {
        classTenvs.put(new ClassDeclType(cdecl.fullType.outerType, cdecl.name), classTenv.get());
      }
    }
    
    if (hitErr) {
      return Optional.empty();
    }

    Optional<TypecheckRet> evalRetType = Optional.empty();
    for (Statement statement : statements) {

      // if already found return statement on this level, then no more code will
      // execute
      if (foundReturnStatement) {
        tenv.msgState.addMessage(MsgType.WARNING, MsgClass.TYPECHECK,
            "This code will not execute (after return statement)", statement.getLine(),
            statement.getColumn());
      }

      if ((statement instanceof Declaration)) {
        Declaration ds = (Declaration) statement;
        if (ds.getScoping() == VariableScoping.UNKNOWN) {
          ds.setScoping(VariableScoping.NORMAL);
        }
      }

      if (statement instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) statement;
        TypeEnvironment classTenv = classTenvs
            .get(new ClassDeclType(cdecl.fullType.outerType, cdecl.name));
        cdecl.typecheck(classTenv);
      } else {
        evalRetType = statement.typecheck(tenv, expectedReturnType);
      }
      
      if (!evalRetType.isPresent()) {
        hitErr = true;
        continue;
      }

      if (expectedReturnType.isNull() && evalRetType.get().returnType.isNotNull()) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Was not expecting a return type, but found: " + evalRetType.get().returnType,
            statement.getLine(), statement.getColumn());
        hitErr = true;
        continue;
      }

      // could verify type cast, but no need since block statement is the one that
      // evaluates return statements

      if (expectedReturnType.isNotNull() && evalRetType.get().returnType.isNotNull()) {
        actualReturnType = evalRetType.get().returnType;
        foundReturnStatement = true; // this statement will force a return
      }
    }

    if (hitErr) {
      return Optional.empty();
    }

    determinedType = VoidType.Create();
    return Optional.of(new TypecheckRet(actualReturnType, determinedType));
  }

  public Optional<Type> typecheckAsFunctionBody(TypeEnvironment bodyTenv, Type returnType)
      throws FatalMessageException {
    // blockstatement must have return statement if returnType is known and non-void
    // if expected return type is void and a non-void return is attempted, then also
    // error
    Optional<TypecheckRet> bodyRetOpt = typecheck(bodyTenv, Nullable.of(returnType));
    if (!bodyRetOpt.isPresent()) {
      return Optional.empty();
    }
    Nullable<Type> actualReturnType = bodyRetOpt.get().returnType;
    /*
     * if we are expecting a return type (non-void), we must have ensured we would
     * return
     */
    if (returnType.equals(VoidType.Create())) {
      if (actualReturnType.isNotNull()) {
        if (!actualReturnType.get().equals(VoidType.Create())) {
          bodyTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
              "BlockStatement failed to check cast on void vs: " + actualReturnType, lineNum,
              columnNum);
        }
      } else {
        /*
         * This block does return (control flow), but no return statement enforces this
         * (void ret)
         */
      }
      return Optional.of(VoidType.Create());
    }
    if (actualReturnType.isNull()) {
      bodyTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Function body failed to provide valid return, was expecting return type: " + returnType,
          lineNum, columnNum);
      return Optional.empty();
    }
    return Optional.of(actualReturnType.get());
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Environment newEnv = env.extend();

    for (Statement statement : statements) {
      Value value = statement.interp(newEnv);

      switch (value.getMetaValue()) {
        case RETURN:
        case BREAK:
        case CONTINUE:
          return value;
        default:
          break;
      }
    }

    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.BlockStatement.Builder subDoc = AstProto.BlockStatement.newBuilder();

    for (Statement statement : statements) {
      subDoc.addStatements(statement.serialize());
    }

    document.setBlockStatement(subDoc);
    return document;
  }

  public static BlockStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.BlockStatement document) throws SerializationError {
    List<Statement> statements = new LinkedList<Statement>();
    for (AstProto.Ast bsVal : document.getStatementsList()) {
      statements.add(AbstractStatement.deserialize(bsVal));
    }
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        statements);
    return value;
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    // each block creates its own scope, so create new mappings so you don't mess up
    // outer scopes
    Renamings scopedRenamings = renamings.extend();

    List<Statement> renamedStatements = new LinkedList<Statement>();
    for (Statement statement : statements) {
      renamedStatements.add(statement.renameIds(scopedRenamings, msgState));
    }
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        renamedStatements);
    value.determinedType = determinedType.renameIds(scopedRenamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<String, Type>();
    // iterate backwards
    LinkedList<Statement> linkedStatements = null;
    if (!(statements instanceof LinkedList<?>)) {
      linkedStatements = new LinkedList<Statement>(statements);
    } else {
      linkedStatements = (LinkedList<Statement>) statements;
    }
    Iterator<Statement> iterBack = linkedStatements.descendingIterator();
    while (iterBack.hasNext()) {
      Statement next = iterBack.next();

      // the lines AFTER this one have this var declared, so not free
      if (next instanceof DeclarationStatement) {
        freeVars.remove(((DeclarationStatement) next).name);
      }

      freeVars.putAll(next.findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof BlockStatement)) {
      return false;
    }
    BlockStatement ourOther = (BlockStatement) other;
    if (ourOther.statements.size() != statements.size()) {
      return false;
    }
    Iterator<Statement> iterUs = statements.iterator();
    Iterator<Statement> iterOther = ourOther.statements.iterator();
    while (iterUs.hasNext() && iterOther.hasNext()) {
      if (!iterUs.next().compare(iterOther.next())) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    List<Statement> newStatements = new LinkedList<Statement>();
    for (Statement statement : statements) {
      newStatements.add((Statement) statement.replace(target, with));
    }
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (Statement statement : statements) {
      Nullable<Ast> tmp = statement.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    // Collect all temps here and pass none up
    List<Statement> newStatements = new LinkedList<>();
    for (Statement statement : statements) {
      Pair<List<DeclarationStatement>, Ast> tmp = statement.toAnf(tuning);
      newStatements.addAll(tmp.a); // add in the new temp lines
      newStatements.add((Statement) tmp.b);
    }
    BlockStatement val = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<>(), val);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    List<Statement> newStatements = new LinkedList<Statement>();
    for (Statement statement : statements) {
      Statement tmp = (Statement) statement.replaceType(mapFromTo);
      newStatements.add(tmp);
    }
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<Statement> newStatements = new LinkedList<Statement>();
    for (Statement statement : statements) {
      Optional<Ast> tmp = statement.replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newStatements.add((Statement) tmp.get());
    }
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    List<Statement> newStatements = new LinkedList<Statement>();
    for (Statement statement : statements) {
      Optional<Statement> tmp = statement.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newStatements.add(tmp.get());
    }
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    Optional<Nullable<Type>> tmp = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    value.determinedType = tmp.get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    TypeEnvironment newTenv = tenv.extend();
    
    Set<String> siblingClasses = tenv.getSiblingClasses();
    for (Statement statement : statements) {
      if (statement instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) statement;
        
        if (siblingClasses.contains(cdecl.name)) {
          tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "Cannot declare same module class twice: " + cdecl.name,
              cdecl.lineNum, cdecl.columnNum);
          continue;
        }
        siblingClasses.add(cdecl.name); // can only extend a class which is previously defined
        
        // check if the tenv already has this class name, or if it is in siblingClasses
        Optional<ClassHeader> classHeader = cdecl.generateHeader(new LinkedList<>(), siblingClasses,
            newTenv.ofDeclType.getModuleType(),
            false, newTenv.effectiveImports, newTenv.msgState);
        
        // now add the class header to the tenv
        newTenv.defineVar(cdecl.name, new ClassTypeBox(classHeader.get(), true, VariableScoping.NORMAL));
      }
    }
    
    for (Statement statement : statements) {
      statement.addDependency(modDepGraph, ourModNode, newTenv);
    }
  }

  public BlockStatement prependInitStatements(List<AssignmentStatement> newInits) {
    List<Statement> newStatements = new LinkedList<Statement>();
    newStatements.addAll(newInits);
    newStatements.addAll(statements);
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    for (Statement statement : statements) {
      if (statement.containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (Statement statement : statements) {
      if (statement.containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  private int calculateSize(MsgState msgState) throws FatalMessageException {
    int size = 0;
    for (Statement statement : statements) {
      if (statement instanceof DeclarationStatement) {
        int tmp = ((DeclarationStatement) statement).getCalculatedSizeof();
        if (tmp == -1) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
              "failed to calculate size of block", lineNum, columnNum);
        }
        size += tmp;
      }
    }

    return size;
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings, withinFn,
        withinFnStatement, enableResolution, false, hitExcId, new LinkedList<>(), stackSize,
        msgState);
  }

  /*
   * if isStartFnBody:
   * 
   * fun int start([int] argv) { return main(argv); }
   * 
   * After resolving exceptions normally:
   * 
   * fun int start([int] argv) { jsmnt_global.ExceptionRef exc; exc.ref = null;
   * int main_rc = main(&exc, argv); if (exc.ref != null) { //modify pre_exc, but
   * there is none return 0; } return main_rc; }
   * 
   * But we want:
   * 
   * fun int start([int] argv) { jsmnt_global.ExceptionRef exc; exc.ref = null;
   * int main_rc = main(&exc, argv); if (exc.ref != null) { //print out backtrace
   * return 255; } return main_rc; }
   */

  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      boolean isStartFnBody, String hitExcId, List<Statement> catchStatements, int stackSize,
      MsgState msgState) throws FatalMessageException {

    final int ourStackSize = calculateSize(msgState);
    stackSize += ourStackSize;

    List<Statement> newStatements = new LinkedList<>();

    // add ourStackSize to memtracker
    if (enableResolution) {
      if (withinFn.isNotNull()) {
        newStatements
            .addAll(ResolveRaiseException.sanityMemcheck(project,
                new IntegerExpression(ourStackSize + ""),
                false, jsmntGlobalRenamings, hitExcId, withinFn.get(), msgState));
      }
    }

    // if we hit return, remove stackSize
    // if we exit normally, remove only ourStackSize

    if (!hitExcId.isEmpty()) {
      if (!enableResolution) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
            "hitExcId is not empty, but enableResolution is false", lineNum, columnNum);
      }
    }

    boolean hitReturnStatement = false;
    for (Statement statement : statements) {
      // remove stackSize from memtracker
      if (enableResolution) {
        if (statement instanceof ReturnStatement && withinFn.isNotNull()) {
          hitReturnStatement = true;
          newStatements
              .addAll(ResolveRaiseException.sanityMemcheck(project,
                  new IntegerExpression(stackSize + ""),
                  true, jsmntGlobalRenamings, hitExcId, withinFn.get(), msgState));
        }
      }
      Optional<ResolveExcRet> tmpOpt = statement.resolveExceptions(project, currentModule,
          auditEntry, jsmntGlobalRenamings, withinFn, false, enableResolution, hitExcId, stackSize,
          msgState);
      if (!tmpOpt.isPresent()) {
        return Optional.empty();
      }
      if (hitExcId.isEmpty()) {
        newStatements.addAll(tmpOpt.get().preStatements); // add in the new temp lines
        if (tmpOpt.get().ast.isNotNull()) {
          newStatements.add((Statement) tmpOpt.get().ast.get());
        }
      } else {
        // wrap these new lines within an exc check
        List<Statement> checkedStatements = new LinkedList<>();
        checkedStatements.addAll(tmpOpt.get().preStatements); // add in the new temp lines
        if (tmpOpt.get().ast.isNotNull()) {
          checkedStatements.add((Statement) tmpOpt.get().ast.get());
        }
        for (Statement iter : checkedStatements) {
          Statement checkedStatement = iter;
          if (checkedStatement instanceof DeclarationStatement) {
            DeclarationStatement decl = (DeclarationStatement) checkedStatement;
            if (decl.getValue().isNull()) {
              newStatements.add(checkedStatement);
              continue;
            } else {
              newStatements.add(new DeclarationStatement(decl.type, decl.name, Nullable.empty(),
                  false, false, decl.getCalculatedSizeof()));
              checkedStatement = new AssignmentStatement(new LvalueId(decl.name),
                  decl.getValue().get());
            }
          }
          List<Statement> singleStatement = new LinkedList<>();
          singleStatement.add(checkedStatement);
          newStatements
              .add(new ConditionalStatement(
                  new BinaryExpression(BinOp.EQUAL, new IdentifierExpression(hitExcId),
                      new NullExpression()),
                  new BlockStatement(singleStatement), Nullable.empty()));
        }
      }
    }

    if (enableResolution) {
      if (withinFn.isNotNull() && !hitReturnStatement) {
        newStatements
            .addAll(ResolveRaiseException.sanityMemcheck(project,
                new IntegerExpression(ourStackSize + ""),
                true, jsmntGlobalRenamings, hitExcId, withinFn.get(), msgState));
      }
    }

    newStatements.addAll(catchStatements);

    BlockStatement val = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(val));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    ScopedIdentifiers ourScopedIds = scopedIds.extend(ScopeFilter.BLOCK);
    List<Statement> newStatements = new LinkedList<>();
    boolean mustReturn = false;
    for (Statement statement : statements) {
      if (statement instanceof ReturnStatement) {
        mustReturn = true;
      }
      InsertDestructorsRet tmp = statement.insertDestructorCalls(ourScopedIds, destructorRenamings);
      newStatements.addAll(tmp.statements);
      if (tmp.mustReturn) {
        mustReturn = tmp.mustReturn;
      }
    }
    if (!mustReturn) {
      newStatements.addAll(ourScopedIds.getDestructors(ScopeFilter.BLOCK, destructorRenamings));
    }

    BlockStatement val = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());

    return new InsertDestructorsRet(val, mustReturn);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    List<ClassDeclaration> scopedLiftedClasses = new LinkedList<>();
    TypeReplacements scopedReplacements = replacements.extend();
    
    // first lift the classes. This will populate type replacements
    List<Statement> afterLiftStatements = new LinkedList<>();
    for (Statement statement : statements) {
      Nullable<Statement> tmp = statement.liftClassDeclarations(scopedLiftedClasses,
          scopedReplacements);
      if (tmp.isNotNull()) {
        afterLiftStatements.add(tmp.get());
      }
    }
    
    final Map<Type, Type> localTypeMapping = scopedReplacements.getMapping();
    
    // now replace types on both the lifted classes and the statements
    List<ClassDeclaration> typeReplacedLiftedClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : scopedLiftedClasses) {
      typeReplacedLiftedClasses.add(cdecl.replaceType(localTypeMapping, "", false));
    }
    
    List<Statement> newStatements = new LinkedList<>();
    for (Statement statement : afterLiftStatements) {
      newStatements.add(statement.replaceType(localTypeMapping));
    }
    
    liftedClasses.addAll(typeReplacedLiftedClasses);
    
    BlockStatement val = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<Statement> newStatements = new LinkedList<>();
    for (Statement statement : statements) {
      newStatements.add(statement.resolveUserTypes(msgState).get());
    }
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    List<Statement> newStatements = new LinkedList<>();
    for (Statement statement : statements) {
      newStatements.add(statement.addThisExpression(classHeader));
    }
    BlockStatement value = new BlockStatement(lineNum, columnNum, preComments, postComments,
        newStatements);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
