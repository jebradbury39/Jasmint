package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserDeclType;
import util.Pair;
import util.ScopedIdentifiers;

public class StaticClassIdentifierExpression extends AbstractExpression {

  public final UserDeclType id; // e.g. A, a.B (ClassDeclType or EnumDeclType)

  public StaticClassIdentifierExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, UserDeclType id) {
    super(lineNum, columnNum, AstType.STATIC_CLASS_IDENTIFIER_EXPRESSION, preComments,
        postComments);
    this.id = id;
  }

  public StaticClassIdentifierExpression(int lineNum, int columnNum, UserDeclType id) {
    super(lineNum, columnNum, AstType.STATIC_CLASS_IDENTIFIER_EXPRESSION);
    this.id = id;
  }

  public StaticClassIdentifierExpression(UserDeclType id) {
    super(AstType.STATIC_CLASS_IDENTIFIER_EXPRESSION);
    this.id = id;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return this;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Type>> normId = id.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!normId.isPresent()) {
      return Optional.empty();
    }
    if (normId.get().isNull()) {
      return Optional.of(Nullable.empty());
    }
    
    StaticClassIdentifierExpression value = new StaticClassIdentifierExpression(lineNum, columnNum,
        preComments, postComments,
        (UserDeclType) normId.get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public String toString(String indent) {
    return preToString() + id.toString() + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<UserDeclTypeBox> userTyBox = tenv.lookupUserDeclType(id, lineNum, columnNum);
    if (!userTyBox.isPresent()) {
      return Optional.empty();
    }
    determinedType = userTyBox.get().getType();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    throw new IllegalArgumentException();
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.StaticClassIdentifierExpression.Builder subDoc = AstProto.StaticClassIdentifierExpression
        .newBuilder();

    subDoc.setClassTypeId(id.serialize());

    document.setStaticClassIdentifierExpression(subDoc);
    return document;
  }

  public static Expression deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.StaticClassIdentifierExpression document)
      throws SerializationError {
    StaticClassIdentifierExpression result = new StaticClassIdentifierExpression(lineNum, columnNum,
        preComments, postComments,
        (UserDeclType) AbstractType.deserialize(document.getClassTypeId()));
    return result;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof StaticClassIdentifierExpression)) {
      return false;
    }
    return id.equals(((StaticClassIdentifierExpression) other).id);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<>(new LinkedList<>(), this);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    StaticClassIdentifierExpression value = new StaticClassIdentifierExpression(lineNum, columnNum,
        preComments, postComments, (UserDeclType) id.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return id.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return id.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return this;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    StaticClassIdentifierExpression value = new StaticClassIdentifierExpression(lineNum, columnNum,
        preComments, postComments, (UserDeclType) determinedType);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
