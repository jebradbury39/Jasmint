package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.UserTypeDeclarationValue;
import interp.Value;
import interp.ValueBox;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.ClassTypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.ScopeType;
import type_env.TypeEnvironment.TypeStatus;
import type_env.UserDeclTypeBox;
import typecheck.AbstractType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class NewClassInstanceExpression extends AbstractExpression {

  public final ClassType name; // classname, potentially with outerTypes. Contains real innerTypes
  public final String originalName;
  public final FunctionCallExpression constructorCall;
  
  private ClassType determinedName; // typecheck

  public NewClassInstanceExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, ClassType name, String originalName,
      FunctionCallExpression constructorCall) {
    super(lineNum, columnNum, AstType.NEW_CLASS_INSTANCE_EXPRESSION, preComments, postComments);
    this.name = name;
    this.originalName = originalName;
    this.constructorCall = constructorCall;
    this.determinedName = name;

    this.constructorCall.setParentAst(this);
  }

  public NewClassInstanceExpression(int lineNum, int columnNum, ClassType name,
      String originalName, FunctionCallExpression constructorCall) {
    super(lineNum, columnNum, AstType.NEW_CLASS_INSTANCE_EXPRESSION);
    this.name = name;
    this.originalName = originalName;
    this.constructorCall = constructorCall;
    this.determinedName = name;

    this.constructorCall.setParentAst(this);
  }

  public NewClassInstanceExpression(ClassType name, String originalName,
      FunctionCallExpression constructorCall) {
    super(AstType.NEW_CLASS_INSTANCE_EXPRESSION);
    this.name = name;
    this.originalName = originalName;
    this.constructorCall = constructorCall;
    this.determinedName = name;

    this.constructorCall.setParentAst(this);
  }
  
  public ClassType getName() {
    return name;
  }

  @Override
  public String toString(String indent) {
    String result = preToString() + name + "(";

    boolean first = true;
    for (Expression arg : constructorCall.arguments) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += arg.toString(indent);
    }

    return result + ")" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    
    TypeStatus tyStatus = tenv.lookupType(name, lineNum, columnNum).get();
    if (tyStatus.isIncomplete()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot instantiate an instance of incomplete type: " + name, lineNum, columnNum);
      return Optional.empty();
    }
    
    determinedName = (ClassType) name.normalize(tenv, lineNum, columnNum).get();
    ClassType className = (ClassType) determinedName;

    Optional<UserDeclTypeBox> userTyBox = tenv.lookupUserDeclType(className.asDeclType(), lineNum,
        columnNum);
    if (!userTyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(userTyBox.get() instanceof ClassTypeBox)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "");
      return Optional.empty();
    }
    ClassTypeBox classTyBox = (ClassTypeBox) userTyBox.get();
    
    ClassHeader classHeader = classTyBox.classHeader;

    if (classHeader.isStatic) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "cannot instantiate static class: " + className, lineNum, columnNum);
      return Optional.empty();
    }
    if (classHeader.innerTypes.size() != className.innerTypes.size()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "improper # of type parameters: "
          + classHeader.innerTypes + " vs given: " + className.innerTypes, lineNum, columnNum);
      return Optional.empty();
    }

    // get our type with real inner types and abs module path
    ClassType instanceType = new ClassType(classHeader.absName.outerType, classHeader.absName.name,
        className.innerTypes);

    instanceType.checkIfNonGeneric(tenv.getParentScope(ScopeType.GLOBAL).get(), lineNum, columnNum);

    // constructors get renamed. we need a link here like a regular function
    // we need to track which constructor function so we can have the new name and
    // correct overload
    // constructor return type should be void

    // INSTANCE (generics replaced with real types)
    Optional<TypeEnvironment> instanceTenv = instanceType.getTenv(tenv, lineNum, columnNum);
    if (!instanceTenv.isPresent()) {
      return Optional.empty();
    }
    
    constructorCall.typecheck(tenv, instanceTenv.get(), classHeader.absName);
    if (constructorCall.calledType.isNull()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "constructor calledType is null", lineNum, columnNum);
    }
    DotAccessType constructorWithin = constructorCall.calledType.get().within.get();
    if (!(constructorWithin instanceof ClassDeclType)) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "constructor is not within a class", lineNum, columnNum);
    }
    /*
    ClassDeclType constructorWithinClass = (ClassDeclType) constructorWithin;
    ClassDeclType absName = (ClassDeclType) name.asDeclType().normalize(tenv).get();
    if (!constructorWithinClass.equals(absName)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Constructor call is of class " + absName + " but actual calledType was "
              + constructorWithinClass, lineNum, columnNum);
      return Optional.empty();
    }
    */
    
    determinedType = instanceType;

    // FunctionType constFn = (FunctionType) constructorTypeBox.getValue();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    ValueBox classDeclValue = env.lookupNestedClassname(name);
    if (classDeclValue == null) {
      throw new InterpError("Class type " + name + " is not defined", lineNum, columnNum);
    }
    if (classDeclValue.getValue() == null) {
      throw new InterpError("Class type " + name + " definition is not initialized", lineNum,
          columnNum);
    }
    if (!(classDeclValue.getValue() instanceof UserTypeDeclarationValue)) {
      throw new InterpError("value from class type " + name + " is not a class declaration",
          lineNum, columnNum);
    }
    // just need a link to the class declaration
    UserTypeDeclarationValue declValue = (UserTypeDeclarationValue) classDeclValue.getValue();

    return null;
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.NewClassInstanceExpression.Builder subDoc = AstProto.NewClassInstanceExpression
        .newBuilder();

    subDoc.setName(name.serialize());
    subDoc.setOriginalName(originalName);
    subDoc.setConstructorCall(constructorCall.serialize());

    AstProto.Ast.Builder document = preSerialize();
    document.setNewClassInstanceExpression(subDoc);
    return document;
  }

  public static NewClassInstanceExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.NewClassInstanceExpression document) throws SerializationError {
    return new NewClassInstanceExpression(lineNum, columnNum, preComments, postComments,
        (ClassType) AbstractType.deserialize(document.getName()),
        document.getOriginalName(),
        (FunctionCallExpression) AbstractExpression.deserialize(document.getConstructorCall()));
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, (ClassType) name.renameIds(renamings, msgState), originalName,
        (FunctionCallExpression) constructorCall.renameIds(renamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<>();
    for (Expression arg : constructorCall.arguments) {
      freeVars.putAll(arg.findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof NewClassInstanceExpression)) {
      return false;
    }
    NewClassInstanceExpression ourOther = (NewClassInstanceExpression) other;
    if (!ourOther.name.equals(name)) {
      return false;
    }
    if (!ourOther.constructorCall.compare(constructorCall)) {
      return false;
    }
    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, name, originalName,
        (FunctionCallExpression) constructorCall.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return constructorCall.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> exprPair = constructorCall.toAnf(tuning);
    insert.addAll(exprPair.a);

    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, name, originalName, (FunctionCallExpression) exprPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    ClassType newName = (ClassType) name.replaceType(mapFromTo);

    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, newName, originalName,
        (FunctionCallExpression) constructorCall.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, name, originalName,
        (FunctionCallExpression) constructorCall.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    ClassType newName = (ClassType) name
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();

    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, newName, originalName,
        (FunctionCallExpression) constructorCall
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    name.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    constructorCall.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (constructorCall.containsType(types)) {
      return true;
    }
    return name.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return constructorCall.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    
    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, name, originalName, constructorCall);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return constructorCall.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn, withinFnStatement, Nullable.of(value), enableResolution,
        hitExcId, msgState);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, (ClassType) name, originalName,
        (FunctionCallExpression) constructorCall.insertDestructorCalls(scopedIds,
            destructorRenamings));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, (ClassType) name, originalName,
        (FunctionCallExpression) constructorCall.liftClassDeclarations(
            liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
        preComments, postComments, (ClassType) determinedName, originalName,
        (FunctionCallExpression) constructorCall.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
