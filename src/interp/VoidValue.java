package interp;

import typecheck.VoidType;

public class VoidValue extends AbstractValue {

  public VoidValue() {
    super(MetaValue.NORMAL, VoidType.Create());
  }
  
  public VoidValue(MetaValue metaValue) {
    super(metaValue, VoidType.Create());
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    throw new InterpError("internal error: add string not supported for type: void", -1, -1);
  }

  @Override
  public Value copy(Environment copyEnv) {
    return new VoidValue();
  }

  @Override
  public boolean equalsValue(Value other) {
    return other instanceof VoidValue;
  }

}
