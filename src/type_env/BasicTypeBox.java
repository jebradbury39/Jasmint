package type_env;

import ast.ClassDeclarationSection.Visibility;
import errors.FatalMessageException;
import errors.Nullable;
import java.util.Map;
import typecheck.DotAccessType;
import typecheck.FunctionImportType;
import typecheck.FunctionType;
import typecheck.ImportType;
import typecheck.Type;
import typecheck.UndeterminedImportType;

//this is general purpose, but does not store any UserDeclType
public class BasicTypeBox extends AbstractTypeBox {
  private Type value;
  public final Nullable<DotAccessType> definedInClass; //may be null
  public final Nullable<Visibility> visibilityLevel; //cannot be null if definedInClass != null
  public final boolean isStatic; //cannot be true if not in class
  
  public BasicTypeBox(Type value, boolean export, boolean isInit,
      VariableScoping scoping, Nullable<DotAccessType> definedInClass,
      Nullable<Visibility> visibilityLevel, boolean isStatic) {
    super(export, isInit, scoping);
    this.value = value;
    this.definedInClass = definedInClass;
    this.visibilityLevel = visibilityLevel;
    this.isStatic = isStatic;
  }
  
  public BasicTypeBox(Type value, boolean export, boolean isInit,
      VariableScoping scoping, DotAccessType definedInClass,
      Visibility visibilityLevel, boolean isStatic) {
    super(export, isInit, scoping);
    this.value = value;
    this.definedInClass = Nullable.of(definedInClass);
    this.visibilityLevel = Nullable.of(visibilityLevel);
    this.isStatic = isStatic;
  }
  
  public BasicTypeBox(Type value, boolean export, boolean isInit,
      VariableScoping scoping) {
    super(export, isInit, scoping);
    this.value = value;
    this.definedInClass = Nullable.empty();
    this.visibilityLevel = Nullable.empty();
    this.isStatic = false;
  }
  
  @Override
  public String toString() {
    return "<type=" + value.toString() + ", init=" + isInit + ">";
  }
  
  public Type getValue() {
    return value;
  }
  
  public void setValue(Type value) {
    this.value = value;
  }

  @Override
  public TypeBox fullCopy() {
    return new BasicTypeBox(value, export, isInit, scoping, definedInClass, visibilityLevel,
        isStatic);
  }

  @Override
  public TypeBox replaceType(TypeEnvironment tenv, Map<Type, Type> mapFromTo, int lineNum,
      int columnNum) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type getType() {
    return value;
  }

  @Override
  public ImportType getImportType(ImportType importType) {
    if (value instanceof FunctionType) {
      return new FunctionImportType(importType.outerType, importType.name);
    }
    return new UndeterminedImportType(importType.outerType, importType.name);
  }
}
