package ast;

public class SerializationError extends Exception {

  public SerializationError(String msg, boolean serialize) {
    super("SerializationError: " + (serialize ? "serialize" : "deserialize") + ": " + msg);
  }
}
