package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.ArrayValue;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.TypeStatus;
import typecheck.AbstractType;
import typecheck.ArrayType;
import typecheck.ClassType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class ArrayInitExpression extends AbstractExpression {

  public final Type elementType;
  public final List<Expression> values;
  
  public ArrayInitExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Type elementType,
      List<Expression> values) {
    super(lineNum, columnNum, AstType.ARRAY_INIT_EXPRESSION, preComments, postComments);
    this.elementType = elementType;
    this.values = values;
    
    determinedType = new ArrayType(elementType);
    
    for (Expression value : values) {
      value.setParentAst(this);
    }
  }
  
  public ArrayInitExpression(int lineNum, int columnNum, Type elementType,
      List<Expression> values) {
    super(lineNum, columnNum, AstType.ARRAY_INIT_EXPRESSION);
    this.elementType = elementType;
    this.values = values;
    
    determinedType = new ArrayType(elementType);
    
    for (Expression value : values) {
      value.setParentAst(this);
    }
  }
  
  public ArrayInitExpression(Type elementType, List<Expression> values) {
    super(AstType.ARRAY_INIT_EXPRESSION);
    this.elementType = elementType;
    this.values = values;
    
    determinedType = new ArrayType(elementType);
    
    for (Expression value : values) {
      value.setParentAst(this);
    }
  }
  
  @Override
  public String toString(String indent) {
    String result = preToString() + "[" + elementType + "][";
    boolean first = true;
    for (Expression value : values) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += value.toString(indent);
    }
    return result + "]" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    if (elementType instanceof ClassType) {
      ((ClassType) elementType).checkIfNonGeneric(tenv, lineNum, columnNum);
    }
    
    Type arrayType = new ArrayType(elementType);
    
    Optional<TypeStatus> tyStatus = tenv.lookupType(arrayType, lineNum, columnNum);
    if (!tyStatus.isPresent()) {
      return Optional.empty();
    }
    if (tyStatus.get().isIncomplete()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot instantiate an instance of incomplete type: "
          + determinedType, lineNum, columnNum);
    }
    
    determinedType = arrayType.normalize(tenv, lineNum, columnNum).get();
    
    for (Expression value : values) {
      Optional<TypecheckRet> optValueType = value.typecheck(tenv, Nullable.empty());
      if (!optValueType.isPresent()) {
        return Optional.empty();
      }
      Type valueType = optValueType.get().type;
      if (!tenv.canSafeCast(valueType, elementType)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "initial value type [" + valueType
            + "] is not of expected type [" + elementType + "]", lineNum, columnNum);
      }
    }
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    List<Value> initValues = new ArrayList<Value>();
    
    for (Expression value : values) {
      initValues.add(value.interp(env).copy(null));
    }
    
    return new ArrayValue(elementType, initValues);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();
    
    AstProto.ArrayInitExpression.Builder subDoc = AstProto.ArrayInitExpression.newBuilder();
    
    subDoc.setElementType(elementType.serialize().build());
    
    for (Expression val : values) {
      subDoc.addValues(val.serialize().build());
    }
    
    document.setArrayInitExpression(subDoc);
    return document;
  }

  public static ArrayInitExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments,
      List<CommentAttr> postComments,
      AstProto.ArrayInitExpression document) throws SerializationError {
    List<Expression> values = new LinkedList<Expression>();
    for (AstProto.Ast bsonVal : document.getValuesList()) {
      values.add(AbstractExpression.deserialize(bsonVal));
    }
    return new ArrayInitExpression(lineNum, columnNum, preComments, postComments,
        AbstractType.deserialize(document.getElementType()), values);
  }

  @Override
  public Expression renameIds(Renamings renamings,
      MsgState msgState) throws FatalMessageException {
    List<Expression> renamedValues = new LinkedList<Expression>();
    for (Expression val : values) {
      renamedValues.add(val.renameIds(renamings, msgState));
    }
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType.renameIds(renamings, msgState), renamedValues);
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<String, Type>();
    for (Expression value : values) {
      freeVars.putAll(value.findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof ArrayInitExpression)) {
      return false;
    }
    ArrayInitExpression ourOther = (ArrayInitExpression) other;
    if (values.size() != ourOther.values.size()) {
      return false;
    }
    Iterator<Expression> iterUs = values.iterator();
    Iterator<Expression> iterOther = ourOther.values.iterator();
    while (iterUs.hasNext() && iterOther.hasNext()) {
      if (!iterUs.next().compare(iterOther.next())) {
        return false;
      }
    }
    return true;
  }
  
  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    List<Expression> newValues = new LinkedList<Expression>();
    for (Expression val : values) {
      newValues.add((Expression) val.replace(target, with));
    }
    
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType,
        newValues);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (Expression val : values) {
      Nullable<Ast> tmp = val.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();
    
    List<Expression> newValues = new LinkedList<>();
    for (Expression item : values) {
      Pair<List<DeclarationStatement>, Ast> tmp = item.toAnf(tuning);
      insert.addAll(tmp.a);
      newValues.add((Expression) tmp.b);
    }
    
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType,
        newValues);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    List<Expression> newValues = new LinkedList<>();
    for (Expression item : values) {
      newValues.add(item.replaceType(mapFromTo));
    }
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType.replaceType(mapFromTo),
        newValues);
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException  {
    List<Expression> newValues = new LinkedList<>();
    for (Expression item : values) {
      Optional<Ast> tmp = item.replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newValues.add((Expression) tmp.get());
    }
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType, newValues);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
          throws FatalMessageException  {
    List<Expression> newValues = new LinkedList<Expression>();
    for (Expression item : values) {
      Optional<Nullable<Expression>> tmp = item.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newValues.add(tmp.get().get());
    }
    Optional<Nullable<Type>> tmp = elementType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        tmp.get().get(),
        newValues);
    tmp = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    value.determinedType = tmp.get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    elementType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    for (Expression val : values) {
      val.addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (types.contains(elementType)) {
      return true;
    }
    for (Expression val : values) {
      if (val.containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (Expression val : values) {
      if (val.containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    
    List<Statement> preStatements = new LinkedList<>();
    List<Expression> newValues = new LinkedList<>();
    
    for (Expression val : values) {
      Optional<ResolveExcRet> tmp = val.resolveExceptions(project, currentModule,
          auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newValues.add((Expression) tmp.get().ast.get());
    }
    
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType, newValues);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, value));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    int size = 8; //base array size
    for (Expression value : values) {
      int eleSize = value.calculateSize(tenv, lineNum, columnNum);
      if (eleSize == -1) {
        return -1;
      }
      size += eleSize;
    }
    return size;
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    List<Expression> newValues = new LinkedList<>();
    
    for (Expression val : values) {
      newValues.add(val.insertDestructorCalls(scopedIds, destructorRenamings));
    }
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType, newValues);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    List<Expression> newValues = new LinkedList<>();
    
    for (Expression val : values) {
      newValues.add(val.liftClassDeclarations(liftedClasses, replacements).get());
    }
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType, newValues);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<Expression> newValues = new LinkedList<>();
    
    for (Expression val : values) {
      newValues.add(val.resolveUserTypes(msgState).get());
    }
    
    ArrayType arrayType = (ArrayType) determinedType;
    
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        arrayType.elementType, newValues);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    List<Expression> newValues = new LinkedList<>();
    
    for (Expression val : values) {
      newValues.add(val.addThisExpression(classHeader));
    }
    ArrayInitExpression value = new ArrayInitExpression(lineNum, columnNum,
        preComments, postComments,
        elementType, newValues);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
