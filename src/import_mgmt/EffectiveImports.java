package import_mgmt;

import ast.SerializationError;
import astproto.AstProto;
import audit.AuditEntryLite;
import audit.AuditRenameTypesLite;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport.EffectiveImportType;
import import_mgmt.EffectiveImport.ImportUsage;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import multifile.Project;
import typecheck.AbstractType;
import typecheck.ImportType;
import typecheck.ModuleImportType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.ResolvedImportType;
import typecheck.UndeterminedImportType;
import util.Pair;

/*
 * Here's an attempt to simplify/combine the way we handle imports.
 * 
 * 1) We have our literal imports from the import statements (l_imports). These need to be checked
 *    for if they are actually used or not.
 * 2) We also have our implied imports (parent module and submods - i_imports). These must always
 *    be included so not much point in checking for usage, but we should check anyway for
 *    consistency.
 * 3) We have our inherited imports (h_imports). These come (recursively) from modules we import.
 *    Say module 'a' imports std.string and returns it from some function used in module 'b'. Then
 *    module 'b' is required to know about 'std.string'. These can be checked for usage in an effort
 *    to trim later (maybe).
 */

public class EffectiveImports {
  private Map<ImportType, EffectiveImport> importMapping = new HashMap<>();
  
  @Override
  public String toString() {
    String res = "";
    
    for (Entry<ImportType, EffectiveImport> entry : importMapping.entrySet()) {
      res += entry.getKey() + " -> " + entry.getValue() + "\n";
    }
    
    return res;
  }
  
  /*
   * relativePath can also be abs
   */
  public void addImport(ImportType relativePath, EffectiveImport imp, boolean allowOverwrite) {
    if (!allowOverwrite && importMapping.containsKey(relativePath)) {
      return;
    }
    //check if we have a matching value for imp already, and if so, point this relative path to that
    //one (to stay in sync for usage since same module)
    Nullable<ImportType> alsoUpdate = Nullable.empty();
    for (EffectiveImport tmp : importMapping.values()) {
      if (tmp.absPath.equals(imp.absPath)) {
        // make sure to keep higher usage
        if (imp.getUsage().ordinal() > tmp.getUsage().ordinal()) {
          tmp.setUsage(imp.getUsage());
        }
        alsoUpdate = Nullable.of(tmp.absPath);
        if (tmp.importType.ordinal() < imp.importType.ordinal()) {
          imp = tmp;
        }
        break;
      }
    }
    importMapping.put(relativePath, imp);
    if (alsoUpdate.isNotNull()) {
      importMapping.put(alsoUpdate.get(), imp);
    }
  }
  
  public void addImport(ModuleType relativePath, EffectiveImport imp, boolean allowOverwrite) {
    addImport(new ModuleImportType(relativePath.outerType, relativePath.name),
        imp, allowOverwrite);
  }
  
  /*
   * Using literal and implied imports as root nodes, traverse up the global class graph of modules
   * and collect all new modules we find as inherited imports.
   */
  public boolean collectInheritedImports(ImportGraph importGraph, MsgState msgState)
      throws FatalMessageException {
    Set<Pair<ImportType, Nullable<ModuleType.ProjectNameVersion>>> inheritImports = new HashSet<>();
    for (EffectiveImport imp : importMapping.values()) {
      if (imp.importType == EffectiveImportType.INHERITED_IMPORT) {
        continue;
      }
      if (imp.importType == EffectiveImportType.PARENT_IMPORT) {
        continue;
      }
      Nullable<Set<Pair<ImportType, Nullable<ModuleType.ProjectNameVersion>>>> dfsFromImp =
          importGraph.dfs((ResolvedImportType) imp.absPath, imp.withinProject);
      if (dfsFromImp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "failed to collect inherited imports");
      }
      inheritImports.addAll(dfsFromImp.get());
    }
    
    //add all new inherit imports
    for (Pair<ImportType, Nullable<ProjectNameVersion>> inherit : inheritImports) {
      addImport(inherit.a, new EffectiveImport(EffectiveImportType.INHERITED_IMPORT, false,
          inherit.a, inherit.b), false);
    }
    return true;
  }
  
  /*
   * relativePath can also be abs
   */
  public Nullable<EffectiveImport> getImport(ImportType relativePath, ImportUsage setUsed) {
    EffectiveImport imp = importMapping.get(relativePath);
    if (imp == null) {
      return Nullable.empty();
    }
    if (setUsed != ImportUsage.INVALID
        && imp.getUsage() == ImportUsage.NONE
        && setUsed != ImportUsage.NONE) {
      imp.setUsage(setUsed);;
    }
    return Nullable.of(imp);
  }
  
  public EffectiveImports applyTransforms(AuditTrailLite auditTrail, MsgState msgState) {
    EffectiveImports transformed = this;
    
    Iterator<AuditEntryLite> iterProjectAudit = auditTrail.iterator();
    
    while (iterProjectAudit.hasNext()) {
      AuditEntryLite projectTransform = iterProjectAudit.next();
      switch (projectTransform.getType()) {
        case RENAME_TYPES:
          AuditRenameTypesLite renameTransform = (AuditRenameTypesLite) projectTransform;
          transformed = transformed.applyTypeRenamings(renameTransform);
          break;
        default:
          break;
      }
    }
    
    return transformed;
  }

  private EffectiveImports applyTypeRenamings(AuditRenameTypesLite renameTransform) {
    EffectiveImports newEffectiveImports = new EffectiveImports();
    
    for (Entry<ImportType, EffectiveImport> entry : importMapping.entrySet()) {
      //only use abs
      if (!entry.getKey().toString().equals(entry.getValue().absPath.toString())) {
        continue;
      }
      
      Nullable<ResolvedImportType> newKey = renameTransform.getNewType((ResolvedImportType) entry.getKey());
      Nullable<EffectiveImport> newImp = entry.getValue().applyTypeRenamings(renameTransform);
      newEffectiveImports.addImport(newKey.get(), newImp.get(), false);
      /*
       * Also add the short version if not inherited
       */
      if (entry.getValue().importType != EffectiveImport.EffectiveImportType.INHERITED_IMPORT) {
        newEffectiveImports.addImport(newKey.get().onlyName(),
            newImp.get(), false);
      }
    }
    
    return newEffectiveImports;
  }

  public Map<ImportType, EffectiveImport> getImportMap(boolean onlyAbs, Set<EffectiveImportType> filterImpTypes) {
    Map<ImportType, EffectiveImport> imports = new HashMap<>();
    for (Entry<ImportType, EffectiveImport> imp : importMapping.entrySet()) {
      if (onlyAbs && !imp.getKey().equals(imp.getValue().absPath)) {
        //skip relative path
        continue;
      }
      if (filterImpTypes.contains(imp.getValue().importType)) {
        imports.put(imp.getKey(), imp.getValue());
      }
    }
    return imports;
  }
  
  public List<Pair<ImportType, Nullable<ProjectNameVersion>>> getImportList(boolean onlyAbs,
      Set<EffectiveImportType> filterImpTypes) {
    List<Pair<ImportType, Nullable<ProjectNameVersion>>> result = new LinkedList<>();
    for (Entry<ImportType, EffectiveImport> entry : getImportMap(onlyAbs, filterImpTypes).entrySet()) {
      result.add(new Pair<>(entry.getKey(), entry.getValue().withinProject));
    }
    return result;
  }

  /*
   * Even with renamings, the number of imports will stay the same (as well as their core identity)
   */
  public void setAllUsage(ImportUsage usage) {
    for (EffectiveImport imp : importMapping.values()) {
      imp.setUsage(usage);
    }
  }

  /*
   * short name to abs name conversion
   */
  public Map<ImportType, ImportType> getModuleMapping(Set<EffectiveImportType> filterImpTypes) {
    Map<ImportType, ImportType> mapping = new HashMap<>();
    
    for (Entry<ImportType, EffectiveImport> imp : importMapping.entrySet()) {
      if (filterImpTypes.contains(imp.getValue().importType)) {
        mapping.put(imp.getKey(), imp.getValue().absPath);
      }
    }
    
    return mapping;
  }
  
  public AstProto.EffectiveImports.Builder serialize() throws SerializationError {
    AstProto.EffectiveImports.Builder document = AstProto.EffectiveImports.newBuilder();
    
    for (Entry<ImportType, EffectiveImport> entry : importMapping.entrySet()) {
      document.addImportMappingKeys(entry.getKey().serialize());
      document.addImportMappingVals(entry.getValue().serialize());
    }
    
    return document;
  }
  
  public static EffectiveImports deserialize(AstProto.EffectiveImports document)
      throws SerializationError {
    EffectiveImports effectiveImports = new EffectiveImports();
    
    if (document.getImportMappingKeysCount() != document.getImportMappingValsCount()) {
      throw new IllegalArgumentException("mismatch keys/values for effective imports");
    }
    
    Iterator<AstProto.Type> iterKeys = document.getImportMappingKeysList().iterator();
    Iterator<AstProto.EffectiveImport> iterVals = document.getImportMappingValsList().iterator();
    
    while (iterKeys.hasNext()) {
      AstProto.Type nextKey = iterKeys.next();
      AstProto.EffectiveImport nextVal = iterVals.next();
      
      ImportType importType = (ImportType) AbstractType.deserialize(nextKey);
      EffectiveImport effectiveImport = EffectiveImport.deserialize(nextVal);
      
      effectiveImports.addImport(importType, effectiveImport, true);
    }
    
    return effectiveImports;
  }

  public void determineWithinProject(Project project, MsgState msgState)
      throws FatalMessageException {
    for (Entry<ImportType, EffectiveImport> entry : importMapping.entrySet()) {
      // lookup the project which defines this module
      Nullable<Project> definingProj = project.lookupDefiningProject(entry.getValue().absPath);
      if (definingProj.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "Module does not exist: " + entry.getKey());
      }
      
      entry.getValue().withinProject = Nullable.of(definingProj.get().getModProjectNameVersion());
    }
  }

  /*
   * Need to preserve the usage info
   */
  public Optional<EffectiveImports> resolveImports(Project project, MsgState msgState) throws FatalMessageException {
    EffectiveImports newMapping = new EffectiveImports();
    for (Entry<ImportType, EffectiveImport> entry : importMapping.entrySet()) {
      Optional<EffectiveImport> resolvedImpOpt = entry.getValue().resolveImports(project, msgState);
      if (!resolvedImpOpt.isPresent()) {
        return Optional.empty();
      }
      EffectiveImport resolvedImp = resolvedImpOpt.get();
      
      if (!entry.getValue().absPath.equals(entry.getKey())) {
        //then this is relative
        if (entry.getKey().outerType.isNotNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
              "Did not expect relative import to have a module type: " + entry.getKey());
          return Optional.empty();
        }
        newMapping.addImport(resolvedImp.absPath.onlyName(), resolvedImp, true);
      } else {
        // then this is absolute
        newMapping.addImport(resolvedImp.absPath, resolvedImp, true);
      }
      
      // if this is a class, make sure we also import the absolute module
      if (!(resolvedImp.absPath instanceof ModuleImportType)) {
        ModuleType modType = resolvedImp.absPath.getModuleType();
        ImportType impModType = modType.asImportType();
        // this is absolute
        newMapping.addImport(impModType, new EffectiveImport(EffectiveImportType.IMPLIED_IMPORT,
            false, impModType, resolvedImp.withinProject), true);
      }
    }
    return Optional.of(newMapping);
  }

  public EffectiveImports removeNonModuleImports() {
    EffectiveImports newMapping = new EffectiveImports();
    for (Entry<ImportType, EffectiveImport> entry : importMapping.entrySet()) {
      if (entry.getValue().absPath instanceof UndeterminedImportType) {
        throw new IllegalArgumentException();
      }
      if (entry.getValue().absPath instanceof ModuleImportType) {
        newMapping.addImport(entry.getKey(), entry.getValue(), true);
      } else {
        // non-module, so just import abs module path
        ModuleImportType impType = (ModuleImportType) entry.getValue().absPath.getModuleType().asImportType();
        EffectiveImport newImp = new EffectiveImport(EffectiveImportType.LITERAL_IMPORT,
            false, impType, entry.getValue().withinProject);
        newImp.setUsage(entry.getValue().getUsage());
        newMapping.addImport(impType, newImp, true);
      }
    }
    return newMapping;
  }

}
