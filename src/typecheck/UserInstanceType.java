package typecheck;

import errors.FatalMessageException;
import errors.Nullable;
import java.util.List;
import java.util.Optional;
import type_env.TypeEnvironment;

/*
 * Either a ClassType or ModuleType
 */
public abstract class UserInstanceType extends AbstractType implements DotAccessType, ReferencedType {

  public final Nullable<ModuleType> outerType; //parent module. If null, not normalized yet
  public final String name;
  
  public UserInstanceType(TypeType typeType, Nullable<ModuleType> outerType, String name) {
    super(typeType);
    this.outerType = outerType;
    this.name = name;
  }
  
  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof UserInstanceType)) {
      return false;
    }
    UserInstanceType otherUt = (UserInstanceType) other;
    return name.equals(otherUt.name) && outerType.equals(otherUt.outerType);
  }
  
  @Override
  public int hashCode() {
    return outerType.hashCode() + name.hashCode();
  }
  
  @Override
  public String toString() {
    String res = "";
    if (outerType.isNotNull()) {
      res += outerType.toString() + ".";
    }
    return res + name;
  }
  
  public abstract List<String> toList();
  
  public abstract Optional<TypeEnvironment> getTenv(TypeEnvironment tenv,
      int lineNum, int columnNum) throws FatalMessageException;

  public abstract UserDeclType asDeclType();
}
