package interp;

public class InterpError extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = -9067775767678106869L;
  public final String coreMsg;
  public final int lineNum;
  public final int columnNum;
  
  public InterpError(String msg, int lineNum, int columnNum) {
    super("Jasmint InterpError: " + msg + " @ line " + lineNum + " column " + columnNum);
    this.coreMsg = msg;
    this.lineNum = lineNum;
    this.columnNum = columnNum;
  }
}
