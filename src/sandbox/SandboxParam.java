package sandbox;

import ast.Ast;
import ast.Ast.AnfTune;
import ast.DeclarationStatement;
import ast.Program.RenamesOfInterest;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import sandbox.AbstractSandboxParam.ParamType;
import type_env.TypeEnvironment;
import typecheck.ModuleType;
import typecheck.Type;
import util.Pair;

public interface SandboxParam {
  public ParamType getParamType();

  public SandboxParam renameIds(Renamings renamings, MsgState msgState)
      throws FatalMessageException;

  public Optional<SandboxParam> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
          throws FatalMessageException;

  public AstProto.SandboxParam.Builder serialize() throws SerializationError;

  public SandboxParam replace(Ast target, Ast with);

  public Map<String, Type> findFreeVariables();

  public boolean compare(SandboxParam other);

  public Nullable<Ast> findByOriginalLabel(int findLabel);

  public Pair<List<DeclarationStatement>, SandboxParam> toAnf(AnfTune tuning);

  public SandboxParam replaceType(Map<Type, Type> mapFromTo);

  public Optional<SandboxParam> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException;

  public boolean containsType(Set<Type> types);

  public boolean containsAst(Set<Ast> asts);

  public Optional<Type> typecheck(TypeEnvironment tenv) throws FatalMessageException;

  // do stuff like: tmp_fn_ctx.memlimit = 20
  public List<Statement> createAssignmentLines(String sandboxLambdaFnCtx,
      RenamesOfInterest jsmntGlobalRenamings);

  public Optional<SandboxParam> resolveUserType(MsgState msgState) throws FatalMessageException;

  public SandboxParam addThisExpression(ClassHeader classHeader);
}
