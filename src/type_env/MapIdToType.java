package type_env;

import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.Nullable;
import java.util.Map;
import typecheck.FunctionType;
import typecheck.Type;
import util.Pair;

public class MapIdToType extends IdToType {
  private TypeBox typeBox;

  @Override
  public String toString() {
    return typeBox.toString();
  }
  
  @Override
  public IdToType write(String name, TypeBox typeBox) {
    if (typeBox instanceof BasicTypeBox) {
      sanityCheck(name, (BasicTypeBox) typeBox);
    }
    this.typeBox = typeBox;
    return this;
  }
  
  @Override
  public Pair<Nullable<TypeBox>, Integer> read(TypeEnvironment tenv, String name,
      FunctionType functionType, boolean ignoreExport,
      int lineNum, int columnNum) throws FatalMessageException {
    if (!ignoreExport && !typeBox.getExport()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot access un-exported id: " + name, lineNum, columnNum);
      return new Pair<>(Nullable.empty(), -1);
    }
    return new Pair<>(Nullable.of(typeBox), -1);
  }
  
  public TypeBox read() {
    return typeBox;
  }

  @Override
  public IdToType fullCopy(String name) {
    MapIdToType copy = new MapIdToType();
    copy.write(name, typeBox.fullCopy());
    return copy;
  }

  @Override
  public IdToType replaceType(TypeEnvironment tenv, String name,
      Map<Type, Type> mapFromTo) throws FatalMessageException {
    MapIdToType copy = new MapIdToType();
    copy.write(name, typeBox.replaceType(tenv, mapFromTo, -1, -1));
    return copy;
  }

}
