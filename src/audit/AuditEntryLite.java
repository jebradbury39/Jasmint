package audit;

import astproto.AstProto;
import audit.AuditEntry.AuditEntryType;

public interface AuditEntryLite {

  AuditEntryType getType();

  AstProto.AuditEntryLite.Builder serializeLite();

}
