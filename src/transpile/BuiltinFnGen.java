package transpile;

import java.util.List;
import java.util.Set;
import transpile.BuiltinFunctionOverloads.FunctionOverload;
import transpile.BuiltinFunctionOverloads.NamespaceName;
import typecheck.Type;
import util.Pair;

public interface BuiltinFnGen {
  //return (newName, generatedFunctionDefinition)
  Pair<String, String> function(BuiltinFunctionOverloads overloads,
      FunctionOverload ctx, Set<NamespaceName> usingNamespaces, List<Type> realArgTypes);
}
