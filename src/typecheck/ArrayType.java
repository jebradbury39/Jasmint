package typecheck;

import ast.ArrayInitExpression;
import ast.Expression;
import ast.Program.DestructorRenamings;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.BasicTypeBox;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.FunctionType.LambdaStatus;

public class ArrayType extends AbstractType implements DotAccessType, ReferencedType {

  public final Type elementType;

  public ArrayType(Type elementType) {
    super(TypeType.ARRAY_TYPE);
    this.elementType = elementType;
  }

  @Override
  public int hashCode() {
    return super.hashCode() + elementType.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof ArrayType)) {
      return false;
    }
    ArrayType otherAT = (ArrayType) other;
    return elementType.equals(otherAT.elementType);
  }

  @Override
  public List<Type> getInnerTypes() {
    List<Type> tyList = new ArrayList<Type>();
    tyList.add(elementType);
    return tyList;
  }

  @Override
  public String toString() {
    return super.toString() + "[" + elementType + "]";
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }

    return new ArrayType(elementType.replaceType(mapFromTo));
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.ArrayType.Builder subDoc = AstProto.ArrayType.newBuilder();

    subDoc.setElementType(elementType.serialize().build());

    document.setArrayType(subDoc);
    return document;
  }

  public static ArrayType deserialize(AstProto.ArrayType document) throws SerializationError {
    Type elementType = AbstractType.deserialize(document.getElementType());

    return new ArrayType(elementType);
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    TypeEnvironment newTenv = new TypeEnvironment(TypeEnvironment.ScopeType.INSTANCE,
        tenv.ofDeclType, tenv.typeGraph, tenv.callGraph,
        tenv.project, tenv.effectiveImports,
        tenv.msgState);

    List<Type> addParam = new LinkedList<>();
    addParam.add(elementType);
    newTenv.defineVar("add",
        new BasicTypeBox(
            new FunctionType(this, VoidType.Create(), addParam, LambdaStatus.NOT_LAMBDA),
            true, true, VariableScoping.CLASS_INSTANCE));
    
    // add sandboxed version if needed
    /*if (tenv.project.getAuditTrail().contains(AuditEntryType.RESOLVE_EXCEPTIONS)) {
      addParam = new LinkedList<>();
    
      //need current type name (might be after type renamings)
      addParam.add(JsmntGlobal.fnCtxRefType);
      addParam.add(elementType);
      
      newTenv.defineVar(false, "add",
          Nullable.of(new FunctionType(this, VoidType.Create(), addParam, LambdaStatus.NOT_LAMBDA)),
          Nullable.empty(), AstType.FUNCTION, Nullable.empty(), Nullable.empty(),
          VariableScoping.CLASS_INSTANCE, false);
    }*/

    List<Type> add2Param = new LinkedList<>();
    add2Param.add(IntType.Create(false, 64));
    add2Param.add(elementType);
    newTenv.defineVar("add", new BasicTypeBox(
        new FunctionType(this, VoidType.Create(), add2Param, LambdaStatus.NOT_LAMBDA),
        true, true, VariableScoping.CLASS_INSTANCE));
    
    // add sandboxed version if needed
    /*if (tenv.project.getAuditTrail().contains(AuditEntryType.RESOLVE_EXCEPTIONS)) {
      add2Param = new LinkedList<>();
      add2Param.add(JsmntGlobal.fnCtxRefType);
      add2Param.add(IntType.Create(false, 64));
      add2Param.add(elementType);
      newTenv.defineVar(false, "add",
          Nullable.of(new FunctionType(this, VoidType.Create(), add2Param,
              LambdaStatus.NOT_LAMBDA)),
          Nullable.empty(), AstType.FUNCTION, Nullable.empty(), Nullable.empty(),
          VariableScoping.CLASS_INSTANCE, false);
    }*/

    List<Type> removeParam = new LinkedList<>();
    removeParam.add(IntType.Create(false, 64));
    newTenv.defineVar("remove", new BasicTypeBox(
        new FunctionType(this, elementType, removeParam, LambdaStatus.NOT_LAMBDA),
        true, true, VariableScoping.CLASS_INSTANCE));

    List<Type> sizeParam = new LinkedList<>();
    newTenv.defineVar("size", new BasicTypeBox(
        new FunctionType(this, IntType.Create(false, 64), sizeParam, LambdaStatus.NOT_LAMBDA),
        true, true, VariableScoping.CLASS_INSTANCE));

    return Optional.of(newTenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (types.contains(this)) {
      return true;
    }
    return elementType.containsType(types);
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return elementType.getUserDeclTypes(stackTypesOnly);
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    Optional<Type> tmp = elementType.normalize(tenv, lineNum, columnNum);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new ArrayType(tmp.get()));
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return new ArrayType(elementType.renameIds(renamings, msgState));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException {
    elementType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
  }

  @Override
  public Type trimRelativeType(UserInstanceType currentModuleFullType) {
    return new ArrayType(elementType.trimRelativeType(currentModuleFullType));
  }

  @Override
  public Type setConst(boolean isConst) {
    ArrayType tmp = new ArrayType(elementType);
    tmp.isConst = isConst;
    return tmp;
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Optional<Nullable<Type>> tmp = elementType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(Nullable.of(new ArrayType(tmp.get().get())));
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState, Set<ModuleType> exportedTypes, boolean skipNormalize)
      throws FatalMessageException {
    return elementType.getNormalizedModType(currentModule, effectiveImports, msgState,
        exportedTypes, skipNormalize);
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new ArrayInitExpression(elementType, new LinkedList<>());
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    ArrayType newType = new ArrayType(elementType.prependFnRetType(prependType, false));
    if (!isTop) {
      return newType;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(newType);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    int eleSize = elementType.calculateSize(tenv, lineNum, columnNum);
    if (eleSize == -1) {
      return -1;
    }
    // add 8 bytes for array ptr+size info
    return 8 + eleSize;
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) {
    // TODO
    /*
     * for (int tmp_idx_0 = 0; tmp_idx_0 < id.size(); tmp_idx_0 = tmp_idx_0 + 1) {
     * id[tmp_idx_0].destructor() }
     * 
     * for (elementType item in id) { item.destructor(); }
     */
    if (!(elementType instanceof DotAccessType)) {
      return Optional.empty();
    }
    return Optional.empty();
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return elementType.copyFromTenvIntoTenv(tenv, newTenv);
  }

  @Override
  public Type asClassDeclType() {
    return new ArrayType(elementType.asClassDeclType());
  }

  @Override
  public ModuleType getModuleType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public UserDeclType asUserDeclType() {
    throw new UnsupportedOperationException();
  }
}
