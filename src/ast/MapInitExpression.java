package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.MapValue;
import interp.Value;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.TypeStatus;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class MapInitExpression extends AbstractExpression {

  public final Type keyType;
  public final Type valueType;
  public final Map<Expression, Expression> pairs;

  public MapInitExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Type keyType, Type valueType,
      Map<Expression, Expression> pairs) {
    super(lineNum, columnNum, AstType.MAP_INIT_EXPRESSION, preComments, postComments);
    this.keyType = keyType;
    this.valueType = valueType;
    this.pairs = pairs;

    for (Entry<Expression, Expression> entry : this.pairs.entrySet()) {
      entry.getKey().setParentAst(this);
      entry.getValue().setParentAst(this);
    }

    determinedType = new MapType(keyType, valueType);
  }

  public MapInitExpression(int lineNum, int columnNum, Type keyType, Type valueType,
      Map<Expression, Expression> pairs) {
    super(lineNum, columnNum, AstType.MAP_INIT_EXPRESSION);
    this.keyType = keyType;
    this.valueType = valueType;
    this.pairs = pairs;

    for (Entry<Expression, Expression> entry : this.pairs.entrySet()) {
      entry.getKey().setParentAst(this);
      entry.getValue().setParentAst(this);
    }

    determinedType = new MapType(keyType, valueType);
  }

  public MapInitExpression(Type keyType, Type valueType, Map<Expression, Expression> pairs) {
    super(AstType.MAP_INIT_EXPRESSION);
    this.keyType = keyType;
    this.valueType = valueType;
    this.pairs = pairs;

    for (Entry<Expression, Expression> entry : this.pairs.entrySet()) {
      entry.getKey().setParentAst(this);
      entry.getValue().setParentAst(this);
    }

    determinedType = new MapType(keyType, valueType);
  }

  @Override
  public String toString(String indent) {
    String result = preToString();
    result += "{" + keyType + ", " + valueType + "}{";
    boolean first = true;
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      if (!first) {
        result += ",";
      }
      first = false;
      String tmpIndent = indent + incIndent;
      result += "\n" + tmpIndent + pair.getKey().toString(tmpIndent) + ": "
          + pair.getValue().toString(tmpIndent);
    }
    if (!first) {
      result += "\n";
    }
    return result + "}" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    boolean hitErr = false;

    TypeStatus tyStatus = tenv.lookupType(determinedType, lineNum, columnNum).get();
    if (tyStatus.isGeneric()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot instantiate an instance of incomplete type: " + determinedType, lineNum,
          columnNum);
      hitErr = true;
    }
    
    determinedType = determinedType.normalize(tenv, lineNum, columnNum).get();

    for (Entry<Expression, Expression> entry : pairs.entrySet()) {
      Optional<TypecheckRet> entryKeyTypeOpt = entry.getKey().typecheck(tenv, Nullable.empty());
      Optional<TypecheckRet> entryValueTypeOpt = entry.getValue().typecheck(tenv, Nullable.empty());

      if (!entryKeyTypeOpt.isPresent() || !entryValueTypeOpt.isPresent()) {
        hitErr = true;
        continue;
      }

      Type entryKeyType = entryKeyTypeOpt.get().type;
      Type entryValueType = entryValueTypeOpt.get().type;

      // source casting up to target
      if (!tenv.canSafeCast(entryKeyType, keyType)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "cannot convert map key type [" + entryKeyType + "] to expected [" + keyType + "]",
            lineNum, columnNum);
        hitErr = true;
      }
      if (!tenv.canSafeCast(entryValueType, valueType)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "cannot convert map value type [" + entryValueType + "] to expected [" + valueType
                + "]",
            lineNum, columnNum);
        hitErr = true;
      }
    }

    if (hitErr) {
      return Optional.empty();
    }
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Map<Value, Value> mapValues = new HashMap<Value, Value>();

    for (Entry<Expression, Expression> entry : pairs.entrySet()) {
      mapValues.put(entry.getKey().interp(env).copy(null), entry.getValue().interp(env).copy(null));
    }

    return new MapValue(keyType, valueType, mapValues);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.MapInitExpression.Builder subDoc = AstProto.MapInitExpression.newBuilder();

    subDoc.setKeyType(keyType.serialize());
    subDoc.setValueType(valueType.serialize());

    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      subDoc.addKeys(pair.getKey().serialize());
      subDoc.addValues(pair.getValue().serialize());
    }

    document.setMapInitExpression(subDoc);
    return document;
  }

  public static MapInitExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.MapInitExpression document) throws SerializationError {
    Map<Expression, Expression> pairs = new HashMap<Expression, Expression>();
    Iterator<AstProto.Ast> iterBvKeys = document.getKeysList().iterator();
    Iterator<AstProto.Ast> iterBvValues = document.getValuesList().iterator();
    while (iterBvKeys.hasNext() && iterBvValues.hasNext()) {
      pairs.put(AbstractExpression.deserialize(iterBvKeys.next()),
          AbstractExpression.deserialize(iterBvValues.next()));
    }

    return new MapInitExpression(lineNum, columnNum, preComments, postComments,
        AbstractType.deserialize(document.getKeyType()),
        AbstractType.deserialize(document.getValueType()), pairs);
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Map<Expression, Expression> newPairs = new HashMap<Expression, Expression>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put(pair.getKey().renameIds(renamings, msgState),
          pair.getValue().renameIds(renamings, msgState));
    }
    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType.renameIds(renamings, msgState), valueType.renameIds(renamings, msgState), newPairs);
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<String, Type>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      freeVars.putAll(pair.getKey().findFreeVariables());
      freeVars.putAll(pair.getValue().findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof MapInitExpression)) {
      return false;
    }
    MapInitExpression ourOther = (MapInitExpression) other;
    if (!ourOther.keyType.equals(keyType) || !ourOther.valueType.equals(valueType)) {
      return false;
    }

    if (ourOther.pairs.size() != pairs.size()) {
      return false;
    }
    Iterator<Entry<Expression, Expression>> iterOtherP = ourOther.pairs.entrySet().iterator();
    Iterator<Entry<Expression, Expression>> iterUsP = pairs.entrySet().iterator();
    while (iterOtherP.hasNext() && iterUsP.hasNext()) {
      Entry<Expression, Expression> otherE = iterOtherP.next();
      Entry<Expression, Expression> usE = iterUsP.next();
      if (!otherE.getKey().compare(usE.getKey())) {
        return false;
      }
      if (!otherE.getValue().compare(usE.getValue())) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    Map<Expression, Expression> newPairs = new HashMap<Expression, Expression>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put((Expression) pair.getKey().replace(target, with),
          (Expression) pair.getValue().replace(target, with));
    }

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType, valueType, newPairs);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      Nullable<Ast> tmp = pair.getKey().findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
      tmp = pair.getValue().findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Map<Expression, Expression> newPairs = new HashMap<Expression, Expression>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      Pair<List<DeclarationStatement>, Ast> keyPair = pair.getKey().toAnf(tuning);
      insert.addAll(keyPair.a);
      Pair<List<DeclarationStatement>, Ast> valuePair = pair.getValue().toAnf(tuning);
      insert.addAll(valuePair.a);

      newPairs.put((Expression) keyPair.b, (Expression) valuePair.b);
    }

    // create newValues array init
    MapInitExpression val = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType, valueType, newPairs);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());

    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    Map<Expression, Expression> newPairs = new HashMap<Expression, Expression>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put((Expression) pair.getKey().replaceType(mapFromTo),
          (Expression) pair.getValue().replaceType(mapFromTo));
    }

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType.replaceType(mapFromTo), valueType.replaceType(mapFromTo), newPairs);
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Map<Expression, Expression> newPairs = new HashMap<Expression, Expression>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put((Expression) pair.getKey().replaceVtableAccess(project, msgState).get(),
          (Expression) pair.getValue().replaceVtableAccess(project, msgState).get());
    }

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType, valueType, newPairs);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Map<Expression, Expression> newPairs = new HashMap<>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put(pair.getKey()
          .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get(),
          pair.getValue()
              .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
              .get().get());
    }

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType.basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        valueType.basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        newPairs);
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    keyType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    valueType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      pair.getKey().addDependency(modDepGraph, ourModNode, tenv);
      pair.getValue().addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return keyType.containsType(types) || valueType.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      if (pair.getKey().containsAst(asts) || pair.getValue().containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Map<Expression, Expression> newPairs = new HashMap<>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      Optional<ResolveExcRet> tmpKey = pair.getKey().resolveExceptions(project,
          currentModule, auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement,
          enableResolution, hitExcId, stackSize, msgState);
      Optional<ResolveExcRet> tmpVal = pair.getValue().resolveExceptions(project,
          currentModule, auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement,
          enableResolution, hitExcId, stackSize, msgState);
      if (!tmpKey.isPresent() || !tmpVal.isPresent()) {
        return Optional.empty();
      }

      preStatements.addAll(tmpKey.get().preStatements);
      preStatements.addAll(tmpVal.get().preStatements);
      newPairs.put((Expression) tmpKey.get().ast.get(), (Expression) tmpVal.get().ast.get());
    }

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType, valueType, newPairs);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    int size = 8; //base map size
    for (Entry<Expression, Expression> entry : pairs.entrySet()) {
      int keySize = entry.getKey().calculateSize(tenv, lineNum, columnNum);
      if (keySize == -1) {
        return -1;
      }
      int valueSize = entry.getValue().calculateSize(tenv, lineNum, columnNum);
      if (valueSize == -1) {
        return -1;
      }
      size += keySize + valueSize;
    }
    return size;
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    Map<Expression, Expression> newPairs = new HashMap<>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put(pair.getKey().insertDestructorCalls(scopedIds, destructorRenamings),
          pair.getValue().insertDestructorCalls(scopedIds, destructorRenamings));
    }

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType, valueType, newPairs);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    Map<Expression, Expression> newPairs = new HashMap<>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put(pair.getKey()
          .liftClassDeclarations(liftedClasses, replacements).get(),
          pair.getValue()
              .liftClassDeclarations(liftedClasses, replacements)
              .get());
    }

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType,
        valueType,
        newPairs);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    Map<Expression, Expression> newPairs = new HashMap<>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put(pair.getKey()
          .resolveUserTypes(msgState).get(),
          pair.getValue()
              .resolveUserTypes(msgState)
              .get());
    }
    
    MapType mapDetTy = (MapType) determinedType;

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        mapDetTy.keyType,
        mapDetTy.valueType,
        newPairs);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    Map<Expression, Expression> newPairs = new HashMap<>();
    for (Entry<Expression, Expression> pair : pairs.entrySet()) {
      newPairs.put(pair.getKey()
          .addThisExpression(classHeader),
          pair.getValue()
              .addThisExpression(classHeader));
    }

    MapInitExpression value = new MapInitExpression(lineNum, columnNum, preComments, postComments,
        keyType,
        valueType,
        newPairs);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
