package threading;

import errors.Nullable;

public class Worker extends Thread {
  private final ThreadManager manager;
  private boolean stopped;

  public Worker(ThreadManager manager) {
    this.manager = manager;
  }

  @Override
  public void run() {
    while (!stopped) {
      //get the next item from the queue
      Nullable<Job> tmpJob = manager.getNextJob();
      if (tmpJob.isNull()) {
        continue;
      }
      Job job = tmpJob.get();
      manager.jobDone(job.id, job.run());
    }
  }

  public void stopWorker() {
    stopped = true;
  }
}
