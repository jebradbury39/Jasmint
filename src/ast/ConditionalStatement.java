package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.BoolType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import typegraph.TypeGraph.CastDirection;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class ConditionalStatement extends AbstractStatement {

  public final Expression guard;
  public final Statement thenBlock;
  /* may be null or any statement, including an if-statement */
  public final Nullable<Statement> elsBlock;

  public ConditionalStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression guard, Statement thenBlock,
      Nullable<Statement> elsBlock) {
    super(lineNum, columnNum, AstType.CONDITIONAL_STATEMENT, preComments, postComments);
    this.guard = guard;
    this.thenBlock = thenBlock;
    this.elsBlock = elsBlock;

    guard.setParentAst(this);
    thenBlock.setParentAst(this);
    if (elsBlock.isNotNull()) {
      elsBlock.get().setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  public ConditionalStatement(int lineNum, int columnNum, Expression guard, Statement thenBlock,
      Nullable<Statement> elsBlock) {
    super(lineNum, columnNum, AstType.CONDITIONAL_STATEMENT);
    this.guard = guard;
    this.thenBlock = thenBlock;
    this.elsBlock = elsBlock;

    guard.setParentAst(this);
    thenBlock.setParentAst(this);
    if (elsBlock.isNotNull()) {
      elsBlock.get().setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  public ConditionalStatement(Expression guard, Statement thenBlock, Nullable<Statement> elsBlock) {
    super(AstType.CONDITIONAL_STATEMENT);
    this.guard = guard;
    this.thenBlock = thenBlock;
    this.elsBlock = elsBlock;

    guard.setParentAst(this);
    thenBlock.setParentAst(this);
    if (elsBlock.isNotNull()) {
      elsBlock.get().setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    String result = preToString() + "if (" + guard.toString(indent) + ") ";
    result += thenBlock.toString(indent);
    if (elsBlock.isNotNull()) {
      result += " else " + elsBlock.get().toString(indent);
    }
    return result + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    boolean hitErr = false;

    Optional<TypecheckRet> guardType = guard.typecheck(tenv, Nullable.empty());
    if (!guardType.isPresent()) {
      hitErr = true;
    } else {
      if (!tenv.typeGraph.canCastTo(guardType.get().type, CastDirection.UP, BoolType.Create())) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "guard in conditional is not a bool", lineNum, columnNum);
        hitErr = true;
      }
    }

    Nullable<Type> condRet = Nullable.empty();
    Optional<TypecheckRet> retOpt = thenBlock.typecheck(tenv.extend(), expectedReturnType);
    if (!retOpt.isPresent()) {
      hitErr = true;
    } else {
      condRet = retOpt.get().returnType;
    }

    if (elsBlock.isNotNull()) {
      retOpt = elsBlock.get().typecheck(tenv.extend(), expectedReturnType);
      if (!retOpt.isPresent()) {
        hitErr = true;
      } else {
        // check if this els block also returns
        if (retOpt.get().returnType.isNull()) {
          condRet = Nullable.empty();
        }
      }
    } else {
      /*
       * Cannot say for sure that we will exit since we don't know about the els case:
       * 
       * fun (x) { if (x) { return; } //no return here! }
       */
      condRet = Nullable.empty();
    }

    if (hitErr) {
      return Optional.empty();
    }

    return Optional.of(new TypecheckRet(condRet, determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value guardValue = guard.interp(env);
    if (guardValue.toBool()) {
      return thenBlock.interp(env);
    }
    if (elsBlock.isNotNull()) {
      return elsBlock.get().interp(env);
    }
    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.ConditionalStatement.Builder subDoc = AstProto.ConditionalStatement.newBuilder();

    subDoc.setGuard(guard.serialize().build());
    subDoc.setThenBlock(thenBlock.serialize().build());
    if (elsBlock.isNotNull()) {
      subDoc.addElsBlock(elsBlock.get().serialize().build());
    }

    document.setConditionalStatement(subDoc);
    return document;
  }

  public static ConditionalStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.ConditionalStatement document) throws SerializationError {
    Nullable<Statement> elsBlock = Nullable.empty();
    if (document.getElsBlockCount() > 0) {
      elsBlock = Nullable.of(AbstractStatement.deserialize(document.getElsBlock(0)));
    }
    return new ConditionalStatement(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getGuard()),
        AbstractStatement.deserialize(document.getThenBlock()), elsBlock);
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Nullable<Statement> newEls = elsBlock;
    if (elsBlock.isNotNull()) {
      newEls = Nullable.of(elsBlock.get().renameIds(renamings, msgState));
    }
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments, guard.renameIds(renamings, msgState),
        thenBlock.renameIds(renamings, msgState), newEls);
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = guard.findFreeVariables();
    freeVars.putAll(thenBlock.findFreeVariables());
    if (elsBlock.isNotNull()) {
      freeVars.putAll(elsBlock.get().findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof ConditionalStatement)) {
      return false;
    }
    ConditionalStatement ourOther = (ConditionalStatement) other;
    if ((ourOther.elsBlock.isNull() && elsBlock.isNotNull())
        || (ourOther.elsBlock.isNotNull() && elsBlock.isNull())) {
      return false;
    }
    if (elsBlock.isNotNull()) {
      return ourOther.elsBlock.get().compare(elsBlock.get());
    }
    return ourOther.guard.compare(guard) && ourOther.thenBlock.compare(thenBlock);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments, (Expression) guard.replace(target, with),
        (Statement) thenBlock.replace(target, with), elsBlock.isNull() ? Nullable.empty()
            : Nullable.of((Statement) elsBlock.get().replace(target, with)));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = guard.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    tmp = thenBlock.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    if (elsBlock.isNotNull()) {
      tmp = elsBlock.get().findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return null;
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> guardPair = guard.toAnf(tuning);
    insert.addAll(guardPair.a);
    Pair<List<DeclarationStatement>, Ast> thenPair = thenBlock.toAnf(tuning);
    insert.addAll(thenPair.a);

    Pair<List<DeclarationStatement>, Ast> elsPair = null;
    if (elsBlock.isNotNull()) {
      elsPair = elsBlock.get().toAnf(tuning);
      insert.addAll(elsPair.a);
    }
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments, (Expression) guardPair.b, (Statement) thenPair.b,
        elsPair == null ? Nullable.empty() : Nullable.of((Statement) elsPair.b));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments, (Expression) guard.replaceType(mapFromTo),
        (Statement) thenBlock.replaceType(mapFromTo), elsBlock.isNull() ? Nullable.empty()
            : Nullable.of((Statement) elsBlock.get().replaceType(mapFromTo)));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments, (Expression) guard.replaceVtableAccess(project, msgState).get(),
        (Statement) thenBlock.replaceVtableAccess(project, msgState).get(),
        elsBlock.isNull() ? Nullable.empty()
            : Nullable.of((Statement) elsBlock.get().replaceVtableAccess(project, msgState).get()));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments,
        guard.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        thenBlock.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get(),
        elsBlock.isNull() ? Nullable.empty()
            : Nullable.of(elsBlock.get()
                .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
                .get()));
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    guard.addDependency(modDepGraph, ourModNode, tenv);
    thenBlock.addDependency(modDepGraph, ourModNode, tenv);
    if (elsBlock.isNotNull()) {
      elsBlock.get().addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (guard.containsType(types)) {
      return true;
    }
    if (thenBlock.containsType(types)) {
      return true;
    }
    if (elsBlock.isNotNull()) {
      if (elsBlock.get().containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    if (thenBlock.containsAst(asts)) {
      return true;
    }
    if (elsBlock.isNotNull()) {
      if (elsBlock.get().containsAst(asts)) {
        return true;
      }
    }
    return guard.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize, MsgState msgState)
      throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = guard.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newGuard = (Expression) tmp.get().ast.get();

    tmp = thenBlock.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Statement newThen = (Statement) tmp.get().ast.get();

    Nullable<Statement> newEls = elsBlock;
    if (elsBlock.isNotNull()) {
      tmp = elsBlock.get().resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newEls = Nullable.of((Statement) tmp.get().ast.get());
    }

    ConditionalStatement val = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments, newGuard, newThen, newEls);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(val)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    List<Statement> ourStatements = new LinkedList<>();

    boolean mustReturn = false;
    
    InsertDestructorsRet tmp = thenBlock.insertDestructorCalls(scopedIds, destructorRenamings);
    final Statement newThenBlock = tmp.statements.remove(tmp.statements.size() - 1);
    ourStatements.addAll(tmp.statements);
    if (tmp.mustReturn) {
      mustReturn = true;
    }

    Nullable<Statement> newElsBlock = elsBlock;
    if (elsBlock.isNotNull()) {
      InsertDestructorsRet tmpEls = elsBlock.get().insertDestructorCalls(scopedIds,
          destructorRenamings);
      newElsBlock = Nullable.of(tmpEls.statements.remove(tmpEls.statements.size() - 1));
      ourStatements.addAll(tmpEls.statements);
      if (tmpEls.mustReturn && tmp.mustReturn) {
        mustReturn = true;
      }
    }

    ConditionalStatement val = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments, guard, newThenBlock, newElsBlock);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());

    ourStatements.add(val);

    return new InsertDestructorsRet(ourStatements, mustReturn);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments,  guard.liftClassDeclarations(liftedClasses, replacements).get(),
        thenBlock.liftClassDeclarations(liftedClasses, replacements).get(),
        elsBlock.isNull() ? Nullable.empty()
            : Nullable.of(elsBlock.get().liftClassDeclarations(liftedClasses, replacements).get()));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments,  guard.resolveUserTypes(msgState).get(),
        thenBlock.resolveUserTypes(msgState).get(),
        elsBlock.isNull() ? Nullable.empty()
            : Nullable.of(elsBlock.get().resolveUserTypes(msgState).get()));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    ConditionalStatement value = new ConditionalStatement(lineNum, columnNum, preComments,
        postComments,  guard.addThisExpression(classHeader),
        thenBlock.addThisExpression(classHeader),
        elsBlock.isNull() ? Nullable.empty()
            : Nullable.of(elsBlock.get().addThisExpression(classHeader)));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
