package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.ReferenceValue;
import interp.Value;
import interp.ValueBox;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.ClassTypeBox;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;
import typecheck.ArrayType;
import typecheck.FunctionType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.ReferencedType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserInstanceType;
import util.Pair;
import util.ScopedIdentifiers;

public class ReferenceExpression extends AbstractExpression {

  public final Expression referenced;

  public ReferenceExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression referenced) {
    super(lineNum, columnNum, AstType.REFERENCE_EXPRESSION, preComments, postComments);

    this.referenced = referenced;

    referenced.setParentAst(this);
  }

  public ReferenceExpression(int lineNum, int columnNum, Expression referenced) {
    super(lineNum, columnNum, AstType.REFERENCE_EXPRESSION);

    this.referenced = referenced;

    referenced.setParentAst(this);
  }

  public ReferenceExpression(Expression referenced) {
    super(AstType.REFERENCE_EXPRESSION);

    this.referenced = referenced;

    referenced.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + "&" + referenced.toString(indent) + postToString();
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    ReferenceExpression val = new ReferenceExpression(lineNum, columnNum, preComments, postComments,
        referenced.renameIds(renamings, msgState));
    val.determinedType = determinedType.renameIds(renamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    if (!(referenced instanceof IdentifierExpression) && !(referenced instanceof DotExpression)
        && !(referenced instanceof BracketExpression)) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Can only reference IdentifierExpression, " + "DotExpression, or BracketExpression, not: "
              + referenced,
          lineNum, columnNum);
    }

    Optional<TypecheckRet> tyOpt = referenced.typecheck(tenv, Nullable.empty());
    if (!tyOpt.isPresent()) {
      return Optional.empty();
    }
    Type refTy = tyOpt.get().type;

    // Can only reference arrays, maps, and class instances (not enums)
    boolean canReference = true;
    if (refTy instanceof ArrayType || refTy instanceof MapType) {
      canReference = true;
    } else if (refTy instanceof UserInstanceType) {
      Optional<UserDeclTypeBox> userTyBox = tenv.lookupUserDeclType(
          ((UserInstanceType) refTy).asDeclType(), lineNum, columnNum);
      if (!userTyBox.isPresent()) {
        return Optional.empty();
      }
      if (!(userTyBox.get() instanceof ClassTypeBox)) {
        canReference = false;
      }
    }
    if (!canReference) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot reference value of type: " + refTy, lineNum, columnNum);
      return Optional.empty();
    }

    determinedType = new ReferenceType((ReferencedType) refTy);
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    if (!(referenced instanceof IdentifierExpression) && !(referenced instanceof DotExpression)
        && !(referenced instanceof BracketExpression)) {
      throw new InterpError(
          "Can only reference IdentifierExpression, DotExpression, or BracketExpression, not: "
              + referenced,
          lineNum, columnNum);
    }

    Value ref = referenced.interp(env); // not by copy
    if (ref.getBox() == null) {
      throw new InterpError(
          "Cannot reference null memory (no memory box from this expression...rvalue)", lineNum,
          columnNum);
    }
    ValueBox refBox = ref.getBox();
    return new ReferenceValue((ReferencedType) ref.getType(), refBox);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.ReferenceExpression.Builder subDoc = AstProto.ReferenceExpression.newBuilder();

    subDoc.setReferenced(referenced.serialize());

    document.setReferenceExpression(subDoc);
    return document;
  }

  public static ReferenceExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.ReferenceExpression document) throws SerializationError {
    return new ReferenceExpression(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getReferenced()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return referenced.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof ReferenceExpression)) {
      return false;
    }
    ReferenceExpression ourOther = (ReferenceExpression) other;
    return ourOther.referenced.compare(referenced);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    ReferenceExpression value = new ReferenceExpression(lineNum, columnNum, preComments,
        postComments, (Expression) referenced.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return referenced.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> referencedPair = referenced.toAnf(tuning);
    insert.addAll(referencedPair.a);

    ReferenceExpression value = new ReferenceExpression(lineNum, columnNum, preComments,
        postComments, (Expression) referencedPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    ReferenceExpression value = new ReferenceExpression(lineNum, columnNum, preComments,
        postComments, referenced.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    ReferenceExpression value = new ReferenceExpression(lineNum, columnNum, preComments,
        postComments, (Expression) referenced.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    ReferenceExpression value = new ReferenceExpression(lineNum, columnNum, preComments,
        postComments,
        referenced.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    referenced.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return referenced.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return referenced.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    Optional<ResolveExcRet> tmp = referenced.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }

    ReferenceExpression val = new ReferenceExpression(lineNum, columnNum, preComments, postComments,
        (Expression) tmp.get().ast.get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(tmp.get().preStatements, Nullable.of(val)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    ReferenceExpression val = new ReferenceExpression(lineNum, columnNum, preComments, postComments,
        referenced.insertDestructorCalls(scopedIds, destructorRenamings));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    ReferenceExpression val = new ReferenceExpression(lineNum, columnNum, preComments, postComments,
        referenced.liftClassDeclarations(liftedClasses, replacements).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    ReferenceExpression val = new ReferenceExpression(lineNum, columnNum, preComments, postComments,
        referenced.resolveUserTypes(msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    ReferenceExpression val = new ReferenceExpression(lineNum, columnNum, preComments, postComments,
        referenced.addThisExpression(classHeader));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
