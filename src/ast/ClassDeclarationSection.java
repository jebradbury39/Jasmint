package ast;

import ast.Function.MetaType;
import ast.ParentCallExpression.IdType;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.ClassHeader.ClassMember;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class ClassDeclarationSection extends AbstractAst {
  public static enum Visibility {
    PUBLIC("public"), PROTECTED("protected"), PRIVATE("private");

    public final String visTypeName;

    private Visibility(String name) {
      this.visTypeName = name;
    }
  }

  public final Visibility visibility;
  // order does not matter here since declarations cannot use functions in the
  // class
  // but they may use previous declarations
  public final List<DeclarationStatement> declarations;
  public final List<Function> functions;
  public final List<FfiStatement> ffiBlocks;
  public final List<Function> constructors;
  public final Nullable<Function> destructor; // if more than one in a class, this is an error
  public final List<Function> overrides;
  // TODO track comments for reformatter tools

  public ClassDeclarationSection(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, String visibility, List<DeclarationStatement> declarations,
      List<Function> functions, List<FfiStatement> ffiBlocks, List<Function> constructors,
      Nullable<Function> destructor, List<Function> overrides) {
    super(lineNum, columnNum, AstType.CLASS_DECLARATION_SECTION, preComments, postComments);

    visibility = visibility.toLowerCase();
    switch (visibility) {
      case "public":
        this.visibility = Visibility.PUBLIC;
        break;
      case "protected":
        this.visibility = Visibility.PROTECTED;
        break;
      case "private":
        this.visibility = Visibility.PRIVATE;
        break;
      default:
        throw new IllegalArgumentException("invalid visibility: " + visibility);
    }

    this.declarations = declarations;
    this.functions = functions;
    this.ffiBlocks = ffiBlocks;
    this.constructors = constructors;
    this.destructor = destructor;
    this.overrides = overrides;

    for (Ast line : getSectionLines()) {
      line.setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  /*
   * Combines all line types together into a single list. TODO make this ordered
   */
  public List<Ast> getSectionLines() {
    List<Ast> sectionLines = new LinkedList<>();
    sectionLines.addAll(declarations);
    sectionLines.addAll(functions);
    sectionLines.addAll(ffiBlocks);
    sectionLines.addAll(constructors);
    if (destructor.isNotNull()) {
      sectionLines.add(destructor.get());
    }
    sectionLines.addAll(overrides);

    sectionLines.sort(new AbstractAst.SortByOrdering());

    return sectionLines;
  }

  @Override
  public String toString(String indent) {
    String result = preToString() + visibility.visTypeName + " {\n";
    String save = indent;
    indent += incIndent;

    for (Ast line : getSectionLines()) {
      result += indent + line.toString(indent) + "\n";
    }

    indent = save;
    return result + indent + "}" + postToString();
  }

  public void preGenHeader(boolean isTemplate, Set<Type> genericTypes, Set<Ast> templateFields,
      Set<Ast> isTemplateSet) {
    if (!isTemplate) {
      return;
    }

    IdentifierExpression tmpId;
    LvalueId tmpLval;
    for (DeclarationStatement decl : declarations) {
      if (decl.type.containsType(genericTypes)) {
        tmpId = new IdentifierExpression(decl.lineNum, decl.columnNum, new LinkedList<>(),
            new LinkedList<>(), decl.name);
        tmpLval = new LvalueId(decl.lineNum, decl.columnNum, new LinkedList<>(), new LinkedList<>(),
            decl.name);
        templateFields.add(tmpId); // id
        templateFields.add(tmpLval); // lvalue
      }
    }

    for (Function fn : functions) {
      if (fn.fullType.containsType(genericTypes)) {
        tmpId = new IdentifierExpression(fn.lineNum, fn.columnNum, new LinkedList<>(),
            new LinkedList<>(), fn.name);
        tmpLval = new LvalueId(fn.lineNum, fn.columnNum, new LinkedList<>(), new LinkedList<>(),
            fn.name);
        templateFields.add(tmpId); // id
        templateFields.add(tmpLval); // lvalue
      }
    }

    for (Function fn : constructors) {
      if (fn.fullType.containsType(genericTypes)) {
        tmpId = new IdentifierExpression(fn.lineNum, fn.columnNum, new LinkedList<>(),
            new LinkedList<>(), fn.name);
        tmpLval = new LvalueId(fn.lineNum, fn.columnNum, new LinkedList<>(), new LinkedList<>(),
            fn.name);
        templateFields.add(tmpId); // id
        templateFields.add(tmpLval); // lvalue
      }
    }

    for (Function fn : overrides) {
      if (fn.fullType.containsType(genericTypes)) {
        tmpId = new IdentifierExpression(fn.lineNum, fn.columnNum, new LinkedList<>(),
            new LinkedList<>(), fn.name);
        tmpLval = new LvalueId(fn.lineNum, fn.columnNum, new LinkedList<>(), new LinkedList<>(),
            fn.name);
        templateFields.add(tmpId); // id
        templateFields.add(tmpLval); // lvalue
      }
    }

    if (destructor.isNotNull()) {
      if (destructor.get().fullType.containsType(genericTypes)) {
        tmpId = new IdentifierExpression(destructor.get().lineNum, destructor.get().columnNum,
            new LinkedList<>(), new LinkedList<>(), destructor.get().name);
        tmpLval = new LvalueId(destructor.get().lineNum, destructor.get().columnNum,
            new LinkedList<>(), new LinkedList<>(), destructor.get().name);
        templateFields.add(tmpId); // id
        templateFields.add(tmpLval); // lvalue
      }
    }
  }

  public void generateHeader(ClassHeader header, boolean isTemplate, Set<Type> genericTypes,
      Set<Ast> templateFields, Set<Ast> isTemplateSet, boolean afterResolveGenerics,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    // add members to the header (normalize all types)
    // collect template fields

    // first compute templateFields so we can use containsAst later

    Nullable<Ast> tmp = Nullable.empty();
    for (DeclarationStatement decl : declarations) {
      tmp = Nullable.empty();
      if (isTemplate) {
        if (isTemplateSet.contains(decl)) {
          tmp = Nullable.of(decl);
        } else if (decl.containsType(genericTypes) || decl.containsAst(templateFields)) {
          tmp = Nullable.of(decl);
        }
      }
      boolean isInit = decl.getValue().isNotNull() || decl.required;

      header.addMember(new ClassMember(decl.lineNum, decl.columnNum, decl.label,
          true, (ClassDeclType) header.absName, header.isFfi, decl.name, decl.name,
          decl.type.basicNormalize(false, header.moduleType, effectiveImports, msgState).get().get(),
          visibility, decl.astType, decl.getIsStatic(), false, Nullable.empty(), false, isInit,
          isTemplate,
          afterResolveGenerics, tmp), msgState);
    }
    for (Function fn : functions) {
      tmp = Nullable.empty();
      if (isTemplate) {
        if (isTemplateSet.contains(fn)) {
          tmp = Nullable.of(fn);
        } else if (fn.containsType(genericTypes) || fn.containsAst(templateFields)) {
          tmp = Nullable.of(fn);
        }
      }
      if (fn.isOverride) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Function cannot be an override");
      }
      header
          .addMember(new ClassMember(fn.lineNum, fn.columnNum, fn.label,
              true, (ClassDeclType) header.absName, header.isFfi, fn.name, fn.name,
              fn.fullType.basicNormalize(false, header.moduleType, effectiveImports, msgState)
                  .get().get(),
              visibility, fn.astType, fn.getIsStatic(), false, Nullable.empty(), false, true,
              isTemplate,
              afterResolveGenerics, tmp), msgState);
    }
    for (Function fn : constructors) {
      tmp = Nullable.empty();
      if (isTemplate) {
        if (isTemplateSet.contains(fn)) {
          tmp = Nullable.of(fn);
        } else if (fn.containsType(genericTypes) || fn.containsAst(templateFields)) {
          tmp = Nullable.of(fn);
        }
      }
      if (fn.isOverride) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Function cannot be an override");
      }
      header
          .addMember(new ClassMember(fn.lineNum, fn.columnNum, fn.label,
              true, (ClassDeclType) header.absName, header.isFfi, fn.name, fn.name,
              fn.fullType.basicNormalize(false, header.moduleType, effectiveImports, msgState)
                  .get().get(),
              visibility, fn.astType, fn.getIsStatic(), false, Nullable.empty(), false, true,
              isTemplate,
              afterResolveGenerics, tmp), msgState);
    }
    for (Function fn : overrides) {
      tmp = Nullable.empty();
      if (isTemplate) {
        if (isTemplateSet.contains(fn)) {
          tmp = Nullable.of(fn);
        } else if (fn.containsType(genericTypes) || fn.containsAst(templateFields)) {
          tmp = Nullable.of(fn);
        }
      }
      // should be the only place isOverride is true
      if (!fn.isOverride) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Function must be an override");
      }
      // find the original defining class (traverse up the parent tree)
      if (header.getParent().isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "If a class contains an override method, it must have a parent class");
      }
      // lookup the header for the class (can't do it here, since nothing has been linked yet)
      
      // find this function in the class, and see if it is override or not. If it is not,
      // then this must be the original class, but if it is, then just get the original class
      
      header
          .addMember(new ClassMember(fn.lineNum, fn.columnNum, fn.label,
              true, (ClassDeclType) header.absName, false, fn.name, fn.name,
              fn.fullType.basicNormalize(false, header.moduleType, effectiveImports, msgState)
                  .get().get(),
              visibility, fn.astType, fn.getIsStatic(), true, Nullable.empty(),
              false, true, isTemplate,
              afterResolveGenerics, tmp), msgState);
    }
    if (destructor.isNotNull()) {
      if (isTemplate) {
        if (isTemplateSet.contains(destructor.get())) {
          tmp = Nullable.of(destructor.get());
        } else if (destructor.get().containsType(genericTypes)
            || destructor.get().containsAst(templateFields)) {
          tmp = Nullable.of(destructor.get());
        }
      }
      if (destructor.get().isOverride) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Function cannot be an override");
      }
      header.addMember(
          new ClassMember(destructor.get().lineNum, destructor.get().columnNum,
              destructor.get().label,
              true, (ClassDeclType) header.absName, header.isFfi, destructor.get().name,
              destructor.get().name,
              destructor.get().fullType
                  .basicNormalize(false, header.moduleType, effectiveImports, msgState).get().get(),
              visibility, destructor.get().astType, destructor.get().getIsStatic(),
              false, Nullable.empty(), false, true, isTemplate, afterResolveGenerics, tmp),
          msgState);
    }
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType) {
    // DO NOT USE
    throw new UnsupportedOperationException();
  }

  /*
   * genericTypes contains ClassType rather than GenericType, since class body
   * must parse
   */
  public Optional<Type> typecheck(TypeEnvironment classTenv, DotAccessType inClass,
      Set<Type> genericTypes) throws FatalMessageException {
    // from class header, already validated that these types exist
    // can only typecheck values if those values do not contain generics, else wait
    // until
    // we see a real usage (e.g. can't resolve T - int,
    // so wait until T is replaced by int/string/ect)
    boolean hitErr = false;
    for (DeclarationStatement decl : declarations) {
      decl.setScoping(VariableScoping.CLASS_INSTANCE);
      //if (decl.containsType(genericTypes)) {
        //continue;
      //}
      if (!decl
          .typecheckAsField(classTenv, classTenv, Nullable.empty(),
              Nullable.of(inClass), Nullable.of(visibility))
          .isPresent()) {
        hitErr = true;
      }
    }

    for (Function fn : functions) {
      fn.setScoping(VariableScoping.CLASS_INSTANCE);
      //if (fn.containsType(genericTypes)) {
        //continue;
      //}
      if (!fn.typecheckAsField(classTenv, inClass, Nullable.of(visibility), false).isPresent()) {
        hitErr = true;

      }
    }

    for (Function override : overrides) {
      override.setScoping(VariableScoping.CLASS_INSTANCE);
      //if (override.containsType(genericTypes)) {
      //  continue;
      //}
      if (!override.typecheckAsField(classTenv, inClass, Nullable.of(visibility), false)
          .isPresent()) {
        hitErr = true;
      }
    }

    // Constructors were already typechecked by ClassDeclaration.validateInitDecls()

    if (destructor.isNotNull()) {
      destructor.get().setScoping(VariableScoping.CLASS_INSTANCE);
      if (!destructor.get().containsType(genericTypes)) {
        if (!destructor.get().typecheckAsField(classTenv, inClass, Nullable.of(visibility), false)
            .isPresent()) {
          hitErr = true;
        }
      }
    }

    if (hitErr) {
      return Optional.empty();
    }

    return Optional.of(VoidType.Create());
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    // DO NOT USE
    throw new InterpError("Internal: DO NOT CALL", lineNum, columnNum);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.ClassDeclarationSection.Builder subDoc = AstProto.ClassDeclarationSection.newBuilder();

    subDoc.setVisibility(visibility.visTypeName);

    for (DeclarationStatement decl : declarations) {
      subDoc.addDeclarations(decl.serialize().build());
    }
    for (Function fn : functions) {
      subDoc.addFunctions(fn.serialize().build());
    }
    for (FfiStatement ffi : ffiBlocks) {
      subDoc.addFfiBlocks(ffi.serialize().build());
    }
    for (Function cons : constructors) {
      subDoc.addConstructors(cons.serialize().build());
    }
    if (destructor.isNotNull()) {
      subDoc.addDestructor(destructor.get().serialize());
    }
    for (Function ovr : overrides) {
      subDoc.addOverrides(ovr.serialize().build());
    }

    document.setClassDeclarationSection(subDoc);
    return document;
  }

  public static ClassDeclarationSection deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.ClassDeclarationSection document) throws SerializationError {
    List<DeclarationStatement> declarations = new LinkedList<>();
    List<Function> functions = new LinkedList<>();
    List<FfiStatement> ffiBlocks = new LinkedList<>();
    List<Function> constructors = new LinkedList<>();
    Nullable<Function> destructor = Nullable.empty();
    List<Function> overrides = new LinkedList<>();

    for (AstProto.Ast bvDecl : document.getDeclarationsList()) {
      declarations.add(DeclarationStatement.deserialize(bvDecl));
    }
    for (AstProto.Ast bvFn : document.getFunctionsList()) {
      functions.add(Function.deserialize(bvFn));
    }
    for (AstProto.Ast bvFfi : document.getFfiBlocksList()) {
      ffiBlocks.add((FfiStatement) FfiStatement.deserialize(bvFfi));
    }
    for (AstProto.Ast bvCons : document.getConstructorsList()) {
      constructors.add(Function.deserialize(bvCons));
    }
    if (document.getDestructorCount() == 1) {
      destructor = Nullable.of(Function.deserialize(document.getDestructor(0)));
    } else if (document.getDestructorCount() > 1) {
      throw new SerializationError("internal: more than one destructor", false);
    }
    for (AstProto.Ast bvOvr : document.getOverridesList()) {
      overrides.add(Function.deserialize(bvOvr));
    }

    return new ClassDeclarationSection(lineNum, columnNum, preComments, postComments,
        document.getVisibility(), declarations, functions, ffiBlocks, constructors, destructor,
        overrides);
  }

  public static ClassDeclarationSection deserialize(AstProto.Ast document)
      throws SerializationError {
    int deLineNum = document.getLineNum();
    int deColumnNum = document.getColNum();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();
    AbstractAst.preDeserialize(document, preComments, postComments);
    Type deDeterminedType = AbstractType.deserialize(document.getDeterminedType());

    ClassDeclarationSection value = deserialize(deLineNum, deColumnNum, preComments, postComments,
        document.getClassDeclarationSection());

    value.determinedType = deDeterminedType;
    return value;
  }

  /*
   * This ensures unique names and also takes care of shadowing. This is a utility
   * for the users of the ast
   */
  ClassDeclarationSection renameIds(Renamings globalRenamings, Renamings scopedRenamings,
      ClassDeclType withinClass, MsgState msgState) throws FatalMessageException {

    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    for (DeclarationStatement decl : declarations) {
      newDeclarations.add((DeclarationStatement) decl.renameIdsCustom(globalRenamings,
          Nullable.of(withinClass), msgState));
    }

    for (Function fn : functions) {
      newFunctions.add(fn.renameIds(scopedRenamings, msgState));
    }
    for (Function fn : constructors) {
      newConstructors.add(fn.renameIds(scopedRenamings, msgState));
    }
    if (destructor.isNotNull()) {
      newDestructor = Nullable.of(destructor.get().renameIds(scopedRenamings, msgState));
    }
    for (Function fn : overrides) {
      newOverrides.add(fn.renameIds(scopedRenamings, msgState));
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.toString(), newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType.renameIds(globalRenamings, msgState);
    val.setOriginalLabel(getOriginalLabel());

    return val;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof ClassDeclarationSection)) {
      return false;
    }
    ClassDeclarationSection ourOther = (ClassDeclarationSection) other;
    if (ourOther.visibility != visibility) {
      return false;
    }

    List<Ast> otherSectionLines = ourOther.getSectionLines();
    List<Ast> sectionLines = getSectionLines();

    if (otherSectionLines.size() != sectionLines.size()) {
      return false;
    }
    Iterator<Ast> iterOther = otherSectionLines.iterator();
    Iterator<Ast> iterUs = sectionLines.iterator();
    while (iterOther.hasNext() && iterUs.hasNext()) {
      if (!iterOther.next().compare(iterUs.next())) {
        return false;
      }
    }

    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    for (DeclarationStatement decl : declarations) {
      newDeclarations.add((DeclarationStatement) decl.replace(target, with));
    }
    for (Function fn : functions) {
      newFunctions.add((Function) fn.replace(target, with));
    }
    for (Function fn : constructors) {
      newConstructors.add((Function) fn.replace(target, with));
    }
    if (destructor.isNotNull()) {
      newDestructor = Nullable.of((Function) destructor.get().replace(target, with));
    }
    for (Function fn : overrides) {
      newOverrides.add((Function) fn.replace(target, with));
    }

    ClassDeclarationSection value = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (Ast line : getSectionLines()) {
      Nullable<Ast> tmp = line.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    // Collect all temps here and pass none up

    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    for (DeclarationStatement decl : declarations) {
      Pair<List<DeclarationStatement>, Ast> tmp = decl.toAnf(tuning);
      newDeclarations.addAll(tmp.a); // add tmp lines
      newDeclarations.add((DeclarationStatement) tmp.b);
    }
    for (Function fn : functions) {
      Pair<List<DeclarationStatement>, Ast> tmp = fn.toAnf(tuning);
      if (tmp.a.size() > 0) {
        throw new IllegalArgumentException("internal: too many anf");
      }
      newFunctions.add((Function) tmp.b);
    }
    for (Function fn : constructors) {
      Pair<List<DeclarationStatement>, Ast> tmp = fn.toAnf(tuning);
      if (tmp.a.size() > 0) {
        throw new IllegalArgumentException("internal: too many anf");
      }
      newConstructors.add((Function) tmp.b);
    }
    if (destructor.isNotNull()) {
      Pair<List<DeclarationStatement>, Ast> tmp = destructor.get().toAnf(tuning);
      if (tmp.a.size() > 0) {
        throw new IllegalArgumentException("internal: too many anf");
      }
      newDestructor = Nullable.of((Function) tmp.b);
    }
    for (Function fn : overrides) {
      Pair<List<DeclarationStatement>, Ast> tmp = fn.toAnf(tuning);
      if (tmp.a.size() > 0) {
        throw new IllegalArgumentException("internal: too many anf");
      }
      newOverrides.add((Function) tmp.b);
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), val);
  }

  public ClassDeclarationSection replaceType(Map<Type, Type> mapFromTo,
      ClassDeclType newClassDecl) {
    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    for (DeclarationStatement decl : declarations) {
      newDeclarations.add((DeclarationStatement) decl.replaceType(mapFromTo));
    }
    for (Function fn : functions) {
      newFunctions.add((Function) fn.replaceType(mapFromTo, newClassDecl));
    }
    for (Function fn : constructors) {
      newConstructors.add((Function) fn.replaceType(mapFromTo, newClassDecl));
    }
    if (destructor.isNotNull()) {
      newDestructor = Nullable.of((Function) destructor.get().replaceType(mapFromTo, newClassDecl));
    }
    for (Function fn : overrides) {
      newOverrides.add((Function) fn.replaceType(mapFromTo, newClassDecl));
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  /**
   * Look at declarations, remove values, move to assignments in top of
   * constructors (this.id = initValue)
   * 
   * @return
   * @throws FatalMessageException
   */
  public ClassDeclarationSection moveInitsToConstructors(List<AssignmentStatement> newInits,
      MsgState msgState) throws FatalMessageException {
    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();

    for (DeclarationStatement decl : declarations) {
      if (decl.getIsStatic()) {
        newDeclarations.add(decl);
      } else {
        // remove init value from these
        newDeclarations.add(decl.removeValue());
      }
    }
    for (Function fn : constructors) {
      // insert declarations at top
      if (fn.getBody() == null) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "Cannot move inits " + newInits + " to required constructor which has not been linked",
            lineNum, columnNum);
      }
      newConstructors.add(fn.prependInitStatements(newInits));
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, functions, ffiBlocks,
        newConstructors, destructor, overrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  public static ClassDeclarationSection createDefaultConstructor(ClassDeclType within,
      Nullable<ClassType> parent, boolean foundDefaultConstructor, boolean foundDestructor,
      List<AssignmentStatement> newInits) {
    List<Function> newConstructors = new LinkedList<>();

    if (!foundDefaultConstructor) {
      Nullable<FunctionCallExpression> parentCall = Nullable.empty();
      if (parent.isNotNull()) {
        FunctionCallExpression fnCallExpr = new FunctionCallExpression(Nullable.empty(),
            new ParentCallExpression(parent, IdType.CONSTRUCTOR), new LinkedList<>(), "parent");
        parentCall = Nullable.of(fnCallExpr);
      }
      BlockStatement fnBody = new BlockStatement(new LinkedList<>());
      Function fn = new Function(true, false, false, "constructor", within,
          new FunctionType(within, VoidType.Create(), new LinkedList<>(), LambdaStatus.NOT_LAMBDA),
          new LinkedList<>(), Nullable.of(fnBody), false, parentCall, MetaType.CONSTRUCTOR);
      newConstructors.add(fn.prependInitStatements(newInits));
    }

    Nullable<Function> newDestructor = Nullable.empty();

    if (!foundDestructor) {
      Nullable<FunctionCallExpression> parentCall = Nullable.empty();
      if (parent.isNotNull()) {
        FunctionCallExpression fnCallExpr = new FunctionCallExpression(Nullable.empty(),
            new ParentCallExpression(parent, IdType.DESTRUCTOR), new LinkedList<>(), "parent");
        parentCall = Nullable.of(fnCallExpr);
      }
      BlockStatement fnBody = new BlockStatement(new LinkedList<>());
      Function fn = new Function(true, false, false, "destructor", within,
          new FunctionType(within, VoidType.Create(), new LinkedList<>(), LambdaStatus.NOT_LAMBDA),
          new LinkedList<>(), Nullable.of(fnBody), false, parentCall, MetaType.DESTRUCTOR);
      newDestructor = Nullable.of(fn);
    }

    ClassDeclarationSection val = new ClassDeclarationSection(-1, -1, new LinkedList<>(),
        new LinkedList<>(), Visibility.PUBLIC.visTypeName, new LinkedList<>(), new LinkedList<>(),
        new LinkedList<>(), newConstructors, newDestructor, new LinkedList<>());
    val.determinedType = VoidType.Create();
    val.setOriginalLabel(val.label);
    return val;
  }

  public ClassDeclarationSection filterOnlyStatics(boolean noStatics) throws FatalMessageException {
    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newOverrides = new LinkedList<>();

    for (DeclarationStatement decl : declarations) {
      if (noStatics == decl.getIsStatic()) {
        continue;
      }
      newDeclarations.add(decl);
    }
    for (Function fn : functions) {
      if (noStatics == fn.getIsStatic()) {
        continue;
      }
      newFunctions.add(fn);
    }
    for (Function fn : overrides) {
      if (noStatics == fn.getIsStatic()) {
        continue;
      }
      newOverrides.add(fn);
    }

    List<Function> newConstructors = new LinkedList<>(); // empty
    if (noStatics) {
      newConstructors = constructors;
    }
    Nullable<Function> newDestructor = Nullable.empty(); // empty
    if (noStatics) {
      newDestructor = destructor;
    }
    List<FfiStatement> newFfiBlocks = new LinkedList<>(); // empty
    if (noStatics) {
      newFfiBlocks = ffiBlocks;
    }
    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, newFfiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    Optional<Ast> tmp = Optional.empty();
    boolean hitError = false;
    for (DeclarationStatement decl : declarations) {
      tmp = decl.replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newDeclarations.add((DeclarationStatement) tmp.get());
    }
    for (Function fn : functions) {
      tmp = fn.replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newFunctions.add((Function) tmp.get());
    }
    for (Function fn : constructors) {
      tmp = fn.replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newConstructors.add((Function) tmp.get());
    }
    if (destructor.isNotNull()) {
      tmp = destructor.get().replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
      } else {
        newDestructor = Nullable.of((Function) tmp.get());
      }
    }
    for (Function fn : overrides) {
      tmp = fn.replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newOverrides.add((Function) tmp.get());
    }

    if (hitError) {
      return Optional.empty();
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  public Optional<ClassDeclarationSection> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    boolean hitError = false;
    for (DeclarationStatement decl : declarations) {
      Optional<Statement> tmp = decl.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newDeclarations.add((DeclarationStatement) tmp.get());
    }
    for (Function fn : functions) {
      Optional<Statement> tmp = fn.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newFunctions.add((Function) tmp.get());
    }
    for (Function fn : constructors) {
      Optional<Statement> tmp = fn.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newConstructors.add((Function) tmp.get());
    }
    if (destructor.isNotNull()) {
      Optional<Statement> tmp = destructor.get().normalizeType(keepCurrentModRelative,
          currentModule, effectiveImports, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
      } else {
        newDestructor = Nullable.of((Function) tmp.get());
      }
    }
    for (Function fn : overrides) {
      Optional<Statement> tmp = fn.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newOverrides.add((Function) tmp.get());
    }

    if (hitError) {
      return Optional.empty();
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  // Only a function from a provider module calls this
  public boolean linkRequiredProvidedFunctions(boolean withinExportedClass, Program program,
      ClassDeclaration reqClassAst, List<FfiStatement> newFfiLines, MsgState msgState)
      throws FatalMessageException {

    boolean hitErr = false;
    // iterate through ffi members, functions, and inner classes
    for (Ast line : getSectionLines()) {
      if (line instanceof FfiStatement) {
        newFfiLines.add((FfiStatement) line);
      } else if (line instanceof Function) {
        Nullable<Function> reqFn = ((Function) line).linkRequiredProvidedFunctions(program,
            msgState);
        if (reqFn.isNull()) {
          hitErr = true;
        }
        // no need to add, since not allowed (new ffi lines go into another section)
      } else if (line instanceof DeclarationStatement) {
        if (!((DeclarationStatement) line).linkRequiredStaticFields(this, reqClassAst, msgState)) {
          hitErr = true;
        }
      }
    }

    return !hitErr;
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    for (Ast line : getSectionLines()) {
      line.addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    throw new IllegalArgumentException("No need to call this function!");
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (DeclarationStatement decl : declarations) {
      if (decl.containsAst(asts)) {
        return true;
      }
    }
    for (Function fn : functions) {
      if (fn.containsAst(asts)) {
        return true;
      }
    }
    for (FfiStatement ffi : ffiBlocks) {
      if (ffi.containsAst(asts)) {
        return true;
      }
    }
    for (Function fn : constructors) {
      if (fn.containsAst(asts)) {
        return true;
      }
    }
    if (destructor.isNotNull()) {
      if (destructor.get().containsAst(asts)) {
        return true;
      }
    }
    for (Function fn : overrides) {
      if (fn.containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  public Nullable<DeclarationStatement> lookupDeclaration(String name) {
    for (DeclarationStatement decl : declarations) {
      if (decl.name.equals(name)) {
        return Nullable.of(decl);
      }
    }
    return Nullable.empty();
  }

  @Override
  public Optional<Set<ModuleType>> findExportedTypes(ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Set<ModuleType> exportedTypes = new HashSet<>();

    if (visibility == Visibility.PRIVATE) {
      // subclasses cannot access, nor can direct access, so no types in here are
      // exported
      return Optional.of(exportedTypes);
    }

    for (DeclarationStatement decl : declarations) {
      if (!decl.type.getNormalizedModType(currentModule, effectiveImports, msgState, exportedTypes,
          false)) {
        return Optional.empty();
      }
    }

    List<Function> effectiveFuns = new LinkedList<>(functions);
    effectiveFuns.addAll(constructors);
    effectiveFuns.addAll(overrides);

    for (Function fn : effectiveFuns) {
      Optional<Set<ModuleType>> optSet = fn.findExportedTypes(currentModule, effectiveImports,
          msgState);
      if (!optSet.isPresent()) {
        return Optional.empty();
      }
      exportedTypes.addAll(optSet.get());
    }

    return Optional.of(exportedTypes);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    boolean hitError = false;
    for (DeclarationStatement decl : declarations) {
      Optional<ResolveExcRet> tmp = decl.resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, false, enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newDeclarations.add((DeclarationStatement) tmp.get().ast.get());
      /*
       * we can discard the prestatements (memcheck), since the decl size is already
       * accounted for
       */
    }
    for (Function fn : functions) {
      Optional<ResolveExcRet> tmp = fn.resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, false, enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      for (Statement statement : tmp.get().preStatements) {
        Function copyFn = (Function) statement;
        newFunctions.add(copyFn);
      }
      newFunctions.add((Function) tmp.get().ast.get());
    }
    for (Function fn : constructors) {
      Optional<ResolveExcRet> tmp = fn.resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, false, enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      for (Statement statement : tmp.get().preStatements) {
        newConstructors.add((Function) statement);
      }
      newConstructors.add((Function) tmp.get().ast.get());
    }
    if (destructor.isNotNull()) {
      Optional<ResolveExcRet> tmp = destructor.get().resolveExceptions(project, currentModule,
          auditEntry, jsmntGlobalRenamings, withinFn, false, enableResolution, hitExcId, stackSize,
          msgState);
      if (!tmp.isPresent()) {
        hitError = true;
      } else {
        for (Statement statement : tmp.get().preStatements) {
          newFunctions.add((Function) statement);
        }
        newDestructor = Nullable.of((Function) tmp.get().ast.get());
      }
    }
    for (Function fn : overrides) {
      Optional<ResolveExcRet> tmp = fn.resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, false, enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        hitError = true;
        continue;
      }
      newOverrides.add((Function) tmp.get().ast.get());
    }

    if (hitError) {
      return Optional.empty();
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());

    return Optional.of(new ResolveExcRet(new LinkedList<>(), Nullable.of(val)));
  }

  public ClassDeclarationSection insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings, Nullable<ClassType> parentType)
      throws FatalMessageException {
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    ScopedIdentifiers ourScope = scopedIds.extend(ScopeFilter.BLOCK);

    for (Function fn : functions) {
      newFunctions.add(fn.insertDestructorCalls(ourScope, destructorRenamings, parentType));
    }
    for (Function fn : constructors) {
      newConstructors.add(fn.insertDestructorCalls(ourScope, destructorRenamings, parentType));
    }
    if (destructor.isNotNull()) {
      newDestructor = Nullable
          .of(destructor.get().insertDestructorCalls(ourScope, destructorRenamings, parentType));
    }
    for (Function fn : overrides) {
      newOverrides.add(fn.insertDestructorCalls(ourScope, destructorRenamings, parentType));
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, declarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  public ClassDeclarationSection liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    boolean hitError = false;
    for (DeclarationStatement decl : declarations) {
      newDeclarations.add((DeclarationStatement) decl.liftClassDeclarations(liftedClasses,
          replacements).get());
    }
    for (Function fn : functions) {
      newFunctions.add((Function) fn.liftClassDeclarations(liftedClasses, replacements).get());
    }
    for (Function fn : constructors) {
      newConstructors.add((Function) fn.liftClassDeclarations(liftedClasses, replacements).get());
    }
    if (destructor.isNotNull()) {
      newDestructor = Nullable.of((Function) destructor.get().liftClassDeclarations(liftedClasses,
          replacements).get());
    }
    for (Function fn : overrides) {
      newOverrides.add((Function) fn.liftClassDeclarations(liftedClasses, replacements).get());
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  public Optional<ClassDeclarationSection> resolveUserTypes(MsgState msgState)
      throws FatalMessageException {
    
    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    boolean hitError = false;
    for (DeclarationStatement decl : declarations) {
      newDeclarations.add((DeclarationStatement) decl.resolveUserTypes(msgState).get());
    }
    for (Function fn : functions) {
      newFunctions.add((Function) fn.resolveUserTypes(msgState).get());
    }
    for (Function fn : constructors) {
      newConstructors.add((Function) fn.resolveUserTypes(msgState).get());
    }
    if (destructor.isNotNull()) {
      newDestructor = Nullable.of((Function) destructor.get().resolveUserTypes(msgState).get());
    }
    for (Function fn : overrides) {
      newOverrides.add((Function) fn.resolveUserTypes(msgState).get());
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  public ClassDeclarationSection addThisExpression(ClassHeader classHeader) {
    List<DeclarationStatement> newDeclarations = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<Function> newConstructors = new LinkedList<>();
    Nullable<Function> newDestructor = Nullable.empty();
    List<Function> newOverrides = new LinkedList<>();

    for (DeclarationStatement decl : declarations) {
      newDeclarations.add((DeclarationStatement) decl.addThisExpression(classHeader));
    }
    for (Function fn : functions) {
      newFunctions.add((Function) fn.addThisExpression(classHeader));
    }
    for (Function fn : constructors) {
      newConstructors.add((Function) fn.addThisExpression(classHeader));
    }
    if (destructor.isNotNull()) {
      newDestructor = Nullable.of((Function) destructor.get().addThisExpression(classHeader));
    }
    for (Function fn : overrides) {
      newOverrides.add((Function) fn.addThisExpression(classHeader));
    }

    ClassDeclarationSection val = new ClassDeclarationSection(lineNum, columnNum, preComments,
        postComments, visibility.visTypeName, newDeclarations, newFunctions, ffiBlocks,
        newConstructors, newDestructor, newOverrides);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
