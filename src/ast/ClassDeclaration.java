package ast;

import ast.ClassDeclarationSection.Visibility;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditRenameTypes;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.ClassHeader.ClassMember;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.UserTypeDeclarationValue;
import interp.Value;
import interp.ValueBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.BasicTypeBox;
import type_env.ClassTypeBox;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.ScopeType;
import type_env.TypeEnvironment.TypeStatus;
import type_env.UserDeclTypeBox;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.AnyType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.FunctionType;
import typecheck.GenericType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import util.GenericTree;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class ClassDeclaration extends AbstractStatement implements Declaration {
  public final boolean export;
  public final boolean isFfi; // if true, do not replace this class type, nor its field names
  public final boolean isStatic; // if true, may not be instantiated
  public final ModuleType moduleType; // abs path
  public final String name;
  public final ClassDeclType originalName; // full outer as well since we might have been lifted
  /*
   * may be empty. Must never contain a class name here, e.g. class A<A> is not
   * allowed, nor class A, class B<A> (sibling classes and imported classes, class
   * B<A> is fine if a.A)
   */
  public final List<String> genericTypeNames;
  public final Nullable<ClassType> parent; /* what we extend. May be null */
  public final List<ClassDeclarationSection> sections;

  public final List<ClassLine> lines; /* computed */

  public final ClassType fullType; /* computed, does contain module path */
  public final ClassDeclType fullDeclType; /* computed, does contain module path */
  public final List<Type> typedInnerTypes; // GenericType only
  
  /* only determined by typechecker (set before typecheck() call) */
  private VariableScoping scoping = VariableScoping.UNKNOWN;
  private Nullable<ClassType> determinedParent = Nullable.empty();

  public ClassDeclaration(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, boolean export, boolean isFfi, boolean isStatic,
      ModuleType moduleType,
      String name, ClassDeclType originalName, List<String> genericTypeNames,
      Nullable<ClassType> parent, List<ClassDeclarationSection> sections) {
    super(lineNum, columnNum, AstType.CLASS_DECLARATION, preComments, postComments);
    this.export = export;
    this.isFfi = isFfi;
    this.isStatic = isStatic;
    this.moduleType = moduleType;
    this.name = name;
    this.originalName = originalName;
    this.genericTypeNames = genericTypeNames;
    if (!parent.instanceOf(UserInstanceType.class) && parent.isNotNull()) {
      throw new IllegalArgumentException("cannot extend non-user type: " + parent);
    }
    if (parent.isNotNull()) {
      this.parent = Nullable.of((ClassType) parent.get());
    } else {
      this.parent = Nullable.empty();
    }

    List<ClassLine> tmpLines = new ArrayList<ClassLine>();
    for (ClassDeclarationSection section : sections) {
      for (Ast line : section.getSectionLines()) {
        tmpLines.add(new ClassLine(section.visibility, line));
      }
      section.setParentAst(this);
    }
    this.sections = sections;
    lines = tmpLines;

    List<Type> innerTypes = new ArrayList<Type>();
    typedInnerTypes = new LinkedList<>();
    for (String gtName : genericTypeNames) {
      GenericType gte = new GenericType(gtName);
      typedInnerTypes.add(gte);
      innerTypes.add(gte);
    }
    this.fullType = new ClassType(Nullable.of(moduleType), name, innerTypes);
    this.fullDeclType = new ClassDeclType(Nullable.of(moduleType), name);

    determinedType = VoidType.Create();
  }
  
  @Override
  public String toString(String indent) {
    String result = preToString() + "class " + name;
    if (!genericTypeNames.isEmpty()) {
      result += "<";
      boolean first = true;
      for (String gn : genericTypeNames) {
        if (!first) {
          result += ", ";
        }
        first = false;
        result += gn;
      }
      result += ">";
    }
    if (parent.isNotNull()) {
      result += " extends " + parent;
    }
    result += " {\n";
    String save = indent;
    indent += incIndent;

    for (ClassDeclarationSection section : sections) {
      result += indent + section.toString(indent) + "\n";
    }

    indent = save;
    return result + indent + "}" + postToString();
  }

  /*
   * Generate the API entry for our psuedo-header definingModule must be abs
   * module path
   * 
   * After this, we link the headers to with their parent headers
   */
  public Optional<ClassHeader> generateHeader(List<ImportStatement> imports,
      Set<String> siblingClasses, ModuleType definingModule, boolean afterResolveGenerics,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    // parent is relative now, but we can determine the absolute path from
    // imports/sibling classes
    Nullable<ClassType> absParent = parent;
    boolean found = false;
    if (parent.isNotNull()) {
      ClassType tmpParent = parent.get();
      if (tmpParent.outerType.isNull() || tmpParent.outerType.equals(Nullable.of(definingModule))) {
        // then could be a sibling, so check
        if (siblingClasses.contains(tmpParent.name)) {
          absParent = Nullable
              .of(new ClassType(Nullable.of(definingModule), tmpParent.name, tmpParent.innerTypes));
          found = true;
        }
      }

      if (!found) {
        // not a sibling, so check the imports (importType.endswith(parentType))
        /*
         * parent = A | a.A import a.A
         * 
         * parent = A | a.A | std.a.A import std.a import std.a.A
         */
        List<String> parentAsList = tmpParent.toList();

        for (ImportStatement imp : imports) {
          int lastMatch = parentAsList.size() - 1;
          List<String> impAsList = imp.target.toList();
          if (impAsList.get(impAsList.size() - 1)
              .equals(parentAsList.get(parentAsList.size() - 1))) {
            // the last elements match (and we know the last and only the last
            // element of parent MUST be a classname)
            lastMatch += 1;
          }
          // if both last items match, then compare the full substr
          // otherwise, we might have (import foo.std.a, extend a.A) situation, so minus
          // one substr
          if (impAsList.subList(impAsList.size() - lastMatch, impAsList.size())
              .equals(parentAsList.subList(0, lastMatch))) {
            // the parent is from this module. If last elements matched, then trim that
            ModuleType impOnlyModule = null;
            if (lastMatch == parentAsList.size()) {
              impOnlyModule = imp.target.outerType.get();
            } else {
              impOnlyModule = new ModuleType(imp.target.outerType, imp.target.name);
            }
            absParent = Nullable.of(
                new ClassType(Nullable.of(impOnlyModule), tmpParent.name, tmpParent.innerTypes));
            found = true;
            break;
          }

        }
      }

      if (!found) {
        // failed to find parent among siblings or imports. This is likely a user error
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Failed to find parent class '" + parent + "' of class '" + name + "' in module '"
                + definingModule + "'. Siblings: " + siblingClasses
                + " did not have it, nor did any imports.",
            lineNum, columnNum);
        return Optional.empty();
      }
    }

    ClassHeader header = new ClassHeader(lineNum, columnNum, label,
        definingModule, name, originalName, absParent,
        typedInnerTypes, export, isFfi, isStatic);
    header.isLinked = false;

    // add our members
    Set<Ast> templateFields = new HashSet<>(); // id/lvalue
    Set<Ast> isTemplateSet = new HashSet<>();
    for (ClassDeclarationSection section : sections) {
      section.preGenHeader(!typedInnerTypes.isEmpty(), new HashSet<>(typedInnerTypes),
          templateFields, isTemplateSet);
    }
    for (ClassDeclarationSection section : sections) {
      section.generateHeader(header, !typedInnerTypes.isEmpty(), new HashSet<>(typedInnerTypes),
          templateFields, isTemplateSet, afterResolveGenerics, effectiveImports, msgState);
    }
    
    // determine all the required types for this class
    header.determineRequiredTypes();

    return Optional.of(header);
  }

  // if noPrefixAsWell, then add std.String.String and just String.
  // Otherwise only add abs path of class
  public void populateTypeGraph(ModuleType ourModuleType, Nullable<UserInstanceType> shortImportModType,
      TypeEnvironment tenv, boolean noPrefixAsWell) throws FatalMessageException {

  }

  // final pass, does not actually use tenv
  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment globalTenv,
      Nullable<Type> expectedReturnType) throws FatalMessageException {
    throw new UnsupportedOperationException("Use other typecheck");
  }

  private Optional<Pair<ClassHeader, TypeEnvironment>> addGenericTypesToTenv(
      TypeEnvironment globalTenv) throws FatalMessageException {

    // find our class header
    Optional<UserDeclTypeBox> userTyBox = globalTenv.lookupUserDeclType(fullDeclType,
        lineNum, columnNum);
    if (!userTyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(userTyBox.get() instanceof ClassTypeBox)) {
      globalTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "not a class",
          lineNum, columnNum);
      return Optional.empty();
    }
    ClassTypeBox classTyBox = (ClassTypeBox) userTyBox.get();
    
    // class header must already have transforms applied
    return addGenericTypesToTenv(globalTenv, classTyBox.classHeader);
  }
  
  private Optional<Pair<ClassHeader, TypeEnvironment>> addGenericTypesToTenv(
      TypeEnvironment tenv, ClassHeader classHeader) throws FatalMessageException {

    // ensure that none of our generic types clash with class types
    // extend the tenv with these generics
    TypeEnvironment classTenv = tenv.extend(fullDeclType);

    boolean hitErr = false;
    for (Type innerTy : classHeader.innerTypes) {
      GenericType genericType = (GenericType) innerTy;
      Optional<TypeStatus> tyStatusOpt = tenv.lookupType(genericType, lineNum, columnNum);
      if (tyStatusOpt.isPresent()) {
        if (tyStatusOpt.get() != TypeStatus.UNDEFINED && tyStatusOpt.get() != TypeStatus.GENERIC) {
          tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "Generic '"
              + genericType + "' for class '" + classHeader.absName + "' shadows an existing class",
              lineNum, columnNum);
          continue;
        }
        classTenv.defineVar(genericType.name, new BasicTypeBox(genericType, true, true,
            VariableScoping.GENERIC));
      }
    }
    if (hitErr) {
      return Optional.empty();
    }
    return Optional.of(new Pair<>(classHeader, classTenv));
  }

  public Optional<TypeEnvironment> typecheckDeclare(TypeEnvironment globalTenv)
      throws FatalMessageException {

    Optional<Pair<ClassHeader, TypeEnvironment>> headerTenv = addGenericTypesToTenv(globalTenv);
    if (!headerTenv.isPresent()) {
      return Optional.empty();
    }
    ClassHeader classHeader = headerTenv.get().a;
    TypeEnvironment classTenv = headerTenv.get().b;
    
    if (parent.isNotNull()) {
      determinedParent = Nullable.of((ClassType) parent.get().normalize(classTenv, lineNum, columnNum).get());
    }

    // add 'this'
    classTenv.defineVar("this", new BasicTypeBox(new ReferenceType(fullType),
        true, true, VariableScoping.CLASS_INSTANCE, fullDeclType, Visibility.PRIVATE, false));

    // verify types for all fields that originate from this class (abs path)
    // fields from parent classes/modules may rely on imports that we don't have,
    // but can still see
    // via the project
    if (!classHeader.typecheck(fullDeclType, classTenv, lineNum, columnNum)) {
      return Optional.empty();
    }
    

    // once the class header has been typechecked once, we can easily convert it to
    // a type env
    // at any time
    classHeader.isTypechecked = true;
    // The class is now complete (we know the full size)
    globalTenv.project.addCompletedClass(new ClassDeclType(fullType.outerType, fullType.name));

    // pull in all constructors and ensure that every one inits all non-init decls
    if (!validateInitDecls(classHeader, classTenv)) {
      return Optional.empty();
    }

    return Optional.of(classTenv);
  }

  // this typechecks all the constructors
  private boolean validateInitDecls(ClassHeader classHeader, TypeEnvironment classTenv)
      throws FatalMessageException {
    // gather all non-init decls with this class of origin
    Set<String> nonInitDecls = new HashSet<>();
    for (ClassMember member : classHeader.getMemberList()) {
      if (!member.isInit && member.classOfOrigin.equals(classHeader.absName)) {
        if (nonInitDecls.contains(member.id)) {
          classTenv.msgState.addMessage(
              MsgType.ERROR, MsgClass.TYPECHECK, "Found non-init member '" + member.id
                  + "' of type '" + member.getType() + "' in class '" + classHeader.absName
                  + "' twice",
              lineNum, columnNum);
          return false;
        }
        nonInitDecls.add(member.id);
      }
    }

    // typecheck each constructor
    Nullable<TypeBox> tyBox = Nullable.empty();
    boolean hitErr = false;
    for (ClassDeclarationSection section : sections) {
      for (Function constructor : section.constructors) {
        constructor.setScoping(VariableScoping.CLASS_INSTANCE);
        if (constructor.containsType(new HashSet<>(typedInnerTypes))) {
          //continue;
        }
        constructor.typecheckAsField(classTenv, fullType, Nullable.of(section.visibility),
            false);

        // verify that all non-init are now init, then reset if this is not the last
        // constructor
        for (String id : nonInitDecls) {
          tyBox = classTenv.lookup(id, lineNum, columnNum);
          if (tyBox.isNull()) {
            classTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
                "Failed to find " + id + " which we know exists in class " + classHeader.absName,
                lineNum, columnNum);
            return false;
          }

          if (!tyBox.get().getIsInit()) {
            if (!classTenv.project.lookupClassMemberIsInit(classHeader.absName, id)) {
              classTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
                  "Constructor in class " + classHeader.absName + " failed to init '" + id + "'",
                  lineNum, columnNum);
              hitErr = true;
            }
          }

          // so reset
          tyBox.get().setIsInit(false);
          classTenv.project.removeInitClassMember(classHeader.absName, id);
        }

      }
    }

    if (!hitErr) {
      // set all to init
      for (String id : nonInitDecls) {
        tyBox = classTenv.lookup(id, lineNum, columnNum);
        if (tyBox.isNull()) {
          classTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
              "Failed to find " + id + " which we know exists in class " + classHeader.absName,
              lineNum, columnNum);
          return false;
        }
        tyBox.get().setIsInit(true);
        classTenv.project.addInitClassMember(new ClassDeclType(fullType.outerType, fullType.name),
            id);
      }
    }

    return !hitErr;
  }

  public Optional<Type> typecheck(TypeEnvironment classTenv)
      throws FatalMessageException {
    boolean hitErr = false;
    
    if (isFfi && !genericTypeNames.isEmpty()) {
      classTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "ffi class " + fullType + " cannot have generics", lineNum, columnNum);
      hitErr = true;
    }

    // now typecheck the bodies/values of our fields
    for (ClassDeclarationSection section : sections) {
      if (!section.typecheck(classTenv, new ClassDeclType(fullType.outerType, fullType.name),
          new HashSet<>(fullType.innerTypes)).isPresent()) {
        hitErr = true;
      }
    }
    if (hitErr) {
      return Optional.empty();
    }

    return Optional.of(determinedType);
  }

  // put the class declaration in the environment
  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    return interp(env, env);
  }

  public Value interp(Environment outerClassEnv, Environment globalEnv)
      throws InterpError, FatalMessageException {
    // GLOBAL -> STATIC
    Environment staticUserEnv = new Environment(ScopeType.STATIC, fullType, globalEnv.typeGraph,
        globalEnv.project);
    staticUserEnv.setParent(globalEnv);

    UserTypeDeclarationValue parentDecl = null;

    if (parent.isNotNull()) {
      ValueBox parentBox = globalEnv.lookupNestedClassname(parent.get());
      if (parentBox == null) {
        throw new InterpError("parent [" + parent + "] declaration of " + name + " does not exist",
            lineNum, columnNum);
      }
      if (parentBox.getValue() == null) {
        throw new InterpError(
            "parent [" + parent + "] declaration of " + name + " is not initialized", lineNum,
            columnNum);
      }
      if (!(parentBox.getValue() instanceof UserTypeDeclarationValue)) {
        throw new InterpError(
            "parent [" + parent + "] declaration of " + name + " is not a class declaration value",
            lineNum, columnNum);
      }
      // not by copy since linked (need statics, functions)
      parentDecl = (UserTypeDeclarationValue) parentBox.getValue(); 

      // replace global link with parent: GLOBAL -> STATIC (A) -> STATIC (B)
      staticUserEnv.setParent(parentDecl.getStaticEnv(true)); // preserve global access
    }

    return null;
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.ClassDeclaration.Builder subDoc = AstProto.ClassDeclaration.newBuilder();

    subDoc.setModuleType(moduleType.serialize());
    subDoc.setName(name);
    subDoc.setOriginalName(originalName.serialize());

    for (String gname : genericTypeNames) {
      subDoc.addGenericTypeNames(gname);
    }

    if (parent.isNotNull()) {
      subDoc.addParent(parent.get().serialize().build());
    }

    for (ClassDeclarationSection cds : sections) {
      subDoc.addSections(cds.serialize().build());
    }

    subDoc.setIsFfi(isFfi);
    subDoc.setIsStatic(isStatic);
    subDoc.setExport(export);

    AstProto.Ast.Builder document = preSerialize();
    document.setClassDeclaration(subDoc);
    return document;
  }

  public static ClassDeclaration deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments, long ordering,
      AstProto.ClassDeclaration document) throws SerializationError {

    List<String> genericTypeNames = new LinkedList<String>();
    for (String bvName : document.getGenericTypeNamesList()) {
      genericTypeNames.add(bvName);
    }

    Nullable<ClassType> parent = Nullable.empty();
    if (document.getParentCount() > 0) {
      parent = Nullable.of((ClassType) AbstractType.deserialize(document.getParent(0)));
    }

    List<ClassDeclarationSection> sections = new LinkedList<ClassDeclarationSection>();
    for (AstProto.Ast bvSection : document.getSectionsList()) {
      sections.add(ClassDeclarationSection.deserialize(bvSection));
    }

    ClassDeclaration cdecl = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        document.getExport(), document.getIsFfi(), document.getIsStatic(),
        (ModuleType) AbstractType.deserialize(document.getModuleType()), document.getName(),
        (ClassDeclType) AbstractType.deserialize(document.getOriginalName()), genericTypeNames,
        parent, sections);
    cdecl.ordering = ordering;
    return cdecl;
  }

  public static ClassDeclaration deserialize(AstProto.Ast document) throws SerializationError {
    int deLineNum = document.getLineNum();
    int deColumnNum = document.getColNum();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();
    Pair<AstType, Long> tmp = AbstractAst.preDeserialize(document, preComments, postComments);
    long ordering = tmp.b;

    Type deDeterminedType = AbstractType.deserialize(document.getDeterminedType());

    ClassDeclaration value = deserialize(deLineNum, deColumnNum, preComments, postComments,
        ordering, document.getClassDeclaration());
    value.determinedType = deDeterminedType;

    return value;
  }

  public class ClassLine {
    public final Visibility visibility;
    public final Ast ast;

    public ClassLine(Visibility visibility, Ast ast) {
      this.visibility = visibility;
      this.ast = ast;

      if (!(ast instanceof DeclarationStatement) && !(ast instanceof Function)
          && !(ast instanceof ClassDeclaration) && !(ast instanceof FfiStatement)) {
        throw new IllegalArgumentException(
            "ClassLine ast must be either declaration or function, not: " + ast);
      }
    }

    @Override
    public String toString() {
      return visibility.visTypeName + " " + ast;
    }
  }

  public void populateRenamings(GenericTree<String, String> ourOuterRenamings, MsgState msgState) {
    // name would be newName, but we don't renaming classes here (only in lifting)
    GenericTree<String, String> ourRenamings = ourOuterRenamings.extend();
    ourRenamings.value = Nullable.of(name);
    ourOuterRenamings.putEdge(name, ourRenamings);
    // className -> oldName -> newName
    // in case of nested: outer -> inner -> oldName -> newName

    // add in our generics
    for (Type innerTy : typedInnerTypes) {
      GenericType gte = (GenericType) innerTy;
      String newG = "G_" + gte.name + "_" + strLbl();
      GenericTree<String, String> gt = new GenericTree<String, String>(Nullable.of(newG),
          ourOuterRenamings.currentModuleFullType);
      gt.valueIsGeneric = true;
      ourRenamings.putEdge(gte.name, gt);
    }

    for (ClassLine line : lines) {
      if (line.ast instanceof DeclarationStatement) {
        String oldName = ((DeclarationStatement) line.ast).name;
        String newId = "u_" + oldName + "_" + strLbl();
        ourRenamings.putEdge(oldName,
            new GenericTree<>(Nullable.of(newId), ourOuterRenamings.currentModuleFullType));
      } else if (line.ast instanceof Function) {
        Function fn = (Function) line.ast;
        String oldName = fn.name;
        String newName = "u_fn_" + oldName + "_" + fn.label;
        ourRenamings.putEdge(oldName,
            new GenericTree<>(Nullable.of(newName), ourOuterRenamings.currentModuleFullType));
        // this name is already unique, and no user can create it since *
        ourRenamings.putEdge(oldName + "*" + fn.getOriginalLabel(),
            new GenericTree<>(Nullable.of(newName), ourOuterRenamings.currentModuleFullType));
        // used to update vtable here, but now vtable does not exist until AFTER
        // renaming
      } else if (line.ast instanceof ClassDeclaration) {
        ((ClassDeclaration) line.ast).populateRenamings(ourRenamings, msgState);
      }
    }
  }

  public void linkToParentRenamings(GenericTree<String, String> globalRenamings,
      errors.MsgState msgState) throws FatalMessageException {
    // during linking phase, parent will be linked in if from another module,
    // so then we can link to our parent
    if (parent.isNotNull()) {
      // Collect parent renamings first as our own
      // Locate parent from classIndex
      // run populateRenamings
      // read from parent entry into our own
      ClassType parentType = new ClassType(parent.get().outerType, parent.get().name,
          new LinkedList<Type>());
      Nullable<GenericTree<String, String>> parentCtx = globalRenamings
          .lookupPathTree(parentType.toList());
      if (parentCtx == null) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "Failed to find parent " + parentType + " in renamings", lineNum, columnNum);
      }
      // link in our parent renamings
      // since we inherit the fields, we need to inherit the renamings as well
      globalRenamings.lookupPathTree(fullType.toList()).get().setParentEdges(parentCtx);
    } else {
      // update our value for globalRenamings since we still have the old pointer
      // (before lightCopy)
      globalRenamings.lookupPathTree(fullType.toList()).get()
          .setParentEdges(Nullable.of(globalRenamings));
    }
  }

  /*
   * This ensures unique names and also takes care of shadowing. This is a utility
   * for the users of the ast
   */
  public Optional<ClassDeclaration> renameIds(TypeEnvironment tenv, Renamings globalRenamings,
      MsgState msgState)
      throws FatalMessageException {
    // if we rename in here, no issue, but if we have children, then they need the
    // updated names
    // also have to update dot expressions and lvalues
    // with updated renamings, we can now implement this

    // find our class header and populate local fields renamings with no 'within'

    Optional<UserDeclTypeBox> userTyBox = tenv.lookupUserDeclType(fullDeclType,
        lineNum, columnNum);
    if (!userTyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(userTyBox.get() instanceof ClassTypeBox)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.RENAMINGS, "not a class",
          lineNum, columnNum);
      return Optional.empty();
    }
    ClassTypeBox classTyBox = (ClassTypeBox) userTyBox.get();
    
    ClassHeader classHeader = classTyBox.classHeader;

    Renamings scopedRenamings = globalRenamings.extend();
    for (List<ClassMember> memberList : classHeader.getMembers().values()) {
      for (ClassMember member : memberList) {
        Nullable<String> tmp = globalRenamings.lookup(member.id, member.getType(),
            Nullable.of(member.classOfOrigin));
        if (tmp.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
              "Unable to find renaming for member: " + member.id + " of class: "
                  + member.classOfOrigin,
              lineNum, columnNum);
        }
        scopedRenamings.add(member.id, member.getType(), Nullable.empty(), tmp.get(), member.label);
      }
    }

    List<ClassDeclarationSection> newSections = new LinkedList<ClassDeclarationSection>();
    for (ClassDeclarationSection section : sections) {
      newSections.add(section.renameIds(globalRenamings, scopedRenamings,
          new ClassDeclType(fullType.outerType, fullType.name), msgState));
    }

    // includes our generics e.g. B<T> extends A<T>
    // GenericTree<String, String> genTree = globalRenamings.extend();

    List<String> newGenericTypeNames = genericTypeNames;
    /*
     * TODO revisit. Maybe this can go in renameTypes? for (String gn :
     * genericTypeNames) { Nullable<String> newG = renamings.get().lookupEdge(gn);
     * if (newG.isNull()) { msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
     * "Failed to find generic " + gn + " in renamings", lineNum, columnNum); }
     * newGenericTypeNames.add(newG.get()); genTree.putEdge(gn, new
     * GenericTree<String, String>(newG, globalRenamings.currentModuleFullType)); }
     */

    Nullable<ClassType> newParent = parent;
    if (parent.isNotNull()) {
      // newParent = Nullable.of((ClassType) parent.get().renameIds(genTree,
      // msgState));
    }

    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, newGenericTypeNames, newParent,
        newSections);
    val.determinedType = determinedType.renameIds(globalRenamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return Optional.of(val);
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof ClassDeclaration)) {
      return false;
    }
    ClassDeclaration ourOther = (ClassDeclaration) other;
    if (!ourOther.name.equals(name)) {
      return false;
    }
    if (!parent.equals(ourOther.parent)) {
      return false;
    }

    if (ourOther.genericTypeNames.size() != genericTypeNames.size()) {
      return false;
    }
    Iterator<String> iterOtherGT = ourOther.genericTypeNames.iterator();
    Iterator<String> iterUsGT = genericTypeNames.iterator();
    while (iterUsGT.hasNext() && iterOtherGT.hasNext()) {
      if (!iterUsGT.next().equals(iterOtherGT.next())) {
        return false;
      }
    }

    if (ourOther.sections.size() != sections.size()) {
      return false;
    }
    Iterator<ClassDeclarationSection> iterOtherS = ourOther.sections.iterator();
    Iterator<ClassDeclarationSection> iterUsS = sections.iterator();
    while (iterUsS.hasNext() && iterOtherS.hasNext()) {
      if (!iterUsS.next().compare(iterOtherS.next())) {
        return false;
      }
    }

    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    List<ClassDeclarationSection> newSections = new LinkedList<ClassDeclarationSection>();
    for (ClassDeclarationSection sec : sections) {
      newSections.add((ClassDeclarationSection) sec.replace(target, with));
    }

    ClassDeclaration value = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, parent,
        newSections);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.setOrdering(ordering);
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (ClassLine line : lines) {
      Nullable<Ast> tmp = line.ast.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<ClassDeclarationSection> newSections = new LinkedList<ClassDeclarationSection>();
    for (ClassDeclarationSection section : sections) {
      // sections consume pass-ups
      newSections.add((ClassDeclarationSection) section.toAnf(tuning).b);
    }
    ClassDeclaration value = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, parent,
        newSections);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.setOrdering(ordering);
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), value);
  }

  public ClassDeclaration replaceType(Map<Type, Type> mapFromTo, String newName,
      boolean noGenerics) {
    if (newName.isEmpty()) {
      newName = name;
      Type tmpNewName = mapFromTo.get(new ClassDeclType(fullType.outerType, fullType.name));
      if (tmpNewName != null) {
        newName = ((ClassDeclType) tmpNewName).name;
      }
    }
    Nullable<ModuleType> newOuter = fullType.outerType;
    if (newOuter.isNotNull()) {
      newOuter = Nullable.of((ModuleType) newOuter.get().replaceType(mapFromTo));
    }
    ClassDeclType newNameDecl = new ClassDeclType(newOuter, newName);

    // inner class's generics are separate from surrounding class's generics
    List<ClassDeclarationSection> newSections = new LinkedList<>();

    for (ClassDeclarationSection section : sections) {
      newSections
          .add((ClassDeclarationSection) section.replaceType(mapFromTo, newNameDecl));
    }

    // outerType and name will have already been updated by the lifting
    Nullable<ClassType> newParent = parent;
    if (parent.isNotNull()) {
      newParent = Nullable.of((ClassType) parent.get().replaceType(mapFromTo));
    }

    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, (ModuleType) moduleType.replaceType(mapFromTo), newName,
        originalName,
        noGenerics ? new LinkedList<>() : genericTypeNames, newParent, newSections);

    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return val;
  }

  public void indexLiftedClasses(Map<UserInstanceType, UserInstanceType> liftedClassIndex) {
    for (ClassLine line : lines) {
      if (line.ast instanceof ClassDeclaration) {
        ((ClassDeclaration) line.ast).indexLiftedClasses(liftedClassIndex);
      }
    }

    // generate unique name, make sure it starts with caps
    String newName = "UC_" + name + "_" + label;
    List<Type> ignoreInnerTypes = new ArrayList<Type>();
    for (Type innerTy : fullType.innerTypes) {
      ignoreInnerTypes.add(AnyType.Create());
    }

    // e.g. outer.inner -> uc_inner_0
    ClassType fullPath = new ClassType(fullType.outerType, name, ignoreInnerTypes);
    ClassType newFullPath = new ClassType(null, newName, ignoreInnerTypes);

    liftedClassIndex.put(fullPath, newFullPath);
  }

  public ClassDeclaration moveInitsToConstructors(Project project,
      MsgState msgState) throws FatalMessageException {
    // if no DEFAULT constructor present, add a default constructor with these inits
    //also, if no destructor, then add one
    List<ClassDeclarationSection> newSections = new LinkedList<>();

    List<AssignmentStatement> newInits = new LinkedList<>();
    List<AssignmentStatement> allNonStatic = new LinkedList<>();
    
    boolean foundDefaultConstructor = false;
    boolean foundDestructor = false;
    
    // first just collect all the new declarations (non-static)
    for (ClassDeclarationSection section : sections) {
      for (DeclarationStatement decl : section.declarations) {
        if (!decl.getIsStatic()) {
          ThisExpression tval = new ThisExpression(-1, -1, new LinkedList<>(), new LinkedList<>());
          tval.setOriginalLabel(tval.label);
          tval.setDeterminedType(new ReferenceType(fullType));

          LvalueDot lval = new LvalueDot(-1, -1, new LinkedList<>(), new LinkedList<>(), tval,
              decl.name);
          lval.setOriginalLabel(lval.label);
          lval.setDeterminedType(decl.type);

          Expression newValue = null;
          if (decl.getValue().isNotNull()) {
            newValue = decl.getValue().get();
          } else {
            newValue = decl.type.getDummyExpression(project, msgState);
          }

          AssignmentStatement aval = new AssignmentStatement(-1, -1, new LinkedList<>(),
              new LinkedList<>(), lval, newValue);
          aval.setOriginalLabel(aval.label);

          if (decl.getValue().isNotNull()) {
            newInits.add(aval);
          }
          allNonStatic.add(aval);
        }
      }
      
      for (Function constructor : section.constructors) {
        if (constructor.params.isEmpty()) {
          foundDefaultConstructor = true;
        } else if (section.visibility != Visibility.PUBLIC) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
              "default constructor must be publid", constructor.lineNum, constructor.columnNum);
        }
      }
      
      if (section.destructor.isNotNull()) {
        foundDestructor = true;
      }
    }
    
    for (ClassDeclarationSection section : sections) {
      newSections.add(section.moveInitsToConstructors(newInits, msgState));
    }
    
    //if no default constructor found, then add it
    //if no destructor (can only be one) found, then add it
    if (!foundDefaultConstructor || !foundDestructor) {
      newSections.add(ClassDeclarationSection.createDefaultConstructor(
          new ClassDeclType(fullType.outerType, fullType.name), parent,
          foundDefaultConstructor, foundDestructor, allNonStatic));
    }

    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, parent,
        newSections);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return val;
  }

  /*
   * If noStatics is true, the filter out statics, otherwise only keep statics
   */
  public ClassDeclaration filterStatics(boolean noStatics) throws FatalMessageException {

    List<ClassDeclarationSection> newSections = new LinkedList<ClassDeclarationSection>();
    for (ClassDeclarationSection section : sections) {
      newSections.add(section.filterOnlyStatics(noStatics));
    }

    Nullable<ClassType> newParent = parent;
    if (parent.isNotNull() && !noStatics) {
      newParent = Nullable
          .of(new ClassType(parent.get().outerType, parent.get().name, new LinkedList<>()));
    }

    ClassDeclaration newTop = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName,
        noStatics ? genericTypeNames : new LinkedList<>(), newParent, newSections);
    newTop.setOriginalLabel(getOriginalLabel());
    newTop.determinedType = determinedType;
    newTop.ordering = ordering;
    return newTop;
  }

  public Nullable<ClassDeclaration> getClassDeclaration(ClassType classType) {
    ClassType toCmp = new ClassType(Nullable.empty(), name, new LinkedList<>());
    if (toCmp.equals(classType)) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<ClassDeclarationSection> newSections = new LinkedList<ClassDeclarationSection>();

    for (ClassDeclarationSection section : sections) {
      newSections
          .add((ClassDeclarationSection) section.replaceVtableAccess(project, msgState).get());
    }

    // outerType and name will have already been updated by the lifting
    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, parent,
        newSections);

    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return Optional.of(val);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {

    List<ClassDeclarationSection> newSections = new LinkedList<>();

    for (ClassDeclarationSection section : sections) {
      newSections.add((ClassDeclarationSection) section
          .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get());
    }

    // outerType and name will have already been updated by the lifting
    Nullable<ClassType> newParent = parent;
    if (parent.isNotNull()) {
      newParent = Nullable.of((ClassType) parent.get()
          .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get());
    }
    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, newParent,
        newSections);

    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return Optional.of(val);
  }

  public void buildModuleDependencyGraph(ClassGraph modDepGraph, Node ourModNode,
      TypeEnvironment tenv) throws FatalMessageException {
    /*
     * This is a hard dependency, since the parent must be a complete type by this
     * point in order to calculate memory and avoid circular inheritance
     * shenanigans.
     */

    Optional<Pair<ClassHeader, TypeEnvironment>> headerTenv = addGenericTypesToTenv(tenv);
    if (!headerTenv.isPresent()) {
      return;
    }
    TypeEnvironment classTenv = headerTenv.get().b;

    // already have added parent to classGraph
    // just look at section bodies
    for (ClassDeclarationSection section : sections) {
      section.addDependency(modDepGraph, ourModNode, classTenv);
    }
  }
  
  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    Optional<Pair<ClassHeader, TypeEnvironment>> headerTenv = addGenericTypesToTenv(tenv);
    if (!headerTenv.isPresent()) {
      return;
    }
    TypeEnvironment classTenv = headerTenv.get().b;
    
    for (ClassDeclarationSection section : sections) {
      section.addDependency(modDepGraph, ourModNode, classTenv);
    }
  }

  /*
   * This will be called from a provider module, 'program' will be the require
   * module
   */
  public Nullable<ClassDeclaration> linkRequiredProvidedFunctions(Program program,
      MsgState msgState) throws FatalMessageException {
    Nullable<ClassDeclaration> reqClassAstOpt = program.lookupClassDecl(name);

    if (reqClassAstOpt.isNull()) {
      if (!export) {
        // just add line (private class)
        return Nullable.of(this);
      }

      msgState.addMessage(MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN,
          "Unable to find class to link provided to: " + this, lineNum, columnNum);
      return Nullable.empty();
    }
    ClassDeclaration reqClassAst = reqClassAstOpt.get();

    /*
     * Verify that our export level matches the required export level
     */
    if (export != reqClassAst.export) {
      msgState.addMessage(
          MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN, "Require class " + name + " to be export("
              + reqClassAst.export + "), but found provide class to be export(" + export + ")",
          lineNum, columnNum);
      return Nullable.empty();
    }

    // now loop through functions, inner classes, and add ffi members
    // cannot add new members or redefine existing members
    List<FfiStatement> newFfiLines = new LinkedList<>(); // must be in new public section
    boolean hitErr = false;
    for (ClassDeclarationSection section : sections) {
      if (!section.linkRequiredProvidedFunctions(true, program, reqClassAst, newFfiLines,
          msgState)) {
        hitErr = true;
      }
    }
    if (hitErr) {
      return Nullable.empty();
    }
    // add ffi members to req class decl
    ClassDeclarationSection ffiMembersSection = new ClassDeclarationSection(-1, -1, preComments,
        postComments, "public", new LinkedList<>(), new LinkedList<>(), newFfiLines,
        new LinkedList<>(), Nullable.empty(), new LinkedList<>());
    ffiMembersSection.setOriginalLabel(ffiMembersSection.label);

    reqClassAst.sections.add(ffiMembersSection);

    // make sure our next non-exported line gets added after where we put this
    // (relative positioning)
    return Nullable.of(reqClassAst);
  }

  public boolean findUnlinkedReqFun(MsgState msgState) throws FatalMessageException {
    boolean foundUnlinked = false;
    for (ClassLine line : lines) {
      if (line.ast instanceof Function) {
        if (((Function) line.ast).findUnlinkedReqFun(msgState)) {
          foundUnlinked = true;
        }
      } else if (line.ast instanceof DeclarationStatement) {
        if (((DeclarationStatement) line.ast).findUnlinkedReqStatic(msgState)) {
          foundUnlinked = true;
        }
      }
    }
    return foundUnlinked;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (fullType.containsType(types)) {
      return true;
    }
    for (ClassLine line : lines) {
      if (line.ast.containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (ClassDeclarationSection cds : sections) {
      if (cds.containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  public void populateTypeRenamings(ModuleType newModuleType, AuditRenameTypes auditEntry) {
    String newName = name + "_CT_" + getTemp();
    if (isFfi) {
      newName = name;
    }

    auditEntry.typeRenamings.put(new ClassDeclType(Nullable.of(moduleType), name),
        new ClassDeclType(Nullable.of(newModuleType), newName));

    auditEntry.relativeTypeRenamings.put(new ClassDeclType(Nullable.empty(), name),
        new ClassDeclType(Nullable.empty(), newName));
  }

  public Nullable<Function> lookupFunction(String name, FunctionType fullType) {
    for (ClassLine line : lines) {
      if (line.ast instanceof Function) {
        Function fn = (Function) line.ast;
        if (fn.name.equals(name) && fn.fullType.equals(fullType)) {
          return Nullable.of(fn);
        }
      }
    }
    return Nullable.empty();
  }

  public Nullable<DeclarationStatement> lookupDeclaration(String declName) {
    for (ClassDeclarationSection section : sections) {
      Nullable<DeclarationStatement> tmp = section.lookupDeclaration(declName);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return Nullable.empty();
  }

  @Override
  public Optional<Set<ModuleType>> findExportedTypes(ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Set<ModuleType> exportedTypes = new HashSet<>();
    if (!export) {
      return Optional.of(exportedTypes);
    }

    for (ClassDeclarationSection section : sections) {
      Optional<Set<ModuleType>> optSet = section.findExportedTypes(currentModule, effectiveImports,
          msgState);
      if (!optSet.isPresent()) {
        return Optional.empty();
      }
      exportedTypes.addAll(optSet.get());
    }

    return Optional.of(exportedTypes);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize,
      MsgState msgState)
      throws FatalMessageException {

    List<ClassDeclarationSection> newSections = new LinkedList<>();
    for (ClassDeclarationSection section : sections) {
      Optional<ResolveExcRet> tmpOpt = section.resolveExceptions(project,
          currentModule, auditEntry, jsmntGlobalRenamings, withinFn, false, enableResolution,
          hitExcId, stackSize, msgState);
      if (!tmpOpt.isPresent()) {
        return Optional.empty();
      }
      newSections.add((ClassDeclarationSection) tmpOpt.get().ast.get());
    }

    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, parent,
        newSections);

    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return Optional.of(new ResolveExcRet(new LinkedList<>(), Nullable.of(val)));
  }
  
  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    List<ClassDeclarationSection> newSections = new LinkedList<>();
    for (ClassDeclarationSection section : sections) {
      newSections.add(section.insertDestructorCalls(scopedIds, destructorRenamings, parent));
    }

    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, parent,
        newSections);

    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return new InsertDestructorsRet(val);
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    throw new UnsupportedOperationException("Use other renameIds");
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    return replaceType(mapFromTo, "", false);
  }

  @Override
  public void setScoping(VariableScoping scoping) {
    this.scoping = scoping;
  }

  @Override
  public VariableScoping getScoping() {
    return scoping;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    // this must be a class that needs to be lifted
    // first check that this class does not contain any classes that need to be lifted
    
    // come up with our new class name
    final String newName = name + "_L" + label;
    ClassDeclType newClassDeclType = new ClassDeclType(fullType.outerType, newName);
    
    // the inner classes know about this class, but this class does now know about the inner classes
    // also, stuff outside this class needs to know about our name change
    replacements.put(fullType.asClassDeclType(), newClassDeclType);
    
    ClassDeclaration val = helperLiftClassDeclarations(liftedClasses, replacements, newName);
    
    liftedClasses.add(val);
    
    // return null since this has now been lifted
    return Nullable.empty();
  }
  
  public ClassDeclaration helperLiftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements, String newName) throws FatalMessageException {
    List<ClassDeclarationSection> newSections = new LinkedList<>();
    for (ClassDeclarationSection section : sections) {
      // this will apply the type replacements on everything inside this class, but we may not
      // have the type replacements from sibling classes yet, so we will need another round of
      // replacements. This could be done a bit more efficiently by first collecting all sibling
      // replacements, but that can be a TODO
      newSections.add(section.liftClassDeclarations(liftedClasses, replacements));
    }
    
    // create new class declaration with all classes lifted out
    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, newName, originalName, genericTypeNames, parent,
        newSections);

    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return val;
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<ClassDeclarationSection> newSections = new LinkedList<>();
    for (ClassDeclarationSection section : sections) {
      newSections.add(section.resolveUserTypes(msgState).get());
    }

    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, determinedParent,
        newSections);

    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return Optional.of(val);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    List<ClassDeclarationSection> newSections = new LinkedList<>();
    for (ClassDeclarationSection section : sections) {
      newSections.add(section.addThisExpression(classHeader));
    }

    ClassDeclaration val = new ClassDeclaration(lineNum, columnNum, preComments, postComments,
        export, isFfi, isStatic, moduleType, name, originalName, genericTypeNames, parent,
        newSections);

    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.setOrdering(ordering);
    return val;
  }
}
