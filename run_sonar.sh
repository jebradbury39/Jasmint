#!/bin/bash

gradle clean
rm build/jacoco/test.exec
cd ../JasmintUI
gradle clean
rm build/jacoco/test.exec
gradle fatJar
cd ../JasmintCxxTranspiler
gradle clean
rm build/jacoco/test.exec
gradle fatJar
cd ../JasmintPythonTranspiler
gradle clean
rm build/jacoco/test.exec
gradle fatJar
cd ../Jasmint
RUN_JSMNT_REGRESSION=1 gradle sonarqube --info
