Two ways to build:
  1) Single file build (no modules needed, just pull in libraries)
  2) Project build (every file has module name and project top level provides a json config)
  
Json Config
{
  "author": "John Bradbury",
  "name": "My Program",
  "description": "Demo the jasmint-build.json format",
  "version": 0.1,
  "root": "src/", //searches for *.jsmnt files recursively (if no root/roots, then default to ".")
  "roots": ["src/", "src2/"], //preferably only one root, but this allows for multiple
  "lib": ["~/jsmnt-stdlib/", "third-party-libs/"], //define additional places to look for libraries
  "dist": "dist/" //what directory to place build files into
}

You may run into another project, at which point that project must be built using its own json config.

1) Discover all projects (into MetaProject)
2) Each project has its own settings, but all get compiled at the same time
  a) AST + TypeInfoTable get stored into <filename>.jsmnt-o Only type info is class names (have not yet covered parents) and function signatures.
  b) Finish incremental typechecking, updating the AST + TypeInfoTable
  c) 2 options, can save and reload from files (slower, but less memory), or can hold info in memory (faster, but more memory).
      A compromise might be a caching technique.
3) Result must only have a single main
4) Only recompile if a change has been made (hashes stored either as hidden files or in a temp config file in dist)
5) AST transforms must work on the entire AST, but could make a copy of dist and then put incremental changes in each file
6) Transpilers may keep separate files or concat

Interp must store the entire MetaProject in memory, but interp is for debugging so not so bad.

//Each file is consided to be a library
module A;
module String of std expand AsciiString, UnicodeString;

import B
import std

...std.AsciiString -> std.String.AsciiString

//During transpiling/interp, a library is treated as a class. Everything inside
the file is wrapped in this class, except for imports.

class A {
public:
   class X {
   }
   static fun int main() {
   }
}

//The one main function is tracked an placed in the final executable:
A.main()


Dependencies:

Say Project A imports Project B as a library. Project B imports Project C.v1,
but A imports C.v2, and B cannot affort to upgrade, and A cannot afford to
downgrade.

1) We say this is an issue with Project C/B (need to allow backwards compat/upgrade).
2) We allow it, but then B cannot export any types from C.v1 (cannot compare C.v1 stuff with C.v2 stuff, since we don't know if they are equal)
   If exporting a type from a lib, then any modules that import us must also
   have that same version of lib

module b;
import c;

//does not export (since not exported)
fun c.C fn0() {
}

export fun int fn1() {
   c.C x = c.usage();
}

class B {
public {
   c.C field; //class not exported
}
}

//the return type will export
export fun c.C fn1() {
   return c.usage();
}

export fun (-> c.C) fn3() {
   return c.usage;
}

export fun int fn4(c.C x) {
}

//an exported class cannot export an un-exported class via an accessable field
export class B2 {
public {
   c.C field; //public, so export type
}
protected {
   //might be exported if A2 extends B2
}
private {
   //subclasses cannot access this, so not exported
}
}

module a;
import b;
import c;

//important that if we access any b.api() that uses c, only then do we compare
//if c-version is the same


In order to properly determine implicit imports, must to a pass to check
which of our (the module) imports we implicitly import to any module which
imports us. Then those importing modules must ensure that if they explicitly/implicitly
import a different version, then throw an error, otherwise link the projects together.

lib_dist_v1/
  manifest
  build_files
  
lib_dist_v2/
  manifest
  build_files

dist/
  manifest
  build_files
  lib_dist_v1/
    manifest
    build_files
  lib_dist_v2/
    manifest
    build_files
    
dist/manifest:
  libs: [
    {dist: lib_dist_v1 (read from here),
     new_dist: dist/lib_dist_v1 (write to here, don't change the source)},
     ]
  srcEntries: (checksum + mbo for each of our modules)
