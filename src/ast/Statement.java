package ast;

import ast.Program.DestructorRenamings;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import mbo.Renamings;
import mbo.TypeReplacements;
import typecheck.ModuleType;
import typecheck.Type;
import util.InsertDestructorsRet;
import util.ScopedIdentifiers;

public interface Statement extends Ast {
  /* This ensures unique names and also takes care of shadowing.
   * This is a utility for the users of the ast
   */
  Statement renameIds(Renamings renamings,
      MsgState msgState) throws FatalMessageException;
  
  public Statement replaceType(Map<Type, Type> mapFromTo);

  Optional<Statement> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
          throws FatalMessageException;
  
  /*
   * Call this after renameIds and resolveGenerics but before renameTypes.
   * This function would be messed up by shadow
   * declarations. This may create temp variables, e.g.
   * 
   * delete fnCall();
   * 
   * becomes
   * 
   * tmp = fnCall();
   * tmp.destructor();
   * delete tmp;
   * 
   * We only need to call destructors on class instances, although we will need to iterate
   * through arrays and maps of classes in order to free items in the collection
   * 
   * The returned list of statements should have 'this' as the last item
   */
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException;
  
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException;
  
  //this MUST run exactly ONCE right after the FIRST typecheck
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException;
  
  public Statement addThisExpression(ClassHeader classHeader);
}
