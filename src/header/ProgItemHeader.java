package header;

import typecheck.ModuleType;
import typecheck.ResolvedImportType;

public abstract class ProgItemHeader {
  //purely for debug info
  // the line/column where the header came from
  public final int lineNum;
  public final int columnNum;
  
  //the label of the ast where this came from (needed for renamings)
  public final long label;
 
  // aka outerType. always part of a module (abs pathing here is mandatory)
  // this is the module the class is defined in
  public final ModuleType moduleType;
  
  // if this is visible outside of the module
  public final boolean export;
  
  public final String name;
  
  public ProgItemHeader(int lineNum, int columnNum, long label, ModuleType moduleType,
      boolean export, String name) {
    this.label = label;
    this.lineNum = lineNum;
    this.columnNum = columnNum;
    this.moduleType = moduleType;
    this.export = export;
    this.name = name;
  }

  public abstract ResolvedImportType getImportType();

  public abstract String getName();

}
