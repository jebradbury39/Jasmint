package ast;

import astproto.AstProto;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import java.util.LinkedList;
import java.util.List;
import typecheck.AbstractType;
import typecheck.Type;
import util.Pair;

public abstract class AbstractLvalue extends AbstractAst implements Lvalue {

  public AbstractLvalue(int lineNum, int columnNum, AstType astType, List<CommentAttr> preComments,
      List<CommentAttr> postComments) {
    super(lineNum, columnNum, astType, preComments, postComments);
  }
  
  public AbstractLvalue(int lineNum, int columnNum, AstType astType) {
    super(lineNum, columnNum, astType);
  }
  
  public AbstractLvalue(AstType astType) {
    super(astType);
  }

  public static Lvalue deserialize(AstProto.Ast document) throws SerializationError {
    final int deLineNum = document.getLineNum();
    final int deColumnNum = document.getColNum();
    final Type deDeterminedType = AbstractType.deserialize(document.getDeterminedType());
    final long originalLabel = document.getLabel();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();
    
    Lvalue value = null;
    Pair<AstType, Long> tmp = AbstractAst.preDeserialize(document, preComments, postComments);
    AstType astType = tmp.a;
    long ordering = tmp.b;
    switch (astType) {
      case BRACKET_LVALUE:
        value = LvalueBracket.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getLvalueBracket());
        break;
      case DOT_LVALUE:
        value = LvalueDot.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getLvalueDot());
        break;
      case ID_LVALUE:
        value = LvalueId.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getLvalueId());
        break;
      case DEREFERENCE_LVALUE:
        value = LvalueDereference.deserialize(deLineNum, deColumnNum,
            preComments, postComments, document.getLvalueDereference());
        break;
      case FFI_LVALUE:
        value = LvalueFfi.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getLvalueFfi());
        break;
      case MULTI_LVALUE:
        value = MultiLvalue.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getMultiLvalue());
        break;
      default:
        break;
    }
    
    if (value == null) {
      throw new SerializationError("Unable to deserialize lvalue astType[" + astType + "]", false);
    }
    
    ((AbstractAst) value).determinedType = deDeterminedType;
    ((AbstractAst) value).setOriginalLabel(originalLabel);
    return value;
  }
  
  public static Lvalue fromIdList(int lineNum, int columnNum, List<String> identifiers,
      MsgState msgState) throws FatalMessageException {
    if (identifiers.isEmpty()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "identifers is empty",
          lineNum, columnNum);
    }
    if (identifiers.size() == 1) {
      Lvalue lvalue = new LvalueId(lineNum, columnNum, new LinkedList<>(), new LinkedList<>(),
          identifiers.get(0));
      lvalue.setOriginalLabel(lvalue.getLabel());
      return lvalue;
    }
    
    List<Lvalue> lvalues = new LinkedList<>();
    for (String id : identifiers) {
      Lvalue lvalue = new LvalueId(lineNum, columnNum, new LinkedList<>(), new LinkedList<>(), id);
      lvalue.setOriginalLabel(lvalue.getLabel());
      lvalues.add(lvalue);
    }
    MultiLvalue val = new MultiLvalue(lineNum, columnNum, new LinkedList<>(), new LinkedList<>(),
        lvalues);
    val.setOriginalLabel(val.label);
    return val;
  }
}
