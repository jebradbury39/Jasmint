package multifile;

import ast.SerializationError;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.RegisteredGenerics;
import java.io.File;
import java.io.IOException;
import mbo.ModuleBuildObj;
import multifile.BuildManifest.BuildEntry;

/*
 * This module handle is loaded into RAM
 */
public class LoadedModuleHandle extends ModuleHandle {
  public final ModuleBuildObj moduleBuildObj;
  public RegisteredGenerics registeredGenerics;

  public LoadedModuleHandle(String srcFilename, boolean needRebuild, BuildManifest manifest,
      ModuleBuildObj moduleBuildObj, RegisteredGenerics registeredGenerics) {
    super(srcFilename, moduleBuildObj.moduleType, moduleBuildObj.submods,
        moduleBuildObj.effectiveImports, needRebuild, manifest);
    this.moduleBuildObj = moduleBuildObj;
    this.registeredGenerics = registeredGenerics;
  }
  
  @Override
  public ModuleHandle load(Project project, MsgState msgState) {
    return this;
  }

  @Override
  public synchronized ModuleHandle store(MsgState msgState)
      throws FatalMessageException, IOException, SerializationError {
    //srcFilename is abs path and is thus unique on the system
    Nullable<BuildEntry> entry = manifest.getEntry(srcFilename);
    if (entry.isNull()) {
      throw new IllegalArgumentException("manifest does not contain this entry: " + srcFilename);
    }
    String progBuildPath = entry.get().getBuildFile(true);
    String genericsBuildPath = entry.get().getGenericsFile(false);
    
    //ensure progBuildPath exists (as far as directory structure)
    new File(progBuildPath).getParentFile().mkdirs();
    new File(genericsBuildPath).getParentFile().mkdirs();
    
    try {
      moduleBuildObj.serializeToFile(progBuildPath);
    } catch (SerializationError e) {
      // TODO will go away
      e.printStackTrace();
    } catch (IOException e) {
      msgState.addMessage(MsgType.FATAL, MsgClass.DESERIALIZE,
          "Failed to store module: " + moduleType + " with err: " + e.getMessage());
      return null; //fatal exn thrown
    }
    
    //TODO serialize the registered/resolved generics
    //check if we are still equal to lib non-local. If not, then write to local version and
    //update manifest
    //if (!registeredGenerics.equalsOriginal()) {
      genericsBuildPath = entry.get().getGenericsFile(true);
      registeredGenerics.serializeToFile(genericsBuildPath);
    //}
    
    return new StoredModuleHandle(srcFilename, moduleType, submods, effectiveImports,
        needRebuild, manifest, progBuildPath, genericsBuildPath);
  }

  /*
  public LoadedModuleHandle renameTypes(String moduleAppend) {
    return new LoadedModuleHandle(srcFilename, needRebuild, manifest,
        moduleBuildObj.renameTypes(moduleAppend), registeredGenerics);
  }
  */
}
