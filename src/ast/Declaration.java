package ast;

import type_env.VariableScoping;

public interface Declaration {
  public void setScoping(VariableScoping scoping);
  
  public VariableScoping getScoping();

  public String getName();
}
