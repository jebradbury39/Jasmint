package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.AbstractValue.MetaValue;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class ReturnStatement extends AbstractStatement {

  public final Nullable<Expression> returnValue; /* may be null */

  public ReturnStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Nullable<Expression> returnValue) {
    super(lineNum, columnNum, AstType.RETURN_STATEMENT, preComments, postComments);
    this.returnValue = returnValue;

    if (returnValue.isNotNull()) {
      returnValue.get().setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  public ReturnStatement(int lineNum, int columnNum, Nullable<Expression> returnValue) {
    super(lineNum, columnNum, AstType.RETURN_STATEMENT);
    this.returnValue = returnValue;

    if (returnValue.isNotNull()) {
      returnValue.get().setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  public ReturnStatement(Nullable<Expression> returnValue) {
    super(AstType.RETURN_STATEMENT);
    this.returnValue = returnValue;

    if (returnValue.isNotNull()) {
      returnValue.get().setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    if (returnValue.isNull()) {
      return preToString() + "return" + postToString();
    }
    return preToString() + "return " + returnValue.get().toString(indent) + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {

    // if we are not expecting to return, then throw an error (not inside a
    // function)
    if (expectedReturnType.isNull()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Found unexpected return statement", lineNum, columnNum);
      return Optional.empty();
    }

    Optional<TypecheckRet> evalRetType = Optional.empty();
    if (returnValue.isNotNull()) {
      evalRetType = returnValue.get().typecheck(tenv, Nullable.empty());
      if (!evalRetType.isPresent()) {
        return Optional.empty();
      }
      determinedType = evalRetType.get().type;
    }
    evalRetType = Optional.of(new TypecheckRet(determinedType, determinedType));

    // make sure the returned type matches what we expect
    Type expectedReturn = expectedReturnType.get().normalize(tenv, lineNum, columnNum).get();
    Type actualReturn = evalRetType.get().returnType.get();

    if (!tenv.canSafeCast(actualReturn, expectedReturn)) {
      tenv.msgState.addMessage(
          MsgType.ERROR, MsgClass.TYPECHECK, "return statement type [" + actualReturn
              + "] cannot cast to expected return type [" + expectedReturn + "]",
          lineNum, columnNum);
      return Optional.empty();
    }

    // may not actually want to use actual return type (since casting), but still
    // good info
    // Having this as non-null means that we got a real return (even if void)
    // and this branch of control-flow will exit

    return evalRetType;
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    if (returnValue.isNotNull()) {
      return returnValue.get().interp(env).setMetaValue(MetaValue.RETURN);
    }
    return new VoidValue(MetaValue.RETURN);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.ReturnStatement.Builder subDoc = AstProto.ReturnStatement.newBuilder();

    if (returnValue.isNotNull()) {
      subDoc.addReturnValue(returnValue.get().serialize());
    }

    document.setReturnStatement(subDoc);
    return document;
  }

  public static ReturnStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.ReturnStatement document) throws SerializationError {
    Nullable<Expression> returnValue = Nullable.empty();
    if (document.getReturnValueCount() > 0) {
      returnValue = Nullable.of(AbstractExpression.deserialize(document.getReturnValue(0)));
    }
    return new ReturnStatement(lineNum, columnNum, preComments, postComments, returnValue);
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    if (returnValue.isNull()) {
      return this;
    }
    ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        Nullable.of(returnValue.get().renameIds(renamings, msgState)));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    if (returnValue.isNull()) {
      return new HashMap<String, Type>();
    }
    return returnValue.get().findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof ReturnStatement)) {
      return false;
    }
    ReturnStatement ourOther = (ReturnStatement) other;
    if ((ourOther.returnValue.isNull() && returnValue.isNotNull())
        || (ourOther.returnValue.isNotNull() && returnValue.isNull())) {
      return false;
    }
    if (returnValue.isNotNull()) {
      return returnValue.get().compare(ourOther.returnValue.get());
    }
    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    if (returnValue.isNull()) {
      return this;
    }

    ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        Nullable.of((Expression) returnValue.get().replace(target, with)));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    if (returnValue.isNotNull()) {
      return returnValue.get().findByOriginalLabel(findLabel);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    if (returnValue.isNotNull()) {
      Pair<List<DeclarationStatement>, Ast> tmp = returnValue.get().toAnf(tuning);
      ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
          Nullable.of((Expression) tmp.b));
      value.determinedType = determinedType;
      value.setOriginalLabel(getOriginalLabel());
      return new Pair<List<DeclarationStatement>, Ast>(tmp.a, value);
    }
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), this);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    if (returnValue.isNull()) {
      return this;
    }

    ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        Nullable.of((Expression) returnValue.get().replaceType(mapFromTo)));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    if (returnValue.isNull()) {
      return Optional.of(this);
    }

    ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        Nullable.of((Expression) returnValue.get().replaceVtableAccess(project, msgState).get()));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    if (returnValue.isNull()) {
      return Optional.of(this);
    }

    ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        Nullable.of(returnValue.get()
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get()));
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    if (returnValue.isNotNull()) {
      returnValue.get().addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (returnValue.isNotNull()) {
      if (returnValue.get().containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    if (returnValue.isNotNull()) {
      if (returnValue.get().containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Nullable<Expression> newReturnValue = returnValue;
    if (returnValue.isNotNull()) {
      Optional<ResolveExcRet> tmp = returnValue.get().resolveExceptions(project,
          currentModule, auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement,
          enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newReturnValue = Nullable.of((Expression) tmp.get().ast.get());
    }

    ReturnStatement val = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        newReturnValue);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(val)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    List<Statement> ourStatements = new LinkedList<>();
    ourStatements.addAll(scopedIds.getDestructors(ScopeFilter.FUNCTION, destructorRenamings));
    ourStatements.add(this);
    return new InsertDestructorsRet(ourStatements, true);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    if (returnValue.isNull()) {
      return Nullable.of(this);
    }

    ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        Nullable.of(returnValue.get()
            .liftClassDeclarations(liftedClasses, replacements)
            .get()));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    if (returnValue.isNull()) {
      return Optional.of(this);
    }

    ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        Nullable.of(returnValue.get()
            .resolveUserTypes(msgState)
            .get()));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    if (returnValue.isNull()) {
      return this;
    }

    ReturnStatement value = new ReturnStatement(lineNum, columnNum, preComments, postComments,
        Nullable.of(returnValue.get().addThisExpression(classHeader)));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
