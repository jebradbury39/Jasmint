package type_env;

import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.Nullable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import typecheck.AnyType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.Type;
import util.Pair;

public class MapFunctionToType extends IdToType {
  private List<BasicTypeBox> overloads = new LinkedList<>();

  @Override
  public String toString() {
    String result = "";
    for (BasicTypeBox overload : overloads) {
      if (result.length() != 0) {
        result += ", ";
      }
      result += overload.toString();
    }
    return result;
  }

  @Override
  public IdToType write(String name, TypeBox typeBox) {
    if (!(typeBox instanceof BasicTypeBox)) {
      throw new IllegalArgumentException();
    }
    BasicTypeBox tyBox = (BasicTypeBox) typeBox;
    
    sanityCheck(name, tyBox);
    // check for exact type match. If there is one, overwrite it
    int idx = 0;
    boolean found = false;
    for (BasicTypeBox overload : overloads) {
      if (overload.getValue().equals(tyBox.getValue())) {
        found = true;
        break;
      }
      idx++;
    }
    if (found) {
      overloads.set(idx, tyBox);
    } else {
      overloads.add(tyBox);
    }
    return this;
  }

  @Override
  public Pair<Nullable<TypeBox>, Integer> read(TypeEnvironment tenv, String name,
      FunctionType functionType, boolean ignoreExport, int lineNum, int columnNum)
          throws FatalMessageException {
    if (overloads.isEmpty()) {
      return new Pair<>(Nullable.empty(), 0);
    }
    if (functionType == null) {
      if (overloads.isEmpty()) {
        return new Pair<>(Nullable.empty(), 0);
      }
      // this only comes from assigning. So you get the most recently defined overload
      TypeBox typeBox = overloads.get(overloads.size() - 1);
      // cannot ignore export when looking up a function (only applicable to types)
      if (!typeBox.getExport()) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot access un-exported id: " + name, lineNum, columnNum);
        return new Pair<>(Nullable.empty(), -1);
      }
      
      return new Pair<>(Nullable.of(typeBox), 0);
      // throw new TypeError("cannot access function [" + name + "]
      // without giving argument types", -1, -1);
    }
    return lookupFn(tenv, name, functionType, lineNum, columnNum);
  }

  /* accounts for overloads
   * fnQueryType must specify the returnType as Any and the lambdaStatus as Unknown
   */
  public Pair<Nullable<TypeBox>, Integer> lookupFn(TypeEnvironment tenv, String name,
      FunctionType fnType, int lineNum, int columnNum) throws FatalMessageException {
    BasicTypeBox bestOption = null;
    int bestOptionNumMatched = -1;
    boolean bestOptionMatchedWithin = false;
    final FunctionType fnQueryType = new FunctionType(fnType.within, AnyType.Create(),
        fnType.argTypes, LambdaStatus.UNKNOWN);

    List<BasicTypeBox> ambigMatches = new LinkedList<>();
    
    for (BasicTypeBox box : overloads) {
      final Type ty = box.getValue();

      if (!(ty instanceof FunctionType)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            name + " is not a function, but is of type [" + ty + "]");
        return new Pair<>(Nullable.empty(), 0);
      }
      FunctionType fnTy = (FunctionType) ty;
      if (fnTy.argTypes.size() != fnQueryType.argTypes.size()) {
        /* one of the other overloads may have the right # of args */
        continue;
      }

      // compare stored arg N type with desired arg N type
      Pair<Boolean, Integer> cmpResult = fnTy.scoredCompare(fnQueryType, tenv);
      boolean matchedWithin = cmpResult.a;
      int numMatched = cmpResult.b;

      if (numMatched == -1) {
        // keep looking for overloads
        continue;
      }
      if (numMatched > bestOptionNumMatched) {
        bestOption = box;
        bestOptionNumMatched = numMatched;
        bestOptionMatchedWithin = matchedWithin;
        ambigMatches.clear();
        ambigMatches.add(box);
      } else if (numMatched == bestOptionNumMatched) {
        if (matchedWithin && bestOptionMatchedWithin) {
          // ambiguous, although a better match might come along,
          // so just store this, don't error yet
          ambigMatches.add(box);
        } else if (matchedWithin && !bestOptionMatchedWithin) {
          // use matchedWithin as a tie-breaker
          bestOption = box;
          bestOptionNumMatched = numMatched;
          bestOptionMatchedWithin = matchedWithin;
          ambigMatches.clear();
          ambigMatches.add(box);
        }
      }
    }
    
    if (ambigMatches.size() > 1) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Ambiguous overloaded function call on \"" + name + "\": " + ambigMatches,
          lineNum, columnNum);
      bestOption = null;
      bestOptionNumMatched = -1;
    }

    // don't need parent since on fail, the tenv will just do that itself
    // also, the parent will not have a better overload since all overloads had to
    // have come here

    if (bestOption == null) {
      return new Pair<>(Nullable.empty(), -1);
    }
    
    if (!bestOption.getExport()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot access un-exported id: " + name, lineNum, columnNum);
      return new Pair<>(Nullable.empty(), -1);
    }
    
    return new Pair<>(Nullable.of(bestOption), bestOptionNumMatched);
  }

  @Override
  public IdToType fullCopy(String name) {
    MapFunctionToType copy = new MapFunctionToType();

    for (AbstractTypeBox overload : overloads) {
      copy.write(name, overload.fullCopy());
    }

    return copy;
  }

  @Override
  public IdToType replaceType(TypeEnvironment tenv, String name, Map<Type, Type> mapFromTo)
      throws FatalMessageException {
    MapFunctionToType copy = new MapFunctionToType();

    for (BasicTypeBox overload : overloads) {
      copy.write(name, overload.replaceType(tenv, mapFromTo, -1, -1));
    }
    return copy;
  }

}
