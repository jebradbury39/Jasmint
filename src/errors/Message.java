package errors;

public class Message {
  
  public static boolean DebugMode = true;
  
  public static enum MsgType {
    INFO, //purely info
    WARNING, //style, semantics, keep going
    ERROR, //error, but keep going
    INTERNAL, //internal error, stop now
    FATAL //error, and stop now
  }
  
  public static enum MsgClass {
    SYNTAX, //parsing
    TYPECHECK,
    INTERP,
    AST,
    SERIALIZE,
    DESERIALIZE,
    RENAMINGS,
    VTABLE,
    RENAME_TYPES,
    LINK_REQ_PROV_FN,
    SANDBOX,
    PROJECT_ASSEMBLE,
    RESOLVE_EXCEPTIONS,
    INSERT_DESTRUCTORS,
    LIFT_CLASS_DECLARATIONS,
    RESOLVE_USER_TYPES
  }
  
  private final MsgType msgType;
  private final MsgClass msgClass;
  private final String content;
  private final Nullable<String> filename;
  private final int line;
  private final int column;
  private final StackTraceElement[] traceback;
  
  protected Message(MsgType msgType, MsgClass msgClass, String content,
      Nullable<String> filename, int line, int column) {
    this.msgType = msgType;
    this.msgClass = msgClass;
    this.content = content;
    this.filename = filename;
    this.line = line;
    this.column = column;
    
    traceback = Thread.currentThread().getStackTrace();
    
    //for internal errors, could generate core file with message to send core to dev team
  }
  
  @Override
  public String toString() {
    //TODO can add colors and other fancy stuff later
    String res = msgType + ": " + msgClass + ": " + content;
    if (filename.isNotNull()) {
      res += " in file \"" + filename + "\"";
    }
    if (line != -1 && column != -1) {
      res += " @ line " + line + " column " + column;
    }
    if (DebugMode && msgType.ordinal() > MsgType.WARNING.ordinal()) {
      res += " traceback: ";
      int i = -1;
      boolean start = false;
      for (StackTraceElement s : traceback) {
        i += 1;
        if (!start) {
          if (i < 1) {
            continue;
          }
          if ((s.getClassName() + "." + s.getMethodName()).equals("errors.Message.<init>")) {
            continue;
          }
          if (s.getMethodName().equals("addMessage")) {
            continue;
          }
        }
        start = true;
        res += "\n\tat " + s.getClassName() + "." + s.getMethodName()
          + " (" + s.getFileName() + ", " + s.getLineNumber() + ")";
      }
    }
    return res;
  }
}
