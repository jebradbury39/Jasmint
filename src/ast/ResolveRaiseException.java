package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.RenamesOfInterest;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import java.util.LinkedList;
import java.util.List;
import multifile.JsmntGlobal;
import multifile.Project;
import typecheck.BoolType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.IntType;
import typecheck.Type;
import typecheck.VoidType;

public class ResolveRaiseException {

  /*
   * fn_ctx.exc = new Exception(code);
   * 
   * Then if hitExcId, we say:
   * 
   * hitExcId = fn_ctx.exc;
   * 
   * Otherwise:
   * 
   * return <dummy return values matching fn return type>
   */
  public static List<Statement> raiseResolvedException(Project project,
      RenamesOfInterest jsmntGlobalRenamings,
      String hitExcId, FunctionType withinFn, String renamedExcCode, boolean skipAlloc,
      MsgState msgState)
          throws FatalMessageException {

    List<Statement> statements = new LinkedList<>();
    
    if (!skipAlloc) {
      List<Expression> excConstructorArgs = new LinkedList<>();
      excConstructorArgs
          .add(new DotExpression(new StaticClassIdentifierExpression(JsmntGlobal.excTypeEnumDeclType),
              new IdentifierExpression(renamedExcCode)));
      List<Type> excConstructorParams = new LinkedList<>();
      excConstructorParams.add(JsmntGlobal.fnCtxRefType);
      excConstructorParams.add(JsmntGlobal.excTypeEnumType);
      FunctionCallExpression excConstructorFn = new FunctionCallExpression(Nullable.empty(),
          new IdentifierExpression(jsmntGlobalRenamings.excCodeConstructor), excConstructorArgs, "");
      excConstructorFn.calledType = Nullable.of(new FunctionType(JsmntGlobal.exceptionClassType,
          VoidType.Create(), excConstructorParams, LambdaStatus.NOT_LAMBDA));
      NewExpression newExcExpr = new NewExpression(new NewClassInstanceExpression(
          JsmntGlobal.exceptionClassType, JsmntGlobal.exceptionClassType.toString(),
          excConstructorFn));
      newExcExpr.determinedType = VoidType.Create();
  
      statements.add(new AssignmentStatement(
          new LvalueDot(new IdentifierExpression(JsmntGlobal.fnCtxId), jsmntGlobalRenamings.exc),
          newExcExpr));
    }

    Statement retStatement = null;
    if (hitExcId.isEmpty()) {
      // return <dummy return values matching fn return type>
      Nullable<Expression> retExcExpr = Nullable.empty();
      Type withinFnReturnType = withinFn.returnType;
      if (!(withinFnReturnType instanceof VoidType)) {
        // add dummy/null values
        retExcExpr = Nullable.of(withinFnReturnType.getDummyExpression(project, msgState));
      }
      retStatement = new ReturnStatement(retExcExpr);
    } else {
      // hitExcId = fn_ctx.exc;
      retStatement = new AssignmentStatement(new LvalueId(hitExcId),
          new DotExpression(new IdentifierExpression(JsmntGlobal.fnCtxId),
              new IdentifierExpression(jsmntGlobalRenamings.exc)));
    }
    statements.add(retStatement);

    return statements;
  }

  /*
   * if (<provided guard>) { <raise expression statements> }
   */
  public static ConditionalStatement raiseResolvedExcWithinCond(Project project,
      RenamesOfInterest jsmntGlobalRenamings, String hitExcId, FunctionType withinFn,
      String renamedExcCode, Expression guardExpr, boolean skipAlloc, MsgState msgState)
          throws FatalMessageException {
    List<Statement> raiseBodyStatements = raiseResolvedException(project,
        jsmntGlobalRenamings, hitExcId,
        withinFn, renamedExcCode, skipAlloc, msgState);
    BlockStatement raiseBody = new BlockStatement(raiseBodyStatements);

    ConditionalStatement cond = new ConditionalStatement(guardExpr, raiseBody, Nullable.empty());
    return cond;
  }
  
  public static List<Statement> sanityMemcheck(Project project, Expression sizeExpr,
      boolean isFree,
      RenamesOfInterest jsmntGlobalRenamings, String hitExcId, FunctionType withinFn,
      MsgState msgState)
          throws FatalMessageException {
    List<Type> sanityChecksTypes = new LinkedList<>();
    sanityChecksTypes.add(JsmntGlobal.fnCtxRefType);
    sanityChecksTypes.add(IntType.Create(false, 32));
    sanityChecksTypes.add(BoolType.Create());
    List<Expression> sanityChecksArgs = new LinkedList<>();
    sanityChecksArgs.add(new IdentifierExpression(JsmntGlobal.fnCtxId));
    sanityChecksArgs.add(sizeExpr); //must run typecheck after
    sanityChecksArgs.add(new BoolExpression(isFree));
    FunctionCallExpression sanityChecksCall = new FunctionCallExpression(
        Nullable.of(new StaticClassIdentifierExpression(JsmntGlobal.modType)),
        new IdentifierExpression(Program.sanityCheckMemtrackFnName), sanityChecksArgs,
        Program.sanityCheckRecursionFnName);
    sanityChecksCall.calledType = Nullable.of(new FunctionType(JsmntGlobal.modType,
        VoidType.Create(), sanityChecksTypes, LambdaStatus.NOT_LAMBDA));

    List<Statement> statements = new LinkedList<>();
    statements.add(new FunctionCallStatement(sanityChecksCall));
    
    statements.add(raiseResolvedExcWithinCond(project, jsmntGlobalRenamings, hitExcId, withinFn,
        jsmntGlobalRenamings.memLimitHit,
        new BinaryExpression(BinOp.NOT_EQUAL, new DotExpression(
            new IdentifierExpression(JsmntGlobal.fnCtxId),
            new IdentifierExpression(jsmntGlobalRenamings.exc)),
            new NullExpression()), true, msgState));
    
    return statements;
  }
}
