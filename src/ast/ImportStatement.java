package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.ImportType;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UndeterminedImportType;
import util.GenericTree;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class ImportStatement extends AbstractStatement {

  public final ImportType target; // can only import module paths (full path, obviously)

  public ImportStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, ImportType target) {
    super(lineNum, columnNum, AstType.IMPORT_STATEMENT, preComments, postComments);
    this.target = target;
  }

  @Override
  public String toString(String indent) {
    return preToString() + "import " + target + postToString();
  }

  public void populateTypeGraph(TypeEnvironment tenv) throws FatalMessageException {

  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(target, lineNum, columnNum);
    if (!tyBox.isPresent()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "import " + target + " not found",
          lineNum, columnNum);
      return Optional.empty();
    }
    
    ImportType impTargetType = tyBox.get().getImportType(target);
    if (impTargetType instanceof UndeterminedImportType) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "invalid import: " + target,
          lineNum, columnNum);
      return Optional.empty();
    }
    determinedType = (ResolvedImportType) impTargetType;
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();
    AstProto.ImportStatement.Builder subDoc = AstProto.ImportStatement.newBuilder();

    subDoc.setTarget(target.serialize());

    document.setImportStatement(subDoc);
    return document;
  }

  public static Statement deserialize(int dLineNum, int dColumnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.ImportStatement document) throws SerializationError {
    return new ImportStatement(dLineNum, dColumnNum, preComments, postComments,
        (ImportType) AbstractType.deserialize(document.getTarget()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    // TODO Auto-generated method stub
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    ImportStatement value = new ImportStatement(lineNum, columnNum, preComments, postComments,
        (ImportType) target.replaceType(mapFromTo));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    // TODO Auto-generated method stub
    return Optional.empty();
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    ImportStatement val = new ImportStatement(lineNum, columnNum, preComments, postComments,
        (ImportType) target.renameIds(renamings, msgState));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  public void linkRenamings(Project project, GenericTree<String, String> globalRenamings,
      MsgState msgState) throws FatalMessageException {
    /*
     * List<String> tgtList = target.toList(); ModuleEndpoint mod =
     * project.lookupModule(tgtList.subList(0, 1)).get();
     * globalRenamings.putEdge(tgtList.get(0),
     * mod.getLiveInfo().submodsLinkedRenamings.get()); if (tgtList.size() > 1) {
     * Pair<Nullable<ModuleEndpoint>, Nullable<ClassDeclaration>> modOrClass =
     * project.lookupModuleOrClass(target, msgState, lineNum, columnNum).get(); if
     * (modOrClass.b == null) { globalRenamings.putEdge(target.name,
     * modOrClass.a.get().getLiveInfo().submodsLinkedRenamings.get()); } else {
     * globalRenamings.putEdge(target.name,
     * modOrClass.a.get().getLiveInfo().submodsLinkedRenamings.get().lookupPathTree(
     * tgtList.subList(tgtList.size() - 1, tgtList.size())).get()); } }
     */
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(new LinkedList<>(), Nullable.of(this)));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) {
    return new InsertDestructorsRet(this);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    ImportStatement val = new ImportStatement(lineNum, columnNum, preComments, postComments,
        (ImportType) determinedType);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    return this;
  }
}
