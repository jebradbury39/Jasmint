package mbo;

import ast.SerializationError;
import astproto.AstProto;
import astproto.AstProto.Type;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import typecheck.AbstractType;
import typecheck.ClassDeclType;
import typecheck.FunctionType;
import util.Pair;

public class Vtables {
  public Map<ClassDeclType, ClassVtable> vtables = new HashMap<>();
  
  public static class ClassVtable {
    //This order is important, index into this list to call a function
    //This list will always start with the parent's vtableEntries
    public List<VtableEntry> vtableEntries = new ArrayList<>();

    public AstProto.ClassVtable.Builder serialize() {
      AstProto.ClassVtable.Builder document = AstProto.ClassVtable.newBuilder();
      
      for (VtableEntry entry : vtableEntries) {
        document.addNames(entry.name);
        document.addOriginalNames(entry.originalName);
        document.addFunctionTypes(entry.type.serialize());
        document.addIsRootDecl(true);
      }
      
      return document;
    }

    public static ClassVtable deserialize(AstProto.ClassVtable document) throws SerializationError {
      ClassVtable cv = new ClassVtable();
      
      Iterator<String> iterNames = document.getNamesList().iterator();
      Iterator<String> iterOrigNames = document.getOriginalNamesList().iterator();
      Iterator<Type> iterFnTypes = document.getFunctionTypesList().iterator();
      Iterator<Boolean> iterIsRootDecl = document.getIsRootDeclList().iterator();
      
      while (iterNames.hasNext()) {
        cv.vtableEntries.add(new VtableEntry(iterNames.next(), iterOrigNames.next(),
            (FunctionType) AbstractType.deserialize(iterFnTypes.next()),
            iterIsRootDecl.next()));
      }
      
      return cv;
    }
  }
  
  public static class VtableEntry {
    public String name;
    public final String originalName;
    public FunctionType type;
    public final boolean isRootDecl; //is first defined in this class (that this is in the vtable for)

    public VtableEntry(String name, String originalName, FunctionType type, boolean isRootDecl) {
      this.name = name;
      this.originalName = originalName;
      this.type = type;
      this.isRootDecl = isRootDecl;
    }

    @Override
    public String toString() {
      return name + ',' + originalName + ',' + type;
    }

    public String getName() {
      return name;
    }

    public FunctionType getType() {
      return type;
    }

  }
  
  public AstProto.Vtables.Builder serialize() {
    AstProto.Vtables.Builder document = AstProto.Vtables.newBuilder();
    
    for (Entry<ClassDeclType, ClassVtable> entry : vtables.entrySet()) {
      document.addClassDeclTypes(entry.getKey().serialize());
      document.addClassVtables(entry.getValue().serialize());
    }
    
    return document;
  }

  public static Vtables deserialize(AstProto.Vtables document) throws SerializationError {
    Vtables vtables = new Vtables();
    
    Iterator<Type> iterKeys = document.getClassDeclTypesList().iterator();
    Iterator<AstProto.ClassVtable> iterVals = document.getClassVtablesList().iterator();
    
    while (iterKeys.hasNext()) {
      vtables.vtables.put((ClassDeclType) AbstractType.deserialize(iterKeys.next()),
          ClassVtable.deserialize(iterVals.next()));
    }
    
    return vtables;
  }

  public ClassVtable get(ClassDeclType absName) {
    return vtables.get(absName);
  }

  public void put(ClassDeclType absName, ClassVtable vtable) {
    vtables.put(absName, vtable);
  }
  
  //fnTy may be null and ignored. This is appropriate during renaming,
  //when vtable has already been renamed with links
  //always use original name for searching the vtable,
  //but store renamed name for real vtable creation
  public Nullable<Pair<VtableEntry, Integer>> searchVtable(ClassDeclType absName, MsgState msgState,
      String fnName, FunctionType fnTy, boolean useOriginal) throws FatalMessageException {
    //find our vtable
    ClassVtable vtable = vtables.get(absName);
    if (vtable == null) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
          "Did not find vtable to copy for: " + absName);
    }
    
    int i = 0;
    for (VtableEntry entry : vtable.vtableEntries) {
      String cmpName = entry.name;
      if (useOriginal) {
        cmpName = entry.originalName;
      }

      if (cmpName.equals(fnName)) {
        if (fnTy != null) {
          if (!entry.type.equals(fnTy)) {
            continue;
          }
        }
        return Nullable.of(new Pair<VtableEntry, Integer>(entry, i));
      }
      i++;
    }
    return Nullable.empty();
  }

}
