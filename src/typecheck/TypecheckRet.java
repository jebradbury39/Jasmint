package typecheck;

import errors.Nullable;

/*
 * This is the result of a typecheck() call
 */

public class TypecheckRet {
  //If there was a return nested within this ast and all control flows of this statement return
  //this returnType
  public final Nullable<Type> returnType;
  
  //the actual expression type that this ast returns
  public final Type type;
  
  public TypecheckRet(Type type) {
    this.returnType = Nullable.empty();
    this.type = type;
  }
  
  public TypecheckRet(Type returnType, Type type) {
    this.returnType = Nullable.of(returnType);
    this.type = type;
  }
  
  public TypecheckRet(Nullable<Type> returnType, Type type) {
    this.returnType = returnType;
    this.type = type;
  }
  
  @Override
  public String toString() {
    return type + " : " + returnType;
  }

}
