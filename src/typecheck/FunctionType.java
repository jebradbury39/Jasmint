package typecheck;

import ast.Expression;
import ast.NullExpression;
import ast.SerializationError;
import astproto.AstProto;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;
import util.Pair;

/*
 * Cannot implement ImportableType here, since we do not know the function name!
 */
public class FunctionType extends AbstractType {
  
  public static enum LambdaStatus {
    LAMBDA, //e.g. LambdaExpression
    NOT_LAMBDA, //statically defined e.g. Function
    SANDBOX_NOT_LAMBDA, //a non-lambda Function defined in sandbox mode. May not be passed as an arg
    UNKNOWN; //used for lookups
    
    public boolean isNonLambda() {
      return this == NOT_LAMBDA || this == SANDBOX_NOT_LAMBDA;
    }
    
    public boolean isLambda() {
      return this == LAMBDA;
    }
  }
  
  public final Nullable<DotAccessType> within; // either ModuleType or ClassDeclType (abs)
  public final Type returnType;
  public final List<Type> argTypes; /* may be empty */
  // purely informational, does not come into compare. If true, then that means
  // this fn may be
  // reassigned. Otherwise it may not (statically defined in module/class as
  // member function)
  public final LambdaStatus lambdaStatus;

  public FunctionType(DotAccessType within, Type returnType, List<Type> argTypes,
      LambdaStatus lambdaStatus) {
    super(TypeType.FUNCTION_TYPE);
    
    if (within instanceof ClassType) {
      ClassType withinClass = (ClassType) within;
      within = new ClassDeclType(withinClass.outerType, withinClass.name);
    } else if (within instanceof EnumType) {
      EnumType withinEnum = (EnumType) within;
      within = new EnumDeclType(withinEnum.outerType, withinEnum.name);
    }
    
    this.within = Nullable.of(within);
    this.returnType = returnType;
    this.argTypes = argTypes;
    this.lambdaStatus = lambdaStatus;

    if (!(within instanceof ArrayType || within instanceof MapType || within instanceof ModuleType
        || within instanceof ClassDeclType)) {
      throw new IllegalArgumentException();
    }
  }

  public FunctionType(Nullable<DotAccessType> within, Type returnType, List<Type> argTypes,
      LambdaStatus lambdaStatus) {
    super(TypeType.FUNCTION_TYPE);
    
    if (within.instanceOf(ClassType.class)) {
      ClassType withinClass = (ClassType) within.get();
      within = Nullable.of(new ClassDeclType(withinClass.outerType, withinClass.name));
    }
    
    this.within = within;
    this.returnType = returnType;
    this.argTypes = argTypes;
    this.lambdaStatus = lambdaStatus;

    if (within.isNotNull()) {
      if (!(within.get() instanceof ArrayType || within.get() instanceof MapType
          || within.get() instanceof ModuleType || within.get() instanceof ClassDeclType)) {
        throw new IllegalArgumentException();
      }
    }

    if (lambdaStatus.isNonLambda() && within.isNull()) {
      // if not anonymous, then we must know where this fn was originally defined
      throw new IllegalArgumentException();
    }
  }
  /*
   * public FunctionType(Type returnType, List<Type> argTypes, boolean anonymous)
   * { super(TypeType.FUNCTION_TYPE); this.within = Nullable.empty();
   * this.returnType = returnType; this.argTypes = argTypes; this.anonymous =
   * anonymous; }
   */

  public FunctionType setLambdaStatus(LambdaStatus val) {
    return new FunctionType(within, returnType, argTypes, val);
  }

  @Override
  public int hashCode() {
    int hc = super.hashCode() + returnType.hashCode();
    for (Type arg : argTypes) {
      hc += arg.hashCode();
    }
    if (within.isNotNull()) {
      hc += within.get().hashCode();
    }
    hc += lambdaStatus.ordinal();
    return hc;
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof FunctionType)) {
      return false;
    }
    FunctionType otherFT = (FunctionType) other;
    if (!returnType.equals(otherFT.returnType)) {
      return false;
    }
    if (argTypes.size() != otherFT.argTypes.size()) {
      return false;
    }
    Iterator<Type> iterUs = argTypes.iterator();
    Iterator<Type> iterOther = otherFT.argTypes.iterator();
    for (; iterUs.hasNext() && iterOther.hasNext();) {
      if (!iterUs.next().equals(iterOther.next())) {
        return false;
      }
    }
    
    if (otherFT.within.isNull() && within.isNotNull()
        || otherFT.within.isNotNull() && within.isNull()) {
      return false;
    }
    if (otherFT.within.isNull() && within.isNull()) {
      return true;
    }
    
    return otherFT.within.get().equals(within.get());
  }

  @Override
  public List<Type> getInnerTypes() {
    List<Type> tyList = new ArrayList<Type>();
    tyList.add(returnType);
    tyList.addAll(argTypes);
    return tyList;
  }

  @Override
  public String toString() {
    String result = super.toString() + "(";

    result += "<" + lambdaStatus + ">";
    
    if (within.isNotNull()) {
      result += within;
    }
    result += " :: ";
    
    boolean first = true;
    for (Type ty : argTypes) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += ty;
    }

    return result + " -> " + returnType + ")";
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }

    List<Type> replacedArgTypes = new LinkedList<Type>();
    for (Type ty : argTypes) {
      replacedArgTypes.add(ty.replaceType(mapFromTo));
    }

    Nullable<DotAccessType> newWithin = within;
    if (within.isNotNull()) {
      newWithin = Nullable.of((DotAccessType) within.get().replaceType(mapFromTo));
    }

    return new FunctionType(newWithin, returnType.replaceType(mapFromTo), replacedArgTypes,
        lambdaStatus);
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.FunctionType.Builder subDoc = AstProto.FunctionType.newBuilder();

    if (within.isNotNull()) {
      subDoc.addWithin(within.get().serialize());
    }

    subDoc.setReturnType(returnType.serialize().build());
    for (Type argTy : argTypes) {
      subDoc.addArgTypes(argTy.serialize().build());
    }
    subDoc.setLambdaStatus(lambdaStatus.ordinal());

    AstProto.Type.Builder document = preSerialize();
    document.setFunctionType(subDoc);
    return document;
  }

  public static FunctionType deserialize(AstProto.FunctionType document) throws SerializationError {
    List<Type> argTypes = new LinkedList<Type>();
    for (AstProto.Type argTy : document.getArgTypesList()) {
      argTypes.add(AbstractType.deserialize(argTy));
    }

    Nullable<DotAccessType> within = Nullable.empty();
    if (document.getWithinCount() > 0) {
      within = Nullable.of((DotAccessType) AbstractType.deserialize(document.getWithin(0)));
    }

    return new FunctionType(within, AbstractType.deserialize(document.getReturnType()), argTypes,
        LambdaStatus.values()[document.getLambdaStatus()]);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (types.contains(this)) {
      return true;
    }
    for (Type argTy : argTypes) {
      if (argTy.containsType(types)) {
        return true;
      }
    }
    return returnType.containsType(types);
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    stackTypes.addAll(returnType.getUserDeclTypes(stackTypesOnly));
    for (Type argTy : argTypes) {
      stackTypes.addAll(argTy.getUserDeclTypes(stackTypesOnly));
    }
    return stackTypes;
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    List<Type> replacedArgTypes = new LinkedList<Type>();
    Optional<Type> tmp = Optional.empty();
    for (Type ty : argTypes) {
      tmp = ty.normalize(tenv, lineNum, columnNum);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      replacedArgTypes.add(tmp.get());
    }
    tmp = returnType.normalize(tenv, lineNum, columnNum);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }

    Nullable<DotAccessType> newWithin = within;
    if (within.isNotNull()) {
      Optional<Type> tmpTy = within.get().normalize(tenv, lineNum, columnNum);
      if (!tmpTy.isPresent()) {
        return Optional.empty();
      }
      newWithin = Nullable.of((DotAccessType) tmpTy.get());
    }

    return Optional.of(new FunctionType(newWithin, tmp.get(), replacedArgTypes, lambdaStatus));
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    List<Type> replacedArgTypes = new LinkedList<Type>();
    for (Type ty : argTypes) {
      replacedArgTypes.add(ty.renameIds(renamings, msgState));
    }

    Nullable<DotAccessType> newWithin = within;
    if (within.isNotNull()) {
      newWithin = Nullable.of((DotAccessType) within.get().renameIds(renamings, msgState));
    }

    return new FunctionType(newWithin, returnType.renameIds(renamings, msgState), replacedArgTypes,
        lambdaStatus);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException {
    returnType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    for (Type argTy : argTypes) {
      argTy.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    }
  }

  @Override
  public Type trimRelativeType(UserInstanceType currentModuleFullType) {
    List<Type> replacedArgTypes = new LinkedList<Type>();
    for (Type ty : argTypes) {
      replacedArgTypes.add(ty.trimRelativeType(currentModuleFullType));
    }

    Nullable<DotAccessType> newWithin = within;
    if (within.isNotNull() && !within.get().equals(currentModuleFullType)) {
      newWithin = Nullable.of((DotAccessType) within.get().trimRelativeType(currentModuleFullType));
    }

    return new FunctionType(newWithin, returnType.trimRelativeType(currentModuleFullType),
        replacedArgTypes, lambdaStatus);
  }

  @Override
  public Type setConst(boolean isConst) {
    FunctionType tmp = new FunctionType(within, returnType, argTypes, lambdaStatus);
    tmp.isConst = isConst;
    return tmp;
  }

  /*
   * @return -1 if not a match at all, otherwise return number of exact match
   * params-args. e.g. 0 is still a match
   * return (matched_within, numMatched)
   */
  public Pair<Boolean, Integer> scoredCompare(FunctionType other, TypeEnvironment tenv)
      throws FatalMessageException {
    // compare stored arg N type with desired arg N type
    boolean matched = true;
    int numMatched = 0;
    Iterator<Type> iterArgs = other.argTypes.iterator();
    Iterator<Type> iterParams = argTypes.iterator();
    for (; iterParams.hasNext() && iterArgs.hasNext();) {
      Type tmpTy = iterParams.next();
      Optional<Nullable<Type>> tyOpt = tmpTy.basicNormalize(false, tenv.ofDeclType.getModuleType(),
          tenv.effectiveImports, tenv.msgState);
      if (!tyOpt.isPresent()) {
        tenv.msgState.addMessage(MsgType.FATAL, MsgClass.TYPECHECK,
            "Failed to normalize type: " + tmpTy);
      }
      final Type paramTy = tyOpt.get().get();
      
      tmpTy = iterArgs.next();
      tyOpt = tmpTy.basicNormalize(false, tenv.ofDeclType.getModuleType(), tenv.effectiveImports,
          tenv.msgState);
      if (!tyOpt.isPresent()) {
        tenv.msgState.addMessage(MsgType.FATAL, MsgClass.TYPECHECK,
            "Failed to normalize type: " + tmpTy);
      }
      final Type argTy = tyOpt.get().get();
      

      if (!tenv.canSafeCast(argTy, paramTy)) {
        matched = false;
        numMatched = -1;
        break;
      }
      if (argTy.equals(paramTy)) {
        numMatched++;
      }
    }
    
    boolean matchedWithin = false;
    if (within.isNull() && other.within.isNull()) {
      matchedWithin = true;
    } else if (within.isNull() || other.within.isNull()) {
      matchedWithin = false;
    } else {
      DotAccessType tmpWithin = (DotAccessType) within.get().basicNormalize(false,
          tenv.ofDeclType.getModuleType(), tenv.effectiveImports, tenv.msgState).get().get();
      DotAccessType tmpOtherWithin = (DotAccessType) other.within.get().basicNormalize(false,
          tenv.ofDeclType.getModuleType(), tenv.effectiveImports, tenv.msgState).get().get();
      matchedWithin = tmpWithin.equals(tmpOtherWithin);
    }
    
    return new Pair<>(matchedWithin, numMatched);
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    List<Type> replacedArgTypes = new LinkedList<Type>();
    Optional<Nullable<Type>> tmp = Optional.empty();
    for (Type ty : argTypes) {
      tmp = ty.basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      replacedArgTypes.add(tmp.get().get());
    }
    tmp = returnType.basicNormalize(keepCurrentModRelative, currentModule, effectiveImports,
        msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    Type newReturnType = tmp.get().get();

    Nullable<DotAccessType> newWithin = within;
    if (within.isNotNull()) {
      tmp = within.get().basicNormalize(keepCurrentModRelative, currentModule, effectiveImports,
          msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      if (tmp.get().isNull()) {
        //newWithin = Nullable.empty();
      } else {
        newWithin = Nullable.of((DotAccessType) tmp.get().get());
      }
    }

    return Optional.of(Nullable.of(new FunctionType(newWithin, newReturnType, replacedArgTypes,
        lambdaStatus)));
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState, Set<ModuleType> exportedTypes, boolean skipNormalize)
      throws FatalMessageException {
    FunctionType normType = this;
    if (!skipNormalize) {
      Optional<Nullable<Type>> optType = basicNormalize(false, currentModule, effectiveImports,
          msgState);
      if (!optType.isPresent()) {
        return false;
      }
      normType = (FunctionType) optType.get().get();
    }

    if (!normType.returnType.getNormalizedModType(currentModule, effectiveImports, msgState,
        exportedTypes, true)) {
      return false;
    }

    for (Type ty : normType.argTypes) {
      if (!ty.getNormalizedModType(currentModule, effectiveImports, msgState, exportedTypes,
          skipNormalize)) {
        return false;
      }
    }

    return true;
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new NullExpression();
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    List<Type> newArgTypes = new LinkedList<>();

    for (Type argType : argTypes) {
      newArgTypes.add(argType.prependFnRetType(prependType, false));
    }

    FunctionType newType = new FunctionType(within, returnType.prependFnRetType(prependType, true),
        newArgTypes, lambdaStatus);
    if (!isTop) {
      return newType;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(newType);
    return new MultiType(types);
  }

  public FunctionType prependFnArgType(Type prependType) {
    List<Type> newArgTypes = new LinkedList<>(argTypes);

    newArgTypes.add(0, prependType);

    return new FunctionType(within, returnType, newArgTypes, lambdaStatus);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    //function pointer size (assume 64-bit)
    return 8;
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    if (!returnType.copyFromTenvIntoTenv(tenv, newTenv)) {
      return false;
    }
    if (within.instanceOf(ClassDeclType.class)) {
      if (!within.get().copyFromTenvIntoTenv(tenv, newTenv)) {
        return false;
      }
    }
    for (Type argTy : argTypes) {
      if (!argTy.copyFromTenvIntoTenv(tenv, newTenv)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Type asClassDeclType() {
    List<Type> newArgTypes = new LinkedList<>();
    for (Type argTy : argTypes) {
      newArgTypes.add(argTy.asClassDeclType());
    }
    Nullable<DotAccessType> newWithin = within;
    if (within.isNotNull()) {
      newWithin = Nullable.of((DotAccessType) within.get().asClassDeclType());
    }
    return new FunctionType(newWithin, returnType.asClassDeclType(), newArgTypes, lambdaStatus);
  }

}
