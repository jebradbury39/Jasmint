package header;

import ast.Expression;
import ast.Program.DestructorRenamings;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import multifile.Project;
import type_env.TypeEnvironment;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import typegraph.TypeGraph;
import util.ScopedIdentifiers;

public abstract class PsuedoClassHeader extends ProgItemHeader {

  public PsuedoClassHeader(int lineNum, int columnNum, long label, ModuleType moduleType,
      boolean export, String name) {
    super(lineNum, columnNum, label, moduleType, export, name);
  }
  
  public abstract UserDeclType getAbsName();

  public abstract Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, boolean isStaticTenv,
      UserInstanceType realType) throws FatalMessageException;

  public abstract List<Type> getInnerTypes();

  public abstract Optional<PsuedoClassHeader> replaceGenerics(AuditTrailLite projectAuditTrailSoFar,
      Project project, Nullable<TypeEnvironment> tenv, List<Type> realInnerTypes,
      boolean noRegister, Map<Type, Type> genericToRealMapping, MsgState msgState, int lineNum,
      int columnNum) throws FatalMessageException;

  protected abstract void populateTypeGraph(TypeGraph typeGraph) throws FatalMessageException;

  /*
   * Used for sizeof. Return -1 on error. All generics have already been replaced
   */
  public abstract int calculateSize(TypeEnvironment tenv, boolean isStatic, int lineNum,
      int columnNum) throws FatalMessageException;

  public abstract void insertDestructorCalls(ScopedIdentifiers ourScope,
      DestructorRenamings destructorRenamings);

  public abstract Expression getDummyExpression(UserInstanceType self, MsgState msgState)
      throws FatalMessageException;

  public abstract void defineInTenv(TypeEnvironment tenv);
}
