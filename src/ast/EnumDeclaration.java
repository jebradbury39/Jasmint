package ast;

import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditRenameTypes;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.EnumHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.TypeStatus;
import typecheck.AbstractType;
import typecheck.EnumDeclType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.Pair;

public class EnumDeclaration extends AbstractAst {

  public static final String NONE_ID = "none";

  public final boolean export;
  public final EnumDeclType enumType; // abs type (outer type must be module we are defined in)
  public final List<String> enumIds;

  public EnumDeclaration(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, boolean export, EnumDeclType enumType,
      List<String> enumIds) {
    super(lineNum, columnNum, AstType.ENUM_DECLARATION, preComments, postComments);
    this.export = export;
    this.enumType = enumType;
    this.enumIds = enumIds;

    if (enumType.outerType.isNull()) {
      throw new IllegalArgumentException();
    }

    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    String result = "enum " + enumType.name + " {";
    boolean first = true;
    for (String id : enumIds) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += id;
    }
    return result + "}";
  }

  public EnumHeader generateHeader(ModuleType definingModule, EffectiveImports effectiveImports,
      MsgState msgState) throws FatalMessageException {
    return new EnumHeader(lineNum, columnNum, label,
        definingModule, enumType.name, (EnumDeclType) enumType
            .basicNormalize(false, definingModule, effectiveImports, msgState).get().get(),
        export, enumIds);
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {

    // The enum is now complete (we know the full size)
    tenv.project.addCompletedClass(enumType);

    // validate that our type is in the tenv (from EnumHeader)
    Optional<TypeStatus> typeStatus = tenv.lookupType(enumType, lineNum, columnNum);
    if (!typeStatus.isPresent()) {
      return Optional.empty();
    }
    if (!typeStatus.get().isComplete()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "enum type '" + enumType + "' is not complete: " + typeStatus.get(), lineNum, columnNum);
    }

    // validate that all ids are unique. The first ID must always be the NONE_ID
    // (this is never
    // renamed)
    boolean hitErr = false;
    if (!enumIds.get(0).equals(NONE_ID)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "First item of enum " + enumType
          + " must be '" + NONE_ID + "' not '" + enumIds.get(0) + "'", lineNum, columnNum);
    }

    Set<String> idSet = new HashSet<>();
    for (String id : enumIds) {
      if (idSet.contains(id)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "enum " + enumType + " has duplicate id: " + id, lineNum, columnNum);
        hitErr = true;
        continue;
      }
      idSet.add(id);
    }
    if (hitErr) {
      return Optional.empty();
    }

    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.EnumDeclaration.Builder subDoc = AstProto.EnumDeclaration.newBuilder();

    subDoc.setExport(export);
    subDoc.setEnumType(enumType.serialize());
    subDoc.addAllEnumIds(enumIds);

    AstProto.Ast.Builder document = preSerialize();
    document.setEnumDeclaration(subDoc);
    return document;
  }

  public static EnumDeclaration deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments, long ordering,
      AstProto.EnumDeclaration document) throws SerializationError {
    List<String> enumIds = new LinkedList<>(document.getEnumIdsList());

    EnumDeclaration value = new EnumDeclaration(columnNum, columnNum, postComments, postComments,
        document.getExport(), (EnumDeclType) AbstractType.deserialize(document.getEnumType()),
        enumIds);
    value.setOrdering(ordering);
    return value;
  }

  public static EnumDeclaration deserialize(AstProto.Ast document) throws SerializationError {
    int deLineNum = document.getLineNum();
    int deColumnNum = document.getColNum();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();
    Pair<AstType, Long> tmp = AbstractAst.preDeserialize(document, preComments, postComments);
    long ordering = tmp.b;

    Type deDeterminedType = AbstractType.deserialize(document.getDeterminedType());
    EnumDeclaration value = deserialize(deLineNum, deColumnNum, preComments, postComments, ordering,
        document.getEnumDeclaration());
    value.determinedType = deDeterminedType;

    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof EnumDeclaration)) {
      return false;
    }
    EnumDeclaration ourOther = (EnumDeclaration) other;
    if (!enumType.equals(ourOther.enumType)) {
      return false;
    }

    return enumIds.equals(ourOther.enumIds);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<>(new LinkedList<>(), this);
  }

  public EnumDeclaration replaceType(Map<Type, Type> mapFromTo) {
    Type replTy = enumType.replaceType(mapFromTo);
    EnumDeclaration val = new EnumDeclaration(lineNum, columnNum, preComments, postComments, export,
        (EnumDeclType) replTy, enumIds);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return enumType.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  public EnumDeclaration renameIds(Renamings globalRenamings, MsgState msgState)
      throws FatalMessageException {
    List<String> newEnumIds = new LinkedList<>();
    for (String id : enumIds) {
      Nullable<String> newId = globalRenamings.lookup(id, Nullable.empty(), Nullable.of(enumType));
      if (newId.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
            "Enum declaration failed renamings: " + id);
      }
      newEnumIds.add(newId.get());
    }

    EnumDeclaration val = new EnumDeclaration(lineNum, columnNum, preComments, postComments, export,
        enumType, newEnumIds);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  public void populateTypeRenamings(ModuleType newModuleType, AuditRenameTypes auditEntry) {
    final String newName = enumType.name + "_ET_" + getTemp();
    
    auditEntry.typeRenamings.put(enumType,
        new EnumDeclType(
            Nullable.of(newModuleType),
            newName));

    auditEntry.relativeTypeRenamings.put(new EnumDeclType(Nullable.empty(), enumType.name),
        new EnumDeclType(Nullable.empty(), newName));
  }

}
