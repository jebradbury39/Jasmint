package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.MemValue;
import interp.Value;
import interp.VoidValue;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class AssignmentStatement extends AbstractStatement {

  public final Lvalue target;
  public final Expression source;

  @Override
  public String toString(String indent) {
    return preToString() + target.toString(indent) + " = " + source.toString(indent)
        + postToString();
  }

  public AssignmentStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Lvalue target, Expression source) {
    super(lineNum, columnNum, AstType.ASSIGNMENT_STATEMENT, preComments, postComments);
    this.target = target;
    this.source = source;

    target.setParentAst(this);
    source.setParentAst(this);

    determinedType = VoidType.Create();
  }

  public AssignmentStatement(int lineNum, int columnNum, Lvalue target, Expression source) {
    super(lineNum, columnNum, AstType.ASSIGNMENT_STATEMENT);
    this.target = target;
    this.source = source;

    target.setParentAst(this);
    source.setParentAst(this);

    determinedType = VoidType.Create();
  }

  public AssignmentStatement(Lvalue target, Expression source) {
    super(AstType.ASSIGNMENT_STATEMENT);
    this.target = target;
    this.source = source;

    target.setParentAst(this);
    source.setParentAst(this);

    determinedType = VoidType.Create();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> sourceType = source.typecheck(tenv, Nullable.empty());
    // say this is A, then source must be an A or B (DOWN)
    Optional<TypecheckRet> targetType = target.typecheck(tenv, Nullable.empty());

    if (!sourceType.isPresent() || !targetType.isPresent()) {
      return Optional.empty();
    }
    /* smaller types can fit in the larger or more generic ones */
    if (!tenv.canSafeCast(sourceType.get().type, targetType.get().type)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "cannot assign source type "
          + sourceType.get().type + " to target type " + targetType.get().type, lineNum, columnNum);
    }
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value sourceValue = source.interp(env).copy(null);
    Value targetValue = target.interp(env);

    if (!(targetValue instanceof MemValue)) {
      throw new InterpError("Unable to assign to a non-memory location", lineNum, columnNum);
    }
    ((MemValue) targetValue).location.setValue(sourceValue);
    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.AssignmentStatement.Builder subDoc = AstProto.AssignmentStatement.newBuilder();

    subDoc.setTarget(target.serialize().build());
    subDoc.setSource(source.serialize().build());

    document.setAssignmentStatement(subDoc);
    return document;
  }

  public static AssignmentStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.AssignmentStatement document) throws SerializationError {
    return new AssignmentStatement(lineNum, columnNum, preComments, postComments,
        AbstractLvalue.deserialize(document.getTarget()),
        AbstractExpression.deserialize(document.getSource()));
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, target.renameIds(renamings, msgState), source.renameIds(renamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = target.findFreeVariables();
    freeVars.putAll(source.findFreeVariables());
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof AssignmentStatement)) {
      return false;
    }
    AssignmentStatement ourOther = (AssignmentStatement) other;
    return target.compare(ourOther.target) && source.compare(ourOther.source);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, (Lvalue) this.target.replace(target, with),
        (Expression) source.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = target.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    return source.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> sourcePair = source.toAnf(tuning);
    insert.addAll(sourcePair.a);
    Pair<List<DeclarationStatement>, Ast> targetPair = target.toAnf(tuning);
    insert.addAll(targetPair.a);

    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, (Lvalue) targetPair.b, (Expression) sourcePair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, target.replaceType(mapFromTo),
        source.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Optional<Ast> tmpTarget = this.target.replaceVtableAccess(project, msgState);
    Optional<Ast> tmpSource = source.replaceVtableAccess(project, msgState);
    if (!tmpTarget.isPresent() || !tmpSource.isPresent()) {
      return Optional.empty();
    }
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, (Lvalue) tmpTarget.get(), (Expression) tmpSource.get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Optional<Nullable<Expression>> tmpSource = source.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmpSource.isPresent()) {
      return Optional.empty();
    }
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, this.target.normalizeType(keepCurrentModRelative, currentModule,
            effectiveImports, msgState),
        tmpSource.get().get());
    Optional<Nullable<Type>> tmp = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    value.determinedType = tmp.get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    target.addDependency(modDepGraph, ourModNode, tenv);
    source.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return target.containsType(types) || source.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return target.containsAst(asts) || source.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = target.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Lvalue newTarget = (Lvalue) tmp.get().ast.get();

    tmp = source.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newSource = (Expression) tmp.get().ast.get();

    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, newTarget, newSource);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, target,
        source.insertDestructorCalls(scopedIds, destructorRenamings));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new InsertDestructorsRet(value);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, target.liftClassDeclarations(liftedClasses, replacements).get(),
        source.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, target.resolveUserTypes(msgState).get(),
        source.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    AssignmentStatement value = new AssignmentStatement(lineNum, columnNum, preComments,
        postComments, target.addThisExpression(classHeader),
        source.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }
}
