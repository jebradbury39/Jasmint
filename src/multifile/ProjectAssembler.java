package multifile;

import ast.Program;
import audit.AuditTrail;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.RegisteredGenerics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import mbo.ModuleBuildObj;
import mbo.ModuleBuildObj.ProgressStatus;
import mbo.Vtables;
import multifile.BuildManifest.BuildEntry;
import org.json.JSONArray;
import org.json.JSONObject;
import typecheck.ModuleType;

/*
 * This class takes the json config file and builds the corresponding Project class.
 * If missing a dependency, give alert (TODO download it)
 */
public class ProjectAssembler {
  
  public final Path jsonConfigFilename;
  public final Path jsonConfigDir;
  public final String osPathSep;
  //in case this project is included in another project, you will have multiple build
  //dirs/manifests. The local one (in the other project) takes precedence.
  private final Nullable<BuildManifest> localManifest;
  private final MsgState msgState = new MsgState(Nullable.empty());
  
  /*
   * This is the standard setup. Too many problems in CS arise from the lack of integrated
   * build systems with
   * good error reporting. Just look at C/C++, and the absolute mess of build systems there.
   * You have Make, CMake,
   * Bazel, ect... and that isn't counting all the proprietary build systems which are impossible
   * to search for
   * because the only people who really know how to use those monsters are the people who made
   * them, catering
   * to every special case the company has. These special cases arise from poor understanding of
   * how to structure
   * a project, and from the desire to perform HACKZZZZ. Hacks are the reason we can't have nice,
   * maintainable code.
   * The proliferation of build systems is a blight upon C coders everywhere. This is why we will
   * (at least attempt to)
   * enforce a layout to how JASMINT code is organized as a collection of source files.
   */
  public ProjectAssembler(String jsonConfigFilename, Nullable<BuildManifest> localManifest) {
    this.jsonConfigFilename = new File(jsonConfigFilename).toPath().toAbsolutePath();
    this.jsonConfigDir = new File(jsonConfigFilename).toPath().toAbsolutePath().getParent();
    this.osPathSep = File.separator;
    this.localManifest = localManifest;
  }
  
  /*
   * Special case of a single file, and the user did not want to structure this as a project.
   * While not recommended, this is geared towards small scripts and rapid testing.
   */
  public static Project assembleSimpleProject(
      String sourceFilename, boolean fromSrc)
      throws Exception {
    Map<ModuleType, ModuleEndpoint> sources = new HashMap<>();
    Program prog = null;
    if (fromSrc) {
      prog = Program.loadJasmintSrc(sourceFilename, false);
      sourceFilename = "<from_src>";
    } else {
      sourceFilename = new File(sourceFilename).getAbsolutePath();
      prog = Program.loadJasmintFile(sourceFilename);
    }
    String readWriteDir = new File("dist").getAbsolutePath();
    BuildManifest manifest = new BuildManifest(readWriteDir, readWriteDir);
    MsgState msgState = new MsgState(Nullable.of(sourceFilename));
    Map<String, MsgState> msgStates = new HashMap<>();
    msgStates.put(sourceFilename, msgState);
    
    
    Project proj = new Project(new ArrayList<>(), "<single file project - default>",
        new ProjectNameVersion(false, "simple", 0, 1),
        sources, new HashSet<>(), new File("dist").toPath(),
        new HashMap<>(), msgStates, manifest);
    
    ModuleBuildObj mbo = new ModuleBuildObj(prog.moduleStatement.fullType,
        new HashSet<>(), prog, prog.generateHeader(proj, msgState),
        new AuditTrail(prog.moduleStatement.fullType), ProgressStatus.NONE,
        prog.getEffectiveImports(proj, false), new Vtables());
    LoadedModuleHandle moduleHandle = new LoadedModuleHandle(sourceFilename, true, manifest, mbo,
        new RegisteredGenerics(new HashSet<>()));
    sources.put(prog.moduleStatement.fullType, new ModuleEndpoint(moduleHandle, msgState));
    proj.addAllSources(sources);
    //TODO add standard libs
    
    Project globalProj = addJsmntGlobal(proj.manifest);
    globalProj.setRootProject(proj);
    List<Project> libs = new LinkedList<>();
    libs.add(globalProj);
    proj.setBuildLibs(libs, msgState);
    proj.addLib(globalProj, msgState);
    
    proj.initBuildManifest();
    return proj;
  }
  
  /*
   * add as top-level lib
   */
  private static Project addJsmntGlobal(BuildManifest rootProjManifest) throws Exception {
    Program prog = Program.loadJasmintSrc(JsmntGlobal.src, true);
    
    MsgState msgState = new MsgState(Nullable.of(JsmntGlobal.filename));
    Map<String, MsgState> msgStates = new HashMap<>();
    msgStates.put(JsmntGlobal.filename, msgState);
    
    File buildto = new File(rootProjManifest.writeBuildDir + File.separator + "jsmnt_global");
    
    //attempt to locate and load the build manifest from the build dir
    File manifestPath = new File(buildto.toString() + File.separator
        + BuildManifest.MANIFEST_FILENAME);
    BuildManifest manifest = null;
    if (manifestPath.isFile()) {
      manifest = BuildManifest.deserializeFromFile(manifestPath, buildto.toString());
    } else {
      //create empty manifest
      manifest = new BuildManifest(buildto.toString(), buildto.toString());
    }
    
    Map<ModuleType, ModuleEndpoint> globalSources = new HashMap<>();
    Project proj = new Project(new ArrayList<>(), "<jsmnt_global>",
        JsmntGlobal.nameVersion,
        globalSources, new HashSet<>(), buildto.toPath(),
        new HashMap<>(), msgStates, manifest);
    
    ModuleBuildObj mbo = new ModuleBuildObj(prog.moduleStatement.fullType,
        new HashSet<>(), prog, prog.generateHeader(proj, msgState),
        new AuditTrail(prog.moduleStatement.fullType), ProgressStatus.NONE,
        prog.getEffectiveImports(proj, false), new Vtables());
    LoadedModuleHandle moduleHandle = new LoadedModuleHandle(JsmntGlobal.filename, true,
        manifest, mbo,
        new RegisteredGenerics(new HashSet<>()));
    globalSources.put(prog.moduleStatement.fullType, new ModuleEndpoint(moduleHandle, msgState));
    proj.addAllSources(globalSources);
    
    return proj;
  }
  
  private static String readFile(Path filename) throws IOException {
    String result = "";
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(filename.toString()));
      StringBuilder sb = new StringBuilder();
      String line = br.readLine();
      while (line != null) {
        sb.append(line);
        line = br.readLine();
      }
      result = sb.toString();
      br.close();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    } finally {
      if (br != null) {
        br.close();
      }
    }
    return result;
  }
  
  private JSONObject parseJson() throws IOException {
    String jsonString = readFile(jsonConfigFilename);
    return new JSONObject(jsonString);
  }
  
  private String resolveFileTilde(String path) {
    String prefix = "~" + File.separator;
    if (path.startsWith(prefix) || path.startsWith("~/")) {
      path = System.getProperty("user.home") + path.substring(1);
    } else if (path.startsWith("~")) {
      throw new UnsupportedOperationException(
          "Home dir expansion not implemented for explicit usernames: " + path);
    }
    return path;
  }
  
  private List<Path> getDirsFromJson(JSONObject json, String key) {
    List<Path> dirs = new ArrayList<Path>();
    if (json.has(key)) {
      JSONArray jarr = json.getJSONArray(key);
      for (int i = 0; i < jarr.length(); ++i) {
        String dirname = jarr.getString(i).trim();
        if (dirname.isEmpty() || dirname.equals(".") || dirname.equals("..")) {
          continue;
        }
        
        dirname = resolveFileTilde(dirname);
        Path dir = new File(dirname).toPath();
        if (!dir.isAbsolute()) {
          dir = new File(jsonConfigDir + osPathSep + dirname).toPath().toAbsolutePath();
        }
        dirs.add(dir);
      }
    }
    return dirs;
  }
  
  // Locates all sources and dependencies and parses them, then stores them in a project
  //(acts recursively for libs)
  public Optional<Project> assemble() throws Exception {
    //Maps all projects we find into one dictionary
    Map<ProjectNameVersion, Project> projectMap = new HashMap<>();
    
    /*
     * Find this project as well as all import_libs and track which dependency+versions each project
     * has as well as which dependencies are exported.
     */
    Optional<Project> rootProj = assemble(Nullable.empty(), projectMap);
    if (!rootProj.isPresent()) {
      return Optional.empty();
    }
    
    Project globalProj = addJsmntGlobal(rootProj.get().manifest);
    projectMap.put(globalProj.projectNameVersion, globalProj);
    
    /*
     * Construct project graph, then determine proper order. There can be no cycles here
     */
    ProjectGraph graph = new ProjectGraph(projectMap);
    if (graph.checkForCycle(msgState)) {
      return Optional.empty();
    }
    Optional<List<Project>> rootProjLibs = graph.validateExportedLibs(
        rootProj.get().projectNameVersion,
        msgState);
    if (!rootProjLibs.isPresent()) {
      return Optional.empty();
    }
    
    rootProj.get().setBuildLibs(rootProjLibs.get(), msgState);
    
    /*
     * init build manifests
     */
    for (Project proj : projectMap.values()) {
      proj.initBuildManifest();
    }
    
    // check for errors
    if (msgState.hitError()) {
      return Optional.empty();
    }
    
    return rootProj;
  }

  /*
   * Load all the projects into the map and parse the program files so that we can analyze
   * which ones export other projects.
   */
  private Optional<Project> assemble(Nullable<Project> rootProj, Map<ProjectNameVersion,
      Project> projectMap) throws Exception {
    JSONObject json = parseJson();
    
    //read authors
    List<String> authors = new ArrayList<String>();
    if (json.has("authors")) {
      JSONArray jarr = json.getJSONArray("authors");
      for (int i = 0; i < jarr.length(); ++i) {
        authors.add(jarr.getString(i));
      }
    }
    
    //read name
    String name = "";
    if (json.has("name")) {
      name = json.getString("name");
      if (!ProjectNameVersion.validProjectName(name)) {
        msgState.addMessage(MsgType.ERROR, MsgClass.PROJECT_ASSEMBLE,
            "invalid project name \"" + name + "\" in file: "
                + jsonConfigFilename.toAbsolutePath());
        
      }
    }
    
    //read description
    String description = "";
    if (json.has("description")) {
      description = json.getString("description");
    }
    
    //read Major and Minor version
    int majorVersion = 0;
    int minorVersion = 1;
    if (json.has("version")) {
      JSONArray jarr = json.getJSONArray("version");
      if (jarr.length() == 2) {
        majorVersion = jarr.getInt(0);
        minorVersion = jarr.getInt(1);
        if (majorVersion < 0 || minorVersion < 0) {
          throw new IllegalArgumentException("Invalid major, minor versions. "
              + "Cannot be less than zero.");
        }
      }
    }
    
    Set<ProjectNameVersion> desiredLibs = new HashSet<>();
    desiredLibs.add(JsmntGlobal.desiredNameVersion);
    if (json.has("import_libs")) {
      JSONArray jarr = json.getJSONArray("import_libs");
      for (int idx = 0; idx < jarr.length(); ++idx) {
        String item = jarr.getString(idx);
        Optional<ProjectNameVersion> tmp = ProjectNameVersion.Create(false, item,
            jsonConfigFilename.toAbsolutePath().toString(), msgState);
        if (!tmp.isPresent()) {
          continue;
        }
        desiredLibs.add(tmp.get());
      }
    }
    
    //read roots (source code for this project)
    List<Path> srcDirectories = getDirsFromJson(json, "roots");
    if (srcDirectories.isEmpty()) {
      throw new IllegalArgumentException("Must specifiy at least one root directory for the "
          + "source code.");
    }
    
    //read providers paths
    Map<String, List<Path>> providersPaths = new HashMap<>();
    if (json.has("providers")) {
      JSONObject jsonProviders = json.getJSONObject("providers");
      Iterator<String> pkeyIter = jsonProviders.keys();
      while (pkeyIter.hasNext()) {
        String pkey = pkeyIter.next();
        providersPaths.put(pkey, getDirsFromJson(jsonProviders, pkey));
      }
    }
    
    //read libs (third party projects which this project depends upon)
    final List<Path> libDirectories = getDirsFromJson(json, "libs");
    
    //read buildto (directory for build files)
    Path buildto = new File(jsonConfigDir.toString() + osPathSep + "dist").toPath();
    if (json.has("buildto")) {
      buildto = new File(jsonConfigDir.toString() + osPathSep + json.getString("buildto")).toPath();
    }
    
    //attempt to locate and load the build manifest from the build dir
    File manifestPath = new File(buildto.toString() + File.separator
        + BuildManifest.MANIFEST_FILENAME);
    Nullable<BuildManifest> manifest = Nullable.empty();
    if (manifestPath.isFile()) {
      manifest = Nullable.of(BuildManifest.deserializeFromFile(manifestPath, buildto.toString()));
    } else {
      //create empty manifest
      manifest = Nullable.of(new BuildManifest(buildto.toString(), buildto.toString()));
    }
    
    Map<String, MsgState> msgStates = new HashMap<>();
    
    Map<ModuleType, ModuleEndpoint> sources = new HashMap<>();
    Map<String, Map<ModuleType, ModuleEndpoint>> providers = new HashMap<>();
    
    boolean isFfi = false;
    if (json.has("ffi")) {
      isFfi = json.getBoolean("ffi");
    }
    
    Project proj = new Project(authors, description,
        new ProjectNameVersion(isFfi, name,  majorVersion, minorVersion),
        sources, desiredLibs,
        buildto, providers, msgStates, manifest.get());
    if (rootProj.isNotNull()) {
      proj.setRootProject(rootProj.get());
    }
    
    //find all sources and parse them (into list of Program)
    proj.addAllSources(locateAndParseSources(proj, srcDirectories, buildto,
        manifest, localManifest, msgStates));
    
    //find all providers and parse them
    for (Entry<String, List<Path>> provider : providersPaths.entrySet()) {
      //create a new manifest for each provider
      proj.providers.put(provider.getKey(),
          locateAndParseSources(proj, provider.getValue(), buildto, manifest,
              localManifest, msgStates));
      for (ModuleEndpoint src : proj.providers.get(provider.getKey()).values()) {
        src.project = proj;
      }
    }
    
    //find all libs and parse them (into list of Project)
    locateAndParseLibs(rootProj.isNull() ? Nullable.of(proj) : rootProj, libDirectories,
        rootProj.isNull() ? manifest : localManifest, projectMap);
    
    projectMap.put(proj.projectNameVersion, proj);
    return Optional.of(proj);
  }
  
  private List<File> locateFilesWithExt(Path dirname, String ext, boolean deepsearch)
      throws IOException {
    List<File> results = new ArrayList<>();
    File root = dirname.toFile();

    //Visit dir files first, then children dirs (go into queue)
    
    List<File> queue = new LinkedList<>();
    queue.add(root);
    while (!queue.isEmpty()) {
      File dir = queue.remove(0);
      List<File> files = new LinkedList<>();
      List<File> dirs = new LinkedList<>();
      
      if (dir.isDirectory()) {
        File[] children = dir.listFiles();
        for (File child : children) {
          BasicFileAttributes childAttrs = Files.readAttributes(child.toPath(),
              BasicFileAttributes.class);
          if (childAttrs.isSymbolicLink()) {
            continue;
          }
          if (child.isDirectory()) {
            dirs.add(child);
          } else if (child.isFile()) {
            files.add(child);
          }
        }
      } else if (dir.isFile()) {
        files.add(dir);
      }
      
      int matchCount = 0;
      for (File file : files) {
        //split on dots, check the last one matches ext
        List<String> filenameSplit = util.StringFn.split('.',
            file.toPath().getFileName().toString());
        if (filenameSplit.get(filenameSplit.size() - 1).equals(ext)) {
          results.add(file);
          matchCount += 1;
          if (!deepsearch) {
            dirs.clear(); //don't visit subdirs
            if (matchCount > 1) {
              throw new IllegalArgumentException("Cannot have multiple files with ext \""
                  + ext + "\" with directory: " + dir);
            }
          }
        }
      }
      
      queue.addAll(dirs); //recursively visit subdirs AFTER visiting all files
    }
    
    return results;
  }

  /*
   * localManifest takes precedence. Note that the localManifest could point the general AST to a
   * remote location, but the generics file to a local location.
   */
  private Map<ModuleType, ModuleEndpoint> locateAndParseSources(Project project,
      List<Path> srcDirectories, Path buildDir, Nullable<BuildManifest> manifest,
      Nullable<BuildManifest> localManifest, Map<String, MsgState> msgStates)
      throws Exception {
    
    Map<ModuleType, ModuleEndpoint> sources = new HashMap<>();
    for (Path srcDir : srcDirectories) {
      List<File> srcFiles = locateFilesWithExt(srcDir, "jsmnt", true);
      for (File srcFile : srcFiles) {
        msgStates.put(srcFile.getAbsolutePath(),
            new MsgState(Nullable.of(srcFile.getAbsolutePath())));
        
        Nullable<BuildManifest> fileManifest = manifest;
        //if (localManifest.isNotNull()) {
        //  fileManifest = localManifest;
        //}
        if (fileManifest.isNotNull()) {
          //check localManifest for entry. Could change location of generics file from our
          //dist dir to another
          
          byte[] currentChecksum = BuildManifest.computeChecksum(new FileInputStream(srcFile));
          Nullable<BuildEntry> manifestEntry = fileManifest.get().getEntry(srcFile.getPath());
          if (manifestEntry.isNotNull()) {
            if (Arrays.equals(currentChecksum, manifestEntry.get().checksum)) {
              //ensure this is not a duplicate
              if (sources.containsKey(manifestEntry.get().moduleType)) {
                msgStates.get(srcFile.getAbsolutePath()).addMessage(MsgType.ERROR,
                    MsgClass.TYPECHECK, "Module '" + manifestEntry.get().moduleType
                    + " is already declared by file '"
                    + sources.get(manifestEntry.get().moduleType).getHandle().srcFilename);
                continue;
              }
              //no changes, so just create a StoredModuleHandle
              sources.put(manifestEntry.get().moduleType,
                  new ModuleEndpoint(new StoredModuleHandle(srcFile.getAbsolutePath(),
                      manifestEntry.get().moduleType,
                      manifestEntry.get().submods, manifestEntry.get().effectiveImports, false,
                      manifest.get(),
                      manifestEntry.get().getBuildFile(false),
                      manifestEntry.get().getGenericsFile(false)),
                      msgStates.get(srcFile.getAbsolutePath())));
              continue;
            }
          }
        }
        //need to rebuild, parse the changed/new src file
        
        Program mboProgram = Program.loadJasmintFile(srcFile.getAbsolutePath());
        
        List<ModuleType> mboSubmods = mboProgram.gatherSubmods();
        Set<ModuleType> mboSubmodSet = new HashSet<>();
        for (ModuleType submod : mboSubmods) {
          if (mboSubmodSet.contains(submod)) {
            msgStates.get(srcFile.getAbsolutePath()).addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
                "Duplicate submod statement found: '" + submod + "'");
            continue;
          }
          mboSubmodSet.add(submod);
        }
        
        ModuleBuildObj mbo = new ModuleBuildObj(mboProgram.moduleStatement.fullType,
            mboSubmodSet, mboProgram,
            mboProgram.generateHeader(project, msgStates.get(srcFile.getAbsolutePath())),
            new AuditTrail(mboProgram.moduleStatement.fullType),
            ProgressStatus.NONE, mboProgram.getEffectiveImports(project, false),
            new Vtables());
        //ensure this is not a duplicate
        if (sources.containsKey(mbo.moduleType)) {
          msgStates.get(srcFile.getAbsolutePath()).addMessage(MsgType.ERROR,
              MsgClass.TYPECHECK, "Module '" + mbo.moduleType
              + " is already declared by file '"
              + sources.get(mbo.moduleType).getHandle().srcFilename);
          continue;
        }
        sources.put(mbo.moduleType, new ModuleEndpoint(
            new LoadedModuleHandle(srcFile.getAbsolutePath(), true, manifest.get(), mbo,
                new RegisteredGenerics(new HashSet<>())),
            msgStates.get(srcFile.getAbsolutePath())));
      }
    }
    return sources;
  }
  
  private List<Project> locateAndParseLibs(Nullable<Project> rootProj, List<Path> libDirectories,
      Nullable<BuildManifest> rootManifest, Map<ProjectNameVersion, Project> projectMap)
          throws Exception {
    List<Project> libs = new ArrayList<>();
    for (Path libDir : libDirectories) {
      List<File> libConfigFiles = locateFilesWithExt(libDir, "jsmnt-build", false);
      for (File libConfigFile : libConfigFiles) {
        Optional<Project> tmp = new ProjectAssembler(libConfigFile.getAbsolutePath(), rootManifest)
            .assemble(rootProj, projectMap);
        if (!tmp.isPresent()) {
          continue;
        }
        libs.add(tmp.get());
      }
    }
    return libs;
  }

  public String printErrors() {
    return msgState.toString();
  }
}
