package type_env;

import errors.FatalMessageException;
import header.EnumHeader;
import java.util.Map;
import java.util.Optional;
import typecheck.ImportType;
import typecheck.Type;

public class EnumTypeBox extends UserDeclTypeBox {

  public final EnumHeader enumHeader;
  
  public EnumTypeBox(EnumHeader enumHeader, boolean export, VariableScoping scoping) {
    super(export, true, scoping);
    this.enumHeader = enumHeader;
  }

  @Override
  public TypeBox fullCopy() {
    return new EnumTypeBox(enumHeader, export, scoping);
  }

  @Override
  public TypeBox replaceType(TypeEnvironment tenv, Map<Type, Type> mapFromTo, int lineNum,
      int columnNum) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  protected Optional<TypeEnvironment> getTenv(TypeEnvironment tenv) throws FatalMessageException {
    return enumHeader.getTenv(tenv, true, enumHeader.absName.asInstanceType());
  }

  @Override
  public Type getType() {
    return enumHeader.absName;
  }

  @Override
  public ImportType getImportType(ImportType importType) {
    return enumHeader.absName.asImportType();
  }

}
