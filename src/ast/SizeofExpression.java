package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.SizeofTarget;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UndeterminedType;
import util.Pair;
import util.ScopedIdentifiers;

public class SizeofExpression extends AbstractExpression {

  public final SizeofTarget target; // we want to calculate the size of this

  private int calculatedSize = -1; // find this out during typecheck and save
  
  private Type determinedSizeofType = UndeterminedType.Create(); // only if target is a type

  public SizeofExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, SizeofTarget target) {
    super(lineNum, columnNum, AstType.SIZEOF_EXPRESSION, preComments, postComments);
    this.target = target;

    determinedType = IntType.Create(false, 32);
  }
  
  public SizeofExpression(SizeofTarget target) {
    super(AstType.SIZEOF_EXPRESSION);
    this.target = target;

    determinedType = IntType.Create(false, 32);
  }
  
  public int getCalculatedSize() {
    if (calculatedSize == -1) {
      throw new IllegalArgumentException(
          "must run typecheck before using calculated size from sizeof. "
          + "Note that resolveExceptions will add sizeof expressions");
    }
    return calculatedSize;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    SizeofExpression val = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        target.renameIdsSizeof(renamings, msgState));
    val.calculatedSize = calculatedSize;
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    SizeofExpression val = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        target.replaceTypeSizeof(mapFromTo));
    val.calculatedSize = calculatedSize;
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    SizeofExpression val = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        target
            .normalizeTypeSizeof(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get());
    val.calculatedSize = calculatedSize;
    val.setOriginalLabel(val.label);
    return Optional.of(Nullable.of(val));
  }

  @Override
  public String toString(String indent) {
    return "sizeof(" + target + ")";
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {

    Optional<TypecheckRet> targetType = target.typecheck(tenv, expectedReturnType);
    if (!targetType.isPresent()) {
      return Optional.empty();
    }
    
    if (target instanceof Type) {
      determinedSizeofType = ((Type) target).normalize(tenv, lineNum, columnNum).get();
    }

    /*
     * We could do: targetType.get().type.calculateSize(tenv, lineNum, columnNum);
     * However, if doing sizeof on an expression, then this would lose extra info,
     * like the length of an array/map/string
     */
    int size = target.calculateSize(tenv, lineNum, columnNum);
    if (size == -1) {
      return Optional.empty();
    }
    calculatedSize = size;
    return Optional.of(new TypecheckRet(Nullable.empty(), determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Builder serialize() throws SerializationError {

    AstProto.SizeofExpression.Builder subDoc = AstProto.SizeofExpression.newBuilder();

    subDoc.setTargetIsExpr(target instanceof Expression);
    if (target instanceof Expression) {
      subDoc.setExpression(((Expression) target).serialize());
    } else {
      subDoc.setType(((Type) target).serialize());
    }
    
    subDoc.setCalculatedType(calculatedSize);

    AstProto.Ast.Builder document = preSerialize();
    document.setSizeofExpression(subDoc);
    return document;
  }

  public static SizeofExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.SizeofExpression document) throws SerializationError {

    SizeofTarget target = null;
    if (document.getTargetIsExpr()) {
      target = AbstractExpression.deserialize(document.getExpression());
    } else {
      target = AbstractType.deserialize(document.getType());
    }

    SizeofExpression val = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        target);
    val.calculatedSize = document.getCalculatedType();
    return val;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return target.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof SizeofExpression)) {
      return false;
    }
    SizeofExpression ourOther = (SizeofExpression) other;
    return ourOther.target.compareSizeof(target);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    SizeofTarget newTarget = this.target;
    if (target instanceof Expression) {
      newTarget = (Expression) ((Expression) target).replace(target, with);
    }

    SizeofExpression value = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        newTarget);
    value.calculatedSize = calculatedSize;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return target.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    SizeofTarget newTarget = target;

    if (target instanceof Expression) {
      Pair<List<DeclarationStatement>, Ast> targetPair = ((Expression) target).toAnf(tuning);
      insert.addAll(targetPair.a);
      newTarget = (Expression) targetPair.b;
    }

    SizeofExpression value = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        newTarget);
    value.calculatedSize = calculatedSize;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {

    SizeofTarget newTarget = target;

    if (target instanceof Expression) {
      newTarget = (Expression) ((Expression) target).replaceVtableAccess(project, msgState).get();
    }

    SizeofExpression value = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        newTarget);
    value.calculatedSize = calculatedSize;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    target.addDependency(modDepGraph, ourModNode, tenv, lineNum, columnNum);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return target.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return target.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize,
      MsgState msgState)
      throws FatalMessageException {
    List<Statement> preStatements = new LinkedList<>();

    SizeofTarget newTarget = target;

    if (target instanceof Expression) {
      Optional<ResolveExcRet> tmp = ((Expression) target).resolveExceptions(project,
          currentModule, auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement,
          enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newTarget = (Expression) tmp.get().ast.get();
    }

    SizeofExpression value = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        newTarget);
    value.calculatedSize = calculatedSize;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
        "Cannot calculate size of sizeof expression", lineNum, columnNum);
    return -1;
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    SizeofTarget newTarget = target;

    if (target instanceof Expression) {
      newTarget = (Expression) ((Expression) target).insertDestructorCalls(scopedIds,
          destructorRenamings);
    }

    SizeofExpression value = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        newTarget);
    value.calculatedSize = calculatedSize;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    SizeofTarget newTarget = target;

    if (target instanceof Expression) {
      newTarget = ((Expression) target).liftClassDeclarations(liftedClasses, replacements).get();
    }

    SizeofExpression value = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        newTarget);
    value.calculatedSize = calculatedSize;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    SizeofTarget newTarget = determinedSizeofType;

    if (target instanceof Expression) {
      newTarget = ((Expression) target).resolveUserTypes(msgState).get();
    }

    SizeofExpression value = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        newTarget);
    value.calculatedSize = calculatedSize;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    SizeofTarget newTarget = determinedSizeofType;

    if (target instanceof Expression) {
      newTarget = ((Expression) target).addThisExpression(classHeader);
    }

    SizeofExpression value = new SizeofExpression(lineNum, columnNum, preComments, postComments,
        newTarget);
    value.calculatedSize = calculatedSize;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
