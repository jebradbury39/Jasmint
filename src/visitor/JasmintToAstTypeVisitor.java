package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import errors.Nullable;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.antlr.v4.runtime.tree.TerminalNode;
import typecheck.ArrayType;
import typecheck.BoolType;
import typecheck.CharType;
import typecheck.DotAccessType;
import typecheck.Float32Type;
import typecheck.Float64Type;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.GenericType;
import typecheck.IntType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.MultiType;
import typecheck.ReferenceType;
import typecheck.ReferencedType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.UndeterminedUserDeclType;
import typecheck.UndeterminedUserInstanceType;
import typecheck.VoidType;

public class JasmintToAstTypeVisitor extends JasmintBaseVisitor<Type> {
  
  final JasmintToAstProgramVisitor programVisitor;
  
  public JasmintToAstTypeVisitor(JasmintToAstProgramVisitor programVisitor) {
    this.programVisitor = programVisitor;
  }
  
  @Override
  public Type visitType(JasmintParser.TypeContext ctx) {
    Type type = visit(ctx.wrappedType());
    type = type.setConst(ctx.CONST() != null);
    return type;
  }
  
  @Override
  public Type visitBoolType(JasmintParser.BoolTypeContext ctx) {
    return BoolType.Create();
  }
  
  @Override
  public Type visitCharType(JasmintParser.CharTypeContext ctx) {
    return CharType.Create();
  }
  
  @Override
  public Type visitIntType(JasmintParser.IntTypeContext ctx) {
    String ty = ctx.getText();
    int size = 0;
    boolean signed = !ty.startsWith("u");
    if (signed) {
      ty = ty.substring("int".length());
    } else {
      ty = ty.substring("uint".length());
    }
    if (!ty.isEmpty()) {
      size = Integer.parseInt(ty);
    }
    return IntType.Create(signed, size);
  }
  
  @Override
  public Type visitStringType(JasmintParser.StringTypeContext ctx) {
    return StringType.Create();
  }
  
  @Override
  public Type visitFloatType(JasmintParser.FloatTypeContext ctx) {
    String tmp = ctx.getText();
    if (tmp.endsWith("32")) {
      //by default, use 64 bit floats (after all, it's 2k19)
      return Float32Type.Create();
    }
    return Float64Type.Create();
  }
  
  @Override
  public Type visitReturnType(JasmintParser.ReturnTypeContext ctx) {
    if (ctx.returnTypeReal() != null) {
      return visit(ctx.returnTypeReal());
    }
    return visit(ctx.returnTypeVoid());
  }
  
  @Override
  public Type visitReturnTypeReal(JasmintParser.ReturnTypeRealContext ctx) {
    List<Type> types = new LinkedList<>();
    for (JasmintParser.TypeContext ctxTy : ctx.type()) {
      types.add(visit(ctxTy));
    }
    if (types.size() == 1) {
      return types.get(0);
    }
    return new MultiType(types);
  }
  
  @Override
  public Type visitReturnTypeVoid(JasmintParser.ReturnTypeVoidContext ctx) {
    return VoidType.Create();
  }
  
  @Override
  public Type visitArrayType(JasmintParser.ArrayTypeContext ctx) {
    Type arrTy = new ArrayType(visit(ctx.elementType));
    if (ctx.REFERENCE() != null) {
      Type tmp = new ReferenceType((ReferencedType) arrTy);
      arrTy = tmp;
    }
    return arrTy;
  }
  
  @Override
  public Type visitMapType(JasmintParser.MapTypeContext ctx) {
    Type mapTy = new MapType(visit(ctx.keyType), visit(ctx.valueType));
    if (ctx.REFERENCE() != null) {
      Type tmp = new ReferenceType((ReferencedType) mapTy);
      mapTy = tmp;
    }
    return mapTy;
  }
  
  @Override
  public Type visitFunctionType(JasmintParser.FunctionTypeContext ctx) {
    List<Type> paramTypes = new LinkedList<>();
    
    if (ctx.typelist() != null) {
      for (JasmintParser.TypeContext tctx : ctx.typelist().type()) {
        paramTypes.add(visit(tctx));
      }
    }
    
    //function declarations take care of their fullType internally
    //any written declaration of function type must refer to a non-declaration
    //assume this anonymous
    return new FunctionType((DotAccessType) programVisitor.getWithinModuleOrClass(),
        visit(ctx.retType), paramTypes, LambdaStatus.LAMBDA);
  }
  
  @Override
  public Type visitUndeterminedUserType(JasmintParser.UndeterminedUserTypeContext ctx) {
    Type classType = visit(ctx.undeterminedUserTypeInstance());
    if (ctx.REFERENCE() != null) {
      return new ReferenceType((ReferencedType) classType);
    }
    return classType;
  }
  
  @Override
  public Type visitUndeterminedUserTypeInstance(JasmintParser.UndeterminedUserTypeInstanceContext ctx) {
    List<Type> innerTypes = new LinkedList<Type>();
    
    Set<GenericType> currentClassGenerics = programVisitor.getCurrentClassGenerics();
    
    if (ctx.typelist() != null) {
      for (JasmintParser.TypeContext tctx : ctx.typelist().type()) {
        innerTypes.add(visit(tctx));
      }
    }
    
    Nullable<ModuleType> outerType = Nullable.empty();
    if (ctx.modType() != null) {
      outerType = Nullable.of((ModuleType) visit(ctx.modType()));
    }
    String name = ctx.ID().getText();
    
    if (innerTypes.isEmpty() && outerType.isNull()) {
      GenericType tmpGen = new GenericType(name);
      if (currentClassGenerics.contains(tmpGen)) {
        return tmpGen;
      }
    }
    
    return new UndeterminedUserInstanceType(outerType, name, innerTypes);
  }
  
  @Override
  public Type visitUndeterminedUserTypeDecl(JasmintParser.UndeterminedUserTypeDeclContext ctx) {
    
    Nullable<ModuleType> outerType = Nullable.empty();
    if (ctx.modType() != null) {
      outerType = Nullable.of((ModuleType) visit(ctx.modType()));
    }
    String name = ctx.ID().getText();
    
    return new UndeterminedUserDeclType(outerType, name);
  }
  
  @Override
  public Type visitModType(JasmintParser.ModTypeContext ctx) {
    Nullable<ModuleType> outerType = Nullable.empty();
    for (TerminalNode mod : ctx.ID()) {
      outerType = Nullable.of(new ModuleType(outerType, mod.getText()));
    }
    return outerType.get();
  }
  
}
