package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import ast.AssignmentStatement;
import ast.BinaryExpression;
import ast.BlockStatement;
import ast.BreakStatement;
import ast.CommentAttr;
import ast.CommentStatement;
import ast.ConditionalStatement;
import ast.ContinueStatement;
import ast.DeclarationStatement;
import ast.DeleteStatement;
import ast.Expression;
import ast.ExpressionStatement;
import ast.FfiExpression;
import ast.FfiStatement;
import ast.ForStatement;
import ast.Function;
import ast.FunctionCallExpression;
import ast.FunctionCallStatement;
import ast.ImportStatement;
import ast.Lvalue;
import ast.ModuleStatement;
import ast.ReturnStatement;
import ast.Statement;
import ast.StringExpression;
import ast.SubmodStatement;
import ast.SwitchCaseStatement;
import ast.SwitchStatement;
import ast.WhileStatement;
import errors.Nullable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import multifile.JsmntGlobal;
import org.antlr.v4.runtime.tree.TerminalNode;
import typecheck.ImportType;
import typecheck.ModuleType;
import typecheck.UndeterminedImportType;
import util.Pair;

public class JasmintToAstStatementVisitor extends JasmintBaseVisitor<Statement> {

  private final JasmintToAstExpressionVisitor expressionVisitor;
  private final JasmintToAstTypeVisitor typeVisitor;
  private final JasmintToAstLvalueVisitor lvalueVisitor;
  private final JasmintToAstProgramVisitor programVisitor;

  public JasmintToAstStatementVisitor(JasmintToAstProgramVisitor programVisitor,
      JasmintToAstExpressionVisitor exprVisitor) {
    this.expressionVisitor = exprVisitor;
    this.typeVisitor = new JasmintToAstTypeVisitor(programVisitor);
    this.lvalueVisitor = new JasmintToAstLvalueVisitor(programVisitor, exprVisitor);
    this.programVisitor = programVisitor;
  }

  @Override
  public Statement visitModname(JasmintParser.ModnameContext ctx) {
    List<String> parent = new ArrayList<>();

    if (ctx.parentmod() != null) {
      for (TerminalNode par : ctx.parentmod().ID()) {
        parent.add(par.getText());
      }
    }
    parent.add(ctx.ID().getText());
    ModuleType fullType = ModuleType.staticFromList(parent);
    ModuleStatement val = new ModuleStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(), fullType,
        fullType, ctx.PROVIDER() != null, ctx.FFI() != null);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitFfiBlock(JasmintParser.FfiBlockContext ctx) {
    FfiStatement val = new FfiStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        (StringExpression) expressionVisitor.visit(ctx.stringExpression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitFfiBlockStatement(JasmintParser.FfiBlockStatementContext ctx) {
    FfiStatement val = new FfiStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        (StringExpression) expressionVisitor.visit(ctx.ffiBlock().stringExpression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitFfiLineStatement(JasmintParser.FfiLineStatementContext ctx) {
    int lineNum = ctx.getStart().getLine();
    int colNum = ctx.getStart().getCharPositionInLine();

    if (ctx.expression() == null) {
      FfiStatement val = new FfiStatement(lineNum, colNum, new LinkedList<>(), new LinkedList<>(),
          (StringExpression) expressionVisitor.visit(ctx.ffiBlock().stringExpression()));
      val.setOriginalLabel(val.label);
      return val;
    }

    // this is actually a statement (usually an ffi function call)
    FfiExpression ffiVal = new FfiExpression(lineNum, colNum, new LinkedList<>(),
        new LinkedList<>(),
        (StringExpression) expressionVisitor.visit(ctx.ffiBlock().stringExpression()));
    ffiVal.setOriginalLabel(ffiVal.label);

    BinaryExpression exprVal = new BinaryExpression(lineNum, colNum,
        new LinkedList<>(), new LinkedList<>(), BinaryExpression.BinOp.FFI_CONCAT, ffiVal,
        expressionVisitor.visit(ctx.expression()));
    exprVal.setOriginalLabel(exprVal.label);

    ExpressionStatement val = new ExpressionStatement(lineNum, colNum, new LinkedList<>(),
        new LinkedList<>(), exprVal);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitChildmod(JasmintParser.ChildmodContext ctx) {
    ModuleType fullType = new ModuleType(Nullable.of(programVisitor.getModuleType()),
        ctx.ID().getText());
    SubmodStatement val = new SubmodStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        fullType);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitImportmod(JasmintParser.ImportmodContext ctx) {
    List<String> target = new ArrayList<>();
    for (TerminalNode id : ctx.ID()) {
      target.add(id.getText());
    }

    ImportType moduleName = UndeterminedImportType.staticFromList(target);
    if (moduleName.equals(JsmntGlobal.importModType)) {
      throw new IllegalArgumentException(
          "User cannot explicitly import module " + moduleName + " (reserved)");
    }

    ImportStatement val = new ImportStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(), moduleName);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitDeclaration(JasmintParser.DeclarationContext ctx) {
    if (ctx.declExpr() != null) {
      return visit(ctx.declExpr());
    }
    return visit(ctx.declNoExpr());
  }

  @Override
  public Statement visitDeclExpr(JasmintParser.DeclExprContext ctx) {
    // assume not static or const for now, that is applied further up
    DeclarationStatement val = new DeclarationStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        typeVisitor.visit(ctx.type()), ctx.ID().getText(),
        Nullable.of(expressionVisitor.visit(ctx.expression())), false, false);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitDeclNoExpr(JasmintParser.DeclNoExprContext ctx) {
    // assume not static or const for now, that is applied further up
    DeclarationStatement val = new DeclarationStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        typeVisitor.visit(ctx.type()), ctx.ID().getText(), Nullable.empty(), false, false);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitDeclarationStatement(JasmintParser.DeclarationStatementContext ctx) {
    return visit(ctx.declaration());
  }

  @Override
  public Statement visitDeclarationLine(JasmintParser.DeclarationLineContext ctx) {
    return visit(ctx.declaration());
  }
  
  @Override
  public Statement visitInnerDeclarationLine(JasmintParser.InnerDeclarationLineContext ctx) {
    return visit(ctx.declaration());
  }

  @Override
  public Statement visitAssignmentStatement(JasmintParser.AssignmentStatementContext ctx) {
    AssignmentStatement val = new AssignmentStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        lvalueVisitor.visit(ctx.multiLvalue()), expressionVisitor.visit(ctx.expression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitIfStatement(JasmintParser.IfStatementContext ctx) {
    Nullable<Statement> els = Nullable.empty();
    if (ctx.ifElseStatement() != null) {
      els = Nullable.of(visit(ctx.ifElseStatement()));
    }

    ConditionalStatement val = new ConditionalStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        expressionVisitor.visit(ctx.expression()), visit(ctx.block()), els);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitWhileStatement(JasmintParser.WhileStatementContext ctx) {
    WhileStatement val = new WhileStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        expressionVisitor.visit(ctx.expression()), (BlockStatement) visit(ctx.block()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitForStatement(JasmintParser.ForStatementContext ctx) {
    Nullable<Statement> init = Nullable.empty();
    if (ctx.init != null) {
      init = Nullable.of(visit(ctx.init));
    }
    Nullable<Statement> action = Nullable.empty();
    if (ctx.action != null) {
      action = Nullable.of(visit(ctx.action));
    }

    ForStatement val = new ForStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(), init,
        expressionVisitor.visit(ctx.expression()), action, (BlockStatement) visit(ctx.block()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitSwitchStatement(JasmintParser.SwitchStatementContext ctx) {
    SwitchStatement val = new SwitchStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        expressionVisitor.visit(ctx.expression()), gatherCases(ctx),
        ctx.switchDefaultCase() == null ? Nullable.empty()
            : Nullable.of(getDefaultCase(ctx.switchDefaultCase())));
    val.setOriginalLabel(val.label);
    return val;
  }

  private List<SwitchCaseStatement> gatherCases(JasmintParser.SwitchStatementContext ctx) {
    List<SwitchCaseStatement> cases = new LinkedList<SwitchCaseStatement>();

    for (JasmintParser.SwitchCaseContext cctx : ctx.switchCase()) {
      SwitchCaseStatement val = new SwitchCaseStatement(ctx.getStart().getLine(),
          ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
          Nullable.of(expressionVisitor.visit(cctx.expression())),
          (BlockStatement) visit(cctx.statementList()));
      val.setOriginalLabel(val.label);
      cases.add(val);
    }

    return cases;
  }

  private SwitchCaseStatement getDefaultCase(JasmintParser.SwitchDefaultCaseContext ctx) {
    SwitchCaseStatement val = new SwitchCaseStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(), null,
        (BlockStatement) visit(ctx.statementList()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitDeleteStatement(JasmintParser.DeleteStatementContext ctx) {
    DeleteStatement val = new DeleteStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        expressionVisitor.visit(ctx.expression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitReturnStatement(JasmintParser.ReturnStatementContext ctx) {
    Nullable<Expression> rv = Nullable.empty();
    if (ctx.multiExpression() != null) {
      rv = Nullable.of(expressionVisitor.visit(ctx.multiExpression()));
    }
    ReturnStatement val = new ReturnStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(), rv);
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Statement visitFunctionStatement(JasmintParser.FunctionStatementContext ctx) {
    Function tmp = programVisitor.functionVisitor.visit(ctx.regularFun());
    
    Function val = new Function(tmp.export, tmp.required, tmp.isOverride, tmp.name,
        tmp.originalWithin, tmp.fullType, tmp.params, tmp.getBody(), tmp.getIsStatic(),
        tmp.parentCall, Function.MetaType.PSEUDO_LAMBDA);
    val.setOriginalLabel(tmp.getOriginalLabel());
    return val;
  }
  
  @Override
  public Statement visitClassDeclarationStatement(
      JasmintParser.ClassDeclarationStatementContext ctx) {
    return programVisitor.classDeclarationVisitor.visit(ctx.innerClassDeclaration());
  }

  @Override
  public Statement visitBreakStatement(JasmintParser.BreakStatementContext ctx) {
    BreakStatement val = new BreakStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>());
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitContinueStatement(JasmintParser.ContinueStatementContext ctx) {
    ContinueStatement val = new ContinueStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>());
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitInvocationStatement(JasmintParser.InvocationStatementContext ctx) {
    int lineNum = ctx.getStart().getLine();
    int columnNum = ctx.getStart().getCharPositionInLine();

    FunctionCallExpression fexpr = new FunctionCallExpression(lineNum, columnNum,
        new LinkedList<>(), new LinkedList<>(), Nullable.empty(),
        expressionVisitor.visit(ctx.expression()), gatherArguments(ctx.arguments()), "");
    FunctionCallExpression expr = fexpr.flattenDot();
    expr.setOriginalLabel(expr.getLabel());

    FunctionCallStatement val = new FunctionCallStatement(lineNum, columnNum, new LinkedList<>(),
        new LinkedList<>(), expr);
    val.setOriginalLabel(val.getLabel());
    return val;
  }

  private List<Expression> gatherArguments(JasmintParser.ArgumentsContext ctx) {
    List<Expression> args = new LinkedList<Expression>();

    for (JasmintParser.ExpressionContext ectx : ctx.expression()) {
      args.add(expressionVisitor.visit(ectx));
    }

    return args;
  }

  @Override
  public Statement visitBlockStatement(JasmintParser.BlockStatementContext ctx) {
    return visit(ctx.block());
  }

  @Override
  public Statement visitBlock(JasmintParser.BlockContext ctx) {
    return visit(ctx.statementList());
  }

  @Override
  public Statement visitStatementList(JasmintParser.StatementListContext ctx) {
    List<Statement> statements = new ArrayList<>();

    for (JasmintParser.StatementLineContext sctx : ctx.statementLine()) {
      statements.add(visit(sctx));
    }

    BlockStatement val = new BlockStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(), statements);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitLineStatement(JasmintParser.LineStatementContext ctx) {
    Statement val = visit(ctx.value);
    if (ctx.comment() != null) {
      val.addComments(gatherComments(ctx.comment()), false);
    }
    return val;
  }

  public static List<CommentAttr> gatherComments(JasmintParser.CommentContext ctx) {
    List<CommentAttr> comments = new LinkedList<>();

    for (TerminalNode bctx : ctx.BLOCK_COMMENT()) {
      comments.add(new CommentAttr(bctx.getText(), true));
    }

    if (ctx.LINE_COMMENT() != null) {
      comments.add(new CommentAttr(ctx.LINE_COMMENT().getText(), false));
    }

    return comments;
  }

  public static Pair<CommentAttr, List<CommentAttr>> gatherNlComments(
      JasmintParser.Nl_commentContext ctx) {
    List<CommentAttr> comments = new LinkedList<>();
    CommentAttr firstComment = null;

    if (ctx.NL_LINE_COMMENT() != null) {
      firstComment = new CommentAttr(ctx.NL_LINE_COMMENT().getText(), false);
    } else if (ctx.NL_BLOCK_COMMENT() != null) {
      firstComment = new CommentAttr(ctx.NL_BLOCK_COMMENT().getText(), true);
    } else {
      throw new IllegalArgumentException("invalid nl comment");
    }

    for (TerminalNode bctx : ctx.BLOCK_COMMENT()) {
      comments.add(new CommentAttr(bctx.getText(), true));
    }

    if (ctx.LINE_COMMENT() != null) {
      comments.add(new CommentAttr(ctx.LINE_COMMENT().getText(), false));
    }

    return new Pair<>(firstComment, comments);
  }

  @Override
  public Statement visitModifierAssignmentStatement(
      JasmintParser.ModifierAssignmentStatementContext ctx) {
    int lineNum = ctx.getStart().getLine();
    int columnNum = ctx.getStart().getCharPositionInLine();
    String opStr = ctx.assignmentOperator().getText();
    if (opStr.length() == 2) {
      opStr = opStr.substring(0, 1);
    } else if (opStr.length() == 3) {
      opStr = opStr.substring(0, 2);
    } else {
      throw new IllegalArgumentException();
    }
    Lvalue target = lvalueVisitor.visit(ctx.lvalue());

    BinaryExpression bval = new BinaryExpression(lineNum, columnNum,
        new LinkedList<>(), new LinkedList<>(), BinaryExpression.stringToBinOp(opStr),
        target.toExpression(),
        expressionVisitor.visit(ctx.expression()));
    bval.setOriginalLabel(bval.label);

    AssignmentStatement val = new AssignmentStatement(lineNum, columnNum, new LinkedList<>(),
        new LinkedList<>(), target, bval);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitCommentStatement(JasmintParser.CommentStatementContext ctx) {
    Pair<CommentAttr, List<CommentAttr>> comments = gatherNlComments(ctx.nl_comment());

    CommentStatement val = new CommentStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), comments.b, comments.a.comment,
        comments.a.isBlockComment);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Statement visitComment(JasmintParser.CommentContext ctx) {
    throw new UnsupportedOperationException("use gatherComments instead");
  }

  @Override
  public Statement visitNl_comment(JasmintParser.Nl_commentContext ctx) {
    throw new UnsupportedOperationException("use gatherNlComments instead");
  }
}
