package interp;

import typecheck.Float64Type;
import typecheck.IntType;

public class Float64Value extends AbstractValue {

  public final double value;
  
  public Float64Value(double value) {
    super(MetaValue.NORMAL, Float64Type.Create());
    this.value = value;
  }
  
  public Float64Value(MetaValue metaValue, double value) {
    super(metaValue, Float64Type.Create());
    this.value = value;
  }

  @Override
  public Value add(Value rightValue) throws InterpError {
    return rightValue.add(value);
  }
  
  @Override
  public Value add(long leftValue, IntType leftType) {
    return new Float64Value((double)leftValue + value);
  }
  
  @Override
  public Value add(double leftValue) {
    return new Float64Value(leftValue + value);
  }
  
  @Override
  public Value add(String leftValue) {
    return new StringValue(leftValue + value);
  }

  @Override
  public Value sub(Value rightValue) throws InterpError {
    return rightValue.sub(value);
  }

  @Override
  public Value sub(long leftValue, IntType leftType) {
    return new Float64Value((double)leftValue - value);
  }

  @Override
  public Value sub(double leftValue) {
    return new Float64Value(leftValue - value);
  }

  @Override
  public boolean toBool() throws InterpError {
    return value != 0.0;
  }
  
  @Override
  public long toInt() throws InterpError {
    return (int) value;
  }
  
  @Override
  public double toDouble() throws InterpError {
    return value;
  }

  @Override
  public Value mul(Value rightValue) throws InterpError {
    return rightValue.mul(value);
  }
  
  @Override
  public Value mul(long leftValue, IntType leftType) throws InterpError {
    return new Float64Value((double)leftValue * value);
  }
  
  @Override
  public Value mul(double leftValue) throws InterpError {
    return new Float64Value(leftValue * value);
  }

  @Override
  public Value div(Value rightValue) throws InterpError {
    return rightValue.div(value);
  }
  
  @Override
  public Value div(long leftValue, IntType leftType) throws InterpError {
    if (value == 0.0) {
      throw new InterpError("division by zero", -1, -1);
    }
    return new Float64Value((double)leftValue / value);
  }
  
  @Override
  public Value div(double leftValue) throws InterpError {
    if (value == 0.0) {
      throw new InterpError("division by zero", -1, -1);
    }
    return new Float64Value(leftValue / value);
  }

  @Override
  public Value neg() throws InterpError {
    return new Float64Value(-value);
  }

  @Override
  public Value copy(Environment copyEnv) {
    return new Float64Value(value);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return ((Float64Value) other).value == value;
  }
}
