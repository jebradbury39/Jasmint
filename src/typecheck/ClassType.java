package typecheck;

import ast.Expression;
import ast.FunctionCallExpression;
import ast.FunctionCallStatement;
import ast.IdentifierExpression;
import ast.Program.DestructorRenamings;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.EnumHeader;
import header.PsuedoClassHeader;
import import_mgmt.EffectiveImport.ImportUsage;
import import_mgmt.EffectiveImports;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.ModuleEndpoint;
import multifile.Project;
import type_env.ClassTypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.TypeStatus;
import type_env.UserDeclTypeBox;
import typecheck.FunctionType.LambdaStatus;

public class ClassType extends UserInstanceType {

  public final List<Type> innerTypes; // not generics

  public ClassType(Nullable<ModuleType> outerType, String name, List<Type> innerTypes) {
    super(TypeType.CLASS_TYPE, outerType, name);
    this.innerTypes = innerTypes;
  }
  
  public UserDeclType asDeclType() {
    return new ClassDeclType(outerType, name);
  }

  /*
   * Ignores inner types and only compares outerType and name. Note that outerType
   * will never have inner types, but may have it's own outerType
   */
  public boolean cmpNames(ClassType other) {
    if (!other.name.equals(name)) {
      return false;
    }

    if (outerType == null || other.outerType == null) {
      return outerType != other.outerType;
    }
    return outerType.equals(other.outerType);
  }

  @Override
  public boolean equals(Object other) {

    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof ClassType)) {
      return false;
    }

    ClassType otherUT = (ClassType) other;
    if (!name.equals(otherUT.name)) {
      return false;
    }
    if (innerTypes.size() != otherUT.innerTypes.size()) {
      return false;
    }

    Iterator<Type> iterUs = innerTypes.iterator();
    Iterator<Type> iterOther = otherUT.innerTypes.iterator();
    for (; iterUs.hasNext() && iterOther.hasNext();) {
      if (!iterUs.next().equals(iterOther.next())) {
        return false;
      }
    }

    return outerType.equals(otherUT.outerType);
  }

  @Override
  public int hashCode() {
    return super.hashCode() + (outerType.isNull() ? 0 : outerType.get().hashCode())
        + name.hashCode();
  }

  @Override
  public String toString() {
    String result = super.toString();

    if (innerTypes.size() > 0) {
      result += "<";
      boolean first = true;
      for (Type ty : innerTypes) {
        if (!first) {
          result += ", ";
        }
        first = false;
        result += ty;
      }
      result += ">";
    }
    return result;
  }

  @Override
  public List<Type> getInnerTypes() {
    return innerTypes;
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);

    List<Type> replTypes = new LinkedList<Type>();

    if (repl == null) {
      // failed to match, so replace inner types (class name and outer name stay the
      // same)
      Nullable<ModuleType> newOuter = outerType;
      String newName = name;
      ClassDeclType onlyClassDecl = (ClassDeclType) mapFromTo
          .get(new ClassDeclType(outerType, name));
      if (onlyClassDecl != null) {
        newOuter = onlyClassDecl.outerType;
        newName = onlyClassDecl.name;
      }

      for (Type ty : innerTypes) {
        Type newTy = ty.replaceType(mapFromTo);
        replTypes.add(newTy);
      }

      // don't replace on the outerType, since the outer type is meant to be purely
      // organizational
      // (and has no inner types anyway)
      return new ClassType(newOuter, newName, replTypes);
    }

    if (!(repl instanceof UserInstanceType)) {
      return repl;
    }
    ClassType utRepl = (ClassType) repl;

    /*
     * Replace T -> List{int} T -> List{<any>} This would have to be invalid, since
     * T must be replaced with a solid type List{<any>} -> List_0{T} List{<any>} ->
     * List_0 This is still fine since List could be replaced with a non-solid or
     * solid type
     */

    // check for <any> types. If we find one, we perform replaceType on our
    // innerType and use that
    if (utRepl.getInnerTypes().size() == innerTypes.size()) {
      Iterator<Type> iterReplTypes = repl.getInnerTypes().iterator();
      Iterator<Type> iterOurTypes = innerTypes.iterator();
      while (iterReplTypes.hasNext() && iterOurTypes.hasNext()) {
        Type replType = iterReplTypes.next();
        Type ourType = iterOurTypes.next();
        if (replType instanceof AnyType) {
          replTypes.add(ourType.replaceType(mapFromTo));
        } else {
          replTypes.add(replType);
        }
      }
    } else {
      replTypes = utRepl.getInnerTypes();
    }

    // use the new names since we matched
    return new ClassType(utRepl.outerType, utRepl.name, replTypes);
  }

  @Override
  public Builder serialize() {
    AstProto.ClassType.Builder subDoc = AstProto.ClassType.newBuilder();
    if (outerType.isNotNull()) {
      subDoc.addOuterType(outerType.get().serialize());
    }
    subDoc.setName(name);
    for (Type innerTy : innerTypes) {
      subDoc.addInnerTypes(innerTy.serialize());
    }

    AstProto.Type.Builder document = preSerialize();
    document.setClassType(subDoc);
    return document;
  }

  public static ClassType deserialize(AstProto.ClassType document) throws SerializationError {
    Nullable<ModuleType> outerType = Nullable.empty();
    if (document.getOuterTypeCount() != 0) {
      outerType = Nullable.of((ModuleType) AbstractType.deserialize(document.getOuterType(0)));
    }

    List<Type> innerTypes = new LinkedList<>();
    for (AstProto.Type bvType : document.getInnerTypesList()) {
      innerTypes.add(AbstractType.deserialize(bvType));
    }
    return new ClassType(outerType, document.getName(), innerTypes);
  }

  // check if this is a non-generic version of a generic type, and if so, add to
  // tenv
  public void checkIfNonGeneric(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    if (innerTypes.size() == 0) {
      return;
    }

    if (tenv.registerNonGeneric(this, lineNum, columnNum)) {
      // can't even use this for an instance since it wraps
      // in global scope for lookup of class names
      getTenv(tenv, true, lineNum, columnNum);
    }
  }

  @Override
  public Type setConst(boolean isConst) {
    ClassType tmp = new ClassType(outerType, name, innerTypes);
    tmp.isConst = isConst;
    return tmp;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (types.contains(this)) {
      return true;
    }
    for (Type innerTy : innerTypes) {
      if (innerTy.containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    stackTypes.add(new ClassDeclType(outerType, name));
    for (Type iterInner : innerTypes) {
      stackTypes.addAll(iterInner.getUserDeclTypes(stackTypesOnly));
    }
    return stackTypes;
  }

  public List<String> toList() {
    List<String> namePath = new LinkedList<>();
    if (outerType.isNotNull()) {
      namePath.addAll(outerType.get().toList());
    }
    namePath.add(name);
    return namePath;
  }

  public ClassType prepend(ModuleType impPrefix) {
    if (outerType.isNull()) {
      return new ClassType(Nullable.of(impPrefix), name, innerTypes);
    }
    return new ClassType(Nullable.of(outerType.get().prepend(impPrefix)), name, innerTypes);
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    // normalize our name
    Optional<Type> normName = new ClassDeclType(outerType, name).normalize(tenv, lineNum, columnNum);
    if (!normName.isPresent()) {
      return Optional.empty();
    }
    UserDeclType normDecl = (UserDeclType) normName.get();

    // normalize inner types
    List<Type> normInner = new LinkedList<>();
    boolean hitError = false;
    for (Type innerTy : innerTypes) {
      Optional<Type> tmp = innerTy.normalize(tenv, lineNum, columnNum);
      if (tmp.isPresent()) {
        normInner.add(tmp.get());
      } else {
        hitError = true;
      }
    }
    if (hitError) {
      return Optional.empty();
    }
    return Optional.of(new ClassType(normDecl.outerType, normDecl.name, normInner));
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return this;
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException {
    
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(asDeclType(), lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return;
    }
    if (!(tyBox.get() instanceof ClassTypeBox)) {
      return;
    }
    ClassTypeBox classTyBox = (ClassTypeBox) tyBox.get();
    ClassHeader classHeader = classTyBox.classHeader;
    
    Nullable<Node> parentNode = modDepGraph.findNode(classHeader.moduleType);
    if (parentNode.isNull()) {
      // add the parentNode to the graph
      parentNode = Nullable.of(modDepGraph.addClassNode(classHeader.getAbsName()));
    }
    ourModNode.populateUplinks(modDepGraph, tenv.effectiveImports, classHeader.absName,
        encapTypeIsRef ? ImportUsage.SOFT : ImportUsage.HARD);

    // check the inner types
    for (Type innerTy : innerTypes) {
      innerTy.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    }
  }

  public Optional<ModuleEndpoint> getModuleEndpoint(TypeEnvironment typeEnvironment) {
    // TODO Auto-generated method stub
    return null;
  }

  /*
   * Return class instance tenv with no link to GLOBAL tenv
   * 
   * TODO caching optimization
   */
  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return getTenv(tenv, false, lineNum, columnNum);
  }

  private Optional<PsuedoClassHeader> getRealClassHeader(TypeEnvironment tenv, int lineNum,
      int columnNum) throws FatalMessageException {
    // find the class header
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(asDeclType(), true,
        lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(tyBox.get() instanceof ClassTypeBox)) {
      return Optional.empty();
    }
    ClassTypeBox classTyBox = (ClassTypeBox) tyBox.get();
    
    PsuedoClassHeader classHeader = classTyBox.classHeader;

    // verify that our inner types match up. Since we are getting an instance of the
    // class,
    // all generics must be resolved. All of this.innerTypes must be real types, not
    // generic
    if (classHeader.getInnerTypes().size() != innerTypes.size()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Number of parameterized types mismatch: " + this + " vs " + classHeader.getInnerTypes(),
          lineNum, columnNum);
      return Optional.empty();
    }

    // verify that none of our inner types are generic
    Optional<TypeStatus> typeStatus = tenv.lookupType(this, true, lineNum, columnNum);
    if (!typeStatus.isPresent()) {
      return Optional.empty();
    }
    /*
    if (typeStatus.get().isGeneric()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Class instance cannot have any generics left in it: " + this + " has status: "
              + typeStatus,
          lineNum, columnNum);
      return Optional.empty();
    }
    */

    // Create a new class header with all generics replaced
    // Each class can only replace its own members, not inherited members
    // don't change module types
    // TODO caching optimization
    return classHeader.replaceGenerics(tenv.project.getAuditTrail(), tenv.project,
        Nullable.of(tenv), innerTypes, false, new HashMap<>(), tenv.msgState, lineNum, columnNum);
  }

  // this is for an instance of the class, no global tenv link
  private Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, boolean registration, int lineNum,
      int columnNum) throws FatalMessageException {

    Optional<PsuedoClassHeader> classHeader = getRealClassHeader(tenv, lineNum, columnNum);
    if (!classHeader.isPresent()) {
      return Optional.empty();
    }
    // generate a type environment from the new class header

    Optional<TypeEnvironment> newTenv = classHeader.get().getTenv(tenv, false, this);

    // TODO caching optimization
    return newTenv;
  }

  /*
   * Take a sample program: module a; import b; submod c;
   * 
   * types: a.A, a.c.C, b.B
   * 
   * If going for relative usage: a.A -> A a.c.C -> c.C b.B -> b.B
   * 
   * If going for absolute usage: A -> a.A c.C -> a.c.C b.B -> b.B
   * 
   * mod c extends a; a.c.C -> C //technically ok, but not likely c.C -> C ABS C
   * -> c.C
   */
  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    
    // normalize our name
    Optional<Nullable<Type>> normDecl = asDeclType().basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!normDecl.isPresent()) {
      return Optional.empty();
    }
    ClassDeclType normClassDecl = (ClassDeclType) normDecl.get().get();

    List<Type> normInner = new LinkedList<>();
    for (Type innerTy : innerTypes) {
      Optional<Nullable<Type>> tmp = innerTy.basicNormalize(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to normalize inner type: " + innerTy);
        return Optional.empty();
      }
      normInner.add(tmp.get().get());
    }

    return Optional.of(Nullable.of(new ClassType(normClassDecl.outerType, normClassDecl.name, normInner)));
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState, Set<ModuleType> exportedTypes, boolean skipNormalize)
      throws FatalMessageException {
    // make sure to normalize all types first, then pull out their modules
    ClassType normType = this;
    if (!skipNormalize) {
      Optional<Nullable<Type>> optType = basicNormalize(false, currentModule, effectiveImports, msgState);
      if (!optType.isPresent()) {
        return false;
      }
      normType = (ClassType) optType.get().get();
    }
    exportedTypes.add(normType.outerType.get());

    // now go through inner types (can skip normalize)
    for (Type ty : normType.innerTypes) {
      if (!ty.getNormalizedModType(currentModule, effectiveImports, msgState, exportedTypes,
          true)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) throws FatalMessageException {
    // every class must provide a default constructor, or we will provide one
    // ourselves.
    // Find the default constructor name
    PsuedoClassHeader pch = project.lookupClassHeader(asDeclType(), Nullable.empty(), true,
        project.getAuditTrail());
    return pch.getDummyExpression(this, msgState);
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    List<Type> newInnerTypes = new LinkedList<>();
    for (Type innerType : innerTypes) {
      newInnerTypes.add(innerType.prependFnRetType(prependType, false));
    }
    ClassType newType = new ClassType(outerType, name, newInnerTypes);
    if (!isTop) {
      return newType;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(newType);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    // lookup class header with all generic types replaced

    Optional<PsuedoClassHeader> classHeader = getRealClassHeader(tenv, lineNum, columnNum);
    if (!classHeader.isPresent()) {
      return -1;
    }

    return classHeader.get().calculateSize(tenv, false, lineNum, columnNum);
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    // id.end()
    final ClassDeclType cdeclType = new ClassDeclType(outerType, name);
    PsuedoClassHeader classHeader = destructorRenamings.project.lookupClassHeader(cdeclType,
        Nullable.empty(), true, destructorRenamings.project.getAuditTrail());
    if (classHeader instanceof EnumHeader) {
      return Optional.empty();
    }
    
    final String destructorId = destructorRenamings.getRenaming(cdeclType);
    
    Nullable<Expression> prefix = Nullable.empty();
    if (!id.isEmpty()) {
      prefix = Nullable.of(new IdentifierExpression(id));
    }
    
    FunctionCallExpression fnCallExpr = new FunctionCallExpression(
        prefix, new IdentifierExpression(destructorId),
        new LinkedList<>(), "destructor");
    fnCallExpr.setCalledType(new FunctionType(cdeclType, VoidType.Create(),
        new LinkedList<>(), LambdaStatus.NOT_LAMBDA));
    fnCallExpr.setIsDestructorCall(true);
    return Optional.of(new FunctionCallStatement(fnCallExpr));
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    if (!tenv.copyFromTenvIntoTenv(asDeclType(), newTenv)) {
      return false;
    }
    
    for (Type innerTy : innerTypes) {
      if (!innerTy.copyFromTenvIntoTenv(tenv, newTenv)) {
        return false;
      }
    }
    
    return true;
  }

  @Override
  public Type asClassDeclType() {
    return asDeclType();
  }

  @Override
  public ModuleType getModuleType() {
    return outerType.get();
  }

  @Override
  public UserDeclType asUserDeclType() {
    return asDeclType();
  }

}
