package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.StringValue;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class StringExpression extends AbstractExpression {

  public final String unparsed;
  public final String value;

  public StringExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, String value) {
    super(lineNum, columnNum, AstType.STRING_EXPRESSION, preComments, postComments);
    this.unparsed = value;
    /* remove quotes and handle escape seq */
    this.value = parseString(value, '"');
  }

  public StringExpression(int lineNum, int columnNum, String value) {
    super(lineNum, columnNum, AstType.STRING_EXPRESSION);
    this.unparsed = value;
    /* remove quotes and handle escape seq */
    this.value = parseString(value, '"');
  }

  public StringExpression(String value) {
    super(AstType.STRING_EXPRESSION);
    this.unparsed = value;
    /* remove quotes and handle escape seq */
    this.value = parseString(value, '"');
  }

  @Override
  public String toString(String indent) {
    return preToString() + unparsed + postToString();
  }

  public static String parseString(String value, char delim) {
    String parsedValue = "";

    for (int i = 0; i < value.length(); i++) {
      char c = value.charAt(i);
      if (c == delim) {
        // TODO throw error if unexpected
        continue;
      }
      if (c == '\\') {
        if (i + 1 >= value.length()) {
          // TODO throw error
          break;
        }
        i++;
        char escaped = value.charAt(i);
        switch (escaped) {
          case 'n':
            parsedValue += '\n';
            break;
          case 'r':
            parsedValue += '\r';
            break;
          case 't':
            parsedValue += '\t';
            break;
          case 'b':
            parsedValue += '\b';
            break;
          default:
            parsedValue += escaped;
            break;
        }
      } else {
        parsedValue += c;
      }
    }

    return parsedValue;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType) {
    determinedType = StringType.Create();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    return new StringValue(value);
  }

  @Override
  public AstProto.Ast.Builder serialize() {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.StringExpression.Builder subDoc = AstProto.StringExpression.newBuilder();

    subDoc.setUnparsed(unparsed);

    document.setStringExpression(subDoc);
    return document;
  }

  public static StringExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.StringExpression document) {
    return new StringExpression(lineNum, columnNum, preComments, postComments,
        document.getUnparsed());
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof StringExpression)) {
      return false;
    }
    StringExpression ourOther = (StringExpression) other;
    return ourOther.value.equals(value);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), this);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    return this;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState) {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(StringType.Create());
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return value.length();
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return this;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    return this;
  }
}
