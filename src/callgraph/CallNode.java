package callgraph;

import java.util.HashSet;
import java.util.Set;
import typecheck.FunctionType;
import util.DotGen;
import util.DotGen.Color;

public class CallNode {
  public final String fnName;
  public final FunctionType fullType;
  public boolean withinSandboxExpr;
  
  protected Set<CallNode> downlinks = new HashSet<>();
  
  public CallNode(String fnName, FunctionType fullType, boolean withinSandboxExpr) {
    this.fnName = fnName;
    this.fullType = fullType;
    this.withinSandboxExpr = withinSandboxExpr;
  }
  
  @Override
  public String toString() {
    return fullType + ":" + fnName;
  }
  
  public void updateDotGraph(DotGen dotGraph) {
    DotGen.Node node = new DotGen.Node(toString());
    if (withinSandboxExpr) {
      node.color = Color.BLUE;
    }
    dotGraph.addNode(node);
    for (CallNode downNode : downlinks) {
      DotGen.Edge edge = new DotGen.Edge(toString(), downNode.toString());
      edge.color = DotGen.Color.BLACK;
      dotGraph.addEdge(edge);
    }
  }
  
  @Override
  public int hashCode() {
    return fnName.hashCode() + fullType.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof CallNode)) {
      return false;
    }
    CallNode otherNode = (CallNode) other;
    return fnName.equals(otherNode.fnName) && fullType.equals(otherNode.fullType);
  }

  public void addDownlink(CallNode childNode) {
    downlinks.add(childNode);
  }

  public void dfs(Set<CallNode> visited, Set<CallNode> sandboxedFunctions,
      boolean parentIsSandbox) {
    if (visited.contains(this)) {
      return;
    }
    visited.add(this);
    
    boolean sandboxed = withinSandboxExpr || parentIsSandbox;
    if (sandboxed) {
      sandboxedFunctions.add(this);
    }
    for (CallNode downlink : downlinks) {
      downlink.dfs(visited, sandboxedFunctions, sandboxed);
    }
  }
  
}
