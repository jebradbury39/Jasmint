package typecheck;

import ast.Expression;
import ast.Program.DestructorRenamings;
import ast.Statement;
import astproto.AstProto.Type.Builder;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;

// could be an EnumDeclType, ClassDeclType, or ModuleType after typecheck

public class UndeterminedUserDeclType extends UserDeclType {

  public UndeterminedUserDeclType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.UNDETERMINED_USER_DECL_TYPE, outerType, name);
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<Type> getInnerTypes() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Builder serialize() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type setConst(boolean isConst) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsType(Set<Type> types) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    stackTypes.add(this);
    return stackTypes;
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(this, lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(tyBox.get() instanceof UserDeclTypeBox)) {
      return Optional.empty();
    }
    UserDeclTypeBox userTyBox = (UserDeclTypeBox) tyBox.get();
    
    return Optional.of(userTyBox.getType());
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    // first try to find our class name in the effectiveImports
    Nullable<EffectiveImport> tmp = effectiveImports.getImport(asImportType(),
        EffectiveImport.ImportUsage.INVALID);
    
    Nullable<ModuleType> normOuter = Nullable.empty();
    if (tmp.isNull()) {
      if (outerType.isNull()) {
        //must be in our module
        if (keepCurrentModRelative) {
          return Optional.of(Nullable.of(new UndeterminedUserDeclType(Nullable.empty(), name)));
        }
        return Optional.of(Nullable.of(new UndeterminedUserDeclType(Nullable.of(currentModule), name)));
      }
      
      // not found, so now just look for module type (not the class name)
      tmp = effectiveImports.getImport(outerType.get().asImportType(),
          EffectiveImport.ImportUsage.INVALID);
      if (tmp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to find module in effective imports: " + outerType);
        return Optional.empty();
      }
      ImportType foundImp = tmp.get().absPath;
      normOuter = Nullable.of(new ModuleType(foundImp.outerType, foundImp.name));
    } else {
      // matched the class (must have outerType/ModuleType)
      normOuter = Nullable.of(tmp.get().absPath.outerType.get());
    }
    
    if (keepCurrentModRelative) {
      //remove any part of the outer type that starts with current module
      Nullable<ModuleType> uniqueRemainder = normOuter.get().removeCommonStart(currentModule);
      //unique/relative part
      normOuter = uniqueRemainder;
    }
    return Optional.of(Nullable.of(new UndeterminedUserDeclType(normOuter, name)));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type asClassDeclType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<String> toList() {
    List<String> namePath = new LinkedList<>();
    if (outerType.isNotNull()) {
      namePath.addAll(outerType.get().toList());
    }
    namePath.add(name);
    return namePath;
  }
  
  public UserDeclType fromList(List<String> list) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public UserInstanceType asInstanceType() {
    return new UndeterminedUserInstanceType(outerType, name, new LinkedList<>());
  }

  @Override
  public UserDeclType asUserDeclType() {
    return this;
  }

  @Override
  public ImportType asImportType() {
    return new UndeterminedImportType(outerType, name);
  }

}
