package typecheck;

import ast.Expression;
import ast.Program.DestructorRenamings;
import ast.Statement;
import astproto.AstProto.Type.Builder;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.PsuedoClassHeader;
import import_mgmt.EffectiveImports;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.ClassTypeBox;
import type_env.EnumTypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.TypeStatus;
import type_env.UserDeclTypeBox;

// could be an EnumType or a ClassType after typecheck

public class UndeterminedUserInstanceType extends UserInstanceType {
  
  public final List<Type> innerTypes;

  public UndeterminedUserInstanceType(Nullable<ModuleType> outerType,
      String name, List<Type> innerTypes) {
    super(TypeType.UNDETERMINED_USER_INSTANCE_TYPE, outerType, name);
    this.innerTypes = innerTypes;
  }
  
  @Override
  public String toString() {
    String result = super.toString();

    if (innerTypes.size() > 0) {
      result += "<";
      boolean first = true;
      for (Type ty : innerTypes) {
        if (!first) {
          result += ", ";
        }
        first = false;
        result += ty;
      }
      result += ">";
    }
    return result;
  }
  
  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<Type> getInnerTypes() {
    return innerTypes;
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);

    List<Type> replTypes = new LinkedList<Type>();

    if (repl == null) {
      // failed to match, so replace inner types (class name and outer name stay the
      // same)
      Nullable<ModuleType> newOuter = outerType;
      String newName = name;
      UndeterminedUserDeclType onlyClassDecl = (UndeterminedUserDeclType) mapFromTo
          .get(asDeclType());
      if (onlyClassDecl != null) {
        newOuter = onlyClassDecl.outerType;
        newName = onlyClassDecl.name;
      }

      for (Type ty : innerTypes) {
        Type newTy = ty.replaceType(mapFromTo);
        replTypes.add(newTy);
      }

      // don't replace on the outerType, since the outer type is meant to be purely
      // organizational
      // (and has no inner types anyway)
      return new UndeterminedUserInstanceType(newOuter, newName, replTypes);
    }

    if (!(repl instanceof UserInstanceType)) {
      return repl;
    }
    UndeterminedUserInstanceType utRepl = (UndeterminedUserInstanceType) repl;

    /*
     * Replace T -> List{int} T -> List{<any>} This would have to be invalid, since
     * T must be replaced with a solid type List{<any>} -> List_0{T} List{<any>} ->
     * List_0 This is still fine since List could be replaced with a non-solid or
     * solid type
     */

    // check for <any> types. If we find one, we perform replaceType on our
    // innerType and use that
    if (utRepl.getInnerTypes().size() == innerTypes.size()) {
      Iterator<Type> iterReplTypes = repl.getInnerTypes().iterator();
      Iterator<Type> iterOurTypes = innerTypes.iterator();
      while (iterReplTypes.hasNext() && iterOurTypes.hasNext()) {
        Type replType = iterReplTypes.next();
        Type ourType = iterOurTypes.next();
        if (replType instanceof AnyType) {
          replTypes.add(ourType.replaceType(mapFromTo));
        } else {
          replTypes.add(replType);
        }
      }
    } else {
      replTypes = utRepl.getInnerTypes();
    }

    // use the new names since we matched
    return new UndeterminedUserInstanceType(utRepl.outerType, utRepl.name, replTypes);
  }

  @Override
  public Builder serialize() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type setConst(boolean isConst) {
    UndeterminedUserInstanceType val = new UndeterminedUserInstanceType(outerType, name, innerTypes);
    val.isConst = isConst;
    return val;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (types.contains(this)) {
      return true;
    }
    for (Type innerTy : innerTypes) {
      if (innerTy.containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    stackTypes.add(new ClassDeclType(outerType, name));
    for (Type iterInner : innerTypes) {
      stackTypes.addAll(iterInner.getUserDeclTypes(stackTypesOnly));
    }
    return stackTypes;
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    // normalize our name
    Optional<Type> normName = asDeclType().normalize(tenv, lineNum, columnNum);
    if (!normName.isPresent()) {
      return Optional.empty();
    }
    UserDeclType normDecl = (UserDeclType) normName.get();

    // normalize inner types
    List<Type> normInner = new LinkedList<>();
    boolean hitError = false;
    for (Type innerTy : innerTypes) {
      Optional<Type> tmp = innerTy.normalize(tenv, lineNum, columnNum);
      if (tmp.isPresent()) {
        normInner.add(tmp.get());
      } else {
        hitError = true;
      }
    }
    if (hitError) {
      return Optional.empty();
    }
    
    if (normDecl instanceof ClassDeclType) {
      return Optional.of(new ClassType(normDecl.outerType, normDecl.name, normInner));
    }
    if (normDecl instanceof EnumDeclType) {
      if (!innerTypes.isEmpty()) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "enum cannot have inner types: " + this);
        return Optional.empty();
      }
      return Optional.of(new EnumType(normDecl.outerType, normDecl.name));
    }
    tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "not an enum or class: " + this);
    return Optional.empty();
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }
  
  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException {
    Optional<Type> normType = normalize(tenv, lineNum, columnNum);
    if (!normType.isPresent()) {
      return;
    }
    
    normType.get().addDependency(modDepGraph, ourModNode, tenv, encapTypeIsRef, lineNum, columnNum);
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    
    // normalize our name
    Optional<Nullable<Type>> normDecl = asDeclType().basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!normDecl.isPresent()) {
      return Optional.empty();
    }
    UndeterminedUserDeclType normClassDecl = (UndeterminedUserDeclType) normDecl.get().get();

    List<Type> normInner = new LinkedList<>();
    for (Type innerTy : innerTypes) {
      Optional<Nullable<Type>> tmp = innerTy.basicNormalize(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to normalize inner type: " + innerTy);
        return Optional.empty();
      }
      normInner.add(tmp.get().get());
    }

    return Optional.of(Nullable.of(new UndeterminedUserInstanceType(normClassDecl.outerType,
        normClassDecl.name, normInner)));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type asClassDeclType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    // lookup class header with all generic types replaced

    Optional<PsuedoClassHeader> classHeader = getRealClassHeader(tenv, lineNum, columnNum);
    if (!classHeader.isPresent()) {
      return -1;
    }

    return classHeader.get().calculateSize(tenv, false, lineNum, columnNum);
  }
  
  private Optional<PsuedoClassHeader> getRealClassHeader(TypeEnvironment tenv, int lineNum,
      int columnNum) throws FatalMessageException {
    // find the class header
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(asDeclType(), lineNum, lineNum);
    if (!tyBox.isPresent()) {
      return Optional.empty();
    }
    PsuedoClassHeader classHeader = null;
    if (tyBox.get() instanceof ClassTypeBox) {
      classHeader = ((ClassTypeBox) tyBox.get()).classHeader;
    } else if (tyBox.get() instanceof EnumTypeBox) {
      classHeader = ((EnumTypeBox) tyBox.get()).enumHeader;
    } else {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "user type must be class or enum, but found module: " + this, lineNum, columnNum);
      return Optional.empty();
    }
    
    // verify that our inner types match up. Since we are getting an instance of the
    // class,
    // all generics must be resolved. All of this.innerTypes must be real types, not
    // generic
    if (classHeader.getInnerTypes().size() != innerTypes.size()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Number of parameterized types mismatch: " + this + " vs " + classHeader.getInnerTypes(),
          lineNum, columnNum);
      return Optional.empty();
    }

    // verify that none of our inner types are generic
    Optional<TypeStatus> typeStatus = tenv.lookupType(this, lineNum, columnNum);
    if (!typeStatus.isPresent()) {
      return Optional.empty();
    }
    if (typeStatus.get().isGeneric()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Class instance cannot have any generics left in it: " + this + " has status: "
              + typeStatus,
          lineNum, columnNum);
      return Optional.empty();
    }

    // Create a new class header with all generics replaced
    // Each class can only replace its own members, not inherited members
    // don't change module types
    // TODO caching optimization
    return classHeader.replaceGenerics(tenv.project.getAuditTrail(), tenv.project,
        Nullable.of(tenv), innerTypes, false, new HashMap<>(), tenv.msgState, lineNum, columnNum);
  }

  @Override
  public List<String> toList() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public UserDeclType asDeclType() {
    return new UndeterminedUserDeclType(outerType, name);
  }

  @Override
  public ModuleType getModuleType() {
    return outerType.get();
  }

  @Override
  public UserDeclType asUserDeclType() {
    return asDeclType();
  }

}
