package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.ClosureValue;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.SandboxModeInfo;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.Pair;
import util.ScopedIdentifiers;

public class LambdaExpression extends AbstractExpression {

  public final Type returnType;
  public final List<DeclarationStatement> params;
  public final BlockStatement body;

  public final FunctionType fullType; // computed

  /*
   * A lambda definition is never tied to a specific class/module. For this
   * reason, a lambda member of a class can never inherently use the 'this' ptr
   * since it is not bound to that instance of the class.
   */
  public LambdaExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Type returnType, List<DeclarationStatement> params,
      BlockStatement body) {
    super(lineNum, columnNum, AstType.LAMBDA_EXPRESSION, preComments, postComments);
    this.returnType = returnType;
    this.params = params;
    this.body = body;

    body.setParentAst(this);

    List<Type> paramTypes = new ArrayList<Type>();
    for (DeclarationStatement param : params) {
      paramTypes.add(param.type);
      param.setParentAst(this);
    }
    this.fullType = new FunctionType(Nullable.empty(), returnType, paramTypes, LambdaStatus.LAMBDA);
  }

  public LambdaExpression(int lineNum, int columnNum, Type returnType,
      List<DeclarationStatement> params, BlockStatement body) {
    super(lineNum, columnNum, AstType.LAMBDA_EXPRESSION);
    this.returnType = returnType;
    this.params = params;
    this.body = body;

    body.setParentAst(this);

    List<Type> paramTypes = new ArrayList<Type>();
    for (DeclarationStatement param : params) {
      paramTypes.add(param.type);
      param.setParentAst(this);
    }
    this.fullType = new FunctionType(Nullable.empty(), returnType, paramTypes, LambdaStatus.LAMBDA);
  }

  public LambdaExpression(Type returnType, List<DeclarationStatement> params, BlockStatement body) {
    super(AstType.LAMBDA_EXPRESSION);
    this.returnType = returnType;
    this.params = params;
    this.body = body;

    body.setParentAst(this);

    List<Type> paramTypes = new ArrayList<Type>();
    for (DeclarationStatement param : params) {
      paramTypes.add(param.type);
      param.setParentAst(this);
    }
    this.fullType = new FunctionType(Nullable.empty(), returnType, paramTypes, LambdaStatus.LAMBDA);
  }

  @Override
  public String toString(String indent) {
    String result = preToString() + "fun ";
    result += returnType + "(";
    boolean first = true;
    for (DeclarationStatement param : params) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += param.toString(indent);
    }
    return result + ") " + body.toString(indent) + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Nullable<SandboxModeInfo> sandboxModeInfo = tenv.inSandboxMode();
    if (sandboxModeInfo.isNotNull()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.SANDBOX,
          "lambda expressions are not allowed in sandbox mode (" + sandboxModeInfo + "). "
          + "Lambdas could not be executed anyway, and dangerous to pass to outside.", lineNum,
          columnNum);
      return Optional.empty();
    }
    
    TypeEnvironment newTenv = tenv.extend();

    // Populate newTenv with params
    for (DeclarationStatement param : params) {
      param.setScoping(VariableScoping.NORMAL);
      param.typecheck(newTenv, Nullable.empty());
      newTenv.lookup(param.name, lineNum, columnNum).get().setIsInit(true);
    }

    if (!body.typecheckAsFunctionBody(newTenv, returnType).isPresent()) {
      return Optional.empty();
    }

    determinedType = fullType.normalize(tenv, lineNum, columnNum).get();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    return new ClosureValue(fullType, params, body, env.makeClosureCopy(), null, null);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.LambdaExpression.Builder subDoc = AstProto.LambdaExpression.newBuilder();

    subDoc.setReturnType(returnType.serialize());

    for (DeclarationStatement param : params) {
      subDoc.addParams(param.serialize());
    }

    subDoc.setBody(body.serialize());

    document.setLambdaExpression(subDoc);
    return document;
  }

  public static LambdaExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.LambdaExpression document) throws SerializationError {
    List<DeclarationStatement> params = new LinkedList<DeclarationStatement>();
    for (AstProto.Ast bv : document.getParamsList()) {
      params.add(DeclarationStatement.deserialize(bv));
    }

    return new LambdaExpression(lineNum, columnNum, preComments, postComments,
        AbstractType.deserialize(document.getReturnType()), params,
        (BlockStatement) AbstractStatement.deserialize(document.getBody()));
  }

  @Override
  public Expression renameIds(mbo.Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    Renamings scopedRenamings = renamings.extend();

    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.renameIds(scopedRenamings, msgState));
    }

    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType.renameIds(scopedRenamings, msgState), newParams,
        (BlockStatement) body.renameIds(scopedRenamings, msgState));
    value.determinedType = determinedType.renameIds(scopedRenamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = body.findFreeVariables();
    // remove params from freeVars. no need to check params for freeVars since no
    // values
    for (DeclarationStatement param : params) {
      freeVars.remove(param.name);
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof LambdaExpression)) {
      return false;
    }
    LambdaExpression ourOther = (LambdaExpression) other;
    if (!ourOther.returnType.equals(returnType)) {
      return false;
    }

    if (ourOther.params.size() != params.size()) {
      return false;
    }
    Iterator<DeclarationStatement> iterOtherP = ourOther.params.iterator();
    Iterator<DeclarationStatement> iterUsP = params.iterator();
    while (iterOtherP.hasNext() && iterUsP.hasNext()) {
      if (!iterOtherP.next().compare(iterUsP.next())) {
        return false;
      }
    }
    return ourOther.body.compare(body);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.replace(target, with));
    }

    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType, newParams, (BlockStatement) body.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (DeclarationStatement param : params) {
      Nullable<Ast> tmp = param.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return body.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      Pair<List<DeclarationStatement>, Ast> tmp = param.toAnf(tuning);
      newParams.add((DeclarationStatement) tmp.b);
      insert.addAll(tmp.a);
    }
    Pair<List<DeclarationStatement>, Ast> bodyPair = body.toAnf(tuning);
    insert.addAll(bodyPair.a);

    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType, newParams, (BlockStatement) bodyPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.replaceType(mapFromTo));
    }

    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType.replaceType(mapFromTo), newParams, (BlockStatement) body.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.replaceVtableAccess(project, msgState).get());
    }

    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType, newParams, (BlockStatement) body.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    List<DeclarationStatement> newParams = new LinkedList<DeclarationStatement>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param
          .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get());
    }

    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType.basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        newParams,
        (BlockStatement) body
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    returnType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    for (DeclarationStatement param : params) {
      param.addDependency(modDepGraph, ourModNode, tenv);
    }

    body.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (body.containsType(types)) {
      return true;
    }
    return fullType.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return body.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> statements = new LinkedList<>();
    if (enableResolution) {
      // timeout check
      List<Type> sanityChecksTypes = new LinkedList<>();
      sanityChecksTypes.add(JsmntGlobal.fnCtxRefType);
      List<Expression> sanityChecksArgs = new LinkedList<>();
      sanityChecksArgs.add(new ReferenceExpression(new IdentifierExpression(JsmntGlobal.fnCtxId)));
      FunctionCallExpression sanityChecksCall = new FunctionCallExpression(
          Nullable.of(new IdentifierExpression(JsmntGlobal.modType.name)),
          new IdentifierExpression(Program.sanityCheckTimeoutFnName), sanityChecksArgs,
          Program.sanityCheckTimeoutFnName);
      sanityChecksCall.calledType = Nullable.of(new FunctionType(JsmntGlobal.modType,
          VoidType.Create(), sanityChecksTypes, LambdaStatus.NOT_LAMBDA));

      statements.add(new FunctionCallStatement(sanityChecksCall));
    }
    if (enableResolution) {
      // recursion check
      List<Type> sanityChecksTypes = new LinkedList<>();
      sanityChecksTypes.add(JsmntGlobal.fnCtxRefType);
      List<Expression> sanityChecksArgs = new LinkedList<>();
      sanityChecksArgs.add(new ReferenceExpression(new IdentifierExpression(JsmntGlobal.fnCtxId)));
      FunctionCallExpression sanityChecksCall = new FunctionCallExpression(
          Nullable.of(new IdentifierExpression(JsmntGlobal.modType.name)),
          new IdentifierExpression(Program.sanityCheckRecursionFnName), sanityChecksArgs,
          Program.sanityCheckRecursionFnName);
      sanityChecksCall.calledType = Nullable.of(new FunctionType(JsmntGlobal.modType,
          VoidType.Create(), sanityChecksTypes, LambdaStatus.NOT_LAMBDA));

      statements.add(new FunctionCallStatement(sanityChecksCall));
    }
    statements.addAll(body.statements);

    Optional<ResolveExcRet> bodyResOpt = new BlockStatement(statements).resolveExceptions(
        project, currentModule, auditEntry, jsmntGlobalRenamings, Nullable.of(fullType), false,
        enableResolution, "", stackSize, msgState);
    if (!bodyResOpt.isPresent()) {
      return Optional.empty();
    }

    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType, params, (BlockStatement) bodyResOpt.get().ast.get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(new LinkedList<>(), Nullable.of(value)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {

    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType, params,
        (BlockStatement) body.insertDestructorCalls(scopedIds, destructorRenamings).statements
            .get(0));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType, params,
        (BlockStatement) body.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<DeclarationStatement> newParams = new LinkedList<>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.resolveUserTypes(msgState).get());
    }
    
    FunctionType determinedFnTy = (FunctionType) determinedType;
    
    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        determinedFnTy.returnType, params,
        (BlockStatement) body.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    List<DeclarationStatement> newParams = new LinkedList<>();
    for (DeclarationStatement param : params) {
      newParams.add((DeclarationStatement) param.addThisExpression(classHeader));
    }
    
    LambdaExpression value = new LambdaExpression(lineNum, columnNum, preComments, postComments,
        returnType, params,
        (BlockStatement) body.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }
}
