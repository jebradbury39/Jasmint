package util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

import errors.Nullable;
import typecheck.UserInstanceType;

public class GenericTree<K, V> implements Iterable<V> {
  public Map<K, GenericTree<K, V>> edges = new HashMap<>();
  public Nullable<V> value = Nullable.empty();
  public boolean valueIsGeneric = false;
  private Nullable<GenericTree<K, V>> parentEdges = Nullable.empty(); //must be same Map memory loc
  public final Nullable<UserInstanceType> currentModuleFullType; //only null for root
  
  public GenericTree(Nullable<UserInstanceType> currentModuleFullType) {
    this.currentModuleFullType = currentModuleFullType;
  }
  
  public GenericTree(Nullable<V> value, Nullable<UserInstanceType> currentModuleFullType) {
    this.value = value;
    this.currentModuleFullType = currentModuleFullType;
  }
  
  //use this for blocks
  public GenericTree<K, V> extend() {
    GenericTree<K, V> newTree = new GenericTree<>(currentModuleFullType);
    newTree.setParentEdges(Nullable.of(this));
    return newTree;
  }
  
  //use this for classes
  public void setParentEdges(Nullable<GenericTree<K, V>> parentEdges) {
    this.parentEdges = parentEdges;
  }
  
  public void putEdge(K name, GenericTree<K, V> edge) {
    edges.put(name, edge);
  }
  
  @Override
  public String toString() {
    String res =  toStringIndent("", 5);
    if (parentEdges != null) {
      res += "\n -> \n" + parentEdges.toString();
    }
    return res;
  }
  
  private String toStringIndent(String indent, int max) {
    if (max <= 0) {
      return "...";
    }
    String result = "(" + value + ", {\n";
    for (Entry<K, GenericTree<K, V>> edge : edges.entrySet()) {
      result += indent + "  " + edge.getKey() + ": "
          + edge.getValue().toStringIndent(indent + "  ", max - 1) + ",\n";
    }
    result += indent + "})";
    return result;
  }
  
  public Nullable<V> lookupEdge(K name) {
    Nullable<GenericTree<K, V>> tmp = lookupEdgeTree(name);
    if (tmp.isNull()) {
      return Nullable.empty();
    }
    return tmp.get().value;
  }
  
  public Nullable<V> lookupPath(List<K> keys) {
    Nullable<GenericTree<K, V>> tmp = lookupPathTree(keys);
    if (tmp.isNull()) {
      return Nullable.empty();
    }
    return tmp.get().value;
  }
  
  public Nullable<GenericTree<K, V>> lookupEdgeTree(K name) {
    List<K> path = new LinkedList<>();
    path.add(name);
    return lookupPathTree(path);
  }
  
  //useful to follow a path from the root
  public Nullable<GenericTree<K, V>> lookupPathTree(List<K> keys) {
    //work left to right
    if (keys.isEmpty()) {
      return Nullable.of(this);
    }
    GenericTree<K, V> tmp = edges.get((keys.get(0)));
    Nullable<GenericTree<K, V>> match = Nullable.empty();
    if (tmp != null) {
      match = Nullable.of(tmp);
    }
    if (match.isNull() && parentEdges.isNotNull()) {
      match = parentEdges.get().lookupEdgeTree(keys.get(0));
    }
    if (match.isNull()) {
      return Nullable.empty();
    }
    return match.get().lookupPathTree(keys.subList(1, keys.size()));
  }
  
  public List<V> convertPath(List<K> keys) {
    //work left to right
    List<V> converted = new LinkedList<>();
    if (keys.isEmpty()) {
      return converted;
    }
    GenericTree<K, V> tmp = edges.get((keys.get(0)));
    Nullable<GenericTree<K, V>> match = Nullable.empty();
    if (tmp != null) {
      match = Nullable.of(tmp);
    }
    if (match.isNull() && parentEdges.isNotNull()) {
      match = parentEdges.get().lookupEdgeTree(keys.get(0));
    }
    if (match.isNull()) {
      return converted;
    }
    if (match.get().value.isNull()) {
      throw new NoSuchElementException("internal error: no such key value");
    }
    converted.add(match.get().value.get());
    converted.addAll(match.get().convertPath(keys.subList(1, keys.size())));
    return converted;
  }
  
  public List<List<K>> recursiveKeys() {
    List<List<K>> keys = new LinkedList<>();
    for (K key : edges.keySet()) {
      List<K> thisKey = new LinkedList<>();
      thisKey.add(key);
      keys.add(thisKey);
      for (GenericTree<K, V> value : edges.values()) {
        for (List<K> x : value.recursiveKeys()) {
          x.add(0, key);
          keys.add(x);
        }
      }
    }
    return keys;
  }

  @Override
  public Iterator<V> iterator() {
    LinkedList<GenericTree<K, V>> self = new LinkedList<>();
    for (GenericTree<K, V> edge : edges.values()) {
      self.addLast(edge);
    }
    Iterator<V> it = new Iterator<V>() {

      private LinkedList<GenericTree<K, V>> queue = self;

      @Override
      public boolean hasNext() {
        return !queue.isEmpty();
      }

      @Override
      public V next() {
        if (!hasNext()) {
          throw new NoSuchElementException("internal error: Generic tree hasNext failed");
        }
        GenericTree<K, V> node = queue.removeFirst();
        for (GenericTree<K, V> edge : node.edges.values()) {
          queue.addLast(edge);
        }
        if (node.value.isNull()) {
          throw new NoSuchElementException("internal error: no such key value while iterating");
        }
        return node.value.get();
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
    return it;
  }

  //Rebox the values into a new generic tree
  public GenericTree<K, V> lightCopy() {
    GenericTree<K, V> newTree = new GenericTree<K, V>(value, currentModuleFullType);
    newTree.valueIsGeneric = valueIsGeneric;
    if (parentEdges.isNotNull()) {
      newTree.parentEdges = Nullable.of(parentEdges.get().lightCopy());
    }
    for (Entry<K, GenericTree<K, V>> edge : edges.entrySet()) {
      newTree.edges.put(edge.getKey(), edge.getValue());
    }
    return newTree;
  }
}
