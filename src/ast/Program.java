package ast;

import antlrgen.JasmintLexer;
import antlrgen.JasmintParser;
import ast.Ast.AnfTune;
import ast.BinaryExpression.BinOp;
import astproto.AstProto;
import audit.AuditEntry;
import audit.AuditEntry.AuditEntryType;
import audit.AuditLiftedClasses;
import audit.AuditRenameIds;
import audit.AuditRenameTypes;
import audit.AuditRenameTypesLite;
import audit.AuditResolveExceptions;
import callgraph.CallNode;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.EnumHeader;
import header.FunctionHeader;
import header.ProgHeader;
import header.ProgItemHeader;
import header.RegisteredGenerics;
import header.RegisteredGenerics.Registration;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImport.ImportUsage;
import import_mgmt.EffectiveImports;
import interp.ArrayValue;
import interp.ClosureValue;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.ValueBox;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.Renamings.RenamingIdKey;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.ArrayType;
import typecheck.BoolType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ImportType;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.StringType;
import typecheck.Type;
import typecheck.UndeterminedImportType;
import typecheck.UserDeclType;
import typecheck.VoidType;
import typegraph.TypeGraph;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;
import visitor.JasmintToAstProgramVisitor;

public class Program {

  public static final String sanityCheckTimeoutFnName = "sanityCheckTimeout";
  public static final String sanityCheckRecursionFnName = "sanityCheckRecursion";
  public static String sanityCheckMemtrackFnName = "sanityCheckMemtrack";

  public final Nullable<String> srcFileName; // null if direct src for unit tests

  public final ModuleStatement moduleStatement;
  public final List<SubmodStatement> submods;
  public final List<ImportStatement> imports;
  /*
   * classDeclarations, functionDeclarations, and statements (declarations should
   * be global)
   */
  public final List<ClassDeclaration> classes;
  public final List<Function> functions;
  public final List<FfiStatement> ffi;
  public final List<EnumDeclaration> enums;

  // reset this before each parse
  // public static List<ClassDeclaration> liftedClasses = new
  // LinkedList<ClassDeclaration>();

  public Program(Nullable<String> srcFileName, ModuleStatement moduleStatement,
      List<SubmodStatement> submods, List<ImportStatement> imports, List<ClassDeclaration> classes,
      List<Function> functions, List<FfiStatement> ffi, List<EnumDeclaration> enums) {
    this.srcFileName = srcFileName;
    this.moduleStatement = moduleStatement;
    this.submods = submods;
    this.imports = imports;
    this.classes = classes;
    this.functions = functions;
    this.ffi = ffi;
    this.enums = enums;
  }

  public List<Ast> getLines() {
    List<Ast> lines = new LinkedList<>();
    lines.addAll(classes);
    lines.addAll(functions);
    lines.addAll(ffi);
    lines.addAll(enums);

    // sort by ordering
    lines.sort(new AbstractAst.SortByOrdering());

    return lines;
  }

  public List<ClassDeclaration> getSortedClasses() {
    List<ClassDeclaration> sclasses = new ArrayList<>(classes);

    sclasses.sort(new AbstractAst.SortByOrdering());

    return sclasses;
  }

  private static JasmintLexer createLexer(String fname, String srcCode) throws IOException {
    CharStream input;
    if (fname == null) {
      if (srcCode == null) {
        System.out.println("Type in Jasmint code\n");
        input = CharStreams.fromStream(System.in);
      } else {
        input = CharStreams.fromString(srcCode);
      }
    } else {
      input = CharStreams.fromFileName(fname);
    }
    return new JasmintLexer(input);
  }

  public static Program loadJasmintFile(String fname) throws Exception {
    return loadJasmintInput(fname, false, createLexer(fname, null));
  }

  public static Program loadJasmintSrc(String srcCode, boolean isJsmntGlobal) throws Exception {
    return loadJasmintInput("<from src>", isJsmntGlobal, createLexer(null, srcCode));
  }

  private static Program loadJasmintInput(String fname, boolean isJsmntGlobal,
      JasmintLexer jasmintLexer) throws Exception {
    CommonTokenStream tokens = new CommonTokenStream(jasmintLexer);
    /*
     * String res = ""; Map<String, Integer> typeMap =
     * jasmintLexer.getTokenTypeMap(); for (org.antlr.v4.runtime.Token tok :
     * jasmintLexer.getAllTokens()) { int ty = tok.getType(); String name = "UNK";
     * for (Entry<String, Integer> entry : typeMap.entrySet()) { if
     * (entry.getValue().equals(ty)) { name = entry.getKey(); break; } } res += "<("
     * + name + "): " + tok.getText() + ">\n"; } System.out.println(res);
     */

    JasmintParser parser = new JasmintParser(tokens);
    ParseTree tree = parser.program();

    Program program = null;
    if (parser.getNumberOfSyntaxErrors() == 0) {
      JasmintToAstProgramVisitor programVisitor = new JasmintToAstProgramVisitor(fname,
          isJsmntGlobal);
      /* the Program is the AST */
      program = programVisitor.visit(tree);
    } else {
      throw new Exception("Parser errors found: " + parser.getNumberOfSyntaxErrors()
          + (fname == null ? "" : " in file " + fname));
    }

    return program;
  }

  @Override
  public String toString() {
    String result = "";

    if (moduleStatement != null) {
      result += moduleStatement + ";\n";
    }

    for (SubmodStatement submod : submods) {
      result += submod + ";\n";
    }

    for (ImportStatement imp : imports) {
      result += imp + ";\n";
    }

    for (Ast line : getLines()) {
      result += line.toString("");
      if (result.charAt(result.length() - 1) != '}') {
        result += ";";
      }
      result += "\n";
    }

    return result;
  }

  public ProgHeader generateHeader(Project project, MsgState msgState)
      throws FatalMessageException {
    Set<String> siblingClasses = new HashSet<>();
    List<FunctionHeader> functionHeaders = new LinkedList<>();
    Set<Pair<String, FunctionType>> fnNames = new HashSet<>();
    List<ClassHeader> classHeaders = new LinkedList<>();
    List<EnumHeader> enumHeaders = new LinkedList<>();
    Set<ModuleType> submodSet = new HashSet<>();
    for (SubmodStatement submod : submods) {
      submodSet.add(submod.absSubmodPath);
    }

    for (Function fn : functions) {
      Pair<String, FunctionType> tmp = new Pair<>(fn.name, fn.fullType);
      if (fnNames.contains(tmp)) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot declare same module function twice: " + fn.name + " with type: "
                + fn.fullType);
        continue;
      }
      fnNames.add(tmp);
      functionHeaders
          .add(fn.generateHeader(moduleStatement.fullType,
              getEffectiveImports(project, false), msgState));
    }

    for (ClassDeclaration cdecl : classes) {
      if (siblingClasses.contains(cdecl.name)) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot declare same module class twice: " + cdecl.name,
            cdecl.lineNum, cdecl.columnNum);
        continue;
      }
      siblingClasses.add(cdecl.name); // can only extend a class which is previously defined
      Optional<ClassHeader> tmpHeader = cdecl.generateHeader(imports, siblingClasses,
          moduleStatement.fullType, false, getEffectiveImports(project, false), msgState);
      if (tmpHeader.isPresent()) {
        classHeaders.add(tmpHeader.get());
      }
    }

    for (EnumDeclaration en : enums) {
      enumHeaders
          .add(en.generateHeader(moduleStatement.fullType, getEffectiveImports(project, false),
              msgState));
    }

    return new ProgHeader(srcFileName.get(), moduleStatement.fullType, submodSet, functionHeaders,
        classHeaders, enumHeaders);
  }

  public Nullable<Function> getMainOrStart(Function.MetaType metaType) {
    for (Function fn : functions) {
      if (fn.metaType == metaType) {
        return Nullable.of(fn);
      }
    }
    return Nullable.empty();
  }

  /*
   * 1) Declare class names 2) Declare class fields (static and non-static) but no
   * inits yet (mark class as complete) and declare all functions 3) Init class
   * fields/functions (static and non-static) (bodies) and init all functions
   * (bodies). Everything has been declared in step 2 and so is ready for use 4)
   * Check for main function
   */

  // populate the tenv.typeGraph
  // we need to add classes from our mod and our imports (we kinda import
  // ourselves, in that
  // a.Test is valid)
  public void populateTypeGraph(TypeGraph typeGraph, LoadedModuleHandle moduleHandle,
      Project project, MsgState msgState) throws FatalMessageException {

    ProgHeader newProg = moduleHandle.moduleBuildObj.progHeader.applyTransforms(project,
        project.getAuditTrail(), moduleHandle.moduleBuildObj, moduleHandle.registeredGenerics,
        msgState);

    newProg.populateTypeGraph(moduleHandle, project, typeGraph, false, msgState);
  }

  public void typecheck(Project project, LoadedModuleHandle moduleHandle, TypeEnvironment tenv)
      throws FatalMessageException {
    boolean hitErr = false;
    /*
     * Class names already typechecked, and we know all the fields. We can check for
     * completeness and that all the field types actually exist (and are complete).
     * All imported types are known to be complete due to module ordering.
     */

    populateTypeGraph(tenv.typeGraph, moduleHandle, project, tenv.msgState);

    // typecheck the imports, but only for the purpose of filling in determined types
    // Tenv is already populated with imports
    for (ImportStatement imp : imports) {
      imp.typecheck(tenv, Nullable.empty());
    }
    
    // typecheck the enums
    for (EnumDeclaration en : enums) {
      en.typecheck(tenv, Nullable.empty());
    }

    // typecheck the function headers

    for (Function fn : functions) {
      fn.setScoping(VariableScoping.GLOBAL);
      fn.typecheckDeclare(tenv);
    }

    // next typecheck class setup and ensure all class members are initialized
    // order of classes matters

    Map<ClassDeclType, TypeEnvironment> classTenvs = new HashMap<>();
    for (ClassDeclaration cdecl : getSortedClasses()) {
      Optional<TypeEnvironment> classTenv = cdecl.typecheckDeclare(tenv);
      if (!classTenv.isPresent()) {
        hitErr = true;
      } else {
        classTenvs.put(new ClassDeclType(cdecl.fullType.outerType, cdecl.name), classTenv.get());
      }
    }
    if (hitErr) {
      return;
    }

    // final general typecheck

    for (Ast line : getLines()) {
      if (line instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) line;
        TypeEnvironment classTenv = classTenvs
            .get(new ClassDeclType(cdecl.fullType.outerType, cdecl.name));
        cdecl.typecheck(classTenv);
      } else {
        line.typecheck(tenv, Nullable.empty());
      }
    }
  }

  // interp stages

  /*
   * 1) Declare class fields and init class functions and global functions
   * (nothing gets executed yet) 2) Init class statics (stuff gets executed,
   * vulnerable to circular ref, but unlikely in real app) 3) Find main function
   * and execute it
   */

  public void interpPreamble(Environment env) throws InterpError, FatalMessageException {
    for (ImportStatement imp : imports) {
      imp.interp(env);
    }
  }

  public void interpDeclarations(Environment env) throws InterpError, FatalMessageException {
    // make class (include instancing stuff, but no static fields, only functions)
    // and function boxes first (can make closures since global is linked, not
    // copied)
    for (Ast line : getLines()) {
      if (line instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) line;
        // only do the declarations for static fields (init the functions, though)
        // populate class static fields (in order to avoid circular issues
        // e.g. A.x = A.fn(); A.fn is a function with: {return A.x})
        cdecl.interp(env);
      } else if (line instanceof Function) {
        Function fn = (Function) line;
        fn.interp(env);
      } else {
        // throw new InterpError("Can only declare functions and class declarations,
        // not: " + line, line.getLine(), line.getColumn());
      }
    }
  }

  public void interpClassStatics(Environment env) throws InterpError, FatalMessageException {
    // init the class statics
    for (Ast line : getLines()) {
      if (line instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) line;
        cdecl.interp(env);
      }
    }
  }

  public void interpMain(Environment env) throws InterpError, FatalMessageException {
    ClosureValue mainClo = findMain(env);
    if (mainClo == null) {
      throw new InterpError("no main() or main([String]) found", -1, -1);
    }

    // TODO populate command line args
    ArrayValue mainArgv = null;
    if (!mainClo.params.isEmpty()) {
      List<Value> cmdlineArgs = new ArrayList<Value>();
      mainArgv = new ArrayValue(StringType.Create(), cmdlineArgs);
    }

    List<Value> mainParams = new ArrayList<Value>();
    if (mainArgv != null) {
      mainParams.add(mainArgv);
    }
    // TODO return int (error code)
    mainClo.interp(mainParams);
  }

  public ClosureValue findMain(Environment env) throws InterpError, FatalMessageException {
    // find main function (make sure it takes no args or a [string] arg)
    ValueBox mainFnBox = env.lookup("main", new FunctionType(moduleStatement.fullType,
        IntType.Create(true, 32), new ArrayList<Type>(), LambdaStatus.NOT_LAMBDA));

    if (mainFnBox == null) {
      List<Type> mainArgs = new ArrayList<Type>();
      mainArgs.add(new ArrayType(StringType.Create()));
      mainFnBox = env.lookup("main", new FunctionType(moduleStatement.fullType,
          IntType.Create(true, 32), mainArgs, LambdaStatus.NOT_LAMBDA));
    }
    if (mainFnBox == null) {
      return null;
    }
    return (ClosureValue) mainFnBox.getValue();
  }

  public void interp(Environment env) throws InterpError, FatalMessageException {
    interpPreamble(env);
    interpDeclarations(env);
    interpClassStatics(env);
    interpMain(env);
  }

  public AstProto.Program.Builder serialize() throws SerializationError {
    AstProto.Program.Builder document = AstProto.Program.newBuilder();

    document.setSrcFilename(srcFileName.get());
    document.addModuleStatement(moduleStatement.serialize());

    for (SubmodStatement submod : submods) {
      document.addSubmods(submod.serialize());
    }

    for (ImportStatement imp : imports) {
      document.addImports(imp.serialize());
    }

    for (ClassDeclaration cdecl : classes) {
      document.addClasses(cdecl.serialize());
    }

    for (Function fn : functions) {
      document.addFunctions(fn.serialize());
    }

    for (FfiStatement f : ffi) {
      document.addFfi(f.serialize());
    }

    for (EnumDeclaration en : enums) {
      document.addEnums(en.serialize());
    }

    return document;
  }

  public static Program deserialize(AstProto.Program document) throws SerializationError {
    Nullable<ModuleStatement> moduleStatement = Nullable.empty();
    if (document.getModuleStatementCount() > 0) {
      moduleStatement = Nullable
          .of((ModuleStatement) AbstractStatement.deserialize(document.getModuleStatement(0)));
    }

    List<SubmodStatement> submods = new LinkedList<>();
    for (AstProto.Ast submod : document.getSubmodsList()) {
      submods.add((SubmodStatement) AbstractStatement.deserialize(submod));
    }

    List<ImportStatement> imports = new LinkedList<>();
    for (AstProto.Ast imp : document.getImportsList()) {
      imports.add((ImportStatement) AbstractStatement.deserialize(imp));
    }

    List<ClassDeclaration> classes = new LinkedList<>();
    for (AstProto.Ast bvCDecl : document.getClassesList()) {
      classes.add(ClassDeclaration.deserialize(bvCDecl));
    }

    List<Function> functions = new LinkedList<>();
    for (AstProto.Ast bvFn : document.getFunctionsList()) {
      functions.add(Function.deserialize(bvFn));
    }

    List<FfiStatement> ffi = new LinkedList<>();
    for (AstProto.Ast bvFfi : document.getFfiList()) {
      ffi.add((FfiStatement) AbstractStatement.deserialize(bvFfi));
    }

    List<EnumDeclaration> enums = new LinkedList<>();
    for (AstProto.Ast bvEnum : document.getEnumsList()) {
      enums.add(EnumDeclaration.deserialize(bvEnum));
    }

    return new Program(Nullable.of(document.getSrcFilename()), moduleStatement.get(), submods,
        imports, classes, functions, ffi, enums);
  }

  public Optional<Program> renameIds(Project project, LoadedModuleHandle moduleHandle,
      TypeEnvironment globalTenv, Renamings lockedGlobalRenamings, MsgState msgState)
          throws FatalMessageException {
    final Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
    filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.INHERITED_IMPORT);
    
    ModuleType currentModType = moduleHandle.getModuleType(project.getAuditTrail(), msgState);
    ImportType currentModImpType = currentModType.asImportType();

    // add imported renamings (full path and end element only)
    Renamings globalRenamings = lockedGlobalRenamings.extend();
    for (Pair<ImportType, Nullable<ProjectNameVersion>> impModType : moduleHandle.getImports(
        project.getAuditTrail(), msgState).getImportList(true, filterImpTypes))
    {
      // find the renamings for the imported mod
      if (impModType.a.equals(currentModImpType)) {
        continue;
      }
      Nullable<ModuleEndpoint> modEp = project.lookupModule(impModType.a, project.getAuditTrail());
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
            "Failed to find module: " + impModType.a);
      }
      modEp.get().load();
      LoadedModuleHandle impModHandle = (LoadedModuleHandle) modEp.get().getHandle();
      globalRenamings.updateWithImport(
          ((AuditRenameIds) impModHandle.moduleBuildObj.auditTrail.getLast()).renamings);
    }

    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      newClasses.add(cdecl.renameIds(globalTenv, globalRenamings, msgState).get());
    }

    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      newFunctions.add(fn.renameIds(globalRenamings, msgState));
    }

    List<FfiStatement> newFfi = new LinkedList<>();
    for (FfiStatement f : ffi) {
      newFfi.add(f);
    }

    List<EnumDeclaration> newEnums = new LinkedList<>();
    for (EnumDeclaration en : enums) {
      newEnums.add(en.renameIds(globalRenamings, msgState));
    }

    if (msgState.hitError()) {
      return Optional.empty();
    }
    return Optional.of(new Program(srcFileName, moduleStatement, submods, imports, newClasses,
        newFunctions, newFfi, newEnums));
  }

  public Program toAnf(AnfTune tuning) {
    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      Pair<List<DeclarationStatement>, Ast> tmp = cdecl.toAnf(tuning);
      // newClasses.addAll(tmp.a);
      newClasses.add((ClassDeclaration) tmp.b);
    }

    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      Pair<List<DeclarationStatement>, Ast> tmp = fn.toAnf(tuning);
      // newFunctions.addAll(tmp.a);
      newFunctions.add((Function) tmp.b);
    }

    List<FfiStatement> newFfi = new LinkedList<>();
    for (FfiStatement f : ffi) {
      Pair<List<DeclarationStatement>, Ast> tmp = f.toAnf(tuning);
      // newFfi.addAll(tmp.a);
      newFfi.add((FfiStatement) tmp.b);
    }

    return new Program(srcFileName, moduleStatement, submods, imports, newClasses, newFunctions,
        newFfi, enums);
  }

  public void populateTypeRenamings(AuditRenameTypes auditEntry,
      AuditRenameTypesLite moduleRenamings, boolean moduleOnly, MsgState msgState)
          throws FatalMessageException {
    if (moduleOnly) {
      moduleStatement.populateTypeRenamings(auditEntry, moduleRenamings,
          msgState);
      return;
    }
    ModuleType newModuleType = moduleRenamings.getNewType(moduleStatement.fullType).get();

    for (EnumDeclaration en : enums) {
      en.populateTypeRenamings(newModuleType, auditEntry);
    }

    for (ClassDeclaration cdecl : classes) {
      cdecl.populateTypeRenamings(newModuleType, auditEntry);
    }
  }

  public Program replaceType(Map<Type, Type> typeRenamings) {

    List<ImportStatement> newImports = new LinkedList<>();
    for (ImportStatement imp : imports) {
      newImports.add((ImportStatement) imp.replaceType(typeRenamings));
    }

    List<SubmodStatement> newSubmods = new LinkedList<>();
    for (SubmodStatement submod : submods) {
      newSubmods.add((SubmodStatement) submod.replaceType(typeRenamings));
    }

    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      newClasses.add((ClassDeclaration) cdecl.replaceType(typeRenamings, "", false));
    }

    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      newFunctions.add((Function) fn.replaceType(typeRenamings));
    }

    List<FfiStatement> newFfi = new LinkedList<>();
    for (FfiStatement f : ffi) {
      newFfi.add((FfiStatement) f.replaceType(typeRenamings));
    }

    List<EnumDeclaration> newEnums = new LinkedList<>();
    for (EnumDeclaration en : enums) {
      newEnums.add((EnumDeclaration) en.replaceType(typeRenamings));
    }

    return new Program(srcFileName, (ModuleStatement) moduleStatement.replaceType(typeRenamings),
        newSubmods, newImports, newClasses, newFunctions, newFfi, newEnums);
  }

  /*
   * Also creates the default constructor if there is no constructor. Should be
   * done BEFORE renaming
   */
  public Program moveInitsToConstructors(Project project, MsgState msgState)
      throws FatalMessageException {

    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      newClasses.add((ClassDeclaration) cdecl.moveInitsToConstructors(project, msgState));
    }

    return new Program(srcFileName, moduleStatement, submods, imports, newClasses, functions, ffi,
        enums);
  }

  /////////////////////////////////////////////// Resolve Generics Stage (must be
  /////////////////////////////////////////////// AFTER renaming)

  public Optional<Program> insertResolvedGenerics(Project project,
      RegisteredGenerics regGenerics,
      Map<Type, Type> genericToRealMapping, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {

    List<ClassDeclaration> newClassDecls = new LinkedList<>();
    Set<ClassDeclType> nonTemplateClasses = new HashSet<>();
    // exclude the original class declaration (must only be used for statics)
    for (ClassDeclaration cdecl : classes) {
      if (!cdecl.typedInnerTypes.isEmpty()) {
        // found generic class, but we cannot leave it generic
        String newName = cdecl.name;
        newClassDecls.add((ClassDeclaration) cdecl.filterStatics(false)
            .replaceType(genericToRealMapping, newName, true));
      } else {
        newClassDecls
            .add((ClassDeclaration) cdecl.replaceType(genericToRealMapping, cdecl.name, true));
        nonTemplateClasses.add(new ClassDeclType(cdecl.fullType.outerType, cdecl.fullType.name));
      }
    }

    for (Registration reg : regGenerics.getRegistrations()) {
      Nullable<ClassDeclaration> tmp = reg.getResolvedDecl(project, msgState, genericToRealMapping,
          nonTemplateClasses);
      if (tmp.isNotNull()) {
        newClassDecls.add(tmp.get());
      }
    }

    // perform type replacements in functions
    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      newFunctions.add(fn.replaceType(genericToRealMapping, moduleStatement.fullType));
    }

    Optional<Program> newProg = Optional.of(new Program(srcFileName, moduleStatement, submods,
        imports, newClassDecls, newFunctions, ffi, enums));

    // reorder class definitions (this would only happen with generics)
    ClassGraph classGraph = new ClassGraph(Nullable.of(moduleStatement.fullType), msgState);
    newProg.get().populateClassGraph(classGraph,
        effectiveImports.applyTransforms(project.getAuditTrail(), msgState), msgState);
    newProg = classGraph.reorder(newProg.get(), msgState);

    return newProg;
  }

  // used by resolveGenerics (adds parent links). Contrast with
  // ModuleEndpoint.populateClassGraph
  private void populateClassGraph(ClassGraph classGraph, EffectiveImports effectiveImports,
      MsgState msgState) throws FatalMessageException {
    for (Ast line : getLines()) {
      if (line instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) line;
        Optional<Nullable<Type>> norm = cdecl.fullType.basicNormalize(false, moduleStatement.fullType,
            effectiveImports, msgState);
        ClassType normType = (ClassType) norm.get().get();
        // register with the ClassGraph Engine
        classGraph.addClassNode(new ClassDeclType(normType.outerType, normType.name));
      }
    }

    for (Ast line : getLines()) {
      if (line instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) line;
        // populate member uplinks with ClassGraph Engine
        Nullable<Node> tmp = classGraph
            .findNode(new ClassDeclType(cdecl.fullType.outerType, cdecl.fullType.name));
        if (tmp.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
              "Unable to find node in classGraph: " + classGraph + " for: " + cdecl.fullType);
        }

        if (cdecl.parent.isNotNull()) {
          // add an uplink to the parent
          tmp.get().populateUplinks(classGraph, effectiveImports, cdecl.parent.get(),
              ImportUsage.HARD);
        }

        tmp.get().populateUplinks(classGraph, effectiveImports, cdecl, ImportUsage.HARD);
      }
    }
  }

  /////////////////////////////////////////////// End Resolve Generics Stage

  public Program reorderClassWriteOrder(List<UserDeclType> classWriteOrder, MsgState msgState)
      throws FatalMessageException {
    // Collect all class declarations and map UserType -> Declaration
    // Then write them in order at the start of the program

    Map<UserDeclType, Ast> registered = new HashMap<>();
    List<Ast> otherLines = new LinkedList<>();
    for (Ast line : getLines()) {
      if (line instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) line;
        registered.put(new ClassDeclType(cdecl.fullType.outerType, cdecl.fullType.name), cdecl);
      } else if (line instanceof EnumDeclaration) {
        EnumDeclaration enumDecl = (EnumDeclaration) line;
        registered.put(enumDecl.enumType, enumDecl);
      } else {
        otherLines.add(line);
      }
    }

    List<Ast> newLines = new LinkedList<>();
    for (UserDeclType order : classWriteOrder) {
      Ast ast = registered.get(order);
      if (ast == null) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
            "Failed to find class: " + order + " in registrations: " + registered.keySet());
      }
      newLines.add(ast);
    }
    newLines.addAll(otherLines);

    int newOrder = 0;
    for (Ast line : newLines) {
      line.setOrdering(newOrder++);
    }

    return this;
  }

  // renaming kept original function and function call data (name, override)
  public Optional<Program> replaceWithVtableAccess(Project project, MsgState msgState)
      throws InterpError, FatalMessageException {

    // replace function usage vtable access if function name is same as old vtable
    // name

    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      newClasses.add((ClassDeclaration) cdecl.replaceVtableAccess(project, msgState).get());
    }

    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      newFunctions.add((Function) fn.replaceVtableAccess(project, msgState).get());
    }

    Optional<Program> newProg = Optional.of(new Program(srcFileName, moduleStatement, submods,
        imports, newClasses, newFunctions, ffi, enums));
    return newProg;
  }

  public List<ModuleType> gatherSubmods() {
    List<ModuleType> absSubmods = new ArrayList<>();
    for (SubmodStatement submod : submods) {
      absSubmods.add(submod.absSubmodPath);
    }
    return absSubmods;
  }

  public Optional<Program> normalizeTypes(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      newClasses.add((ClassDeclaration) cdecl.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState).get());
    }

    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      newFunctions.add((Function) fn
          .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get());
    }

    Optional<Program> newProg = Optional.of(new Program(srcFileName, moduleStatement, submods,
        imports, newClasses, newFunctions, ffi, enums));
    return newProg;
  }

  public void buildModuleDependencyGraph(ClassGraph modDepGraph, TypeEnvironment tenv)
      throws FatalMessageException {
    Node ourModNode = modDepGraph.addModuleNode(moduleStatement.fullType);
    for (Ast line : getLines()) {
      if (line instanceof Function) {
        ((Function) line).buildModuleDependencyGraph(modDepGraph, ourModNode, tenv);
      } else if (line instanceof ClassDeclaration) {
        ((ClassDeclaration) line).buildModuleDependencyGraph(modDepGraph, ourModNode, tenv);
      } else if (line instanceof FfiStatement) {
        // after linking
      } else if (line instanceof EnumDeclaration) {

      } else {
        throw new IllegalArgumentException(
            "internal error: expected function or class declaration but found: " + line);
      }
    }
  }

  // this is called on non-provider module (real module)
  public Optional<Program> linkRequiredProvidedFunctions(Program provMod, MsgState msgState)
      throws FatalMessageException {

    List<ImportStatement> newImports = new LinkedList<>(imports);
    // TODO
    List<ClassDeclaration> newClasses = new LinkedList<>();
    List<Function> newFunctions = new LinkedList<>();
    List<FfiStatement> newFfi = new LinkedList<>();

    // same objects as in lines, but might add prov lines in between

    boolean hitErr = false;
    long currentOrdering = 0;
    Iterator<Ast> iterReqLines = getLines().iterator();
    for (Ast line : provMod.getLines()) {
      long backfillToOrdering = -1;

      if (line instanceof Function) {
        Nullable<Function> reqFn = ((Function) line).linkRequiredProvidedFunctions(this, msgState);
        if (reqFn.isNull()) {
          hitErr = true;
          continue;
        }
        backfillToOrdering = reqFn.get().getOrdering(); // original ordering from require program
      } else if (line instanceof ClassDeclaration) {
        ClassDeclaration cdecl = (ClassDeclaration) line;
        Nullable<ClassDeclaration> reqClass = cdecl.linkRequiredProvidedFunctions(this, msgState);
        if (reqClass.isNull()) {
          hitErr = true;
          continue;
        }
        backfillToOrdering = reqClass.get().getOrdering();
      } else if (line instanceof FfiStatement) {
        // just add new ffi
        line.setOrdering(currentOrdering++);
        newFfi.add((FfiStatement) line);
      }

      if (backfillToOrdering >= 0) {
        // backfill ordering
        while (iterReqLines.hasNext()) {
          Ast reqLine = iterReqLines.next();
          long originalOrdering = reqLine.getOrdering();

          reqLine.setOrdering(currentOrdering++);
          if (reqLine instanceof Function) {
            newFunctions.add((Function) reqLine);
          } else if (reqLine instanceof ClassDeclaration) {
            newClasses.add((ClassDeclaration) reqLine);
          } else if (reqLine instanceof FfiStatement) {
            newFfi.add((FfiStatement) reqLine);
          } else {
            throw new IllegalArgumentException();
          }

          // check if this is the required function we just filled in
          if (originalOrdering == backfillToOrdering) {
            break;
          }
        }
      }
    }

    if (hitErr) {
      return Optional.empty();
    }

    if (findUnlinkedReqFun(msgState)) {
      return Optional.empty();
    }

    return Optional.of(new Program(srcFileName, moduleStatement, submods, newImports, newClasses,
        newFunctions, newFfi, enums));
  }

  public boolean findUnlinkedReqFun(MsgState msgState) throws FatalMessageException {
    boolean foundUnlinked = false;
    for (Ast line : getLines()) {
      // error if this finds any fn with body == null
      if (line instanceof Function) {
        if (((Function) line).findUnlinkedReqFun(msgState)) {
          foundUnlinked = true;
        }
      } else if (line instanceof ClassDeclaration) {
        ((ClassDeclaration) line).findUnlinkedReqFun(msgState);
      }
    }
    return foundUnlinked;
  }

  /*
   * Return map of relative import types to full types
   */
  public EffectiveImports getEffectiveImports(Project project, boolean importedPov)
      throws FatalMessageException {
    EffectiveImports effectiveImps = new EffectiveImports();
    ModuleType.ProjectNameVersion withinProject = project.getModProjectNameVersion();

    EffectiveImport selfImp = new EffectiveImport(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT,
        false, moduleStatement.fullType, Nullable.of(withinProject));
    selfImp.setUsage(EffectiveImport.ImportUsage.HARD); // obviously we use ourself
    effectiveImps.addImport(moduleStatement.fullType, selfImp, false);
    if (!importedPov) {
      effectiveImps.addImport(new ModuleType(Nullable.empty(), moduleStatement.fullType.name),
          selfImp, false);
    }

    if (moduleStatement.fullType.outerType.isNotNull()) {
      ModuleType parent = moduleStatement.fullType.outerType.get();
      // lookup the project the parent is defined in (should be the same project)
      Nullable<Project> definingProj = project.lookupDefiningProject(parent);
      Nullable<ModuleType.ProjectNameVersion> tmpWithinProj = Nullable.empty();
      if (definingProj.isNotNull()) {
        tmpWithinProj = Nullable.of(definingProj.get().getModProjectNameVersion());
      }
      
      EffectiveImport parentImp = new EffectiveImport(
          EffectiveImport.EffectiveImportType.PARENT_IMPORT,
          false, parent, tmpWithinProj);
      effectiveImps.addImport(parent, parentImp, false);
      if (!importedPov) {
        effectiveImps.addImport(new ModuleType(Nullable.empty(), parent.name), parentImp, false);
      }
    }

    for (SubmodStatement submod : submods) {
      // lookup the project the parent is defined in (should be the same project)
      Nullable<Project> definingProj = project.lookupDefiningProject(submod.absSubmodPath);
      Nullable<ModuleType.ProjectNameVersion> tmpWithinProj = Nullable.empty();
      if (definingProj.isNotNull()) {
        tmpWithinProj = Nullable.of(definingProj.get().getModProjectNameVersion());
      }
      
      EffectiveImport submodImp = new EffectiveImport(
          EffectiveImport.EffectiveImportType.IMPLIED_IMPORT,
          true, submod.absSubmodPath, tmpWithinProj);
      effectiveImps.addImport(submod.absSubmodPath, submodImp, false);
      if (!importedPov) {
        effectiveImps.addImport(new ModuleType(Nullable.empty(), submod.absSubmodPath.name),
            submodImp, false);
      }
    }

    for (ImportStatement imp : imports) {
      // lookup the project the parent is defined in (should be the same project)
      Nullable<Project> definingProj = project.lookupDefiningProject(imp.target);
      Nullable<ModuleType.ProjectNameVersion> tmpWithinProj = Nullable.empty();
      if (definingProj.isNotNull()) {
        tmpWithinProj = Nullable.of(definingProj.get().getModProjectNameVersion());
      }
      
      EffectiveImport tmp = new EffectiveImport(EffectiveImport.EffectiveImportType.LITERAL_IMPORT, false,
          imp.target, tmpWithinProj);
      effectiveImps.addImport(imp.target, tmp, false);
      if (!importedPov) {
        effectiveImps.addImport(new ModuleType(Nullable.empty(), imp.target.name), tmp, false);
      }
    }

    return effectiveImps;
  }

  public Nullable<ClassDeclaration> lookupClassDecl(String className) {
    for (ClassDeclaration cdecl : classes) {
      if (cdecl.name.equals(className)) {
        return Nullable.of(cdecl);
      }
    }
    return Nullable.empty();
  }

  public Nullable<Function> lookupFunction(UserDeclType within, String name, FunctionType fullType) {
    if (!within.equals(moduleStatement.fullType)) {
      // within class
      Nullable<ClassDeclaration> cdecl = lookupClassDecl(within.name);
      return cdecl.get().lookupFunction(name, fullType);
    }

    for (Function fn : functions) {
      if (fn.name.equals(name) && fn.fullType.equals(fullType)) {
        return Nullable.of(fn);
      }
    }
    return Nullable.empty();
  }

  /*
   * public ModuleType createRenaming(String moduleAppend) { return (ModuleType)
   * moduleStatement.fullType.modAppend(moduleAppend); }
   */
  public Optional<Set<ModuleType>> findExportedTypes(ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Set<ModuleType> exportedTypes = new HashSet<>();
    Optional<Set<ModuleType>> optSet = Optional.empty();

    for (ClassDeclaration cdecl : classes) {
      optSet = cdecl.findExportedTypes(currentModule, effectiveImports, msgState);
      if (!optSet.isPresent()) {
        return Optional.empty();
      }
    }

    for (Function fn : functions) {
      optSet = fn.findExportedTypes(currentModule, effectiveImports, msgState);
      if (!optSet.isPresent()) {
        return Optional.empty();
      }
    }

    return Optional.of(exportedTypes);
  }

  //////////////////// Resolve Exceptions

  public static class RenamesOfInterest {
    public String defaultConstructor; // Exception.init()
    public String excCodeConstructor; // Exception.init(code)
    public String exc; // FunctionCtx.exc
    public String startTime;
    public String timeoutRemaining;
    public String defaultCtxConstructor; // FunctionCtx.init()
    public String nullPtrExc; // ExcType.nullptrDeref
    public String timeoutExc;
    public String divByZeroExc;
    public String outOfBoundsExc;
    public String checkRecursion;
    public String recursionRemaining;
    public String recursionLimitHit;
    public String memtracker; //FunctionContext.memtracker
    public String memtrackerConstructor; //MemTracker.init(limit)
    public String memtrackerCurrent;
    public String memtrackerLimit;
    public String memLimitHit;
  }

  public static RenamesOfInterest findRenamesOfInterest(Renamings jsmntGlobalRenamings,
      MsgState msgState) throws FatalMessageException {
    
    RenamesOfInterest renames = new RenamesOfInterest();

    Nullable<String> tmp = jsmntGlobalRenamings.lookup("exc", Nullable.empty(),
        Nullable.of(JsmntGlobal.fnCtxClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for exc");
    }
    renames.exc = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("startTime", Nullable.empty(),
        Nullable.of(JsmntGlobal.fnCtxClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for startTime");
    }
    renames.startTime = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("timeoutRemaining", Nullable.empty(),
        Nullable.of(JsmntGlobal.fnCtxClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for timeoutRemaining");
    }
    renames.timeoutRemaining = tmp.get();

    tmp = jsmntGlobalRenamings.lookup(
        "constructor", new FunctionType(JsmntGlobal.exceptionClassType, VoidType.Create(),
            new LinkedList<>(), LambdaStatus.NOT_LAMBDA),
        Nullable.of(JsmntGlobal.exceptionClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for constructor");
    }
    renames.defaultConstructor = tmp.get();

    List<Type> excCodeConstructorParams = new LinkedList<>();
    excCodeConstructorParams.add(JsmntGlobal.excTypeEnumType);
    FunctionType tmpFnTy = new FunctionType(JsmntGlobal.exceptionClassType, VoidType.Create(),
        excCodeConstructorParams, LambdaStatus.NOT_LAMBDA);
    tmp = jsmntGlobalRenamings.lookup("constructor",
        tmpFnTy,
        Nullable.of(JsmntGlobal.exceptionClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for constructor");
    }
    renames.excCodeConstructor = tmp.get();

    tmp = jsmntGlobalRenamings
        .lookup(
            "constructor", new FunctionType(JsmntGlobal.fnCtxClassType, VoidType.Create(),
                new LinkedList<>(), LambdaStatus.NOT_LAMBDA),
            Nullable.of(JsmntGlobal.fnCtxClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for constructor");
    }
    renames.defaultCtxConstructor = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("nullptrDeref", Nullable.empty(),
        Nullable.of(JsmntGlobal.excTypeEnumDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for nullptrDeref");
    }
    renames.nullPtrExc = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("timeoutExpired", Nullable.empty(),
        Nullable.of(JsmntGlobal.excTypeEnumDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for timeoutExpired");
    }
    renames.timeoutExc = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("divByZero", Nullable.empty(),
        Nullable.of(JsmntGlobal.excTypeEnumDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for divByZero");
    }
    renames.divByZeroExc = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("outOfBounds", Nullable.empty(),
        Nullable.of(JsmntGlobal.excTypeEnumDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for outOfBounds");
    }
    renames.outOfBoundsExc = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("checkRecursion", Nullable.empty(),
        Nullable.of(JsmntGlobal.fnCtxClassDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for checkRecursion");
    }
    renames.checkRecursion = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("recursionRemaining", Nullable.empty(),
        Nullable.of(JsmntGlobal.fnCtxClassDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for recursionRemaining");
    }
    renames.recursionRemaining = tmp.get();

    tmp = jsmntGlobalRenamings.lookup("recursionLimitHit", Nullable.empty(),
        Nullable.of(JsmntGlobal.excTypeEnumDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for recursionLimitHit");
    }
    renames.recursionLimitHit = tmp.get();
    
    tmp = jsmntGlobalRenamings.lookup("memLimitHit", Nullable.empty(),
        Nullable.of(JsmntGlobal.excTypeEnumDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for memLimitHit");
    }
    renames.memLimitHit = tmp.get();
    
    tmp = jsmntGlobalRenamings.lookup("memtracker", Nullable.empty(),
        Nullable.of(JsmntGlobal.fnCtxClassDeclType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for memtracker");
    }
    renames.memtracker = tmp.get();
    
    List<Type> memtrackerConstructorParams = new LinkedList<>();
    memtrackerConstructorParams.add(IntType.Create(false, 32));
    tmp = jsmntGlobalRenamings.lookup("constructor",
        new FunctionType(JsmntGlobal.memtrackerClassType, VoidType.Create(),
            memtrackerConstructorParams, LambdaStatus.NOT_LAMBDA),
        Nullable.of(JsmntGlobal.memtrackerClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for MemTracker constructor");
    }
    renames.memtrackerConstructor = tmp.get();
    
    tmp = jsmntGlobalRenamings.lookup("current", Nullable.empty(),
        Nullable.of(JsmntGlobal.memtrackerClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for current");
    }
    renames.memtrackerCurrent = tmp.get();
    
    tmp = jsmntGlobalRenamings.lookup("limit", Nullable.empty(),
        Nullable.of(JsmntGlobal.memtrackerClassType));
    if (tmp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find renaming for limit");
    }
    renames.memtrackerLimit = tmp.get();

    return renames;
  }

  /*
   * After all sanity function calls, make sure to add a check for exception: if
   * (fn_ctx.exc != null) { //if just in a function, then return //else, if in
   * 'timeout' block, then set hitExc to true }
   * 
   * these will raise a resolved exception
   */
  private List<Function> createSanityCheckFunctions(Project project,
      RenamesOfInterest jsmntGlobalRenamings, MsgState msgState) throws FatalMessageException {
    List<Function> newFunctions = new LinkedList<>();

    /*
     * fun void checkSanityTimeout(FunctionCtx* fn_ctx) {
     *   if (fn_ctx.startTime != 0)
     *    {
     *      if (currentUnixEpochTime() - fn_ctx.startTime > fn_ctx.timeoutRemaining) {
     *        fn_ctx.exc = new Exception(15); return;
     *      } } }
     *      
     * fun void checkSanityRecursion(FunctionCtx* fn_ctx) {
     *   if (fn_ctx.recursionRemaining == 0) {
     *     fn_ctx.exc = new Exception(16); return;
     *   }
     *   fn_ctx.recursionRemaining = fn_ctx.recursionRemaining - 1;
     * }
     */
    if (true) {
      List<Statement> statements = new LinkedList<>();
      BinaryExpression timeoutGuard = new BinaryExpression(BinOp.GT,
          new BinaryExpression(BinOp.SUB,
              new FunctionCallExpression(Nullable.empty(),
                  new IdentifierExpression("currentUnixEpochTime"), new LinkedList<>(),
                  "currentUnixEpochTime"),
              new DotExpression(new IdentifierExpression(JsmntGlobal.fnCtxId),
                  new IdentifierExpression(jsmntGlobalRenamings.startTime))),
          new DotExpression(new IdentifierExpression(JsmntGlobal.fnCtxId),
              new IdentifierExpression(jsmntGlobalRenamings.timeoutRemaining)));
  
      List<Type> sanityFnParams = new LinkedList<>();
      sanityFnParams.add(JsmntGlobal.fnCtxRefType);
      statements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
          jsmntGlobalRenamings, "", new FunctionType(JsmntGlobal.modType, VoidType.Create(),
              sanityFnParams, LambdaStatus.NOT_LAMBDA),
          jsmntGlobalRenamings.timeoutExc, timeoutGuard, false, msgState));
  
      List<Statement> timeoutStatements = new LinkedList<>();
  
      timeoutStatements.add(new ConditionalStatement(new BinaryExpression(BinOp.NOT_EQUAL,
          new DotExpression(new IdentifierExpression(JsmntGlobal.fnCtxId),
              new IdentifierExpression(jsmntGlobalRenamings.startTime)),
          new IntegerExpression("0")), new BlockStatement(statements), Nullable.empty()));
      List<DeclarationStatement> fnParams = new LinkedList<>();
      fnParams.add(new DeclarationStatement(JsmntGlobal.fnCtxRefType, JsmntGlobal.fnCtxId,
          Nullable.empty(), false, false, 8));
      Function fnDef = new Function(false, false, false, sanityCheckTimeoutFnName,
          moduleStatement.fullType,
          new FunctionType(moduleStatement.fullType, VoidType.Create(), sanityFnParams,
              LambdaStatus.NOT_LAMBDA),
          fnParams, Nullable.of(new BlockStatement(timeoutStatements)), false,
          Nullable.empty(), Function.MetaType.NORMAL);
      newFunctions.add(fnDef);
    }
    
    /*
     * add recursion check: if (fn_ctx.checkRecursion) { if
     * (fn_ctx.recursionRemaining == 0) { //raise exception }
     * fn_ctx.recursionRemaining = fn_ctx.recursionRemaining - 1; }
     */
    if (true) {
      List<Type> sanityRecFnParams = new LinkedList<>();
      sanityRecFnParams.add(JsmntGlobal.fnCtxRefType);
      List<Statement> recursionBlock = new LinkedList<>();
      recursionBlock.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
          jsmntGlobalRenamings, "",
          new FunctionType(
              JsmntGlobal.modType, VoidType.Create(), sanityRecFnParams, LambdaStatus.NOT_LAMBDA),
          jsmntGlobalRenamings.recursionLimitHit,
          new BinaryExpression(BinOp.EQUAL,
              new DotExpression(new IdentifierExpression(JsmntGlobal.fnCtxId),
                  new IdentifierExpression(jsmntGlobalRenamings.recursionRemaining)),
              new IntegerExpression("0")), false, msgState));
      recursionBlock.add(new AssignmentStatement(
          new LvalueDot(new IdentifierExpression(JsmntGlobal.fnCtxId),
              jsmntGlobalRenamings.recursionRemaining),
          new BinaryExpression(BinOp.SUB,
              new DotExpression(new IdentifierExpression(JsmntGlobal.fnCtxId),
                  new IdentifierExpression(jsmntGlobalRenamings.recursionRemaining)),
              new IntegerExpression("1"))));
      ConditionalStatement checkRecursionCond = new ConditionalStatement(
          new DotExpression(new IdentifierExpression(JsmntGlobal.fnCtxId),
              new IdentifierExpression(jsmntGlobalRenamings.checkRecursion)),
          new BlockStatement(recursionBlock), Nullable.empty());
  
      List<Statement> recStatements = new LinkedList<>();
      recStatements.add(checkRecursionCond);
      BlockStatement recFnBody = new BlockStatement(recStatements);
      List<DeclarationStatement> recFnParams = new LinkedList<>();
      recFnParams.add(new DeclarationStatement(JsmntGlobal.fnCtxRefType, JsmntGlobal.fnCtxId,
          Nullable.empty(), false, false, 8));
      Function recFnDef = new Function(false, false, false, sanityCheckRecursionFnName,
          moduleStatement.fullType,
          new FunctionType(moduleStatement.fullType, VoidType.Create(), sanityRecFnParams,
              LambdaStatus.NOT_LAMBDA),
          recFnParams, Nullable.of(recFnBody), false, Nullable.empty(),
          Function.MetaType.NORMAL);
      newFunctions.add(recFnDef);
    }
    
    /*
     * fun void sanityCheckMemtrack(FunctionContext* fun_ctx, uint32 nbytes, bool toFree) {
     *   if (fun_ctx == null) {
     *     return;
     *   }
     *   if (fun_ctx.memtracker != null) {
     *     if (toFree) {
     *       if (nbytes > fun_ctx.memtracker.current) {
     *         //raise exception: trying to free more than we allocated
     *       }
     *       fun_ctx.memtracker.current = fun_ctx.memtracker.current - nbytes;
     *     } else {
     *       if (fun_ctx.memtracker.current + nbytes > fun_ctx.memtracker.limit) {
     *         //raise exception: trying to allocate over the limit
     *       }
     *       fun_ctx.memtracker.current = fun_ctx.memtracker.current + nbytes;
     *     }
     *   }
     * }
     */
    if (true) {
      List<Type> fnParamTypes = new LinkedList<>();
      fnParamTypes.add(JsmntGlobal.fnCtxRefType);
      fnParamTypes.add(IntType.Create(false, 32));
      fnParamTypes.add(BoolType.Create());
      
      DotExpression memtrackerCurrent = new DotExpression(new DotExpression(
          new IdentifierExpression(JsmntGlobal.fnCtxId),
          new IdentifierExpression(jsmntGlobalRenamings.memtracker)),
          new IdentifierExpression(jsmntGlobalRenamings.memtrackerCurrent));
      DotExpression memtrackerLimit = new DotExpression(new DotExpression(
          new IdentifierExpression(JsmntGlobal.fnCtxId),
          new IdentifierExpression(jsmntGlobalRenamings.memtracker)),
          new IdentifierExpression(jsmntGlobalRenamings.memtrackerLimit));
      LvalueDot memtrackerCurrentLvalue = new LvalueDot(new DotExpression(
          new IdentifierExpression(JsmntGlobal.fnCtxId),
          new IdentifierExpression(jsmntGlobalRenamings.memtracker)),
          jsmntGlobalRenamings.memtrackerCurrent);
      
      List<Statement> ifToFreeStatements = new LinkedList<>();
      ifToFreeStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
          jsmntGlobalRenamings,
          "",
          new FunctionType(
              JsmntGlobal.modType, VoidType.Create(), fnParamTypes, LambdaStatus.NOT_LAMBDA),
          jsmntGlobalRenamings.memLimitHit,
          new BinaryExpression(BinOp.GT,
              new IdentifierExpression("nbytes"), memtrackerCurrent), false, msgState));
      ifToFreeStatements.add(new AssignmentStatement(memtrackerCurrentLvalue,
          new BinaryExpression(BinOp.SUB, memtrackerCurrent, new IdentifierExpression("nbytes"))));
      
      List<Statement> notToFreeStatements = new LinkedList<>();
      notToFreeStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
          jsmntGlobalRenamings,
          "",
          new FunctionType(
              JsmntGlobal.modType, VoidType.Create(), fnParamTypes, LambdaStatus.NOT_LAMBDA),
          jsmntGlobalRenamings.memLimitHit,
          new BinaryExpression(BinOp.GT,
              new BinaryExpression(BinOp.ADD,
                  new IdentifierExpression("nbytes"), memtrackerCurrent),
              memtrackerLimit), false, msgState));
      notToFreeStatements.add(new AssignmentStatement(memtrackerCurrentLvalue,
          new BinaryExpression(BinOp.ADD, memtrackerCurrent, new IdentifierExpression("nbytes"))));
      
      List<Statement> statements = new LinkedList<>();
      statements.add(new ConditionalStatement(new IdentifierExpression("toFree"),
          new BlockStatement(ifToFreeStatements),
          Nullable.of(new BlockStatement(notToFreeStatements))));
      
      
      List<DeclarationStatement> fnParams = new LinkedList<>();
      fnParams.add(new DeclarationStatement(JsmntGlobal.fnCtxRefType, JsmntGlobal.fnCtxId,
          Nullable.empty(), false, false, 8));
      fnParams.add(new DeclarationStatement(IntType.Create(false, 32), "nbytes", Nullable.empty(),
          false, false, 4));
      fnParams.add(new DeclarationStatement(BoolType.Create(), "toFree", Nullable.empty(),
          false, false, 1));
      
      List<Statement> skipCheckStatements = new LinkedList<>();
      skipCheckStatements.add(new ReturnStatement(Nullable.empty()));
      
      List<Statement> fnStatements = new LinkedList<>();
      fnStatements.add(new ConditionalStatement(new BinaryExpression(BinOp.EQUAL,
          new IdentifierExpression(JsmntGlobal.fnCtxId), new NullExpression()),
          new BlockStatement(skipCheckStatements), Nullable.empty()));
      fnStatements.add(new ConditionalStatement(new BinaryExpression(BinOp.NOT_EQUAL,
          new DotExpression(new IdentifierExpression(JsmntGlobal.fnCtxId),
              new IdentifierExpression(jsmntGlobalRenamings.memtracker)),
          new NullExpression()), new BlockStatement(statements), Nullable.empty()));
      
      Function fnDef = new Function(false, false, false, sanityCheckMemtrackFnName,
          moduleStatement.fullType,
          new FunctionType(moduleStatement.fullType, VoidType.Create(), fnParamTypes,
              LambdaStatus.NOT_LAMBDA),
          fnParams, Nullable.of(new BlockStatement(fnStatements)), false,
          Nullable.empty(),
          Function.MetaType.NORMAL);
      newFunctions.add(fnDef);
    }

    return newFunctions;
  }

  public Optional<Program> resolveExceptions(Project project, AuditResolveExceptions auditEntry,
      Renamings jsmntGlobalRenamings,
      MsgState msgState) throws FatalMessageException {

    RenamesOfInterest renamings = findRenamesOfInterest(jsmntGlobalRenamings, msgState);

    List<ClassDeclaration> newClasses = new LinkedList<>();
    Optional<ResolveExcRet> tmp = Optional.empty();
    for (ClassDeclaration cdecl : classes) {
      //cdecl = cdecl.prependFunctionArg(JsmntGlobal.fnCtxRefType, JsmntGlobal.fnCtxId, msgState);
      tmp = cdecl.resolveExceptions(project, moduleStatement.fullType, auditEntry, renamings,
          Nullable.empty(), false, false, "", 0, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newClasses.add((ClassDeclaration) tmp.get().ast.get());
    }

    List<Function> newFunctions = new LinkedList<>();

    // add checkTimeout function (and friends) if this is the jsmnt_global module
    if (moduleStatement.fullType.equals(JsmntGlobal.modType)) {
      newFunctions.addAll(createSanityCheckFunctions(project, renamings, msgState));
    }
    
    for (Function fn : functions) {
      tmp = fn.resolveExceptions(project, moduleStatement.fullType, auditEntry, renamings,
          Nullable.empty(), false, false, "", 0, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newFunctions.add((Function) tmp.get().ast.get());
    }

    Optional<Program> newProg = Optional.of(new Program(srcFileName, moduleStatement, submods,
        imports, newClasses, newFunctions, ffi, enums));
    return newProg;
  }

  public Program insertStartFunction() {
    // only add start if we have defined main
    Nullable<Function> mainFn = getMainOrStart(Function.MetaType.MAIN);
    if (mainFn.isNull()) {
      return this;
    }
    List<Expression> mainCallArgs = new LinkedList<>();
    if (!mainFn.get().params.isEmpty()) {
      mainCallArgs.add(new IdentifierExpression("argv"));
    }
    FunctionCallExpression mainFnCall = new FunctionCallExpression(Nullable.empty(),
        new IdentifierExpression("main"), mainCallArgs, "main");

    /*
     * fun int start([int] argv) { return main(argv); }
     * 
     * After resolving exceptions:
     * 
     * fun int start([int] argv) { jsmnt_global.ExceptionRef exc; exc.ref = null;
     * int main_rc = main(&exc, argv); if (exc.ref != null) { //print out backtrace
     * return 255; } return main_rc; }
     */
    List<Function> newFunctions = new LinkedList<>();
    newFunctions.addAll(functions);

    List<DeclarationStatement> startFnParams = new LinkedList<>();
    startFnParams.add(new DeclarationStatement(new ArrayType(StringType.Create()), "argv",
        Nullable.empty(), false, false, 16));

    List<Type> paramTypes = new LinkedList<>();
    paramTypes.add(new ArrayType(StringType.Create()));

    List<Statement> statements = new LinkedList<>();
    statements.add(new ReturnStatement(Nullable.of(mainFnCall)));
    BlockStatement startFnBody = new BlockStatement(statements);
    Function startFn = new Function(false, false, false, "start", moduleStatement.fullType,
        new FunctionType(moduleStatement.fullType, IntType.Create(true, 32), paramTypes,
            LambdaStatus.NOT_LAMBDA),
        startFnParams, Nullable.of(startFnBody), false, Nullable.empty(),
        Function.MetaType.START);

    newFunctions.add(startFn);

    return new Program(srcFileName, moduleStatement, submods, imports, classes, newFunctions, ffi,
        enums);
  }
  
  public static class DestructorRenamings {
    //all abs class types
    private final Map<ClassDeclType, String> destructorRenamings = new HashMap<>();
    public final Project project;
    public final MsgState msgState;
    
    public DestructorRenamings(Project project, MsgState msgState) {
      this.project = project;
      this.msgState = msgState;
    }
    
    public void initOurRenamings(Program program)
        throws FatalMessageException {
      
      EffectiveImports effectiveImports = program.getEffectiveImports(project, false);
      
      Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
      filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
      filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
      
      for (Pair<ImportType, Nullable<ProjectNameVersion>> effImport
          : effectiveImports.getImportList(true, filterImpTypes)) {
        Nullable<ModuleEndpoint> globalModEp = project.lookupModule(effImport.a,
            project.getAuditTrail());
        if (globalModEp.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.INSERT_DESTRUCTORS,
              "Failed to find module: " + effImport.a);
        }
        globalModEp.get().load();
        LoadedModuleHandle globalModHandle = (LoadedModuleHandle) globalModEp.get().getHandle();
        Nullable<AuditEntry> globalRenames = globalModHandle.moduleBuildObj.auditTrail.getCombined(
            AuditEntryType.RENAME_IDS);
        if (globalRenames.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
              "Have not done any renaming yet");
        }
        AuditRenameIds globalRenaming = (AuditRenameIds) globalRenames.get();
        
        for (Entry<RenamingIdKey, String> entry : globalRenaming.renamings.renamings.entrySet()) {
          if (entry.getKey().within.instanceOf(ClassDeclType.class)) {
            if (entry.getKey().fnType.isNotNull() && entry.getKey().id.equals("destructor")) {
              destructorRenamings.put((ClassDeclType) entry.getKey().within.get(),
                  entry.getValue());
            }
          }
        }
      }
    }
    
    public String getRenaming(ClassDeclType cdeclType) throws FatalMessageException {
      String result = destructorRenamings.get(cdeclType);
      if (result == null) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.INSERT_DESTRUCTORS,
            "Cannot find destructor renaming for class: " + cdeclType);
      }
      return result;
    }
  }
  
  /*
   * Insert destructor calls right before variables go out of scope, and right before delete.
   * Also ensure that all declarations are initialized.
   */
  public Program insertDestructorCalls(Project project, MsgState msgState)
      throws FatalMessageException {
    
    DestructorRenamings destructorRenamings = new DestructorRenamings(project, msgState);
    destructorRenamings.initOurRenamings(this);
    
    ScopedIdentifiers scopedIds = new ScopedIdentifiers(ScopeFilter.GLOBAL);
    
    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      InsertDestructorsRet tmp = cdecl.insertDestructorCalls(scopedIds, destructorRenamings);
      newClasses.add((ClassDeclaration) tmp.statements.get(0));
    }
    
    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      newFunctions.add(fn.insertDestructorCalls(scopedIds, destructorRenamings, Nullable.empty()));
    }
    
    return new Program(srcFileName, moduleStatement, submods, imports, newClasses, newFunctions,
        ffi, enums);
  }

  public void sandboxValidate(Set<CallNode> sandboxCalledFunctions) {
    // TODO Auto-generated method stub
    
  }
  
  /*
   * All class declarations that are not currently defined at program level need to be lifted up.
   * They will need unique names, and the old usage of the old names needs to be replaced. These
   * renamings must be local to the scope that the class is defined within.
   * This will function as a transform on the progheader, which will add in these new classheaders.
   * These classes can never be exported, so only this module will ever use them.
   * If the class is within a sandbox, the sandbox must be given access to the new class name.
   */
  public Program liftClassDeclarations(Project project, AuditLiftedClasses auditEntry,
      MsgState msgState) throws FatalMessageException {
    List<ClassDeclaration> liftedClasses = new LinkedList<>();
    TypeReplacements replacements = new TypeReplacements(msgState);
    
    // don't lift classes that are already at program-level, so we skip to analyze the sections
    Set<String> siblingClasses = new HashSet<>();
    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      newClasses.add(cdecl.helperLiftClassDeclarations(liftedClasses, replacements, cdecl.name));
      siblingClasses.add(cdecl.name);
    }
    
    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      newFunctions.add((Function) fn.liftClassDeclarations(liftedClasses, replacements).get());
    }
    
    // now add in all the classes we just lifted and construct headers
    EffectiveImports effectiveImports = getEffectiveImports(project, false);
    for (ClassDeclaration cdecl : liftedClasses) {
      siblingClasses.add(cdecl.name);
      Optional<ClassHeader> classHeader = cdecl.generateHeader(imports, siblingClasses,
          moduleStatement.fullType,
          project.getAuditTrail().contains(AuditEntryType.RESOLVE_GENERICS),
          effectiveImports, msgState);
      auditEntry.liftedClasses.add(classHeader.get());
      
      newClasses.add(cdecl);
    }
    
    return new Program(srcFileName, moduleStatement, submods, imports, newClasses, newFunctions,
        ffi, enums);
  }

  public Optional<Program> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      newClasses.add((ClassDeclaration) cdecl.resolveUserTypes(msgState).get());
    }

    List<Function> newFunctions = new LinkedList<>();
    for (Function fn : functions) {
      newFunctions.add((Function) fn.resolveUserTypes(msgState).get());
    }

    Optional<Program> newProg = Optional.of(new Program(srcFileName, moduleStatement, submods,
        imports, newClasses, newFunctions, ffi, enums));
    return newProg;
  }

  public Optional<Program> resolveImports(Project project, MsgState msgState)
      throws FatalMessageException {
    
    List<ImportStatement> newImports = new LinkedList<>();
    boolean hitErr = false;
    
    for (ImportStatement imp : imports) {
      Optional<ImportType> newImportType = project.resolveImportType(imp.target,
          project.getAuditTrail(), msgState, imp.lineNum, imp.columnNum);
      if (!newImportType.isPresent()) {
        hitErr = true;
        continue;
      }
      imp.determinedType = newImportType.get();
      Optional<Statement> resolvedImp = imp.resolveUserTypes(msgState);
      if (!resolvedImp.isPresent()) {
        hitErr = true;
        continue;
      }
      newImports.add((ImportStatement) resolvedImp.get());
    }
    if (hitErr) {
      return Optional.empty();
    }
    return Optional.of(new Program(srcFileName, moduleStatement, submods, newImports, classes,
        functions, ffi, enums));
  }

  public Program removeNonModuleImports() {
    List<ImportStatement> newImports = new LinkedList<>();
    Map<ModuleType, ImportStatement> importedModules = new HashMap<>();
    for (ImportStatement imp : imports) {
      if (imp.target instanceof UndeterminedImportType) {
        throw new IllegalArgumentException("Must have already resolved imports");
      } else {
        // only save the module type
        importedModules.put(imp.target.asModuleType(), imp);
      }
    }
    
    for (Entry<ModuleType, ImportStatement> entry : importedModules.entrySet()) {
      ImportStatement imp = entry.getValue();
      ImportType importType = entry.getKey().asImportType();
      ImportStatement tmp = new ImportStatement(imp.lineNum, imp.columnNum,
          imp.preComments, imp.postComments,
          importType);
      tmp.determinedType = importType;
      tmp.setOriginalLabel(imp.getOriginalLabel());
      
      newImports.add(tmp);
    }
    
    return new Program(srcFileName, moduleStatement, submods, newImports, classes,
        functions, ffi, enums);
  }
  
  // add the 'this' expr where needed in class declarations
  public Program addThisExpression(ProgHeader progHeader, MsgState msgState) {
    List<ClassDeclaration> newClasses = new LinkedList<>();
    for (ClassDeclaration cdecl : classes) {
      // lookup the class header
      Nullable<ProgItemHeader> pch = progHeader.lookupItem(cdecl.name);
      if (pch.isNull()) {
        
      }
      if (!(pch.get() instanceof ClassHeader)) {
        
      }
      ClassHeader ch = (ClassHeader) pch.get();
      ClassDeclaration newCdecl = (ClassDeclaration) cdecl.addThisExpression(ch);
      newClasses.add(newCdecl);
    }
    
    return new Program(srcFileName, moduleStatement, submods, imports, newClasses,
        functions, ffi, enums);
  }
}
