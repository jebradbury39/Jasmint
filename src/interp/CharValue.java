package interp;

import typecheck.AbstractType;
import typecheck.CharType;
import typecheck.IntType;

public class CharValue extends AbstractValue {

  public final char value;
  
  public CharValue(char value) {
    super(MetaValue.NORMAL, CharType.Create());
    this.value = value;
  }
  
  public CharValue(MetaValue metaValue, char value) {
    super(metaValue, CharType.Create());
    this.value = value;
  }

  @Override
  public Value add(Value rightValue) throws InterpError {
    return rightValue.add(value);
  }
  
  @Override
  public Value add(long leftValue, IntType leftType) {
    return new IntValue(leftValue + value, (IntType) AbstractType.makePromotion(leftType, type));
  }
  
  @Override
  public Value add(double leftValue) {
    return new Float32Value(leftValue + value);
  }
  
  @Override
  public Value add(String leftValue) {
    return new StringValue(leftValue + value);
  }

  @Override
  public Value sub(Value rightValue) throws InterpError {
    return rightValue.sub(value);
  }
  
  @Override
  public Value sub(long leftValue, IntType leftType) {
    return new IntValue(leftValue - value, (IntType) AbstractType.makePromotion(leftType, type));
  }
  
  @Override
  public Value sub(double leftValue) {
    return new Float32Value(leftValue - value);
  }

  @Override
  public long toInt() throws InterpError {
    return value;
  }

  @Override
  public boolean toBool() throws InterpError {
    return value != 0;
  }

  @Override
  public Value mul(Value rightValue) throws InterpError {
    return rightValue.mul(value);
  }
  
  @Override
  public Value mul(long leftValue, IntType leftType) throws InterpError {
    return new IntValue(leftValue * value, (IntType) AbstractType.makePromotion(leftType, type));
  }
  
  @Override
  public Value mul(double leftValue) throws InterpError {
    return new Float32Value(leftValue * (double)value);
  }

  @Override
  public Value div(Value rightValue) throws InterpError {
    return rightValue.div(value);
  }
  
  @Override
  public Value div(long leftValue, IntType leftType) throws InterpError {
    if (value == 0) {
      throw new InterpError("division by zero", -1, -1);
    }
    return new IntValue(leftValue / (int)value,
        (IntType) AbstractType.makePromotion(leftType, type));
  }
  
  @Override
  public Value div(double leftValue) throws InterpError {
    if (value == 0) {
      throw new InterpError("division by zero", -1, -1);
    }
    return new Float32Value(leftValue / (double)value);
  }

  @Override
  public Value neg() throws InterpError {
    return new CharValue((char)(-value));
  }

  @Override
  public Value copy(Environment copyEnv) {
    return new CharValue(value);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return ((CharValue) other).value == value;
  }
  
}
