package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.SandboxModeInfo;
import type_env.TypeEnvironment.TypeStatus;
import typecheck.AbstractType;
import typecheck.Float32Type;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class TimeoutExpression extends AbstractExpression {

  public final Type returnType;
  public final Expression timeoutValue;
  public final BlockStatement body;
  public final CatchExcSection catchSection;

  public TimeoutExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Type returnType, Expression timeoutValue, BlockStatement body,
      CatchExcSection catchSection) {
    super(lineNum, columnNum, AstType.TIMEOUT_EXPRESSION, preComments, postComments);
    this.returnType = returnType;
    this.timeoutValue = timeoutValue;
    this.body = body;
    this.catchSection = catchSection;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType.renameIds(renamings, msgState), timeoutValue.renameIds(renamings, msgState),
        (BlockStatement) body.renameIds(renamings, msgState),
        (CatchExcSection) catchSection.renameIds(renamings, msgState));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType.basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        timeoutValue
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get(),
        (BlockStatement) body
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get(),
        (CatchExcSection) catchSection
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(val));
  }

  @Override
  public String toString(String indent) {
    return "timeout " + returnType + " (" + timeoutValue.toString(indent) + ") "
        + body.toString(indent) + " " + catchSection.toString(indent);
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Nullable<SandboxModeInfo> sandboxModeInfo = tenv.inSandboxMode();
    if (sandboxModeInfo.isNotNull()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.SANDBOX,
          "timeout expressions are not allowed in sandbox mode (" + sandboxModeInfo + ")", lineNum,
          columnNum);
      return Optional.empty();
    }

    // first check returnType is valid
    Optional<TypeStatus> optTyStatus = tenv.lookupType(returnType, lineNum, columnNum);
    if (!optTyStatus.isPresent()) {
      return Optional.empty();
    }
    TypeStatus tyStatus = optTyStatus.get();
    if (tyStatus == TypeStatus.UNDEFINED) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "timeout block cannot return '" + returnType + "' (this type is undefined)", lineNum,
          columnNum);
      return Optional.empty();
    }

    // now ensure that timeoutValue returns a float32
    Optional<TypecheckRet> retOpt = timeoutValue.typecheck(tenv, Nullable.empty());
    if (!retOpt.isPresent()) {
      return Optional.empty();
    }
    Type timeoutType = retOpt.get().type;
    if (!tenv.canSafeCast(timeoutType, Float32Type.Create())) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Timeout must be float32 type, not: " + timeoutType, lineNum, columnNum);
      return Optional.empty();
    }

    // ensure that the body will return a value of returnType
    TypeEnvironment bodyTenv = tenv.extend();
    Optional<Type> typeOpt = body.typecheckAsFunctionBody(bodyTenv, returnType);
    if (!typeOpt.isPresent()) {
      return Optional.empty();
    }

    // ensure that the catch section will return a value of returnType
    typeOpt = catchSection.typecheckAsFunctionBody(tenv, returnType);
    if (!typeOpt.isPresent()) {
      return Optional.empty();
    }

    determinedType = returnType.normalize(tenv, lineNum, columnNum).get();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.TimeoutExpression.Builder subDoc = AstProto.TimeoutExpression.newBuilder();

    subDoc.setReturnType(returnType.serialize());
    subDoc.setTimeoutValue(timeoutValue.serialize());
    subDoc.setBody(body.serialize());
    subDoc.setCatchSection(catchSection.serialize());

    AstProto.Ast.Builder document = preSerialize();
    document.setTimeoutExpression(subDoc);
    return document;
  }

  public static TimeoutExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.TimeoutExpression document) throws SerializationError {

    return new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        AbstractType.deserialize(document.getReturnType()),
        AbstractExpression.deserialize(document.getTimeoutValue()),
        (BlockStatement) AbstractStatement.deserialize(document.getBody()),
        (CatchExcSection) AbstractExpression.deserialize(document.getCatchSection()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = catchSection.findFreeVariables();
    freeVars.putAll(body.findFreeVariables());
    freeVars.putAll(timeoutValue.findFreeVariables());
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof TimeoutExpression)) {
      return false;
    }
    TimeoutExpression ourOther = (TimeoutExpression) other;

    if (!returnType.equals(ourOther.returnType)) {
      return false;
    }
    if (!timeoutValue.compare(ourOther.timeoutValue)) {
      return false;
    }
    if (!body.compare(ourOther.body)) {
      return false;
    }
    return catchSection.compare(ourOther.catchSection);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType, (Expression) timeoutValue.replace(target, with),
        (BlockStatement) body.replace(target, with),
        (CatchExcSection) catchSection.replace(target, with));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = timeoutValue.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    tmp = body.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }

    return catchSection.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    Pair<List<DeclarationStatement>, Ast> tmpPair = timeoutValue.toAnf(tuning);
    insert.addAll(tmpPair.a);
    final Expression newTimeoutValue = (Expression) tmpPair.b;

    tmpPair = body.toAnf(tuning);
    insert.addAll(tmpPair.a);
    final BlockStatement newBody = (BlockStatement) tmpPair.b;

    tmpPair = catchSection.toAnf(tuning);
    insert.addAll(tmpPair.a);
    final CatchExcSection newCatchSection = (CatchExcSection) tmpPair.b;

    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType, newTimeoutValue, newBody, newCatchSection);
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType.replaceType(mapFromTo), (Expression) timeoutValue.replaceType(mapFromTo),
        (BlockStatement) body.replaceType(mapFromTo),
        (CatchExcSection) catchSection.replaceType(mapFromTo));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType, (Expression) timeoutValue.replaceVtableAccess(project, msgState).get(),
        (BlockStatement) body.replaceVtableAccess(project, msgState).get(),
        (CatchExcSection) catchSection.replaceVtableAccess(project, msgState).get());
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    returnType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    timeoutValue.addDependency(modDepGraph, ourModNode, tenv);
    body.addDependency(modDepGraph, ourModNode, tenv);
    catchSection.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (returnType.containsType(types)) {
      return true;
    }
    return timeoutValue.containsType(types) || body.containsType(types)
        || catchSection.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return timeoutValue.containsAst(asts) || body.containsAst(asts)
        || catchSection.containsAst(asts);
  }

  /*
   * will return single-use lambda
   * 
   * timeout int (30) { fn(null); //some other stuff return 0; } catch e {
   * fn2(null, e); return 0; }
   * 
   * Becomes
   * 
   * FunctionCtx tmp_fn_ctx_0 = *fn_ctx; //copy current fn_ctx values
   * tmp_fn_ctx_0.startTime = currentUnixEpochTime();
   * tmp_fn_ctx_0.timeoutRemaining = 30; fun int (FunctionCtx* fn_ctx) {
   * Exception* hitExc = null;
   * 
   * FunctionCtx tmp_fn_ctx_1 = *fn_ctx; fn(&tmp_fn_ctx_1); if (tmp_fn_ctx_1.exc
   * != null) { hitExc = tmp_fn_ctx_1.exc; } if (hitExc != null) { //some other
   * stuff } if (hitExc != null) { return 0; } //catch (no exception handling
   * here) Exception* e = hitExc; FunctionCtx tmp_fn_ctx_2 = *fn_ctx;
   * fn2(&tmp_fn_ctx_2, e); return 0; }(&tmp_fn_ctx_0);
   */
  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    hitExcId = "tmp_hit_exc_" + getTemp();

    Optional<ResolveExcRet> tmp = catchSection.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn, withinFnStatement, false, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    final List<Statement> catchStatements = ((BlockStatement) tmp.get().ast.get()).statements;

    tmp = body.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings, withinFn,
        withinFnStatement, true, false, hitExcId, catchStatements, 0, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    BlockStatement tmpBody = (BlockStatement) tmp.get().ast.get();
    List<Statement> newStatements = new LinkedList<>();
    newStatements.add(new DeclarationStatement(JsmntGlobal.exceptionRefType, hitExcId,
        Nullable.of(new NullExpression()), false, false, 8));
    newStatements.addAll(tmpBody.statements);
    final BlockStatement newBody = new BlockStatement(newStatements);

    final String timeoutLambdaFnCtx = "tmp_" + JsmntGlobal.fnCtxId + "_" + getTemp();
    List<Statement> preStatements = new LinkedList<>();
    tmp = timeoutValue.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    Expression newTimeoutValue = (Expression) tmp.get().ast.get();

    preStatements.add(new DeclarationStatement(JsmntGlobal.fnCtxClassType, timeoutLambdaFnCtx,
        Nullable.of(new DereferenceExpression(new IdentifierExpression(JsmntGlobal.fnCtxId))),
        false, false, JsmntGlobal.fnCtxClassTypeSizeof));
    // set startTime and timeoutRemaining
    // tmp_fn_ctx_0.startTime = currentUnixEpochTime();
    preStatements.add(new AssignmentStatement(
        new LvalueDot(new IdentifierExpression(timeoutLambdaFnCtx), jsmntGlobalRenamings.startTime),
        new FunctionCallExpression(Nullable.empty(),
            new IdentifierExpression("currentUnixEpochTime"), new LinkedList<>(),
            "currentUnixEpochTime")));
    // tmp_fn_ctx_0.timeoutRemaining = 30;
    preStatements
        .add(new AssignmentStatement(new LvalueDot(new IdentifierExpression(timeoutLambdaFnCtx),
            jsmntGlobalRenamings.timeoutRemaining), newTimeoutValue));

    List<DeclarationStatement> timeoutLambdaParams = new LinkedList<>();
    timeoutLambdaParams.add(new DeclarationStatement(JsmntGlobal.fnCtxRefType, JsmntGlobal.fnCtxId,
        Nullable.empty(), false, false, 8));
    LambdaExpression timeoutLambda = new LambdaExpression(returnType, timeoutLambdaParams, newBody);

    List<Expression> timeoutLambdaCallArgs = new LinkedList<>();
    timeoutLambdaCallArgs
        .add(new ReferenceExpression(new IdentifierExpression(timeoutLambdaFnCtx)));
    FunctionCallExpression timeoutLambdaCall = new FunctionCallExpression(Nullable.empty(),
        timeoutLambda, timeoutLambdaCallArgs, "");

    return Optional.of(new ResolveExcRet(preStatements, timeoutLambdaCall));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    ScopedIdentifiers ourScope = scopedIds.extend(ScopeFilter.FUNCTION);

    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType, timeoutValue,
        (BlockStatement) body.insertDestructorCalls(ourScope, destructorRenamings).statements
            .get(0),
        (CatchExcSection) catchSection.insertDestructorCalls(ourScope, destructorRenamings));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType, timeoutValue.liftClassDeclarations(liftedClasses, replacements).get(),
        (BlockStatement) body.liftClassDeclarations(liftedClasses, replacements).get(),
        (CatchExcSection) catchSection.liftClassDeclarations(liftedClasses, replacements).get());
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        determinedType, timeoutValue.resolveUserTypes(msgState).get(),
        (BlockStatement) body.resolveUserTypes(msgState).get(),
        (CatchExcSection) catchSection.resolveUserTypes(msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    TimeoutExpression val = new TimeoutExpression(lineNum, columnNum, preComments, postComments,
        returnType, timeoutValue.addThisExpression(classHeader),
        (BlockStatement) body.addThisExpression(classHeader),
        (CatchExcSection) catchSection.addThisExpression(classHeader));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
