package type_env;

import ast.SerializationError;

import java.util.HashMap;
import java.util.Map;

public enum VariableScoping {
  NORMAL("normal"), //not global or in class instance
  GLOBAL("global"),
  CLASS_INSTANCE("class"), 
  GENERIC("generic"), //used for parameterized types
  UNKNOWN("unknown");
  
  private static final Map<String, VariableScoping> STR_TO_VAR_SCOPE = new HashMap<>();
  static {
    for (VariableScoping b : VariableScoping.values()) {
      STR_TO_VAR_SCOPE.put(b.vsName, b);
    }
  }
  
  public final String vsName;
  private VariableScoping(String name) {
    this.vsName = name;
  }
  
  public String serialize() {
    return vsName;
  }
  
  public static VariableScoping deserialize(String str) throws SerializationError {
    if (!STR_TO_VAR_SCOPE.containsKey(str)) {
      throw new SerializationError("unable to deserialize VariableScoping: [" + str + "]", false);
    }
    return STR_TO_VAR_SCOPE.get(str);
  }
}
