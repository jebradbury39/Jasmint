package test_types;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImport.EffectiveImportType;
import import_mgmt.EffectiveImports;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import typecheck.ArrayType;
import typecheck.BoolType;
import typecheck.CharType;
import typecheck.ClassType;
import typecheck.Float32Type;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.GenericType;
import typecheck.IntType;
import typecheck.MapType;
import typecheck.ModuleImportType;
import typecheck.ModuleType;
import typecheck.NullType;
import typecheck.ReferenceType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.VoidType;
import typegraph.CastFilter;
import typegraph.TypeGraph;
import typegraph.TypeGraph.CastDirection;

public class TestTypeGraph {

  private static final ModuleType modType = new ModuleType(Nullable.empty(), "a");

  private TypeGraph createTypeGraph() {
    EffectiveImports effImps = new EffectiveImports();
    effImps.addImport(new ModuleImportType(modType.outerType, modType.name),
        new EffectiveImport(EffectiveImportType.IMPLIED_IMPORT, false,
            new ModuleImportType(modType.outerType, modType.name), Nullable.empty()),
        true);

    TypeGraph graph = null;
    try {
      graph = new TypeGraph(modType, effImps, new MsgState(Nullable.empty()));
    } catch (FatalMessageException e) {
      fail(e.toString());
    }
    return graph;
  }

  @Test
  public void testDotFormat() {
    TypeGraph graph = createTypeGraph();
    /*
     * String expect = "digraph {\n" + "  \"[null]*\" -> \"null\" [color=blue]\n" +
     * "  \"{null, null}*\" -> \"null\" [color=blue]\n" + "  \"bool\" -> \"char\"\n"
     * + "  \"bool\" -> \"int8\"\n" + "  \"char\" -> \"bool\" [color=blue]\n" +
     * "  \"char\" -> \"int16\"\n" + "  \"char\" -> \"int8\"\n" +
     * "  \"int8\" -> \"bool\" [color=blue]\n" +
     * "  \"int8\" -> \"char\" [color=blue]\n" + "  \"int8\" -> \"int16\"\n" +
     * "  \"int16\" -> \"char\" [color=blue]\n" +
     * "  \"int16\" -> \"int8\" [color=blue]\n" + "  \"int16\" -> \"int32\"\n" +
     * "  \"int32\" -> \"int16\" [color=blue]\n" + "  \"int32\" -> \"int64\"\n" +
     * "  \"int64\" -> \"int32\" [color=blue]\n" + "  \"int64\" -> \"float32\"\n" +
     * "  \"float32\" -> \"int64\" [color=blue]\n" +
     * "  \"float32\" -> \"float64\"\n" +
     * "  \"float64\" -> \"float32\" [color=blue]\n" +
     * "  \"null\" -> \"{null, null}*\"\n" + "  \"null\" -> \"[null]*\"\n" + "}";
     */
    assertTrue(graph.toString().contains("\"bool\" -> \"char\""));
  }

  @Test
  public void testTypeGraphBasic() {
    TypeGraph graph = createTypeGraph();

    try {
      assertTrue(
          graph.canCastTo(IntType.Create(true, 32), CastDirection.UP, IntType.Create(true, 16)));
      assertFalse(
          graph.canCastTo(IntType.Create(true, 16), CastDirection.UP, IntType.Create(true, 32)));
      assertTrue(
          graph.canCastTo(IntType.Create(true, 16), CastDirection.DOWN, IntType.Create(true, 32)));
      assertFalse(
          graph.canCastTo(IntType.Create(true, 32), CastDirection.DOWN, IntType.Create(true, 16)));

      assertFalse(graph.canCastTo(CharType.Create(), CastDirection.UP, IntType.Create(true, 32)));
      assertTrue(graph.canCastTo(CharType.Create(), CastDirection.DOWN, IntType.Create(true, 32)));
      assertTrue(graph.canCastTo(CharType.Create(), CastDirection.DOWN, IntType.Create(true, 8)));
      // assertTrue(graph.canCastTo(CharType.Create(), CastDirection.UP,
      // IntType.Create(true, 8)));
      assertTrue(
          graph.canCastTo(CharType.Create(), CastFilter.UP_OR_DOWN, IntType.Create(true, 32)));

      assertFalse(graph.canCastTo(BoolType.Create(), CastDirection.UP, Float32Type.Create()));
      assertTrue(graph.canCastTo(BoolType.Create(), CastDirection.DOWN, Float32Type.Create()));
      assertTrue(
          graph.canCastTo(BoolType.Create(), CastFilter.UP_OR_DOWN, Float32Type.Create()));

      assertFalse(
          graph.canCastTo(Float32Type.Create(), CastDirection.DOWN, IntType.Create(true, 32)));
      assertTrue(graph.canCastTo(Float32Type.Create(), CastDirection.UP, IntType.Create(true, 32)));

      assertFalse(
          graph.canCastTo(Float32Type.Create(), CastFilter.UP_OR_DOWN, StringType.Create()));
      assertFalse(graph.canCastTo(VoidType.Create(), CastFilter.UP_OR_DOWN, BoolType.Create()));

      assertFalse(
          graph.canCastTo(Float32Type.Create(), CastFilter.UP_OR_DOWN, NullType.Create()));
      assertFalse(graph.canCastTo(new ArrayType(IntType.Create(true, 32)), CastFilter.UP_OR_DOWN,
          NullType.Create()));
    } catch (FatalMessageException e) {
      fail(e.toString());
    }
  }

  @Test
  public void testArrayCasting() {
    TypeGraph graph = createTypeGraph();

    try {
      assertTrue(graph.canCastTo(new ArrayType(IntType.Create(true, 32)), CastDirection.UP,
          new ArrayType(BoolType.Create())));
      assertFalse(graph.canCastTo(new ArrayType(IntType.Create(true, 32)), CastDirection.DOWN,
          new ArrayType(BoolType.Create())));
      assertTrue(graph.canCastTo(new ArrayType(IntType.Create(true, 32)), CastFilter.UP_OR_DOWN,
          new ArrayType(BoolType.Create())));

      assertFalse(graph.canCastTo(new ArrayType(new ArrayType(StringType.Create())),
          CastDirection.UP, new ArrayType(StringType.Create())));
      assertTrue(graph.canCastTo(new ArrayType(new ArrayType(StringType.Create())),
          CastDirection.UP, new ArrayType(new ArrayType(StringType.Create()))));
      assertTrue(graph.canCastTo(new ArrayType(new ArrayType(StringType.Create())),
          CastDirection.DOWN, new ArrayType(new ArrayType(StringType.Create()))));

      assertFalse(graph.canCastTo(new ArrayType(new ArrayType(StringType.Create())),
          CastDirection.UP, new MapType(StringType.Create(), IntType.Create(true, 32))));
    } catch (FatalMessageException e) {
      fail(e.toString());
    }
  }

  @Test
  public void testMapCasting() {
    TypeGraph graph = createTypeGraph();

    try {
      assertTrue(graph.canCastTo(new MapType(StringType.Create(), BoolType.Create()),
          CastDirection.DOWN, new MapType(StringType.Create(), IntType.Create(true, 32))));
      assertFalse(graph.canCastTo(new MapType(StringType.Create(), BoolType.Create()),
          CastDirection.DOWN, new MapType(StringType.Create(), VoidType.Create())));
      assertFalse(graph.canCastTo(new MapType(StringType.Create(), IntType.Create(true, 32)),
          CastDirection.UP, new MapType(NullType.Create(), BoolType.Create())));
    } catch (FatalMessageException e) {
      fail(e.toString());
    }
  }

  @Test
  public void testFunctionCasting() {
    TypeGraph graph = createTypeGraph();

    try {
      assertTrue(graph.canCastTo(
          new FunctionType(modType, IntType.Create(true, 32), Arrays.asList(),
              LambdaStatus.NOT_LAMBDA),
          CastDirection.UP,
          new FunctionType(modType, CharType.Create(), Arrays.asList(), LambdaStatus.NOT_LAMBDA)));
      assertFalse(graph.canCastTo(
          new FunctionType(modType, IntType.Create(true, 32),
              Arrays.asList(IntType.Create(true, 32)), LambdaStatus.NOT_LAMBDA),
          CastDirection.UP,
          new FunctionType(modType, CharType.Create(), Arrays.asList(), LambdaStatus.NOT_LAMBDA)));
      assertFalse(graph.canCastTo(
          new FunctionType(modType, IntType.Create(true, 32), Arrays.asList(BoolType.Create()),
              LambdaStatus.NOT_LAMBDA),
          CastDirection.UP, new FunctionType(modType, CharType.Create(),
              Arrays.asList(Float32Type.Create()), LambdaStatus.NOT_LAMBDA)));
      assertTrue(graph.canCastTo(
          new FunctionType(modType, IntType.Create(true, 32), Arrays.asList(Float32Type.Create()),
              LambdaStatus.NOT_LAMBDA),
          CastDirection.UP, new FunctionType(modType, CharType.Create(),
              Arrays.asList(BoolType.Create()), LambdaStatus.NOT_LAMBDA)));
    } catch (FatalMessageException e) {
      fail(e.toString());
    }
  }

  @Test
  public void testBasicRefCasting() {
    TypeGraph graph = createTypeGraph();

    try {
      assertTrue(graph.canCastTo(NullType.Create(), CastDirection.UP, NullType.Create()));
      assertTrue(graph.canCastTo(new ReferenceType(new ArrayType(IntType.Create(true, 32))),
          CastDirection.UP, NullType.Create()));
      assertFalse(graph.canCastTo(new ReferenceType(new ArrayType(IntType.Create(true, 32))),
          CastDirection.DOWN, NullType.Create()));
      assertFalse(graph.canCastTo(
          new ReferenceType(new MapType(StringType.Create(), IntType.Create(true, 32))),
          CastDirection.UP, new MapType(StringType.Create(), IntType.Create(true, 32))));
      assertTrue(
          graph.canCastTo(new ReferenceType(new MapType(StringType.Create(), CharType.Create())),
              CastDirection.DOWN,
              new ReferenceType(new MapType(StringType.Create(), IntType.Create(true, 32)))));
    } catch (FatalMessageException e) {
      fail(e.toString());
    }
  }

  private ClassType classInst(String name) {
    return new ClassType(Nullable.empty(), name, Arrays.asList());
  }

  private ReferenceType classInstRef(String name) {
    return new ReferenceType(classInst(name));
  }

  @Test
  public void testBasicClassCasting() {
    TypeGraph graph = createTypeGraph();

    try {

      // even without adding the class, we can still cast equal types
      assertTrue(graph.canCastTo(classInst("A"), CastDirection.DOWN, classInst("A")));
      assertFalse(graph.canCastTo(classInst("A"), CastDirection.UP, NullType.Create()));

      // add class A
      graph.addUserNode(classInst("A"), Nullable.empty());

      assertTrue(graph.canCastTo(classInst("A"), CastDirection.DOWN, classInst("A")));
      assertTrue(graph.canCastTo(classInstRef("A"), CastDirection.UP, classInstRef("A")));

      // add class B extends A
      graph.addUserNode(classInst("B"), Nullable.of(classInst("A")));

      assertFalse(graph.canCastTo(classInst("B"), CastDirection.UP, classInst("A")));
      assertTrue(graph.canCastTo(classInstRef("B"), CastDirection.UP, classInstRef("A")));
      assertTrue(graph.canCastTo(classInstRef("B"), CastDirection.UP, NullType.Create()));
      assertFalse(graph.canCastTo(classInstRef("A"), CastDirection.UP, classInstRef("B")));
      assertFalse(graph.canCastTo(classInstRef("B"), CastDirection.DOWN, classInstRef("A")));
      assertTrue(graph.canCastTo(classInstRef("A"), CastDirection.DOWN, classInstRef("B")));

      // class C extends B
      graph.addUserNode(classInst("C"), Nullable.of(classInst("B")));

      assertFalse(graph.canCastTo(classInst("C"), CastDirection.UP, classInst("A")));
      assertTrue(graph.canCastTo(classInstRef("C"), CastDirection.UP, classInstRef("A")));
      assertTrue(graph.canCastTo(classInstRef("C"), CastDirection.UP, NullType.Create()));
    } catch (FatalMessageException e) {
      fail(e.toString());
    }
  }

  private GenericType genType(String name) {
    return new GenericType(name);
  }

  private ClassType classGenInst(String name, List<Type> inner) {
    return new ClassType(Nullable.empty(), name, inner);
  }

  private ReferenceType classGenInstRef(String name, List<Type> inner) {
    return new ReferenceType(classGenInst(name, inner));
  }

  @Test
  public void testBasicGenericClassCasting() {
    TypeGraph graph = createTypeGraph();

    try {
      // add class A ?{A}
      graph.addUserNode(classGenInst("A", Arrays.asList(genType("A"))), Nullable.empty());

      assertTrue(graph.canCastTo(classGenInst("A", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.DOWN, classGenInst("A", Arrays.asList(Float32Type.Create()))));
      assertFalse(graph.canCastTo(classGenInst("A", Arrays.asList(Float32Type.Create())),
          CastDirection.DOWN, classGenInst("A", Arrays.asList(IntType.Create(true, 32)))));
      assertTrue(graph.canCastTo(classGenInstRef("A", Arrays.asList(CharType.Create())),
          CastDirection.UP, classGenInstRef("A", Arrays.asList(BoolType.Create()))));

      // add class B ?{T} extends A{T}
      graph.addUserNode(classGenInst("B", Arrays.asList(genType("T"))),
          Nullable.of(classGenInst("A", Arrays.asList(genType("T")))));

      assertTrue(graph.canCastTo(classGenInst("B", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.DOWN, classGenInst("B", Arrays.asList(Float32Type.Create()))));
      assertFalse(graph.canCastTo(classGenInst("B", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.UP, classGenInst("A", Arrays.asList(Float32Type.Create()))));
      assertTrue(graph.canCastTo(classGenInstRef("B", Arrays.asList(CharType.Create())),
          CastDirection.UP, classGenInstRef("A", Arrays.asList(BoolType.Create()))));
      assertTrue(graph.canCastTo(classGenInstRef("A", Arrays.asList(BoolType.Create())),
          CastDirection.DOWN, classGenInstRef("B", Arrays.asList(IntType.Create(true, 32)))));

      // class D ?{I} extends B{I}
      graph.addUserNode(classGenInst("D", Arrays.asList(genType("I"))),
          Nullable.of(classGenInst("B", Arrays.asList(genType("I")))));

      assertTrue(graph.canCastTo(classGenInst("D", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.DOWN, classGenInst("D", Arrays.asList(Float32Type.Create()))));
      assertFalse(graph.canCastTo(classGenInst("D", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.UP, classGenInst("A", Arrays.asList(Float32Type.Create()))));
      assertTrue(graph.canCastTo(classGenInstRef("D", Arrays.asList(CharType.Create())),
          CastDirection.UP, classGenInstRef("A", Arrays.asList(BoolType.Create()))));
      assertTrue(graph.canCastTo(classGenInstRef("A", Arrays.asList(BoolType.Create())),
          CastDirection.DOWN, classGenInstRef("D", Arrays.asList(IntType.Create(true, 32)))));
      assertFalse(graph.canCastTo(classGenInstRef("A", Arrays.asList(BoolType.Create())),
          CastDirection.UP, classGenInstRef("D", Arrays.asList(IntType.Create(true, 32)))));
      assertTrue(graph.canCastTo(classGenInstRef("B", Arrays.asList(BoolType.Create())),
          CastDirection.DOWN, classGenInstRef("D", Arrays.asList(IntType.Create(true, 32)))));

      // class C ?{U, V} extends A{int}
      graph.addUserNode(classGenInst("C", Arrays.asList(genType("U"), genType("V"))),
          Nullable.of(classGenInst("A", Arrays.asList(IntType.Create(true, 32)))));

      assertTrue(graph.canCastTo(
          classGenInst("C", Arrays.asList(IntType.Create(true, 32), StringType.Create())),
          CastDirection.DOWN,
          classGenInst("C", Arrays.asList(Float32Type.Create(), StringType.Create()))));
      assertFalse(graph.canCastTo(
          classGenInst("C", Arrays.asList(IntType.Create(true, 32), StringType.Create())),
          CastDirection.UP, classGenInst("A", Arrays.asList(IntType.Create(true, 32)))));
      assertTrue(graph.canCastTo(
          classGenInstRef("C", Arrays.asList(CharType.Create(), Float32Type.Create())),
          CastDirection.UP, classGenInstRef("A", Arrays.asList(IntType.Create(true, 32)))));
      assertFalse(graph.canCastTo(
          classGenInstRef("C", Arrays.asList(CharType.Create(), Float32Type.Create())),
          CastDirection.UP, classGenInstRef("A", Arrays.asList(StringType.Create()))));
      assertTrue(graph.canCastTo(classGenInstRef("A", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.DOWN,
          classGenInstRef("C", Arrays.asList(StringType.Create(), StringType.Create()))));

      // class T
      graph.addUserNode(classInst("T"), Nullable.empty());

      // class X extends A{T}
      graph.addUserNode(classInst("X"),
          Nullable.of(classGenInst("A", Arrays.asList(genType("T")))));

      assertTrue(graph.canCastTo(classInst("X"), CastDirection.DOWN, classInst("X")));
      assertFalse(graph.canCastTo(classInst("X"), CastDirection.UP,
          classGenInst("A", Arrays.asList(classInst("T")))));
      assertTrue(graph.canCastTo(classInstRef("X"), CastDirection.UP,
          classGenInstRef("A", Arrays.asList(classInst("T")))));
      assertTrue(graph.canCastTo(classGenInstRef("A", Arrays.asList(classInst("T"))),
          CastDirection.DOWN, classInstRef("X")));

      // class Y ?{T} extends X
      graph.addUserNode(classGenInst("Y", Arrays.asList(genType("T"))),
          Nullable.of(classInst("X")));

      assertTrue(graph.canCastTo(classGenInst("Y", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.DOWN, classGenInst("Y", Arrays.asList(Float32Type.Create()))));
      assertFalse(graph.canCastTo(classGenInst("Y", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.UP, classGenInst("A", Arrays.asList(classInst("T")))));
      assertTrue(graph.canCastTo(classGenInstRef("Y", Arrays.asList(IntType.Create(true, 32))),
          CastDirection.UP, classGenInstRef("A", Arrays.asList(classInst("T")))));
      assertTrue(graph.canCastTo(classGenInstRef("A", Arrays.asList(classInst("T"))),
          CastDirection.DOWN, classGenInstRef("Y", Arrays.asList(BoolType.Create()))));

    } catch (FatalMessageException e) {
      fail(e.toString());
    }
  }

}
