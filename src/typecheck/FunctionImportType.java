package typecheck;

import ast.SerializationError;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import errors.Nullable;
import java.util.List;
import java.util.Map;
import util.DotGen;

public class FunctionImportType extends ResolvedImportType {

  public FunctionImportType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.FUNCTION_IMPORT_TYPE, outerType, name);
  }
  
  @Override
  public Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.FunctionImportType.Builder subDoc = AstProto.FunctionImportType.newBuilder();
    if (outerType.isNotNull()) {
      subDoc.addOuterType(outerType.get().serialize());
    }
    subDoc.setName(name);
    
    document.setFunctionImportType(subDoc);
    return document;
  }
  
  public static FunctionImportType deserialize(AstProto.FunctionImportType document)
      throws SerializationError {
    Nullable<ModuleType> outerType = Nullable.empty();
    if (document.getOuterTypeCount() > 0) {
      outerType = Nullable.of((ModuleType) AbstractType.deserialize(document.getOuterType(0)));
    }
    return new FunctionImportType(outerType, document.getName());
  }

  @Override
  public UserDeclType asUserDeclType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public ModuleType asModuleType() {
    return outerType.get();
  }

  @Override
  public ImportType onlyName() {
    return new FunctionImportType(Nullable.empty(), name);
  }

  @Override
  public ResolvedImportType replaceModuleType(ModuleType newModType) {
    return new FunctionImportType(Nullable.empty(), name);
  }
  
  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type tmp = mapFromTo.get(this);
    if (tmp == null) {
      return this;
    }
    if (tmp instanceof FunctionImportType) {
      return tmp;
    }
    throw new IllegalArgumentException("expected FunctionImportType");
  }

  @Override
  public UserDeclType fromList(List<String> path) {
    if (path.isEmpty()) {
      throw new IllegalArgumentException();
    }
    final String name = path.get(path.size() - 1);
    List<String> subPath = path.subList(0, path.size() - 1);
    
    Nullable<ModuleType> moduleType = Nullable.empty();
    for (String item : subPath) {
      moduleType = Nullable.of(new ModuleType(moduleType, item));
    }
    return new FunctionImportType(moduleType, name);
  }
  
  @Override
  public DotGen.Color getGraphColor() {
    return DotGen.Color.GREEN;
  }
}
