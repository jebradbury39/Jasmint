package multifile;

import ast.SerializationError;
import astproto.AstProto;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import typecheck.AbstractType;
import typecheck.ModuleType;

/*
 * When the project is assembled there is either an existing build manifest or there is not.
 * If there is not, then create one and add all the files to it. If there is, then check checksums,
 * and update.
 * 
 * With a new manifest, there is obviously no generics file yet
 * 
 * Another problem, with plugins, you may want a common build dir, then plugin-specific build dirs
 * So you would have a read-manifest and a write-manifest (or each entry has a read location and 
 * write location).
 * 
 * A plugin will copy the src manifest
 */

public class BuildManifest {

  public static final String MANIFEST_FILENAME = "manifest.jsmnt-manifest";

  private Map<String, BuildEntry> entries = new HashMap<>(); // filename->BuildEntry
  private Map<String, BuildManifestProvider> providers = new HashMap<>();
  private Map<String, BuildEntry> fastLookup = new HashMap<>(); // both regular and providers
  public final String readBuildDir; // do not modify this manifest
  public final String writeBuildDir; // modify this manifest
  // TODO mapping for external filename to local generics file
  // e.g. third party libs have their own build dirs, as well as local build files

  public BuildManifest(String readBuildDir, String writeBuildDir) {
    this.readBuildDir = readBuildDir;
    this.writeBuildDir = writeBuildDir;
  }

  public static class BuildManifestProvider {
    public final String providerName;
    private Map<String, BuildEntry> entries = new HashMap<>();

    public BuildManifestProvider(String providerName) {
      this.providerName = providerName;
    }

    public void addEntry(BuildEntry entry) {
      entry.providerName = providerName;
      entries.put(entry.srcFilename, entry);
    }

    public Collection<BuildEntry> getEntries() {
      return entries.values();
    }

    public AstProto.BuildManifestProvider.Builder serialize() throws SerializationError {
      AstProto.BuildManifestProvider.Builder document = AstProto.BuildManifestProvider.newBuilder();

      document.setKey(providerName);
      for (BuildEntry entry : entries.values()) {
        document.addEntries(entry.serialize());
      }

      return document;
    }

    public static BuildManifestProvider deserialize(BuildManifest manifest,
        AstProto.BuildManifestProvider document) throws SerializationError {
      BuildManifestProvider provider = new BuildManifestProvider(document.getKey());

      for (AstProto.BuildEntry entry : document.getEntriesList()) {
        BuildEntry tmp = BuildEntry.deserialize(manifest, entry);
        provider.entries.put(tmp.srcFilename, tmp);
      }

      return provider;
    }
  }

  public static class BuildEntry {
    private final BuildManifest manifest;
    // when assembling, compare this with new src. If different, need rebuild
    public final byte[] checksum;
    public final ModuleType moduleType; // abs module path
    // list of children, abs module paths
    // if a child no longer lists us as a parent, then the link is broken and the
    // user must edit
    // our src file, which would trigger a rebuild of this module
    public final Set<ModuleType> submods;
    // for creating the initial class graph to find needRebuild. Can ignore
    // h_imports.
    public EffectiveImports effectiveImports;
    public final String srcFilename; // abs src file name
    private String moduleBuildFile; // abs file name (from readable)
    // if not root manifest, first check if the root manifest has a better entry for
    // this
    private String moduleGenericsFile; // abs file name (from readable)
    private String providerName = "";

    public BuildEntry(BuildManifest manifest, byte[] checksum, ModuleType moduleType,
        Set<ModuleType> submods, EffectiveImports effectiveImports, String srcFilename,
        String moduleBuildFile, String moduleGenericsFile) {
      this.manifest = manifest;
      this.checksum = checksum;
      this.moduleType = moduleType;
      this.submods = submods;
      this.effectiveImports = effectiveImports;
      this.srcFilename = srcFilename;
      this.moduleBuildFile = moduleBuildFile;
      this.moduleGenericsFile = moduleGenericsFile;

      if (moduleBuildFile.equals(moduleGenericsFile)) {
        throw new IllegalArgumentException();
      }
    }

    public AstProto.BuildEntry.Builder serialize() throws SerializationError {
      AstProto.BuildEntry.Builder document = AstProto.BuildEntry.newBuilder();

      String checksumStr = "";
      for (int i = 0; i < checksum.length; ++i) {
        checksumStr += checksum[i];
      }
      document.setChecksum(checksumStr);
      document.setModuleType(moduleType.serialize());

      for (ModuleType submod : submods) {
        document.addSubmods(submod.serialize());
      }

      document.setEffectiveImports(effectiveImports.serialize());

      document.setSrcFilename(srcFilename);
      document.setModuleBuildFile(moduleBuildFile);
      document.setModuleGenericsFile(moduleGenericsFile);

      return document;
    }

    public static BuildEntry deserialize(BuildManifest manifest, AstProto.BuildEntry document)
        throws SerializationError {
      String checksumStr = document.getChecksum();
      byte[] checksum = new byte[checksumStr.length()];
      for (int i = 0; i < checksumStr.length(); ++i) {
        checksum[i] = (byte) checksumStr.charAt(1);
      }

      Set<ModuleType> submods = new HashSet<>();
      for (AstProto.Type bvSubmod : document.getSubmodsList()) {
        submods.add((ModuleType) AbstractType.deserialize(bvSubmod));
      }
      
      ModuleType moduleType = (ModuleType) AbstractType.deserialize(document.getModuleType());
      EffectiveImports effectiveImports = EffectiveImports.deserialize(document.getEffectiveImports());

      return new BuildEntry(manifest, checksum,
          moduleType, submods,
          effectiveImports, document.getSrcFilename(),
          document.getModuleBuildFile(), document.getModuleGenericsFile());
    }

    private String adjustedRelativeName(String absPath) {
      String relativeName = new File(absPath).toPath().getFileName().toString();
      String safeProviderName = providerName;
      if (!providerName.isEmpty()) {
        safeProviderName = safeProviderName.replace('+', 'X');
        relativeName = safeProviderName + "_" + relativeName;
      }
      return relativeName;
    }

    public synchronized String getBuildFile(boolean writable) {
      String relativeName = adjustedRelativeName(moduleBuildFile);
      if (writable) {
        // as soon as we write to this entry, we read and write our local version
        String tmp = manifest.writeBuildDir + File.separator + relativeName;
        moduleBuildFile = tmp;
        return tmp;
      }
      return manifest.readBuildDir + File.separator + relativeName;
    }

    public synchronized String getGenericsFile(boolean writable) {
      String relativeName = adjustedRelativeName(moduleGenericsFile);
      if (writable) {
        // as soon as we write to this entry, we read and write our local version
        String tmp = manifest.writeBuildDir + File.separator + relativeName;
        moduleGenericsFile = tmp;
        return tmp;
      }
      return manifest.readBuildDir + File.separator + relativeName;
    }
  }

  public AstProto.BuildManifest.Builder serialize() throws SerializationError {
    AstProto.BuildManifest.Builder document = AstProto.BuildManifest.newBuilder();

    for (BuildEntry entry : entries.values()) {
      document.addEntries(entry.serialize());
    }

    for (BuildManifestProvider provider : providers.values()) {
      document.addProviders(provider.serialize());
    }

    return document;
  }

  public static BuildManifest deserializeFromFile(File manifestPath, String writeBuildDir)
      throws IOException, SerializationError {
    String readBuildDir = manifestPath.toPath().getParent().toString();

    FileInputStream fis = new FileInputStream(manifestPath);
    AstProto.BuildManifest document = AstProto.BuildManifest.parseFrom(fis);
    fis.close();

    BuildManifest manifest = new BuildManifest(readBuildDir, writeBuildDir);
    for (AstProto.BuildEntry bvEntry : document.getEntriesList()) {
      manifest.addEntry(BuildEntry.deserialize(manifest, bvEntry));
    }

    for (AstProto.BuildManifestProvider bvProvider : document.getProvidersList()) {
      BuildManifestProvider provider = BuildManifestProvider.deserialize(manifest, bvProvider);
      manifest.providers.put(provider.providerName, provider);
    }

    return manifest;
  }

  public void addEntry(BuildEntry entry) {
    entries.put(entry.srcFilename, entry);
    fastLookup.put(entry.srcFilename, entry);
  }

  public void addProviderEntry(String providerName, BuildEntry entry) {
    BuildManifestProvider provider = providers.get(providerName);
    if (provider == null) {
      provider = new BuildManifestProvider(providerName);
      providers.put(providerName, provider);
    }
    provider.addEntry(entry);
    fastLookup.put(entry.srcFilename, entry);
  }

  public Nullable<BuildEntry> getEntry(String srcFilename) {
    BuildEntry tmp = fastLookup.get(srcFilename);
    if (tmp == null) {
      return Nullable.empty();
    }
    return Nullable.of(tmp);
  }

  public static byte[] computeChecksum(InputStream in)
      throws NoSuchAlgorithmException, IOException {
    MessageDigest digest = MessageDigest.getInstance("MD5");
    byte[] block = new byte[4096];
    int length;
    while ((length = in.read(block)) > 0) {
      digest.update(block, 0, length);
    }
    in.close();
    return digest.digest();
  }

  public boolean hasEntry(String srcFilename) {
    return fastLookup.containsKey(srcFilename);
  }

  public Collection<BuildEntry> getEntries() {
    return entries.values();
  }

  public Collection<BuildManifestProvider> getProviders() {
    return providers.values();
  }

}
