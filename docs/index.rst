.. Jasmint documentation master file, created by
   sphinx-quickstart on Fri Oct 11 21:01:26 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Jasmint's documentation!
===================================

Jasmint is a library which handles all the heavy lifting of a programming language:
   * parsing
   * typechecking
   * interpreting
   * compiler-level code transformations

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Getting Started

   install


.. toctree::
   :maxdepth: 2 
   :hidden:  
   :caption: Creating a Custom Plugin

   custom_plugins/intro.rst
