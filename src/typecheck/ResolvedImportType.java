package typecheck;

import errors.Nullable;

public abstract class ResolvedImportType extends ImportType {
  public ResolvedImportType(TypeType typeType, Nullable<ModuleType> outerType, String name) {
    super(typeType, outerType, name);
  }

  public abstract ResolvedImportType replaceModuleType(ModuleType newModType);
}
