package audit;

import astproto.AstProto.AuditEntry.Builder;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import multifile.JsmntGlobal;
import typecheck.FunctionType;

public class AuditResolveExceptions implements AuditEntry {
  
  public static class RenamingEntry {
    public final String name;
    public final FunctionType functionType;
    
    public RenamingEntry(String name, FunctionType functionType) {
      this.name = name;
      this.functionType = functionType;
    }
    
    @Override
    public String toString() {
      return "<RenamingEntry (" + name + "): " + functionType + ">";
    }
    
    @Override
    public int hashCode() {
      return name.hashCode() + functionType.hashCode();
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof RenamingEntry)) {
        return false;
      }
      RenamingEntry ourOther = (RenamingEntry) other;
      
      if (!name.equals(ourOther.name)) {
        return false;
      }
      
      return functionType.equals(ourOther.functionType);
    }
  }
  
  // actually just generates modified copies of these functions, since non-sandbox code may also
  // call these
  private Map<RenamingEntry, RenamingEntry> sandboxFnCallRenamings = new HashMap<>();

  public void addFunction(String name, FunctionType fullType, MsgState msgState)
    throws FatalMessageException {

    RenamingEntry tmp = new RenamingEntry(name, fullType);
    if (sandboxFnCallRenamings.containsKey(tmp)) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "found duplicate fn call: " + tmp);
      return;
    }
    /*
     * Only function calls will be renamed to use fn_ctx. We will never pass a lambda into a
     * function where the lambda has a fn_ctx (in fact, no lambda will ever have a fn_ctx).
     * This is because sandbox mode is not allowed to execute lambda functions, nor is it allowed
     * to define them. It is allowed to define non_lambda functions, but even these may not be
     * passed as callbacks to any function.
     * 
     * Therefore, we do not apply this transform recursively.
     */
    sandboxFnCallRenamings.put(tmp, new RenamingEntry(name,
        (FunctionType) fullType.prependFnArgType(JsmntGlobal.fnCtxRefType)));

  }
  
  public Nullable<RenamingEntry> findFunction(String name, FunctionType fullType) {
    RenamingEntry tmp = sandboxFnCallRenamings.get(new RenamingEntry(name, fullType));
    if (tmp == null) {
      return Nullable.empty();
    }
    
    return Nullable.of(tmp);
  }
  
  @Override
  public AuditEntryType getType() {
    return AuditEntryType.RESOLVE_EXCEPTIONS;
  }

  @Override
  public Builder serialize() {
    throw new UnsupportedOperationException();
  }

  @Override
  public AuditEntry combineWith(AuditEntry other) {
    if (other.getType() != getType()) {
      throw new IllegalArgumentException();
    }
    throw new UnsupportedOperationException();
  }

  @Override
  public AuditEntry withRelativeEntries() {
    throw new UnsupportedOperationException();
  }

  public void linkWith(AuditResolveExceptions parentEntry) {
    for (Entry<RenamingEntry, RenamingEntry> fnCall : parentEntry.sandboxFnCallRenamings.entrySet()) {
      sandboxFnCallRenamings.put(fnCall.getKey(), fnCall.getValue());
    }
  }

}
