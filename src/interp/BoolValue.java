package interp;

import typecheck.BoolType;
import typecheck.IntType;

public class BoolValue extends AbstractValue {

  public final boolean value;
  
  public BoolValue(boolean value) {
    super(MetaValue.NORMAL, BoolType.Create());
    this.value = value;
  }
  
  public BoolValue(MetaValue metaValue, boolean value) {
    super(metaValue, BoolType.Create());
    this.value = value;
  }

  @Override
  public Value add(Value rightValue) throws InterpError {
    if (rightValue instanceof StringValue) {
      return rightValue.add(value ? "true" : "false");
    }
    return rightValue.add(value ? 1 : 0);
  }
  
  @Override
  public Value add(long leftValue, IntType leftType) throws InterpError {
    return new IntValue(leftValue + (value ? 1 : 0), leftType);
  }
  
  @Override
  public Value add(double leftValue) throws InterpError {
    return new Float32Value(leftValue + (value ? 1.0 : 0.0));
  }
  
  @Override
  public Value add(String leftValue) throws InterpError {
    return new StringValue(leftValue + (value ? "true" : "false"));
  }

  @Override
  public Value sub(Value rightValue) throws InterpError {
    return rightValue.sub(value ? 1 : 0);
  }
  
  @Override
  public Value sub(long leftValue, IntType leftType) {
    return new IntValue(leftValue - (value ? 1 : 0), leftType);
  }
  
  @Override
  public Value sub(double leftValue) {
    return new Float32Value(leftValue - (value ? 1.0 : 0.0));
  }

  @Override
  public long toInt() throws InterpError {
    return value ? 1 : 0;
  }

  @Override
  public boolean toBool() throws InterpError {
    return value;
  }

  @Override
  public Value mul(Value rightValue) throws InterpError {
    return rightValue.mul(value ? 1 : 0);
  }
  
  @Override
  public Value mul(long leftValue, IntType leftType) throws InterpError {
    return new IntValue(leftValue * (value ? 1 : 0), leftType);
  }
  
  @Override
  public Value mul(double leftValue) throws InterpError {
    return new Float32Value(leftValue * (value ? 1.0 : 0.0));
  }

  @Override
  public Value div(Value rightValue) throws InterpError {
    return rightValue.div(value ? 1 : 0);
  }
  
  @Override
  public Value div(long leftValue, IntType leftType) throws InterpError {
    if (!value) {
      throw new InterpError("division by zero", -1, -1);
    }
    return new IntValue(leftValue / 1, IntType.Create(false, 8));
  }
  
  @Override
  public Value div(double leftValue) throws InterpError {
    if (!value) {
      throw new InterpError("division by zero", -1, -1);
    }
    return new Float32Value(leftValue / 1.0);
  }

  @Override
  public Value copy(Environment copyEnv) {
    //primitives are immutable, unlike arrays, maps, and class instances.
    //However, the box ref must be reset each instance
    return new BoolValue(value); 
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return ((BoolValue) other).value == value;
  }

}
