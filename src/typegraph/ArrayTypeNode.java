package typegraph;

import errors.FatalMessageException;
import java.util.Map;
import java.util.Set;
import typecheck.AbstractType.TypeType;
import typecheck.AnyType;
import typecheck.ArrayType;
import typecheck.Type;
import typegraph.TypeGraph.CastDirection;

public class ArrayTypeNode extends AbstractTypeNode {

  public ArrayTypeNode() {
    super();
  }
  
  @Override
  public int hashCode() {
    return TypeType.ARRAY_TYPE.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    return other instanceof ArrayTypeNode;
  }

  @Override
  public boolean typeEquals(Type type, boolean looseMatch) {
    return type instanceof ArrayType || type instanceof AnyType;
  }

  @Override
  public boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph,
      Set<TypeNode> visited) throws FatalMessageException {
    if (checkVisited(visited)) {
      return false;
    }
    if (fromTy instanceof AnyType || toTy instanceof AnyType) {
      return true;
    }
    if (!typeEquals(fromTy, true)) {
      return false; //should be a redundant check
    }
    if (!typeEquals(toTy, true)) {
      return false; //can only cast if both are arrays, ofc
    }
    
    //inner type can be anything. Does not need to be walkable
    return graph.canCastTo(((ArrayType) fromTy).elementType,
        direction,
        ((ArrayType) toTy).elementType);
  }

  @Override
  public Type getType() {
    return new ArrayType(null);
  }

}
