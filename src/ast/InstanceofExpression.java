package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.BoolType;
import typecheck.ClassType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typegraph.CastFilter;
import util.Pair;
import util.ScopedIdentifiers;

public class InstanceofExpression extends AbstractExpression {

  public static enum OpType {
    INSTANCEOF("instanceof"),
    NOT_INSTANCEOF("!instanceof");
    
    public final String name;
    
    OpType(String name) {
      this.name = name;
    }
  }
  
  public final OpType operator;
  public final Expression left;
  public final ClassType right;
  
  public ClassType determinedRightType;
  
  public InstanceofExpression(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      OpType operator, Expression left, ClassType right) {
    super(lineNum, columnNum, AstType.INSTANCEOF_EXPRESSION, preComments, postComments);
    this.operator = operator;
    this.left = left;
    this.right = right;
    
    determinedRightType = right;
  }
  
  public InstanceofExpression(OpType operator, Expression left, ClassType right) {
    super(-1, -1, AstType.INSTANCEOF_EXPRESSION, new LinkedList<>(), new LinkedList<>());
    this.operator = operator;
    this.left = left;
    this.right = right;
    
    determinedRightType = right;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, left.renameIds(renamings, msgState),
        (ClassType) right.renameIds(renamings, msgState));
    val.determinedRightType = (ClassType) determinedRightType.renameIds(renamings, msgState);
    val.determinedType = determinedType.renameIds(renamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, left.replaceType(mapFromTo), (ClassType) right.replaceType(mapFromTo));
    val.determinedRightType = (ClassType) determinedRightType.replaceType(mapFromTo);
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator,
        left.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get(),
        (ClassType) right.basicNormalize(keepCurrentModRelative, currentModule, effectiveImports,
            msgState).get().get());
    val.determinedRightType = (ClassType) determinedRightType.basicNormalize(keepCurrentModRelative,
        currentModule, effectiveImports, msgState).get().get();
    val.determinedType = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState).get().get();
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(val));
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, left.insertDestructorCalls(scopedIds, destructorRenamings), right);
    val.determinedRightType = determinedRightType;
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, left.liftClassDeclarations(liftedClasses, replacements).get(),
        right);
    val.determinedRightType = determinedRightType;
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, left, determinedRightType);
    val.determinedRightType = determinedRightType;
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public String toString(String indent) {
    return left.toString(indent) + " " + operator.name + " " + right;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<Type> normRight = right.normalize(tenv, lineNum, columnNum);
    if (!normRight.isPresent()) {
      return Optional.empty();
    }
    determinedRightType = (ClassType) normRight.get();
    
    Optional<TypecheckRet> leftRet = left.typecheck(tenv, expectedReturnType);
    if (!leftRet.isPresent()) {
      return Optional.empty();
    }
    Type leftType = leftRet.get().type;
    
    // the left type must be of ClassType* and must be castable
    // (either up or down) to the right type
    if (!(leftType instanceof ReferenceType)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "left side of instanceof must be a class reference, not: " + leftType,
          lineNum, columnNum);
      return Optional.empty();
    }
    ReferenceType leftRefTy = (ReferenceType) leftType;
    
    if (!(leftRefTy.innerType instanceof ClassType)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "left side of instanceof must be a class reference, not: " + leftType,
          lineNum, columnNum);
      return Optional.empty();
    }
    
    if (!tenv.typeGraph.canCastTo(leftRefTy, CastFilter.UP_OR_DOWN,
        new ReferenceType(determinedRightType))) {
      
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "instanceof unable to cast from " + leftRefTy.innerType + " to " + determinedRightType
          + " (no relationship)",
          lineNum, columnNum);
      return Optional.empty();
    }
    
    determinedType = BoolType.Create();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.InstanceofExpression.Builder subDoc = AstProto.InstanceofExpression.newBuilder();
    subDoc.setOperator(operator.ordinal());
    subDoc.setLeft(left.serialize().build());
    subDoc.setRight(right.serialize().build());
    subDoc.setDeterminedRight(determinedRightType.serialize());

    AstProto.Ast.Builder document = preSerialize();
    document.setInstanceofExpression(subDoc);
    return document;
  }
  
  public static InstanceofExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.InstanceofExpression document) throws SerializationError {
    
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum, preComments, postComments,
        OpType.values()[document.getOperator()],
        AbstractExpression.deserialize(document.getLeft()),
        (ClassType) AbstractType.deserialize(document.getRight()));
    val.determinedRightType = (ClassType) AbstractType.deserialize(document.getDeterminedRight());
    return val;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return left.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof InstanceofExpression)) {
      return false;
    }
    InstanceofExpression ourOther = (InstanceofExpression) other;
    return ourOther.right.equals(right) && ourOther.left.compare(left);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, (Expression) left.replace(target, with),
        right);
    val.determinedRightType = determinedRightType;
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return left.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    Pair<List<DeclarationStatement>, Ast> leftPair = left.toAnf(tuning);
    insert.addAll(leftPair.a);
    
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, (Expression) leftPair.b,
        right);
    val.determinedRightType = determinedRightType;
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    
    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, (Expression) left.replaceVtableAccess(project, msgState).get(),
        right);
    val.determinedRightType = determinedRightType;
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    left.addDependency(modDepGraph, ourModNode, tenv, lineNum, columnNum);
    right.addDependency(modDepGraph, ourModNode, tenv, lineNum, columnNum);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return left.containsType(types) || right.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return left.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    
    Optional<ResolveExcRet> tmp = left.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn,
        withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator,
        (Expression) tmp.get().ast.get(),
        right);
    val.determinedRightType = determinedRightType;
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(tmp.get().preStatements, val));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    InstanceofExpression val = new InstanceofExpression(lineNum, columnNum,
        preComments, postComments,
        operator, left.addThisExpression(classHeader),
        right);
    val.determinedRightType = determinedRightType;
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
