package interp;

import typecheck.IntType;
import typecheck.StringType;

public class StringValue extends AbstractValue {

  public final String value;
  
  public StringValue(String value) {
    super(MetaValue.NORMAL, StringType.Create());
    this.value = value;
  }
  
  public StringValue(MetaValue metaValue, String value) {
    super(metaValue, StringType.Create());
    this.value = value;
  }

  @Override
  public Value add(Value rightValue) throws InterpError {
    return rightValue.add(value);
  }
  
  @Override
  public Value add(long leftValue, IntType leftType) {
    return new StringValue(leftValue + value);
  }
  
  @Override
  public Value add(double leftValue) {
    return new StringValue(leftValue + value);
  }
  
  @Override
  public Value add(String leftValue) {
    return new StringValue(leftValue + value);
  }

  @Override
  public boolean toBool() throws InterpError {
    return value.length() != 0;
  }

  @Override
  public Value copy(Environment copyEnv) {
    return new StringValue(value);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return ((StringValue) other).value.equals(value);
  }
  
  @Override
  public boolean lessThan(String leftValue) throws InterpError {
    return leftValue.compareTo(value) < 0;
  }

}
