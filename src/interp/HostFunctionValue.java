package interp;

import java.util.List;

import ast.BlockStatement;
import ast.DeclarationStatement;
import typecheck.FunctionType;
import typecheck.Type;

public class HostFunctionValue extends ClosureValue {

  private HostFunctionLambda func;
  
  private HostFunctionValue(Type type, List<DeclarationStatement> noParams, BlockStatement noBody,
      Environment noEnv, HostFunctionLambda func) throws InterpError {
    super(type, noParams, noBody, noEnv, null, null);
    this.func = func;
  }
  
  public static HostFunctionValue createHostFunctionValue(FunctionType type,
      HostFunctionLambda func) throws InterpError {
    return new HostFunctionValue(type, null, null, null, func);
  }
  
  @Override
  public Value interp(List<Value> paramValues) throws InterpError {
    return func.interp(paramValues).setMetaValue(MetaValue.NORMAL);
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    return new StringValue("<built-in closure>");
  }

  @Override
  public boolean equalsValue(Value other) {
    return this == other;
  }

}
