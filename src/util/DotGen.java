package util;

import java.util.HashSet;
import java.util.Set;

/*
 * Create a simple dot file
 */
public class DotGen {
  public static enum Color {
    BLACK("black"),
    BLUE("blue"),
    RED("red"),
    YELLOW("goldenrod"),
    GREEN("green"),
    PURPLE("purple");
    
    public final String name;
    Color(String name) {
      this.name = name;
    }
  }
  
  private Set<Node> nodes = new HashSet<>();
  private Set<Edge> edges = new HashSet<>();
  
  public void addNode(Node node) {
    nodes.add(node);
  }
  
  public void addEdge(Edge edge) {
    edges.add(edge);
  }
  
  @Override
  public String toString() {
    String res = "digraph {";
    for (Node node : nodes) {
      res += "\n  " + node;
    }
    for (Edge edge : edges) {
      res += "\n  " + edge;
    }
    res += "\n}";
    return res;
  }
  
  public static class Node {
    public final String name;
    public Color color = Color.BLACK;
    
    public Node(String name) {
      this.name = name;
    }
    
    @Override
    public String toString() {
      String res = "\"" + name + "\"";
      res += " [color=" + color.name + "]";
      return res;
    }
    
    @Override
    public int hashCode() {
      return name.hashCode() + color.hashCode();
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof Node)) {
        return false;
      }
      Node otherNode = (Node) other;
      return name.equals(otherNode.name) && color == otherNode.color;
    }
  }
  
  public static class Edge {
    
    public final String from;
    public final String to;
    public Color color = Color.BLACK;
    
    public Edge(String from, String to) {
      this.from = from;
      this.to = to;
    }
    
    @Override
    public String toString() {
      String res = "\"" + from + "\" -> \"" + to + "\"";
      res += " [color=" + color.name + "]";
      return res;
    }
    
    @Override
    public int hashCode() {
      return from.hashCode() + to.hashCode() + color.hashCode();
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof Edge)) {
        return false;
      }
      Edge otherEdge = (Edge) other;
      return from.equals(otherEdge.from) && to.equals(otherEdge.to) && color == otherEdge.color;
    }
  }
}
