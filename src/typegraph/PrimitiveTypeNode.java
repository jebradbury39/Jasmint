package typegraph;

import errors.FatalMessageException;
import errors.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import typecheck.AnyType;
import typecheck.Float32Type;
import typecheck.Float64Type;
import typecheck.IntType;
import typecheck.Type;
import typegraph.TypeGraph.CastDirection;

/* But not function, array, or map */
public class PrimitiveTypeNode extends AbstractTypeNode {

  public final Type primType;
  
  public PrimitiveTypeNode(Type type) {
    super();
    this.primType = type;
  }
  
  @Override
  public int hashCode() {
    return primType.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof PrimitiveTypeNode)) {
      return false;
    }
    PrimitiveTypeNode node = (PrimitiveTypeNode) other;
    return primType.equals(node.primType);
  }
  
  public String toString() {
    return "PrimTyNode<" + primType + ">";
  }
  
  @Override
  public boolean typeEquals(Type type, boolean looseMatch) {
    if (type instanceof AnyType) {
      return true;
    }
    if (type.getTypeType() != primType.getTypeType()) {
      return false;
    }
    if (primType instanceof IntType
        || primType instanceof Float32Type
        || primType instanceof Float64Type) {
      return primType.equals(type);
    }
    return true;
  }

  @Override
  public boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph, Set<TypeNode> visited)
          throws FatalMessageException {
    if (checkVisited(visited)) {
      return false;
    }
    if (fromTy instanceof AnyType || toTy instanceof AnyType) {
      return true;
    }
    if (!typeEquals(fromTy, false)) {
      return false; //should be a redundant check
    }
    if (typeEquals(toTy, false)) {
      return true;
    }
    return stepPathTo(Nullable.empty(), direction, toTy, new HashMap<>(), graph, visited);
  }

  @Override
  public Type getType() {
    return primType;
  }

}
