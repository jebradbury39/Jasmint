package typecheck;

import ast.Expression;
import ast.Program.DestructorRenamings;
import ast.SerializationError;
import ast.Statement;
import ast.StaticClassIdentifierExpression;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.EnumTypeBox;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;

public class EnumDeclType extends UserDeclType {

  public EnumDeclType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.ENUM_DECL_TYPE, outerType, name);
  }
  
  @Override
  public boolean equals(Object other) {

    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof EnumDeclType)) {
      return false;
    }

    EnumDeclType otherUT = (EnumDeclType) other;
    if (!name.equals(otherUT.name)) {
      return false;
    }

    return outerType.equals(otherUT.outerType);
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<Type> getInnerTypes() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type tmp = mapFromTo.get(this);
    if (tmp == null) {
      return this;
    }
    return tmp;
  }

  @Override
  public Builder serialize() {
    AstProto.EnumDeclType.Builder subDoc = AstProto.EnumDeclType.newBuilder();
    if (outerType.isNotNull()) {
      subDoc.addOuterType(outerType.get().serialize());
    }
    subDoc.setName(name);

    AstProto.Type.Builder document = preSerialize();
    document.setEnumDeclType(subDoc);
    return document;
  }
  
  public static EnumDeclType deserialize(AstProto.EnumDeclType document) throws SerializationError {
    Nullable<ModuleType> outerType = Nullable.empty();
    if (document.getOuterTypeCount() != 0) {
      outerType = Nullable.of((ModuleType) AbstractType.deserialize(document.getOuterType(0)));
    }

    return new EnumDeclType(outerType, document.getName());
  }

  @Override
  public Type setConst(boolean isConst) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(this);
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    stackTypes.add(this);
    return stackTypes;
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(this, lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(tyBox.get() instanceof EnumTypeBox)) {
      return Optional.empty();
    }
    EnumTypeBox enumTyBox = (EnumTypeBox) tyBox.get();
    return Optional.of(enumTyBox.enumHeader.absName);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return this;
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    // first try to find our class name in the effectiveImports
    Nullable<EffectiveImport> tmp = effectiveImports.getImport(asImportType(),
        EffectiveImport.ImportUsage.INVALID);
    
    Nullable<ModuleType> normOuter = Nullable.empty();
    if (tmp.isNull()) {
      if (outerType.isNull()) {
        //must be in our module
        if (keepCurrentModRelative) {
          return Optional.of(Nullable.of(new EnumDeclType(Nullable.empty(), name)));
        }
        return Optional.of(Nullable.of(new EnumDeclType(Nullable.of(currentModule), name)));
      }
      
      // not found, so now just look for module type (not the class name)
      tmp = effectiveImports.getImport(outerType.get().asImportType(),
          EffectiveImport.ImportUsage.INVALID);
      if (tmp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to find module in effective imports: " + outerType);
        return Optional.empty();
      }
      normOuter = Nullable.of(tmp.get().absPath.asModuleType());
    } else {
      // matched the class (must have outerType/ModuleType)
      normOuter = Nullable.of(tmp.get().absPath.outerType.get());
    }
    
    if (keepCurrentModRelative) {
      //remove any part of the outer type that starts with current module
      Nullable<ModuleType> uniqueRemainder = normOuter.get().removeCommonStart(currentModule);
      //unique/relative part
      normOuter = uniqueRemainder;
    }
    return Optional.of(Nullable.of(new EnumDeclType(normOuter, name)));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState)
      throws FatalMessageException {
    return new StaticClassIdentifierExpression(this);
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return tenv.copyFromTenvIntoTenv(this, newTenv);
  }

  @Override
  public Type asClassDeclType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(this, true, lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return -1;
    }
    if (!(tyBox.get() instanceof EnumTypeBox)) {
      return -1;
    }
    EnumTypeBox enumTyBox = (EnumTypeBox) tyBox.get();

    return enumTyBox.enumHeader.calculateSize(tenv, false, lineNum, columnNum);
  }

  @Override
  public List<String> toList() {
    List<String> path = new LinkedList<>();
    if (outerType.isNotNull()) {
      path = outerType.get().toList();
    }
    path.add(name);
    return path;
  }
  
  public static EnumDeclType staticFromList(List<String> path) {
    Nullable<ModuleType> moduleType = Nullable.empty();
    int numLeft = path.size();
    for (String item : path) {
      numLeft -= 1;
      if (numLeft == 0) {
        return new EnumDeclType(moduleType, item);
      }
      moduleType = Nullable.of(new ModuleType(moduleType, item));
    }
    throw new IllegalArgumentException("empty path");
  }
  
  public UserDeclType fromList(List<String> list) {
    return staticFromList(list);
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(this, lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(tyBox.get() instanceof EnumTypeBox)) {
      return Optional.empty();
    }
    EnumTypeBox enumTyBox = (EnumTypeBox) tyBox.get();

    return enumTyBox.enumHeader.getTenv(tenv, true, asInstanceType());
  }

  @Override
  public UserInstanceType asInstanceType() {
    return new EnumType(outerType, name);
  }

  @Override
  public UserDeclType asUserDeclType() {
    return this;
  }

  @Override
  public ImportType asImportType() {
    return new EnumImportType(outerType, name);
  }

}
