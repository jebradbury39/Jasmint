package typecheck;

import ast.Ast;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import type_env.TypeEnvironment;

/*
 * Can be a type or expression
 */
public interface SizeofTarget {
  /*
   * Used for sizeof. Return -1 on error
   */
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException;

  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException;

  public Map<String, Type> findFreeVariables();

  public SizeofTarget renameIdsSizeof(Renamings renamings, MsgState msgState)
      throws FatalMessageException;

  public SizeofTarget replaceTypeSizeof(Map<Type, Type> mapFromTo);

  public Optional<SizeofTarget> normalizeTypeSizeof(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException;

  public boolean compareSizeof(SizeofTarget target);

  public Nullable<Ast> findByOriginalLabel(int findLabel);

  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      int lineNum, int columnNum)
      throws FatalMessageException;

  public boolean containsType(Set<Type> types);

  public boolean containsAst(Set<Ast> asts);

}
