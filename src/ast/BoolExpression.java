package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.BoolValue;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.BoolType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class BoolExpression extends AbstractExpression {

  public final boolean value;

  public BoolExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, boolean value) {
    super(lineNum, columnNum, AstType.BOOL_EXPRESSION, preComments, postComments);
    this.value = value;

    determinedType = BoolType.Create();
  }

  public BoolExpression(int lineNum, int columnNum, boolean value) {
    super(lineNum, columnNum, AstType.BOOL_EXPRESSION);
    this.value = value;

    determinedType = BoolType.Create();
  }

  public BoolExpression(boolean value) {
    super(AstType.BOOL_EXPRESSION);
    this.value = value;

    determinedType = BoolType.Create();
  }

  @Override
  public String toString(String indent) {
    return preToString() + (value ? "true" : "false") + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment env, Nullable<Type> expectedReturnType) {
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    return new BoolValue(value);
  }

  @Override
  public AstProto.Ast.Builder serialize() {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.BoolExpression.Builder subDoc = AstProto.BoolExpression.newBuilder();

    subDoc.setValue(value);

    document.setBoolExpression(subDoc);
    return document;
  }

  public static BoolExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.BoolExpression document) {
    return new BoolExpression(lineNum, columnNum, preComments, postComments, document.getValue());
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof BoolExpression)) {
      return false;
    }
    return value == ((BoolExpression) other).value;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), this);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    return this;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState) {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(BoolType.Create());
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize, MsgState msgState)
      throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return this;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    return this;
  }
}
