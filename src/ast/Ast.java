package ast;

import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;

public interface Ast {
  public String toString(String indent);

  /*
   * Check which of our imports we will export to anyone who imports us. This is
   * important for dependencies. All module types must be absolute.
   */
  public Optional<Set<ModuleType>> findExportedTypes(ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException;

  /* check types and fill in determinedType along the way, but no inferencing */
  public abstract Optional<TypecheckRet> typecheck(TypeEnvironment tenv,
      Nullable<Type> expectedReturnType) throws FatalMessageException;

  /*
   * execute without having to do typechecking or inferencing, oh it feels so
   * free! Note we still throw exception here (no msgState) since interp always
   * halts on first error
   */
  public abstract Value interp(Environment env) throws InterpError, FatalMessageException;

  public abstract int getLine();

  public abstract int getColumn();

  public void addComments(List<CommentAttr> comments, boolean isPre);

  public abstract AstProto.Ast.Builder serialize() throws SerializationError;

  Type getDeterminedType();

  void setDeterminedType(Type determinedType);

  String strLbl();

  /*
   * Another utility. This determines which ids are unbound in a box so that
   * transpilers can create lambdas with those arguments. Should be done after
   * renaming Maps names to types (for transpilers)
   */
  Map<String, Type> findFreeVariables();

  /**
   * Ignore types in this comparison. Could add flag for this in the future Return
   * true if both ASTs are the same in regards to data
   * 
   * @param other The ast to compare against
   * @return
   */
  public boolean compare(Ast other);

  /**
   * Replace the 'target' AST with the 'with' AST. Uses compare
   */
  public Ast replace(Ast target, Ast with);

  public long getOriginalLabel();

  public void setOriginalLabel(long val);

  public long getLabel();

  /**
   * Return the ast with the given originalLabel, or null if not found
   * 
   * @param findLabel The label of the ast we are looking for
   * @return
   */
  public Nullable<Ast> findByOriginalLabel(int findLabel);

  public abstract Ast getParentAst();

  public abstract void setParentAst(Ast parent);

  enum AnfTune {
    T0
  }

  /**
   * Tuneable anf, lowest setting only anf's (and functions returning) arrays,
   * maps, and classes being passed to functions This should run after renaming
   * and before typechecking (deserialize -> renaming -> toAnf -> typecheck ->
   * transpile) A higher setting could anf strings and even binary/unary ops
   * 
   * @return A pair of temporaries to insert before this line and the replacement
   *         ast
   */

  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning);

  /*
   * replaceType was useful to 2 cases: 1) Renaming lifted inner classes. We have
   * now removed inner classes and lifting classes 2) Avoiding collisions with
   * native names, like Std. We can avoid this by making a standard prefix
   * replacement, like Std->JSMNT_Std, Vector->JSMNT_Vector and forbidding the
   * user from creating any class which starts with JSMNT_. So replaceType becomes
   * much simpler, and does not need mapFromTo. Generic classes are also not a
   * problem. e.g.: A<int> -> JSMNT_A_int, (in mod a) A<Str>->JSMNT_A_a_Str, (in
   * mod std.b) A<Str> -> JSMNT_A_std_b_Str (always include the full module path,
   * important to normalize anyway) For generic replacements (e.g. T->int), we
   * need to keep this as-is. But it is a simpler use case since the from-value
   * will never have an outerType
   */

  boolean withinConstructor();

  /*
   * NOT USED RIGHT NOW - see AbstractAst public abstract ClassDeclaration
   * withinClassDeclaration();
   */

  // you can't really arbitrarily change type without providing a usage converter
  // this must be done after renaming to ensure uniqueness
  // newUsage should probably be a cast, or dereference, ect
  // public abstract Ast changeType(String targetId, Type newType, Ast newUsage);

  /*
   * Replaces function calls with the correct vtable[idx]() calls
   */
  Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException;

  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException;

  public long getOrdering();

  public void setOrdering(long newOrder);

  public boolean containsType(Set<Type> types);

  public boolean containsAst(Set<Ast> asts);

  /*
   * Must run after toAnf but will also insert statements. Must run before
   * renameTypes (but can run after resolveGenerics)
   * 
   * This will resolve all runtime checks (timeout, recursion, memlimit, ect) and
   * simulate exceptions, as well as adding in backtrace tracking. Exception
   * handling is only inserted in certain ast forms, so using this transform on a
   * program without these will only add the fnctx arg for backtrace support.
   */
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException;

  Nullable<Function> withinFunction();

}
