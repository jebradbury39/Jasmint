package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.IntValue;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class IntegerExpression extends AbstractExpression {

  public final String value;
  public final long ivalue;

  private static final int[] SIZES = { 8, 16, 32, 64 };

  public IntegerExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, String value) {
    super(lineNum, columnNum, AstType.INTEGER_EXPRESSION, preComments, postComments);
    this.value = value;
    this.ivalue = parseIntFromString(value);

    determinedType = determineIntTypeFromConstant(this.value, this.ivalue);
  }

  public IntegerExpression(int lineNum, int columnNum, String value) {
    super(lineNum, columnNum, AstType.INTEGER_EXPRESSION);
    this.value = value;
    this.ivalue = parseIntFromString(value);

    determinedType = determineIntTypeFromConstant(this.value, this.ivalue);
  }

  public IntegerExpression(String value) {
    super(AstType.INTEGER_EXPRESSION);
    this.value = value;
    this.ivalue = parseIntFromString(value);

    determinedType = determineIntTypeFromConstant(this.value, this.ivalue);
  }

  public static IntType determineIntTypeFromConstant(String value, long ivalue) {
    // use ivalue to determine the min size and signing of this int
    boolean signed = ivalue < 0; // if < 0, then must be signed
    int size = 0;
    if (signed) {
      for (int s : SIZES) {
        // e.g. -127 >= -127
        if (ivalue >= -1 * Math.pow(2, s) / 2 - 1) {
          size = s;
          break;
        }
      }
      if (size == 0) {
        // ERROR!
        throw new IllegalArgumentException(
            "Cannot store constant negative integer in signed int of any size: " + value + " -> "
                + ivalue);
      }
    } else {
      for (int s : SIZES) {
        // e.g. 256 <= 256
        if (ivalue <= Math.pow(2, s)) {
          size = s;
          break;
        }
      }
      if (size == 0) {
        // ERROR!
        throw new IllegalArgumentException(
            "Cannot store constant positive integer in unsigned int of any size: " + value + " -> "
                + ivalue);
      }
    }

    return IntType.Create(signed, size);
  }

  public static long parseIntFromString(String unparsed) {
    int radix = 10;
    if (unparsed.startsWith("0x") || unparsed.startsWith("0X")) {
      radix = 16;
      unparsed = unparsed.substring(2);
    } else if (unparsed.startsWith("0b") || unparsed.startsWith("0B")) {
      radix = 2;
      unparsed = unparsed.substring(2);
    } else if (unparsed.startsWith("0") && unparsed.length() > 1) {
      radix = 8;
      unparsed = unparsed.substring(1);
    }

    return Long.parseLong(unparsed, radix);
  }

  @Override
  public String toString(String indent) {
    return preToString() + value + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType) {
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    return new IntValue(ivalue, (IntType) determinedType);
  }

  @Override
  public AstProto.Ast.Builder serialize() {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.IntegerExpression.Builder subDoc = AstProto.IntegerExpression.newBuilder();

    subDoc.setUnparsed(value);

    document.setIntegerExpression(subDoc);
    return document;
  }

  public static IntegerExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.IntegerExpression document) {
    return new IntegerExpression(lineNum, columnNum, preComments, postComments,
        document.getUnparsed());
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof IntegerExpression)) {
      return false;
    }
    IntegerExpression ourOther = (IntegerExpression) other;
    return ourOther.value.equals(value);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), this);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    return this;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState) {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return determinedType.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(new LinkedList<>(), Nullable.of(this)));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return this;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
