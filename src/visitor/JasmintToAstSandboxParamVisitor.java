package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import sandbox.MemlimitParam;
import sandbox.RecursionParam;
import sandbox.SandboxParam;
import sandbox.TimeoutParam;

public class JasmintToAstSandboxParamVisitor extends JasmintBaseVisitor<SandboxParam> {

  private final JasmintToAstExpressionVisitor expressionVisitor;
  
  public JasmintToAstSandboxParamVisitor(JasmintToAstExpressionVisitor expressionVisitor) {
    this.expressionVisitor = expressionVisitor;
  }
  
  @Override
  public SandboxParam visitTimeoutParam(JasmintParser.TimeoutParamContext ctx) {
    return new TimeoutParam(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), expressionVisitor.visit(ctx.expression()));
  }
  
  @Override
  public SandboxParam visitRecursionParam(JasmintParser.RecursionParamContext ctx) {
    return new RecursionParam(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), expressionVisitor.visit(ctx.expression()));
  }
  
  @Override
  public SandboxParam visitMemlimitParam(JasmintParser.MemlimitParamContext ctx) {
    return new MemlimitParam(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), expressionVisitor.visit(ctx.expression()));
  }
}
