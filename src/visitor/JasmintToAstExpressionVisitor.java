package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import antlrgen.JasmintParser.ExpressionContext;
import antlrgen.JasmintParser.FnameExprContext;
import antlrgen.JasmintParser.SandboxParamContext;
import antlrgen.JasmintParser.SizeofExpressionContext;
import ast.ArrayInitExpression;
import ast.BinaryExpression;
import ast.BlockStatement;
import ast.BoolExpression;
import ast.BracketExpression;
import ast.CastExpression;
import ast.CatchExcSection;
import ast.CharExpression;
import ast.DeclarationStatement;
import ast.DereferenceExpression;
import ast.DotExpression;
import ast.Expression;
import ast.FfiExpression;
import ast.FloatExpression;
import ast.FunctionCallExpression;
import ast.IdentifierExpression;
import ast.InstanceofExpression;
import ast.IntegerExpression;
import ast.LambdaExpression;
import ast.LeftUnaryOpExpression;
import ast.MapInitExpression;
import ast.MultiExpression;
import ast.NewClassInstanceExpression;
import ast.NewExpression;
import ast.NullExpression;
import ast.ReferenceExpression;
import ast.SandboxExpression;
import ast.SizeofExpression;
import ast.StringExpression;
import errors.Nullable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.tree.TerminalNode;
import sandbox.SandboxParam;
import typecheck.ClassType;
import typecheck.SizeofTarget;
import typecheck.Type;
import typecheck.UndeterminedUserDeclType;
import typecheck.UndeterminedUserInstanceType;

public class JasmintToAstExpressionVisitor extends JasmintBaseVisitor<Expression> {

  private final JasmintToAstTypeVisitor typeVisitor;
  private final JasmintToAstStatementVisitor statementVisitor;
  JasmintToAstProgramVisitor programVisitor;
  private final JasmintToAstSandboxParamVisitor sandboxParamVisitor;

  public JasmintToAstExpressionVisitor(JasmintToAstProgramVisitor programVisitor) {
    this.programVisitor = programVisitor;
    this.typeVisitor = new JasmintToAstTypeVisitor(programVisitor);
    this.statementVisitor = new JasmintToAstStatementVisitor(programVisitor, this);
    this.sandboxParamVisitor = new JasmintToAstSandboxParamVisitor(this);
  }

  @Override
  public Expression visitInvocationExpr(JasmintParser.InvocationExprContext ctx) {
    FunctionCallExpression fval = new FunctionCallExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        Nullable.empty(), visit(ctx.expr), gatherArguments(ctx.arguments()), null);

    Expression val = fval.flattenDot();
    val.setOriginalLabel(val.getLabel());
    return val;
  }

  public List<Expression> gatherArguments(JasmintParser.ArgumentsContext ctx) {
    List<Expression> args = new LinkedList<Expression>();

    for (JasmintParser.ExpressionContext ectx : ctx.expression()) {
      args.add(visit(ectx));
    }

    return args;
  }

  @Override
  public Expression visitLambdaExpr(JasmintParser.LambdaExprContext ctx) {
    LambdaExpression val = new LambdaExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        typeVisitor.visit(ctx.lambda().returnType()), gatherParams(ctx.lambda().parameters()),
        (BlockStatement) statementVisitor.visit(ctx.lambda().block()));
    val.setOriginalLabel(val.label);
    return val;
  }

  private List<DeclarationStatement> gatherParams(JasmintParser.ParametersContext ctx) {
    List<DeclarationStatement> params = new LinkedList<>();

    for (JasmintParser.PdeclContext pctx : ctx.pdecl()) {
      // cannot be static or required
      DeclarationStatement val = (DeclarationStatement) statementVisitor.visit(pctx.declNoExpr());
      params.add(val);
    }

    return params;
  }

  @Override
  public Expression visitDotExpr(JasmintParser.DotExprContext ctx) {
    int lineNum = ctx.getStart().getLine();
    int columnNum = ctx.getStart().getCharPositionInLine();

    IdentifierExpression idval = new IdentifierExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.ID().getText());
    idval.setOriginalLabel(idval.label);

    DotExpression val = new DotExpression(lineNum, columnNum, new LinkedList<>(),
        new LinkedList<>(), visit(ctx.value), idval);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitLeftUnaryOpExpr(JasmintParser.LeftUnaryOpExprContext ctx) {
    LeftUnaryOpExpression val = LeftUnaryOpExpression.createLeftUnaryOpExpression(
        ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(), new LinkedList<>(),
        new LinkedList<>(), ctx.op.getText(), visit(ctx.unaryExpression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitSizeofExpr(JasmintParser.SizeofExprContext ctx) {
    SizeofExpressionContext subCtx = ctx.sizeofExpression();

    SizeofTarget target = null;
    if (subCtx.type() != null) {
      target = typeVisitor.visit(subCtx.type());
    } else {
      target = visit(subCtx.unaryExpression());
    }

    SizeofExpression val = new SizeofExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(), target);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitMultiplyExpression(JasmintParser.MultiplyExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitAdditionExpression(JasmintParser.AdditionExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitShiftExpression(JasmintParser.ShiftExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitCompareExpression(JasmintParser.CompareExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitEqualsExpression(JasmintParser.EqualsExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    
    String opStr = ctx.op.getText();
    if (ctx.pre != null) {
      opStr = ctx.pre.getText() + opStr;
    }
    
    BinaryExpression.BinOp op = BinaryExpression.stringToBinOp(opStr);
    
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(), op,
        visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitAndExpression(JasmintParser.AndExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitExclusiveOrExpression(JasmintParser.ExclusiveOrExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitInclusiveOrExpression(JasmintParser.InclusiveOrExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitLogicalAndExpression(JasmintParser.LogicalAndExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitLogicalOrExpression(JasmintParser.LogicalOrExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    BinaryExpression val = new BinaryExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        BinaryExpression.stringToBinOp(ctx.op.getText()), visit(ctx.lft), visit(ctx.rht));
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Expression visitInstanceofExpression(JasmintParser.InstanceofExpressionContext ctx) {
    if (ctx.base != null) {
      return visit(ctx.base);
    }
    InstanceofExpression.OpType op = InstanceofExpression.OpType.INSTANCEOF;
    if (ctx.pre != null) {
      op = InstanceofExpression.OpType.NOT_INSTANCEOF;
    }
    
    Type rightType = typeVisitor.visit(ctx.rht);
    if (!(rightType instanceof UndeterminedUserInstanceType)) {
      throw new IllegalArgumentException("right side of instanceof must be user type");
    }
    UndeterminedUserInstanceType rightUserType = (UndeterminedUserInstanceType) rightType;
    ClassType rightClassType = new ClassType(rightUserType.outerType, rightUserType.name, rightUserType.innerTypes);
    
    InstanceofExpression val = new InstanceofExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        op, visit(ctx.lft), rightClassType);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitIdentifierExpr(JasmintParser.IdentifierExprContext ctx) {
    IdentifierExpression val = new IdentifierExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.ID().getText());
    val.setOriginalLabel(val.label);

    return val;
  }

  @Override
  public Expression visitIntegerExpr(JasmintParser.IntegerExprContext ctx) {
    IntegerExpression val = new IntegerExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.IntegerConstant().getText());
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitDoubleExpr(JasmintParser.DoubleExprContext ctx) {
    FloatExpression val = new FloatExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.FloatingConstant().getText());
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitStringExpr(JasmintParser.StringExprContext ctx) {
    return visit(ctx.stringExpression());
  }

  @Override
  public Expression visitCharExpr(JasmintParser.CharExprContext ctx) {
    CharExpression val = new CharExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.CharacterConstant().getText());
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitStringExpression(JasmintParser.StringExpressionContext ctx) {
    String literalText = "";
    for (TerminalNode node : ctx.StringLiteral()) {
      literalText += node.getText();
    }

    StringExpression val = new StringExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        literalText);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitBoolExpr(JasmintParser.BoolExprContext ctx) {
    BoolExpression val = new BoolExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.getText().equals("true"));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitNewExpr(JasmintParser.NewExprContext ctx) {
    NewExpression val = new NewExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        visit(ctx.postfixExpression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitSandboxExpr(JasmintParser.SandboxExprContext ctx) {
    programVisitor.enterSandboxExpression();
    Expression val = visit(ctx.sandboxExpression());
    programVisitor.exitSandboxExpression();
    return val;
  }
  
  @Override
  public Expression visitSandboxExpression(JasmintParser.SandboxExpressionContext ctx) {
    List<Type> types = new LinkedList<>();
    if (ctx.sandboxTypes() != null) {
      for (JasmintParser.UndeterminedUserTypeDeclContext tctx : ctx.sandboxTypes().undeterminedUserTypeDecl()) {
        Type tmpType = typeVisitor.visit(tctx);
        if (tmpType instanceof ClassType) {
          ClassType tmpClassType = (ClassType) tmpType;
          if (tmpClassType.innerTypes.isEmpty()) {
            tmpType = tmpClassType.asDeclType();
          }
        }
        types.add(tmpType);
      }
    }
    
    List<DeclarationStatement> args = new LinkedList<>();
    for (JasmintParser.DeclExprContext actx : ctx.sandboxArgs().declExpr()) {
      // cannot be static or required
      DeclarationStatement val = (DeclarationStatement) statementVisitor.visit(actx);
      args.add(val);
    }
    
    List<SandboxParam> params = new LinkedList<>();
    for (SandboxParamContext pctx : ctx.sandboxParam()) {
      params.add(sandboxParamVisitor.visit(pctx));
    }
    
    List<Expression> importFunctions = new LinkedList<>();
    if (ctx.sandboxFuns() != null) {
      for (FnameExprContext fctx : ctx.sandboxFuns().fnameExpr()) {
        importFunctions.add(visit(fctx));
      }
    }

    SandboxExpression val = new SandboxExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        typeVisitor.visit(ctx.returnTypeReal()), types, args, importFunctions, params,
        (BlockStatement) statementVisitor.visit(ctx.block()),
        (CatchExcSection) visit(ctx.catchExc()));
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Expression visitFnameIdExpr(JasmintParser.FnameIdExprContext ctx) {
    IdentifierExpression val = new IdentifierExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.ID().getText());
    val.setOriginalLabel(val.label);

    return val;
  }
  
  @Override
  public Expression visitFnameDotExpr(JasmintParser.FnameDotExprContext ctx) {
    int lineNum = ctx.getStart().getLine();
    int columnNum = ctx.getStart().getCharPositionInLine();

    IdentifierExpression idval = new IdentifierExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.ID().getText());
    idval.setOriginalLabel(idval.label);

    DotExpression val = new DotExpression(lineNum, columnNum, new LinkedList<>(),
        new LinkedList<>(), visit(ctx.fnameExpr()), idval);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitCatchExc(JasmintParser.CatchExcContext ctx) {
    CatchExcSection val = new CatchExcSection(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        ctx.ID().getText(), (BlockStatement) statementVisitor.visit(ctx.block()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitNullExpr(JasmintParser.NullExprContext ctx) {
    NullExpression val = new NullExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>());
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitNestedExpr(JasmintParser.NestedExprContext ctx) {
    return visit(ctx.multiExpression());
  }

  @Override
  public Expression visitBracketExpr(JasmintParser.BracketExprContext ctx) {
    BracketExpression val = new BracketExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        visit(ctx.lft), visit(ctx.index));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitArrayInitExpr(JasmintParser.ArrayInitExprContext ctx) {
    ArrayInitExpression val = new ArrayInitExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        typeVisitor.visit(ctx.elementType), gatherArguments(ctx.arguments()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitMapInitExpr(JasmintParser.MapInitExprContext ctx) {
    Map<Expression, Expression> pairs = new HashMap<Expression, Expression>();

    for (JasmintParser.KeyValuePairContext kvctx : ctx.newMap().keyValuePair()) {
      pairs.put(visit(kvctx.key), visit(kvctx.value));
    }

    MapInitExpression val = new MapInitExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        typeVisitor.visit(ctx.keyType), typeVisitor.visit(ctx.valueType), pairs);
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitFfiExpr(JasmintParser.FfiExprContext ctx) {
    FfiExpression val = new FfiExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        (StringExpression) visit(ctx.ffiBlock().stringExpression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitCastExpr(JasmintParser.CastExprContext ctx) {
    CastExpression val = new CastExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        typeVisitor.visit(ctx.type()), visit(ctx.expression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitRefExpr(JasmintParser.RefExprContext ctx) {
    ReferenceExpression val = new ReferenceExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        visit(ctx.unaryExpression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitDerefExpr(JasmintParser.DerefExprContext ctx) {
    DereferenceExpression val = new DereferenceExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        visit(ctx.unaryExpression()));
    val.setOriginalLabel(val.label);
    return val;
  }

  @Override
  public Expression visitMultiExpression(JasmintParser.MultiExpressionContext ctx) {
    List<Expression> expressions = new LinkedList<>();
    for (ExpressionContext ectx : ctx.expression()) {
      expressions.add(visit(ectx));
    }
    if (expressions.size() == 1) {
      return expressions.get(0);
    }
    MultiExpression val = new MultiExpression(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(), new LinkedList<>(), new LinkedList<>(),
        expressions);
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Expression visitNewClassInstanceExpr(JasmintParser.NewClassInstanceExprContext ctx) {
    int lineNum = ctx.getStart().getLine();
    int columnNum = ctx.getStart().getCharPositionInLine();

    IdentifierExpression fnName = new IdentifierExpression(lineNum, columnNum, new LinkedList<>(),
        new LinkedList<>(), "constructor");
    fnName.setOriginalLabel(fnName.label);

    FunctionCallExpression constructor = new FunctionCallExpression(lineNum, columnNum,
        new LinkedList<>(), new LinkedList<>(), Nullable.empty(), fnName,
        gatherArguments(ctx.arguments()), "");
    constructor.setOriginalLabel(constructor.label);

    UndeterminedUserDeclType classType = (UndeterminedUserDeclType) typeVisitor.visit(
        ctx.undeterminedUserTypeDecl());

    ClassType name = new ClassType(classType.outerType, classType.name, gatherTypes(ctx.typelist()));

    NewClassInstanceExpression val = new NewClassInstanceExpression(lineNum, columnNum,
        new LinkedList<>(), new LinkedList<>(), name, name.toString(), constructor);
    val.setOriginalLabel(val.label);
    return val;
  }

  private List<Type> gatherTypes(JasmintParser.TypelistContext ctx) {
    List<Type> types = new LinkedList<Type>();

    if (ctx != null) {
      for (JasmintParser.TypeContext tctx : ctx.type()) {
        types.add(typeVisitor.visit(tctx));
      }
    }

    return types;
  }

}
