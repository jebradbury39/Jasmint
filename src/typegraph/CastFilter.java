package typegraph;

import java.util.HashSet;
import java.util.Set;
import typegraph.TypeGraph.CastDirection;

public class CastFilter {
  public static final CastFilter UP_OR_DOWN = CastFilter.Create(CastDirection.UP, CastDirection.DOWN);
  public static final CastFilter UP_OR_DOWN_OR_SIBLING = CastFilter.Create(CastDirection.UP,
      CastDirection.DOWN, CastDirection.SIBLING);
  
  public final Set<CastDirection> direction;
  
  private CastFilter(Set<CastDirection> direction) {
    this.direction = direction;
  }
  
  public static CastFilter Create(CastDirection a) {
    Set<CastDirection> filter = new HashSet<>();
    filter.add(a);
    return new CastFilter(filter);
  }
  
  public static CastFilter Create(CastDirection a, CastDirection b) {
    Set<CastDirection> filter = new HashSet<>();
    filter.add(a);
    filter.add(b);
    return new CastFilter(filter);
  }
  
  public static CastFilter Create(CastDirection a, CastDirection b, CastDirection c) {
    Set<CastDirection> filter = new HashSet<>();
    filter.add(a);
    filter.add(b);
    filter.add(c);
    return new CastFilter(filter);
  }
}
