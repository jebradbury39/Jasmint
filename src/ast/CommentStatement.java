package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class CommentStatement extends AbstractStatement {

  public boolean isBlockComment;
  public final String comment;

  public CommentStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, String comment, boolean isBlockComment) {
    super(lineNum, columnNum, AstType.COMMENT_STATEMENT, preComments, postComments);
    this.comment = comment;
    this.isBlockComment = isBlockComment;
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return this;
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + comment + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    return Optional.of(new TypecheckRet(VoidType.Create()));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    return new VoidValue();
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();
    AstProto.CommentStatement.Builder subDoc = AstProto.CommentStatement.newBuilder();

    subDoc.setComment(comment);
    subDoc.setIsBlockComment(isBlockComment);

    document.setCommentStatement(subDoc);
    return document;
  }

  public static Statement deserialize(int dLineNum, int dColumnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.CommentStatement document)
      throws SerializationError {
    return new CommentStatement(dLineNum, dColumnNum, preComments, postComments,
        document.getComment(), document.getIsBlockComment());
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof CommentStatement)) {
      return false;
    }
    CommentStatement ourOther = (CommentStatement) other;
    return ourOther.comment.equals(comment) && ourOther.isBlockComment == isBlockComment;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<>(new LinkedList<>(), this);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    return this;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) {
    return new InsertDestructorsRet(this);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
