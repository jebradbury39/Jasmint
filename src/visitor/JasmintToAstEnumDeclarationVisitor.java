package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import ast.EnumDeclaration;
import errors.Nullable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import multifile.JsmntGlobal;
import org.antlr.v4.runtime.tree.TerminalNode;
import typecheck.EnumDeclType;

public class JasmintToAstEnumDeclarationVisitor extends JasmintBaseVisitor<EnumDeclaration> {

  private final JasmintToAstProgramVisitor programVisitor;
  
  public JasmintToAstEnumDeclarationVisitor(JasmintToAstProgramVisitor programVisitor) {
    this.programVisitor = programVisitor;
  }
  
  @Override
  public EnumDeclaration visitEnumDeclaration(JasmintParser.EnumDeclarationContext ctx) {
    String enumName = ctx.ID().getText();
    Set<String> invalidNames = new HashSet<>();
    if (!programVisitor.isJsmntGlobal) {
      invalidNames = JsmntGlobal.getReservedTypeNames();
    }
    if (invalidNames.contains(enumName)) {
      throw new IllegalArgumentException("enum name cannot be '" + enumName + "'");
    }
    
    EnumDeclType enumType = new EnumDeclType(Nullable.of(programVisitor.getModuleType()),
        enumName);
    
    List<String> enumIds = new LinkedList<>();
    for (TerminalNode id : ctx.enumEntries().ID()) {
      enumIds.add(id.getText());
    }
    
    EnumDeclaration val = new EnumDeclaration(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        false, //wrapProgramLine ctx can change this
        enumType,
        enumIds);
    val.setOriginalLabel(val.label);
    return val;
  }
  
}
