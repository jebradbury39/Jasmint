package type_env;

import errors.FatalMessageException;
import java.util.Optional;

public abstract class UserDeclTypeBox extends AbstractTypeBox {

  public UserDeclTypeBox(boolean export, boolean isInit, VariableScoping scoping) {
    super(export, isInit, scoping);
  }

  protected abstract Optional<TypeEnvironment> getTenv(TypeEnvironment tenv)
      throws FatalMessageException;

}
