package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class SwitchCaseStatement extends AbstractStatement {

  public final Nullable<Expression> value; /* null if default */
  public final BlockStatement body;

  public SwitchCaseStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Nullable<Expression> value, BlockStatement body) {
    super(lineNum, columnNum, AstType.SWITCH_CASE_STATEMENT, preComments, postComments);
    this.value = value;
    this.body = body;

    if (value.isNotNull()) {
      value.get().setParentAst(this);
    }
    body.setParentAst(this);

    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    String result = preToString();
    if (value.isNull()) {
      result += "default:";
    } else {
      result += value.get().toString(indent) + ":";
    }

    String save = indent;
    indent += incIndent;

    for (Statement statement : body.statements) {
      result += indent + statement.toString(indent);
      if (result.charAt(result.length() - 1) != '}') {
        result += ";";
      }
      result += "\n";
    }

    indent = save;

    return result + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    throw new UnsupportedOperationException("use typecheckCustom");
  }

  public Optional<Pair<Nullable<Type>, TypecheckRet>> customTypecheck(TypeEnvironment tenv,
      Nullable<Type> expectedReturnType) throws FatalMessageException {

    boolean hitErr = false;
    Nullable<Type> valueType = Nullable.empty();
    if (value.isNotNull()) {
      Optional<TypecheckRet> valueOpt = value.get().typecheck(tenv, Nullable.empty());
      if (!valueOpt.isPresent()) {
        hitErr = true;
      } else {
        valueType = Nullable.of(valueOpt.get().type);
      }
    }

    Optional<TypecheckRet> bodyOpt = body.typecheck(tenv, expectedReturnType);
    if (!bodyOpt.isPresent() || hitErr) {
      return Optional.empty();
    }

    return Optional.of(new Pair<Nullable<Type>, TypecheckRet>(valueType, bodyOpt.get()));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    return body.interp(env);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.SwitchCaseStatement.Builder subDoc = AstProto.SwitchCaseStatement.newBuilder();

    if (value.isNotNull()) {
      subDoc.addValue(value.get().serialize());
    }
    subDoc.setBody(body.serialize());

    document.setSwitchCaseStatement(subDoc);
    return document;
  }

  public static SwitchCaseStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.SwitchCaseStatement document) throws SerializationError {
    Nullable<Expression> value = Nullable.empty();
    if (document.getValueCount() > 0) {
      value = Nullable.of(AbstractExpression.deserialize(document.getValue(0)));
    }
    return new SwitchCaseStatement(lineNum, columnNum, preComments, postComments, value,
        (BlockStatement) AbstractStatement.deserialize(document.getBody()));
  }

  public static SwitchCaseStatement deserialize(AstProto.Ast document) throws SerializationError {
    int deLineNum = document.getLineNum();
    int deColumnNum = document.getColNum();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();
    AbstractAst.preDeserialize(document, preComments, postComments);
    Type deDeterminedType = AbstractType.deserialize(document.getDeterminedType());

    SwitchCaseStatement value = deserialize(deLineNum, deColumnNum, preComments, postComments,
        document.getSwitchCaseStatement());

    value.determinedType = deDeterminedType;
    return value;
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Nullable<Expression> newValue = value;
    if (value.isNotNull()) {
      newValue = Nullable.of(value.get().renameIds(renamings, msgState));
    }

    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        newValue, (BlockStatement) body.renameIds(renamings, msgState));
    val.determinedType = determinedType.renameIds(renamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = body.findFreeVariables();
    if (value.isNotNull()) {
      freeVars.putAll(value.get().findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof SwitchCaseStatement)) {
      return false;
    }
    SwitchCaseStatement ourOther = (SwitchCaseStatement) other;
    if ((ourOther.value.isNull() && value.isNotNull())
        || (ourOther.value.isNotNull() && value.isNull())) {
      return false;
    }
    if (value.isNotNull()) {
      if (!value.get().compare(ourOther.value.get())) {
        return false;
      }
    }
    return body.compare(ourOther.body);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        (this.value.isNull() ? Nullable.empty()
            : Nullable.of((Expression) this.value.get().replace(target, with))),
        (BlockStatement) body.replace(target, with));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    if (value.isNotNull()) {
      Nullable<Ast> tmp = value.get().findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return body.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> valuePair = null;
    if (value.isNotNull()) {
      valuePair = value.get().toAnf(tuning);
      insert.addAll(valuePair.a);
    }
    Pair<List<DeclarationStatement>, Ast> bodyPair = body.toAnf(tuning);
    insert.addAll(bodyPair.a);

    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        (this.value.isNull() ? Nullable.empty() : Nullable.of((Expression) valuePair.b)),
        (BlockStatement) bodyPair.b);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        (this.value.isNull() ? Nullable.empty() : Nullable.of(value.get().replaceType(mapFromTo))),
        (BlockStatement) body.replaceType(mapFromTo));
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        (this.value.isNull() ? Nullable.empty()
            : Nullable.of((Expression) value.get().replaceVtableAccess(project, msgState).get())),
        (BlockStatement) body.replaceVtableAccess(project, msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        (this.value
            .isNull()
                ? Nullable.empty()
                : Nullable.of(value.get()
                    .normalizeType(keepCurrentModRelative, currentModule, effectiveImports,
                        msgState)
                    .get().get())),
        (BlockStatement) body
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get());
    val.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    if (value.isNotNull()) {
      value.get().addDependency(modDepGraph, ourModNode, tenv);
    }
    body.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (value.isNotNull()) {
      if (value.get().containsType(types)) {
        return true;
      }
    }
    return body.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    if (value.isNotNull()) {
      if (value.get().containsAst(asts)) {
        return true;
      }
    }
    return body.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();
    Optional<ResolveExcRet> tmp = Optional.empty();

    Nullable<Expression> newValue = value;
    if (value.isNotNull()) {
      tmp = value.get().resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
          stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newValue = Nullable.of((Expression) tmp.get().ast.get());
    }

    tmp = body.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final BlockStatement newBody = (BlockStatement) tmp.get().ast.get();

    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        newValue, newBody);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(val)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    List<Statement> ourStatements = new LinkedList<>();

    InsertDestructorsRet tmp = body.insertDestructorCalls(scopedIds, destructorRenamings);
    BlockStatement newBody = (BlockStatement) tmp.statements.get(0);
    
    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        value, newBody);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());

    ourStatements.add(val);
    return new InsertDestructorsRet(ourStatements, tmp.mustReturn);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        (this.value
            .isNull()
                ? Nullable.empty()
                : Nullable.of(value.get()
                    .liftClassDeclarations(liftedClasses, replacements)
                    .get())),
        (BlockStatement) body
            .liftClassDeclarations(liftedClasses, replacements)
            .get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        (this.value
            .isNull()
                ? Nullable.empty()
                : Nullable.of(value.get()
                    .resolveUserTypes(msgState)
                    .get())),
        (BlockStatement) body
            .resolveUserTypes(msgState)
            .get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    SwitchCaseStatement val = new SwitchCaseStatement(lineNum, columnNum, preComments, postComments,
        (this.value
            .isNull()
                ? Nullable.empty()
                : Nullable.of(value.get()
                    .addThisExpression(classHeader))),
        (BlockStatement) body
            .addThisExpression(classHeader));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }
}
