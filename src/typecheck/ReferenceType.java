package typecheck;

import ast.Expression;
import ast.NullExpression;
import ast.SerializationError;
import astproto.AstProto;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;

public class ReferenceType extends AbstractType {

  public final ReferencedType innerType;
  
  public ReferenceType(ReferencedType innerType) {
    super(TypeType.REFERENCE_TYPE);
    this.innerType = innerType;
  }

  @Override
  public int hashCode() {
    return super.hashCode() + innerType.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof ReferenceType)) {
      return false;
    }
    ReferenceType otherRT = (ReferenceType) other;
    return innerType.equals(otherRT.innerType);
  }

  @Override
  public List<Type> getInnerTypes() {
    List<Type> inner = new LinkedList<Type>();
    inner.add(innerType);
    return inner;
  }
  
  @Override
  public String toString() {
    return super.toString() + innerType + "*";
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }
    return new ReferenceType((ReferencedType) innerType.replaceType(mapFromTo));
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.ReferenceType.Builder subDoc = AstProto.ReferenceType.newBuilder();

    subDoc.setInnerType(innerType.serialize().build());

    document.setReferenceType(subDoc);
    return document;
  }
  
  public static ReferenceType deserialize(AstProto.ReferenceType document)
      throws SerializationError {
    ReferencedType innerType = (ReferencedType) AbstractType.deserialize(document.getInnerType());
    
    return new ReferenceType(innerType);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (types.contains(this)) {
      return true;
    }
    return innerType.containsType(types);
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    if (innerType instanceof ClassType) {
      ClassType userTy = (ClassType) innerType;
      Set<UserDeclType> stackTypes = new HashSet<>();
      if (!stackTypesOnly) {
        stackTypes.add(new ClassDeclType(userTy.outerType, userTy.name));
      }
      for (Type iterInner : userTy.innerTypes) {
        stackTypes.addAll(iterInner.getUserDeclTypes(stackTypesOnly));
      }
      return stackTypes;
    }
    return innerType.getUserDeclTypes(stackTypesOnly);
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    Optional<Type> tmp = innerType.normalize(tenv, lineNum, columnNum);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new ReferenceType((ReferencedType) tmp.get()));
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    return new ReferenceType((ReferencedType) innerType.renameIds(renamings, msgState));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException {
    innerType.addDependency(modDepGraph, ourModNode, tenv, true, lineNum, columnNum);
  }

  @Override
  public Type trimRelativeType(UserInstanceType currentModuleFullType) {
    return new ReferenceType((ReferencedType) innerType.trimRelativeType(currentModuleFullType));
  }

  @Override
  public Type setConst(boolean isConst) {
    ReferenceType tmp = new ReferenceType(innerType);
    tmp.isConst = isConst;
    return tmp;
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState)
          throws FatalMessageException {
    Optional<Nullable<Type>> tmp = innerType.basicNormalize(keepCurrentModRelative,
        currentModule, effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(Nullable.of(new ReferenceType((ReferencedType) tmp.get().get())));
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState,
      Set<ModuleType> exportedTypes, boolean skipNormalize) throws FatalMessageException {
    return innerType.getNormalizedModType(currentModule, effectiveImports, msgState, exportedTypes,
        skipNormalize);
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new NullExpression();
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    ReferenceType newType = new ReferenceType((ReferencedType) innerType.prependFnRetType(prependType, false));
    if (!isTop) {
      return newType;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(newType);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    // 8 byte ptr (assume 64-bit)
    return 8;
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return innerType.copyFromTenvIntoTenv(tenv, newTenv);
  }

  @Override
  public Type asClassDeclType() {
    return new ReferenceType(innerType);
  }
}
