package transform;

import ast.Ast;
import ast.Statement;
import errors.Nullable;
import java.util.LinkedList;
import java.util.List;

public class ResolveExcRet {
  public final List<Statement> preStatements;
  public final Nullable<Ast> ast;
  
  public ResolveExcRet(List<Statement> preStatements, Nullable<Ast> ast) {
    this.preStatements = preStatements;
    this.ast = ast;
  }
  
  public ResolveExcRet(List<Statement> preStatements, Ast ast) {
    this.preStatements = preStatements;
    this.ast = Nullable.of(ast);
  }
  
  public ResolveExcRet(Ast ast) {
    this.preStatements = new LinkedList<>();
    this.ast = Nullable.of(ast);
  }
}
