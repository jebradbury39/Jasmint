package interp;

import errors.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.ArrayType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.IntType;
import typecheck.Type;
import typecheck.VoidType;

public class ArrayValue extends AbstractValue {

  /* memory */
  private List<ValueBox> elements = new ArrayList<ValueBox>();

  public ArrayValue(Type elementType, List<Value> initialElements) {
    super(MetaValue.NORMAL, new ArrayType(elementType));
    for (Value val : initialElements) {
      try {
        ValueBox box = new ValueBox(val);
        elements.add(box);
      } catch (InterpError e) {
        throw new IllegalArgumentException(e.getMessage());
      }
    }
  }

  public ArrayValue(MetaValue metaValue, Type elementType, List<Value> initialElements) {
    super(metaValue, new ArrayType(elementType));
    for (Value val : initialElements) {
      try {
        ValueBox box = new ValueBox(val);
        elements.add(box);
      } catch (InterpError e) {
        throw new IllegalArgumentException(e.getMessage());
      }
    }
  }

  private ArrayValue(boolean x, Type type, List<ValueBox> elements) {
    super(MetaValue.NORMAL, type);
    this.elements = elements;
  }

  public ValueBox getElementByIndex(int index) throws InterpError {
    if (index < 0 || index > elements.size()) {
      throw new InterpError("index [" + index + "] is out of range [0, " + elements.size() + ")",
          -1, -1);
    }
    return elements.get(index);
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    String result = "[";

    boolean first = true;
    for (ValueBox vb : elements) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += ((StringValue) (vb.getValue().add(""))).value; // don't need copy just for stringify
    }

    return new StringValue(leftValue + result + "]");
  }

  @Override
  public Value copy(Environment copyEnv) throws InterpError {
    List<Value> newElements = new ArrayList<Value>(); // new memory
    for (ValueBox element : elements) {
      newElements.add(element.getValue().copy(null)); // copy each element into a new box/memory
    }

    return new ArrayValue(metaValue, type.getInnerTypes().get(0), newElements);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    if (this == other) {
      return true;
    }

    Iterator<ValueBox> iterUs = elements.iterator();
    Iterator<ValueBox> iterOther = ((ArrayValue) other).elements.iterator();

    for (; iterUs.hasNext() && iterOther.hasNext();) {
      if (!iterUs.next().getValue().equals(iterOther.next().getValue())) {
        return false;
      }
    }
    return true;
  }

  public Environment getEnv(Environment env) throws InterpError {
    Map<String, IdToValue> topIds = new HashMap<String, IdToValue>();

    // add 'add' as a system function
    List<Type> addParam = new ArrayList<Type>();
    addParam.add(type.getInnerTypes().get(0));
    topIds.put("add", new MapFunctionToValue().write("add",
        new ValueBox(HostFunctionValue.createHostFunctionValue(new FunctionType(Nullable.empty(),
            VoidType.Create(), addParam, LambdaStatus.NOT_LAMBDA), (List<Value> paramValues) -> {
              // add to end of array, extending size by one
              elements.add(new ValueBox(paramValues.get(0).copy(null)));
              return new VoidValue();
            }), VariableScoping.CLASS_INSTANCE)));

    List<Type> add2Param = new ArrayList<Type>();
    add2Param.add(IntType.Create(false, 64));
    add2Param.add(type.getInnerTypes().get(0));
    topIds.get("add").write("add", new ValueBox(HostFunctionValue.createHostFunctionValue(
        new FunctionType(Nullable.empty(), VoidType.Create(), add2Param, LambdaStatus.NOT_LAMBDA),
        (List<Value> paramValues) -> {
          // add at index in array
          long idx = paramValues.get(0).toInt();
          if (idx < 0) {
            throw new InterpError("cannot add to array at index < 0: " + idx, -1, -1);
          }
          if (idx > elements.size()) {
            throw new InterpError(
                "index greater than size of the array[size = " + elements.size() + "]: " + idx, -1,
                -1);
          }
          elements.add((int) idx, new ValueBox(paramValues.get(1).copy(null)));
          return new VoidValue();
        }), VariableScoping.CLASS_INSTANCE));

    // remove
    List<Type> removeParam = new ArrayList<Type>();
    removeParam.add(IntType.Create(false, 64));
    topIds.put("remove", new MapFunctionToValue().write("remove",
        new ValueBox(HostFunctionValue.createHostFunctionValue(new FunctionType(Nullable.empty(),
            type.getInnerTypes().get(0), removeParam, LambdaStatus.NOT_LAMBDA),
            (List<Value> paramValues) -> {
              // remove at idx and return removed value
              long idx = paramValues.get(0).toInt();
              if (idx < 0) {
                throw new InterpError("cannot add to array at index < 0: " + idx, -1, -1);
              }
              if (idx > elements.size()) {
                throw new InterpError(
                    "index greater than size of the array[size = " + elements.size() + "]: " + idx,
                    -1, -1);
              }

              return elements.remove((int) idx).getValue();
            }), VariableScoping.CLASS_INSTANCE)));

    // size
    List<Type> sizeParam = new ArrayList<Type>();
    topIds.put("size", new MapFunctionToValue().write("size",
        new ValueBox(HostFunctionValue.createHostFunctionValue(new FunctionType(Nullable.empty(),
            IntType.Create(false, 64), sizeParam, LambdaStatus.NOT_LAMBDA),
            (List<Value> paramValues) -> {
              return new IntValue(elements.size(), IntType.Create(false, 64));
            }), VariableScoping.CLASS_INSTANCE)));

    return new Environment(topIds, TypeEnvironment.ScopeType.INSTANCE, null, env.typeGraph,
        env.project);
  }

}
