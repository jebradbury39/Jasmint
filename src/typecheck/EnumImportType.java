package typecheck;

import ast.SerializationError;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import errors.Nullable;
import java.util.List;
import java.util.Map;
import util.DotGen;

public class EnumImportType extends ResolvedImportType {

  public EnumImportType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.ENUM_IMPORT_TYPE, outerType, name);
  }
  
  @Override
  public Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.EnumImportType.Builder subDoc = AstProto.EnumImportType.newBuilder();
    if (outerType.isNotNull()) {
      subDoc.addOuterType(outerType.get().serialize());
    }
    subDoc.setName(name);
    
    document.setEnumImportType(subDoc);
    return document;
  }
  
  public static EnumImportType deserialize(AstProto.EnumImportType document)
      throws SerializationError {
    Nullable<ModuleType> outerType = Nullable.empty();
    if (document.getOuterTypeCount() > 0) {
      outerType = Nullable.of((ModuleType) AbstractType.deserialize(document.getOuterType(0)));
    }
    return new EnumImportType(outerType, document.getName());
  }

  @Override
  public UserDeclType asUserDeclType() {
    return new EnumDeclType(outerType, name);
  }

  @Override
  public ModuleType asModuleType() {
    return outerType.get();
  }

  @Override
  public ImportType onlyName() {
    return new EnumImportType(Nullable.empty(), name);
  }

  @Override
  public ResolvedImportType replaceModuleType(ModuleType newModType) {
    return new EnumImportType(Nullable.of(newModType), name);
  }
  
  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type tmp = mapFromTo.get(this);
    if (tmp == null) {
      return this;
    }
    if (tmp instanceof EnumDeclType) {
      return ((EnumDeclType) tmp).asImportType();
    }
    if (tmp instanceof EnumImportType) {
      return tmp;
    }
    throw new IllegalArgumentException("expected EnumDeclType or EnumImportType");
  }
  
  @Override
  public UserDeclType fromList(List<String> path) {
    if (path.isEmpty()) {
      throw new IllegalArgumentException();
    }
    final String name = path.get(path.size() - 1);
    List<String> subPath = path.subList(0, path.size() - 1);
    
    Nullable<ModuleType> moduleType = Nullable.empty();
    for (String item : subPath) {
      moduleType = Nullable.of(new ModuleType(moduleType, item));
    }
    return new EnumImportType(moduleType, name);
  }
  
  @Override
  public DotGen.Color getGraphColor() {
    return DotGen.Color.PURPLE;
  }

}
