package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

/*
 * (non-Javadoc)
 * @see ast.Expression#renameIds(util.RenamingMap)
 * 
 * Not visible to user, but used during renaming e.g.
 * classinstance.overridden() -> classinstance.<vtable access [index]>()
 * classinstance.overridden -> classinstance.<vtable access [index]>
 * The actual vtable is in the class declaration and is a list of pairs
 * [(new_function_name, FunctionType)]
 * During rename, mark this function as vtable and give index.
 * Can then rename the function to whatever
 * The same process applies to parent overridden function (typechecker marks as overridden)
 * Only used if left is reference, not stack
 * 
 * Further renamings must update the vtable (check for name, replace with new name, function type)
 * Make sure to keep function type updated with new class names
 * 
 * The transpiler then creates a vtable of function pointers, using those function names)
 */
public class VtableAccessExpression extends AbstractExpression {

  public final int index;
  // The type of function which will get called by this vtable access
  public final FunctionType fnType;

  public VtableAccessExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, int index, FunctionType fnType) {
    super(lineNum, columnNum, AstType.VTABLE_ACCESS_EXPRESSION, preComments, postComments);
    this.index = index;
    this.fnType = fnType;

    // since this is on a per-instance basis,
    // we should not really need to worry about generics
    determinedType = fnType;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public String toString(String indent) {
    return preToString() + "<vtable access [" + index + "] type: " + fnType + ">" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType) {
    // Will get this after renamings
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    throw new InterpError("vtable access not supported during interp", lineNum, columnNum);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.VtableAccessExpression.Builder subDoc = AstProto.VtableAccessExpression.newBuilder();

    subDoc.setIndex(index);
    subDoc.setFnType(fnType.serialize());

    document.setVtableAccessExpression(subDoc);
    return document;
  }

  public static VtableAccessExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.VtableAccessExpression document) throws SerializationError {
    return new VtableAccessExpression(lineNum, columnNum, preComments, postComments,
        document.getIndex(), (FunctionType) AbstractType.deserialize(document.getFnType()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof VtableAccessExpression)) {
      return false;
    }
    VtableAccessExpression otherV = (VtableAccessExpression) other;
    return index == otherV.index && fnType.equals(otherV.fnType);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), this);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    VtableAccessExpression val = new VtableAccessExpression(lineNum, columnNum, preComments,
        postComments, index, (FunctionType) fnType.replaceType(mapFromTo));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    VtableAccessExpression val = new VtableAccessExpression(lineNum, columnNum, preComments,
        postComments, index,
        (FunctionType) fnType
            .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(val));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return fnType.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return this;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    // this ast is created AFTER this transform runs
    throw new UnsupportedOperationException();
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
