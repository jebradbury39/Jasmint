package interp;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import type_env.TypeEnvironment;
import errors.FatalMessageException;
import typecheck.FunctionType;
import typecheck.Type;
import typecheck.UserInstanceType;
import typegraph.TypeGraph;
import util.Pair;

public class MapFunctionToValue implements IdToValue {
  private List<ValueBox> overloads = new LinkedList<ValueBox>();
  
  @Override
  public IdToValue write(String name, ValueBox valBox) {
    overloads.add(valBox);
    return this;
  }

  @Override
  public Pair<ValueBox, Integer> read(Environment env, String name,
      FunctionType functionType) throws InterpError, FatalMessageException {
    if (overloads.isEmpty()) {
      return null;
    }
    if (functionType == null) {
      if (overloads.isEmpty()) {
        return new Pair<ValueBox, Integer>(null, 0);
      }
      //this only comes from assigning. So you get the most recently defined overload
      return new Pair<ValueBox, Integer>(overloads.get(overloads.size() - 1), 0);
      //throw new InterpError("cannot access function [" + name
      //+ "] without giving argument types", -1, -1);
    }
    return lookupFn(env.typeGraph, name, functionType.argTypes, null, env.globalTenv);
  }
  
  /* accounts for overloads 
   * if parentType is not null, require that functions match this
   * parentType with their parentCall.parentType
   */
  public Pair<ValueBox, Integer> lookupFn(TypeGraph typeGraph, String name,
      List<Type> argTypes, UserInstanceType parentType, TypeEnvironment tenv) throws InterpError, FatalMessageException {
    ValueBox bestOption = null;
    int bestOptionNumMatched = -1;
    
    for (ValueBox box : overloads) {
      final Value val = box.getValue().copy(null); //just looking
      if (val == null) {
        continue;
      }

      final Type ty = val.getType();
      if (!(ty instanceof FunctionType) || !(val instanceof ClosureValue)) {
        throw new InterpError(name + " is not a function, but is of type [" + ty + "]", -1, -1);
      }

      if (parentType != null) {
        ClosureValue cloVal = (ClosureValue) val;
        if (cloVal.forClass == null) {
          continue;
        }
        if (!cloVal.forClass.equals(parentType)) {
          continue;
        }
      }

      FunctionType fnTy = (FunctionType)ty;
      if (fnTy.argTypes.size() != argTypes.size()) {
        /* one of the other overloads may have the right # of args */
        continue;
      }
      boolean matched = true;
      int numMatched = 0;
      Iterator<Type> iterInner = fnTy.argTypes.iterator();
      Iterator<Type> iterArgs = argTypes.iterator();
      for (; iterInner.hasNext() && iterArgs.hasNext();) {
        Type innerTy = iterInner.next();
        Type argTy = iterArgs.next();

        if (!tenv.canSafeCast(argTy, innerTy)) {
          matched = false;
          break;
        }

        if (argTy.equals(innerTy)) {
          numMatched++;
        }
      }
      if (!matched) {
        // keep looking for overloads
        continue;
      }
      if (numMatched > bestOptionNumMatched) {
        bestOption = box;
        bestOptionNumMatched = numMatched;
      }
    }

    return new Pair<ValueBox, Integer>(bestOption, bestOptionNumMatched);
  }

  @Override
  public IdToValue fullCopy(String name, Environment copyEnv) throws InterpError {
    MapFunctionToValue copy = new MapFunctionToValue();
    
    for (ValueBox overload : overloads) {
      copy.write(name, overload.copy(copyEnv));
    }
    
    return copy;
  }

  @Override
  public IdToValue replaceType(String name, Map<Type, Type> mapFromTo,
      String moduleAppend) throws InterpError {
    MapFunctionToValue copy = new MapFunctionToValue();
    
    for (ValueBox overload : overloads) {
      copy.write(name, overload.replaceType(mapFromTo, moduleAppend));
    }
    
    return copy;
  }
}
