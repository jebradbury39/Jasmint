package test_types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import errors.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import typecheck.ArrayType;
import typecheck.BoolType;
import typecheck.CharType;
import typecheck.ClassType;
import typecheck.Float32Type;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.IntType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.VoidType;

public class TestTypeSystem {

  @Test
  public void testHashing() {
    Map<Type, Type> map = new HashMap<>();

    map.put(new ClassType(Nullable.empty(), "V", new LinkedList<>()), StringType.Create());
    assertEquals(StringType.Create(),
        map.get(new ClassType(Nullable.empty(), "V", new LinkedList<>())));
  }

  @Test
  public void testTypeEquality() {
    assertTrue(VoidType.Create().equals(VoidType.Create()));
    assertTrue(BoolType.Create().equals(BoolType.Create()));
    assertTrue(CharType.Create().equals(CharType.Create()));
    assertTrue(IntType.Create(true, 32).equals(IntType.Create(true, 32)));
    assertTrue(Float32Type.Create().equals(Float32Type.Create()));
    assertTrue(StringType.Create().equals(StringType.Create()));
    assertTrue(
        new ArrayType(IntType.Create(true, 32)).equals(new ArrayType(IntType.Create(true, 32))));
    assertTrue(new MapType(StringType.Create(), IntType.Create(true, 32))
        .equals(new MapType(StringType.Create(), IntType.Create(true, 32))));
    assertTrue(new FunctionType(new ModuleType(Nullable.empty(), "a"), VoidType.Create(),
        new ArrayList<Type>(), LambdaStatus.NOT_LAMBDA)
            .equals(new FunctionType(new ModuleType(Nullable.empty(), "a"), VoidType.Create(),
                new ArrayList<Type>(), LambdaStatus.NOT_LAMBDA)));

    List<Type> p1Ty = new ArrayList<Type>();
    List<Type> p2Ty = new ArrayList<Type>();
    p1Ty.add(IntType.Create(true, 32));
    p2Ty.add(IntType.Create(true, 32));
    assertTrue(new FunctionType(new ModuleType(Nullable.empty(), "a"), VoidType.Create(), p1Ty,
        LambdaStatus.NOT_LAMBDA)
            .equals(new FunctionType(new ModuleType(Nullable.empty(), "a"), VoidType.Create(), p2Ty,
                LambdaStatus.NOT_LAMBDA)));

    assertTrue(new ClassType(Nullable.empty(), "TestClass", p1Ty)
        .equals(new ClassType(Nullable.empty(), "TestClass", p2Ty)));
  }

}
