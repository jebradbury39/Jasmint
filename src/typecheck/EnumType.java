package typecheck;

import ast.Expression;
import ast.Program.DestructorRenamings;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.EnumHeader;
import header.PsuedoClassHeader;
import import_mgmt.EffectiveImport.ImportUsage;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.EnumTypeBox;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;

public class EnumType extends UserInstanceType {

  public EnumType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.ENUM_TYPE, outerType, name);
  }
  
  @Override
  public boolean equals(Object other) {

    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof EnumType)) {
      return false;
    }

    EnumType otherUT = (EnumType) other;
    if (!name.equals(otherUT.name)) {
      return false;
    }

    return outerType.equals(otherUT.outerType);
  }
  
  @Override
  public int hashCode() {
    return super.hashCode() + (outerType.isNull() ? 0 : outerType.get().hashCode())
        + name.hashCode();
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return Optional.empty();
  }

  @Override
  public List<Type> getInnerTypes() {
    return new LinkedList<>();
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type tmp = mapFromTo.get(this);
    if (tmp == null) {
      // failed to match, so now try as decl
      EnumDeclType declTy = (EnumDeclType) asDeclType();
      tmp = mapFromTo.get(declTy);
      if (tmp != null) {
        declTy =  (EnumDeclType) tmp; 
        return declTy.asInstanceType();
      }
      return this;
    }
    return tmp;
  }

  @Override
  public Builder serialize() {
    AstProto.EnumType.Builder subDoc = AstProto.EnumType.newBuilder();
    if (outerType.isNotNull()) {
      subDoc.addOuterType(outerType.get().serialize());
    }
    subDoc.setName(name);

    AstProto.Type.Builder document = preSerialize();
    document.setEnumType(subDoc);
    return document;
  }
  
  public static EnumType deserialize(AstProto.EnumType document) throws SerializationError {
    Nullable<ModuleType> outerType = Nullable.empty();
    if (document.getOuterTypeCount() != 0) {
      outerType = Nullable.of((ModuleType) AbstractType.deserialize(document.getOuterType(0)));
    }

    return new EnumType(outerType, document.getName());
  }

  @Override
  public Type setConst(boolean isConst) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(this);
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    stackTypes.add(asDeclType());
    return stackTypes;
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(asDeclType(), lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(tyBox.get() instanceof EnumTypeBox)) {
      return Optional.empty();
    }
    EnumTypeBox enumTyBox = (EnumTypeBox) tyBox.get();
    return Optional.of(enumTyBox.enumHeader.absName.asInstanceType());
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return this;
  }
  
  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException {
    
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(asDeclType(), lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return;
    }
    if (!(tyBox.get() instanceof EnumTypeBox)) {
      return;
    }
    EnumTypeBox enumTyBox = (EnumTypeBox) tyBox.get();
    EnumHeader enumHeader = enumTyBox.enumHeader;
    
    ourModNode.populateUplinks(modDepGraph, tenv.effectiveImports, enumHeader.moduleType,
        encapTypeIsRef ? ImportUsage.SOFT : ImportUsage.HARD);
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    // normalize our name
    Optional<Nullable<Type>> normDecl = asDeclType().basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!normDecl.isPresent()) {
      return Optional.empty();
    }
    EnumDeclType normEnumDecl = (EnumDeclType) normDecl.get().get();
    
    return Optional.of(Nullable.of(new EnumType(normEnumDecl.outerType, normEnumDecl.name)));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState)
      throws FatalMessageException {
    PsuedoClassHeader pch = project.lookupClassHeader(asDeclType(), Nullable.empty(), true,
        project.getAuditTrail());
    return pch.getDummyExpression(this, msgState);
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return asDeclType().calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public List<String> toList() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public UserDeclType asDeclType() {
    return new EnumDeclType(outerType, name);
  }

  @Override
  public Type asClassDeclType() {
    return asDeclType();
  }

  @Override
  public ModuleType getModuleType() {
    return outerType.get();
  }

  @Override
  public UserDeclType asUserDeclType() {
    return asDeclType();
  }

}
