package type_env;

import ast.DotExpression;
import ast.Expression;
import ast.IdentifierExpression;
import callgraph.CallGraph;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ProgHeader;
import header.PsuedoClassHeader;
import header.RegisteredGenerics;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import typecheck.ArrayType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.DotAccessType;
import typecheck.EnumDeclType;
import typecheck.EnumType;
import typecheck.Float64Type;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ImportType;
import typecheck.MapType;
import typecheck.ModuleImportType;
import typecheck.ModuleType;
import typecheck.NullType;
import typecheck.ReferenceType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UndeterminedImportType;
import typecheck.UndeterminedUserDeclType;
import typecheck.UndeterminedUserInstanceType;
import typecheck.UserDeclType;
import typecheck.VoidType;
import typegraph.TypeGraph;
import typegraph.TypeGraph.CastDirection;
import util.Pair;

/*
 * A Treatise on Scopes
 * By John Bradbury
 * 
 * Honestly, this talk about scoping has gone on way too long. It's more overdone than superhero
 * movies and online tracking. Here's the breakdown:
 * 
 * GLOBAL - This is THE one and only top level scope for the entire file.
 *    Everyone can see this scope (up the parent links) and modify variables in it,
 *    but no one can take a
 *    deep copy of the scope, they can only reference it as a parent.
 *    This means that top level functions
 *    do NOT take a closure of the global scope. No, no, no, no, NO!
 *    This scope is not recorded as being
 *    "of" any class, although it does track which module it belongs to (currentModule).
 * 
 * NORMAL - This is what you get from ENV.extend(), every time.
 * Functions can take a closure (deep copy) on this.
 *    This is the scope that gets created by blocks.
 *    This scope is not recorded as being "of" any class.
 *    
 * STATIC - This scope contains ONLY a single class's static variables,
 *    of all visibilities (public/protected/private).
 *    This is also an effectively global scope. You access this by finding the class name
 *    in the GLOBAL scope,
 *    which then links to one of these scopes (e.g. class_name.x).
 *    You can also access this as the parent of
 *    an INSTANCE scope (e.g. class_instance.x). These are NEVER deep copied,
 *    only referenced as a parent.
 *    This scope IS recorded as being "of" a class.
 *    
 * GENERIC - This scope exists solely to contain the generic typenames
 *    (e.g. class A ?{T}, this scope contains T).
 *    This is only used in type-checking and is placed right above
 *    INSTANCE (STATIC -> GENERIC -> INSTANCE).
 *    After the class definition is type-checked, this scope is destroyed and the
 *    STATIC -> INSTANCE link is restored.
 *    
 * INSTANCE - This scope contains the fields/members of a single class instance.
 *    These are also NEVER deep
 *    copied, only referenced as the parent of a NORMAL scope. There is one exception,
 *    and that of course is when you assign the value to another memory location via a copy.
 *    Again, this contains all visibilities.
 *    
 * Hopefully, this clears up some confusion, but to further shine more light on the matter,
 * here are some
 * examples.
 * 
 * Notice how the child case interleaves the STATIC and INSTANCE scopes.
 * Since A.x can only be static
 * or instance, the order technically does not matter. However, if B also defines an x,
 * then we must find B.x
 * before A.x if x is used in B. Of course, explicitly saying A.x will still get the
 * correct x since first A
 * is resolved up in the GLOBAL scope to the class definition,
 * which contains the static scope for A.
 * 
 * Function closure:
 *    GLOBAL -> NORMAL (empty env since top level function) -> NORMAL (contains params:args)
 * Anon function closure:
 *    GLOBAL -> NORMAL (deep copy, contains variables of surrounding scope)
 * Class function closure:
 *    GLOBAL -> STATIC -> INSTANCE -> NORMAL (deep copy, contains variables of surrounding scope)
 *      -> NORMAL (contains params:args)
 * Static class function closure:
 *    GLOBAL -> STATIC -> NORMAL (empty env since this is also top level)
 * Child class (B extends A) instance:
 *    GLOBAL -> STATIC (A) -> INSTANCE (A) -> STATIC (B) -> INSTANCE (B)
 * Child class static:
 *    GLOBAL -> STATIC (A) -> STATIC (B)
 * Child class function closure:
 *    GLOBAL -> STATIC (A) -> INSTANCE (A) -> STATIC (B) -> INSTANCE (B)
 *      -> NORMAL (empty) -> NORMAL (contains params:args)
 * Static child class closure: GLOBAL -> STATIC (A) -> STATIC (B) -> NORMAL
 * 
 * To create an INSTANCE, run through members with new GLOBAL -> STATIC -> INSTANCE scoping.
 * This will automatically populate the INSTANCE scope. Then run INSTANCE.replaceType
 * (which must not affect parent scopes)
 * Class definition: GLOBAL[classname] == (class def info), STATIC
 * 
 * EDIT (4/24/2019):
 * We no longer require this really awful hack that just does not scale to multiple files well.
 * For example, if we had a class A which extends std.String,
 * then std.String_static would have to extend A_static,
 * which would mess up the imports and cause a circular dependency,
 * which would kill the typechecker...all because
 * we wanted to support <class_instance>.<static_field/function> syntax.
 * Well, not worth. At any rate, it makes
 * more semantic sense to require that static fields are referred to by their full path
 * from the class declaration.
 * For example, A.static_field. For those that may complain that this is too verbose,
 * I refer them to coding styles
 * which mandate a special prefix for static variables, then another special prefix
 * for regular member variables,
 * but don't forget the special prefix for pointers, and globals, ect...
 * It forces additional readability and
 * avoids some side-effect footguns quite nicely.
 * Furthermore, it may remind devs that static variables should be
 * used sparingly, as they are effectively globals, and globals are not easy to parallelize.
 * 
 * These are the changed scenarios which prohibit instances from accessing statics
 * directly (must use full path):
 * Child class (B extends A) instance:
 *    GLOBAL -> INSTANCE (A) -> INSTANCE (B)
 * Child class function closure:
 *    GLOBAL -> INSTANCE (A) -> INSTANCE (B) -> NORMAL (empty) -> NORMAL (contains params:args)
 */

public class TypeEnvironment {
  public static enum TypeStatus {
    COMPLETE,     //no generics, and class is fully defined (can be used on the stack)
    GENERIC,      //contains generics, but is complete
    INCOMPLETE,   //no generics, but the class is not fully defined (can be used only as ptr)
    INCOMPLETE_AND_GENERIC,   //contains inner generics and/or incomplete, may be incomplete itself
    UNDEFINED;    //this type does not exist
    
    public boolean isComplete() {
      return this == COMPLETE || this == GENERIC;
    }
    
    public boolean isIncomplete() {
      return this == INCOMPLETE || this == INCOMPLETE_AND_GENERIC;
    }
    
    public boolean isGeneric() {
      return this == GENERIC || this == INCOMPLETE_AND_GENERIC;
    }
    
    public TypeStatus combine(TypeStatus other) {
      if (this == other) {
        return this;
      }
      if (this == UNDEFINED || other == UNDEFINED) {
        return UNDEFINED;
      }
      if ((this.isIncomplete() && other.isGeneric())
          || (other.isIncomplete() && this.isGeneric())) {
        return INCOMPLETE_AND_GENERIC;
      }
      //this or other may be GENERIC, but in the complete sense
      if ((this.isComplete() && other.isIncomplete())
          || this.isIncomplete() && other.isComplete()) {
        return INCOMPLETE;
      }
      if (this == INCOMPLETE_AND_GENERIC || other == INCOMPLETE_AND_GENERIC) {
        throw new IllegalArgumentException("Internal error: failed logic to combine TypeStatus "
            + this + " with " + other);
      }
      if (this.isComplete() && other == GENERIC
          || other.isComplete() && this == GENERIC) {
        return GENERIC;
      }
      return COMPLETE;
    }
    
    public TypeStatus removeIncomplete() {
      switch (this) {
        case COMPLETE:
        case GENERIC:
        case UNDEFINED:
          return this;
        case INCOMPLETE:
          return COMPLETE;
        case INCOMPLETE_AND_GENERIC:
          return GENERIC;
        default:
          throw new IllegalArgumentException();
      }
    }
  }
  
  //applies to both tenv and env
  public static enum ScopeType {
    GLOBAL,
    NORMAL,
    STATIC,
    GENERIC,
    INSTANCE
  }
  
  public ScopeType envType;
  /* 
   * may be null. Is set to normalized class name (with full mod pathing).
   */
  public DotAccessType ofDeclType; //module/class/enum
  
  private Nullable<TypeEnvironment> parent = Nullable.empty();
  public final EffectiveImports effectiveImports;
  
  // contains both class/enum/module types and variable names
  private Map<String, IdToType> identifiers = new HashMap<>();
  
  // contains only class/enum/module types
  private Nullable<TypeEnvironment> typeTenv = Nullable.empty();
      
  public final TypeGraph typeGraph;
  public final CallGraph callGraph;
  public final Project project;

  public final MsgState msgState;
  
  //only non-null if global and very first typecheck for this module
  private Nullable<RegisteredGenerics> registeredGenerics = Nullable.empty();
  
  public static class SandboxModeInfo {
    public final int lineNum;
    public final int columnNum;
    
    public SandboxModeInfo(int lineNum, int columnNum) {
      this.lineNum = lineNum;
      this.columnNum = columnNum;
    }
    
    @Override
    public String toString() {
      return "<line " + lineNum + " column " + columnNum + ">";
    }
  }
  
  private Nullable<SandboxModeInfo> sandboxMode = Nullable.empty(); //enforced
  
  public void addBuiltins(ModuleType modType) throws FatalMessageException {
    //add the print function
    List<Type> printParam = new ArrayList<>();
    printParam.add(StringType.Create());
    
    defineVar("print", new BasicTypeBox(
        new FunctionType(modType, VoidType.Create(), printParam,
            LambdaStatus.NOT_LAMBDA), true,
        true, VariableScoping.GLOBAL));
    
    //add the currentUnixEpochTime function
    defineVar("currentUnixEpochTime", new BasicTypeBox(
        new FunctionType(modType, Float64Type.Create(), new LinkedList<>(),
            LambdaStatus.NOT_LAMBDA), true,
        true, VariableScoping.GLOBAL));
  }
  
  public static TypeEnvironment createEmptyUserTypeEnv(Project project, DotAccessType userType,
      TypeGraph typeGraph, EffectiveImports effectiveImports, MsgState msgState)
          throws FatalMessageException {
    TypeEnvironment tenv = new TypeEnvironment(ScopeType.GLOBAL, userType,
        typeGraph, project.callGraph,
        project, effectiveImports, msgState);
    return tenv;
  }
  
  public TypeEnvironment(ScopeType envType, DotAccessType ofDeclType, TypeGraph typeGraph, 
      CallGraph callGraph, Project project, EffectiveImports effectiveImports,
      MsgState msgState) {
    this.envType = envType;
    this.ofDeclType = ofDeclType;
    this.typeGraph = typeGraph;
    this.callGraph = callGraph;
    this.project = project;
    this.msgState = msgState;
    this.effectiveImports = effectiveImports;
  }
  
  public Nullable<TypeEnvironment> getTypeTenv() {
    return typeTenv;
  }
  
  public void setTypeTenv(TypeEnvironment tenv) {
    typeTenv = Nullable.of(tenv);
  }
  
  @Override
  public String toString() {
    String result = "[" + envType;
    result += " of " + ofDeclType + "\n";
    for (Entry<String, IdToType> entry : identifiers.entrySet()) {
      result += entry.getKey() + " = " + entry.getValue() + "\n";
    }
    
    if (parent.isNotNull()) {
      result += "\n(extends)->\n" + parent;
    }
    
    return result;
  }
  
  public TypeEnvironment extend() throws FatalMessageException {
    return extend(ofDeclType);
  }
  
  //should be equivalent to deepCopy, switch to this
  public TypeEnvironment extend(DotAccessType newOfDeclType) throws FatalMessageException {
    TypeEnvironment newTenv = new TypeEnvironment(ScopeType.NORMAL, newOfDeclType, typeGraph,
        callGraph, project, effectiveImports, msgState);
    newTenv.setParent(Nullable.of(this));
    newTenv.registeredGenerics = registeredGenerics;
    return newTenv;
  }
  
  //parent manipulation
  public void setParent(Nullable<TypeEnvironment> parent) {
    this.parent = parent;
  }
  
  //actually copy all the memory in the environment (but not in the parent)
  public TypeEnvironment fullCopy() throws FatalMessageException {
    Map<String, IdToType> newIds = new HashMap<>();
    for (Entry<String, IdToType> id : identifiers.entrySet()) {
      newIds.put(id.getKey(), id.getValue().fullCopy(id.getKey()));
    }
    
    TypeEnvironment copy = new TypeEnvironment(envType,
        ofDeclType, typeGraph, callGraph, project, effectiveImports, msgState);
    copy.identifiers = newIds;
    copy.setParent(parent);
    return copy;
  }
  
  public TypeEnvironment copyOnlyTypes() {
    if (typeTenv.isNotNull()) {
      return typeTenv.get();
    }
    
    Map<String, IdToType> newIds = new HashMap<>();
    for (Entry<String, IdToType> id : identifiers.entrySet()) {
      if (!(id.getValue() instanceof MapIdToType)) {
        continue;
      }
      MapIdToType val = (MapIdToType) id.getValue();
      if (!(val.read() instanceof UserDeclTypeBox)) {
        continue;
      }
      newIds.put(id.getKey(), id.getValue());
    }
    
    TypeEnvironment copy = new TypeEnvironment(envType,
        ofDeclType, typeGraph, callGraph, project, effectiveImports, msgState);
    copy.identifiers = newIds;
    if (parent.isNotNull()) {
      copy.setParent(Nullable.of(parent.get().copyOnlyTypes()));
    }
    return copy;
  }
  
  //define a variable
  public TypeBox defineVar(String name, TypeBox tyBox) {
    
    //only search our identifiers (not parents')
    //the parent can never access our overloads
    IdToType existing = identifiers.get(name);
    if (existing == null) {
      existing = new MapIdToType();
      if (tyBox instanceof BasicTypeBox) {
        BasicTypeBox basicTyBox = (BasicTypeBox) tyBox;
        if (basicTyBox.getValue() instanceof FunctionType) {
          existing = new MapFunctionToType();
        }
      }
      
      identifiers.put(name, existing);
    }
    
    existing.write(name, tyBox);
    
    return tyBox;
  }
  
  private void defineVar(String name, IdToType entry) {
    identifiers.put(name, entry);
  }
  
  // returns the leaf tenv
  public Optional<TypeEnvironment> defineUserType(UserDeclType userType, UserDeclType absUserType,
      Nullable<UserDeclTypeBox> finalTyBox)
      throws FatalMessageException {
    
    if (userType instanceof UndeterminedImportType || absUserType instanceof UndeterminedImportType) {
      throw new IllegalArgumentException();
    }
    if (userType instanceof UndeterminedUserDeclType || absUserType instanceof UndeterminedUserDeclType) {
      throw new IllegalArgumentException();
    }
    
    // (e.g. str vs std.str, or X vs std.str.X)
    final boolean addRelativeType = (userType.outerType.isNull() && !userType.equals(absUserType));
    
    Iterator<String> pathIter = absUserType.toList().iterator();
    TypeEnvironment addToTenv = this;
    Nullable<ModuleType> prefixModType = Nullable.empty();

    while (pathIter.hasNext()) {
      String name = pathIter.next();
      
      boolean importModule = true;
      if (!pathIter.hasNext()) {
        if (userType instanceof ImportType && !(userType instanceof ModuleImportType)) {
          importModule = false;
        } else if (userType instanceof ClassDeclType || userType instanceof EnumDeclType) {
          importModule = false;
        }
      }
        
      if (importModule) {
        prefixModType = Nullable.of(new ModuleType(prefixModType, name));
      }
      
      // first try to lookup the item in context tenv
      Nullable<TypeBox> modTypeBox = addToTenv.lookup(name, -1, -1);
      if (modTypeBox.isNotNull()) {
        // if we find the item, then we only define it if we have a new box (and if at the end)
        if (modTypeBox.get() instanceof ModuleTypeBox) {
          if (!pathIter.hasNext() && finalTyBox.isNotNull()) {
            // we already have the type box for the last item (overwrite what is there)
            addToTenv.defineVar(name, finalTyBox.get());
            continue;
          } else {
            addToTenv = ((ModuleTypeBox) modTypeBox.get()).tenv;
          }
        } else {
          if (pathIter.hasNext()) {
            msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
                "Found a non-module, but there is still more in the import path: "
                    + absUserType);
            return Optional.empty();
          }
        }
        
        if (addRelativeType && !pathIter.hasNext()) {
          this.defineVar(name, modTypeBox.get());
        }
        
        if (!pathIter.hasNext() && finalTyBox.isNotNull()) {
          // we already have the type box for the last item (overwrite what is there)
          addToTenv.defineVar(name, finalTyBox.get());
        }
        
        continue;
      }
      
      if (!pathIter.hasNext() && finalTyBox.isNotNull()) {
        // we already have the type box for the last item (add since it is missing)
        addToTenv.defineVar(name, finalTyBox.get());
        continue;
      }
      
      // not in the tenv yet, so lookup module from the project
      Nullable<ModuleEndpoint> prefixModEp = project.lookupModule(prefixModType.get(),
          project.getAuditTrail());
      if (prefixModEp.isNull()) {
        // not a valid module, and since we deal with resolved user types only, this is an error
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "did not find module: " + prefixModType.get());
        return Optional.empty();
      }
      
      prefixModEp.get().load();
      LoadedModuleHandle prefixModuleHandle = (LoadedModuleHandle) prefixModEp.get().getHandle();
      
      ProgHeader modHeader = prefixModuleHandle.moduleBuildObj.progHeader.applyTransforms(
          project, project.getAuditTrail(), prefixModuleHandle.moduleBuildObj,
          prefixModuleHandle.registeredGenerics, msgState);
      
      if (!importModule) {
        Nullable<PsuedoClassHeader> pch = modHeader.getClassHeader(name);
        if (pch.isNull()) {
          msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "did not find class " + name + " in module " + modHeader.moduleType);
          return Optional.empty();
        }
        
        // found the class, ensure that it is the last item
        if (pathIter.hasNext()) {
          msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "Cannot import anything below class level: " + absUserType);
          return Optional.empty();
        }
        
        if (addRelativeType && !pathIter.hasNext()) {
          pch.get().defineInTenv(this);
        } else if (!addRelativeType) {
          pch.get().defineInTenv(addToTenv);
        }
        
        continue;
      }
      
      // we found the module from the project, so create the tenv for the module
      
      // if we are not the last module in the path, then do not add all the submods
      Optional<TypeEnvironment> modTenv = modHeader.createTenv(project, typeGraph,
          msgState, true,
          !pathIter.hasNext(), false);
      
      ModuleTypeBox newModTyBox = new ModuleTypeBox(modHeader.moduleType, true, modTenv.get());

      if (addRelativeType && !pathIter.hasNext()) {
        this.defineVar(name, newModTyBox);
      } else if (!addRelativeType) {
        addToTenv.defineVar(name, newModTyBox);
      }
      addToTenv = modTenv.get();
    }
    
    return Optional.of(addToTenv);
  }
  
  //lookup a variable
  public Nullable<TypeBox> lookup(String name, int lineNum, int columnNum)
      throws FatalMessageException {
    return lookup(name, false, lineNum, columnNum);
  }
  
  public Nullable<TypeBox> lookup(String name, boolean ignoreExport, int lineNum, int columnNum)
      throws FatalMessageException {
    return ourLookup(name, null, ignoreExport, lineNum, columnNum).a;
  }
  
  //only use original when looking up for overrides (vtable construction)
  public Nullable<TypeBox> lookup(String name, FunctionType functionType,
      int lineNum, int columnNum) throws FatalMessageException {
    return ourLookup(name, functionType, false, lineNum, columnNum).a;
  }
  
  //only for functions, and really only for constructors after name-mangling
  //must provide a list of names (candidate functions)
  public Nullable<TypeBox> lookup(List<String> names, FunctionType functionType,
      int lineNum, int columnNum) throws FatalMessageException {
    Pair<Nullable<TypeBox>, Integer> match = new Pair<>(Nullable.empty(), -1);
    for (String name : names) {
      Pair<Nullable<TypeBox>, Integer> tmp = ourLookup(name, functionType, false,
          lineNum, columnNum);
      if (match.b < tmp.b) {
        match = tmp;
      }
    }
    return match.a;
  }
  
  private Pair<Nullable<TypeBox>, Integer> ourLookup(String name, FunctionType functionType,
      boolean ignoreExport, int lineNum, int columnNum) throws FatalMessageException {
    Pair<Nullable<TypeBox>, Integer> result = new Pair<>(Nullable.empty(), -1);
    
    if (identifiers.containsKey(name)) {
      result = identifiers.get(name).read(this, name, functionType, ignoreExport,
          lineNum, columnNum);
    }
    
    if (result.a.isNotNull()) {
      //found something
      if (result.b == -1) {
        //matched an id, not a function. Do not check parent
        return result;
      }
    }
    
    //either no identifier match or is a function and we need to check for a better match
    if (parent.isNotNull()) {
      Pair<Nullable<TypeBox>, Integer> parentResult = parent.get().ourLookup(name, functionType,
          ignoreExport, lineNum, columnNum);
      if (result.a.isNull()) {
        //didn't find anything in our scope, so just take what parent found
        result = parentResult;
      } else if (parentResult.a.isNotNull()) {
        //else, make sure parent's stuff is better than ours
        if (parentResult.b > result.b) {
          result = parentResult;
        }
      }
    }
    return result;
  }
  
  private Nullable<IdToType> internalIdentifierLookup(String name) {
    if (identifiers.containsKey(name)) {
      return Nullable.of(identifiers.get(name));
    }
    if (parent.isNotNull()) {
      return parent.get().internalIdentifierLookup(name);
    }
    return Nullable.empty();
  }
  
  //lookup what scoping a variable is in (global, class instance, normal)
  public VariableScoping lookupScoping(String name, FunctionType functionType)
      throws FatalMessageException {
    IdToType entry = identifiers.get(name);
    if (entry != null) {
      Nullable<TypeBox> tmp = entry.read(this, name, functionType, false, -1, -1).a;
      if (tmp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to find " + name + " in order to find its scoping, even though it is in IDs");
      }
      return tmp.get().getScoping();
    }
    
    /* then check in parent class env */
    if (parent.isNotNull()) {
      return parent.get().lookupScoping(name, functionType);
    }
    return VariableScoping.NORMAL;
  }
  
  public Optional<UserDeclTypeBox> lookupUserDeclType(UserDeclType userDeclType,
      int lineNum, int columnNum) throws FatalMessageException {
    return lookupUserDeclType(userDeclType, false, lineNum, columnNum);
  }
  
  public Optional<UserDeclTypeBox> lookupUserDeclType(UserDeclType userDeclType,
      boolean ignoreExport,
      int lineNum, int columnNum) throws FatalMessageException {
    TypeEnvironment tenv = this;
    if (typeTenv.isNotNull()) {
      tenv = typeTenv.get();
    }
    
    Iterator<String> idIter = userDeclType.toList().iterator();
    UserDeclTypeBox userTypeBox = null;
    String prefix = "";
    while (idIter.hasNext()) {
      final String name = idIter.next();
      Nullable<TypeBox> typeBox = tenv.lookup(name, ignoreExport, lineNum, columnNum);
      if (typeBox.isNull()) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, name + " not found " + prefix,
            lineNum, columnNum);
        return Optional.empty();
      }
      if (!(typeBox.instanceOf(UserDeclTypeBox.class))) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "expected user type declaration for "
            + name + " but found: " + typeBox, lineNum, columnNum);
        return Optional.empty();
      }
      userTypeBox = (UserDeclTypeBox) typeBox.get();
      Optional<TypeEnvironment> tmpTenv = userTypeBox.getTenv(this);
      if (!tmpTenv.isPresent()) {
        return Optional.empty();
      }
      tenv = tmpTenv.get();
      if (!prefix.isEmpty()) {
        prefix += ".";
      } else {
        prefix += " in ";
      }
      prefix += name;
    }
    if (userTypeBox == null) {
      return Optional.empty();
    }
    return Optional.of(userTypeBox);
  }
  
  /*
   * This does check the parent (all lookups check the parent)
   */
  public Optional<TypeStatus> lookupType(Type type, int lineNum, int columnNum)
      throws FatalMessageException {
    return lookupType(type, false, lineNum, columnNum);
  }
  
  public Optional<TypeStatus> lookupType(Type type, boolean ignoreExport,
      int lineNum, int columnNum)
      throws FatalMessageException {
    TypeStatus mainStatus = TypeStatus.UNDEFINED;
    Optional<TypeStatus> tmpTypeStatus = Optional.empty();
    Optional<PsuedoClassHeader> optClassHeader = Optional.empty();
    Optional<UserDeclTypeBox> optUserTypeBox = Optional.empty();
    
    switch (type.getTypeType()) {
      case BOOL_TYPE:
      case CHAR_TYPE:
      case FLOAT32_TYPE:
      case FLOAT64_TYPE:
      case INT_TYPE:
      case STRING_TYPE:
      case VOID_TYPE:
      case NULL_TYPE:
        return Optional.of(TypeStatus.COMPLETE);
      case ARRAY_TYPE:
        return lookupType(((ArrayType) type).elementType, ignoreExport, lineNum, columnNum);
      case MAP_TYPE:
        MapType mapTy = (MapType) type;
        Optional<TypeStatus> keyStatus = lookupType(mapTy.keyType, ignoreExport, lineNum, columnNum);
        if (!keyStatus.isPresent()) {
          return Optional.empty();
        }
        if (keyStatus.get() == TypeStatus.UNDEFINED) {
          return Optional.of(keyStatus.get());
        }
        Optional<TypeStatus> valStatus = lookupType(mapTy.valueType, ignoreExport, lineNum, columnNum);
        if (!valStatus.isPresent()) {
          return Optional.empty();
        }
        return Optional.of(valStatus.get().combine(keyStatus.get()));
      case FUNCTION_TYPE:
        FunctionType fnTy = (FunctionType) type;
        Optional<TypeStatus> retStatus = lookupType(fnTy.returnType, ignoreExport, lineNum, columnNum);
        if (!retStatus.isPresent()) {
          return Optional.empty();
        }
        if (retStatus.get() == TypeStatus.UNDEFINED) {
          return Optional.of(retStatus.get());
        }
        for (Type argTy : fnTy.argTypes) {
          Optional<TypeStatus> argStatus = lookupType(argTy, ignoreExport, lineNum, columnNum);
          if (!argStatus.isPresent()) {
            return Optional.empty();
          }
          if (argStatus.get() != TypeStatus.COMPLETE) {
            return Optional.of(argStatus.get().combine(retStatus.get()));
          }
        }
        //all args were complete
        return retStatus;
      case REFERENCE_TYPE:
        tmpTypeStatus = lookupType(((ReferenceType) type).innerType, ignoreExport, lineNum, columnNum);
        if (!tmpTypeStatus.isPresent()) {
          return tmpTypeStatus;
        }
        //if a pointer, don't care about completeness
        return Optional.of(tmpTypeStatus.get().removeIncomplete());
      case CLASS_DECL_TYPE:
        optUserTypeBox = lookupUserDeclType(
            (ClassDeclType) type, ignoreExport, lineNum, columnNum);
        if (!optUserTypeBox.isPresent()) {
          return Optional.empty();
        }
        
        if (optUserTypeBox.get() instanceof ClassTypeBox) {
          ClassTypeBox tyBox = (ClassTypeBox) optUserTypeBox.get();
          mainStatus = TypeStatus.INCOMPLETE;
          if (project.lookupClassIsComplete(tyBox.classHeader.getAbsName())) {
            mainStatus = TypeStatus.COMPLETE;
          }
          return Optional.of(mainStatus);
        }
        
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "not a class: " + type,
            lineNum, columnNum);
        return Optional.empty();
      case ENUM_DECL_TYPE:
        optUserTypeBox = lookupUserDeclType(
            (EnumDeclType) type, ignoreExport, lineNum, columnNum);
        if (!optUserTypeBox.isPresent()) {
          return Optional.empty();
        }
        
        if (optUserTypeBox.get() instanceof EnumTypeBox) {
          return Optional.of(TypeStatus.COMPLETE);
        }
        
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "not an enum: " + type,
            lineNum, columnNum);
        return Optional.empty();
      case UNDETERMINED_USER_DECL_TYPE:
        optUserTypeBox = lookupUserDeclType(
            (UndeterminedUserDeclType) type, ignoreExport, lineNum, columnNum);
        if (!optUserTypeBox.isPresent()) {
          return Optional.empty();
        }
        
        if (optUserTypeBox.get() instanceof ModuleTypeBox) {
          return Optional.of(TypeStatus.COMPLETE);
        } else if (optUserTypeBox.get() instanceof ClassTypeBox) {
          mainStatus = TypeStatus.INCOMPLETE;
          if (project.lookupClassIsComplete(((ClassTypeBox) optUserTypeBox.get()).classHeader.getAbsName())) {
            mainStatus = TypeStatus.COMPLETE;
          }
          return Optional.of(mainStatus);
        } else if (optUserTypeBox.get() instanceof EnumTypeBox) {
          return Optional.of(TypeStatus.COMPLETE);
        }
        
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "not an user type: " + type,
            lineNum, columnNum);
        return Optional.empty();
      case CLASS_TYPE:
        ClassType uty = (ClassType) type;
        tmpTypeStatus = lookupType(new ClassDeclType(uty.outerType, uty.name), ignoreExport, lineNum, columnNum);
        if (!tmpTypeStatus.isPresent()) {
          return Optional.empty();
        }
        mainStatus = tmpTypeStatus.get();

        for (Type innerTy : uty.innerTypes) {
          Optional<TypeStatus> tmp = lookupType(innerTy, ignoreExport, lineNum, columnNum);
          if (!tmp.isPresent()) {
            return Optional.empty();
          }
          if (tmp.get() != TypeStatus.COMPLETE) {
            return Optional.of(tmp.get().combine(mainStatus));
          }
        }
        return Optional.of(mainStatus);
      case ENUM_TYPE:
        EnumType enumTy = (EnumType) type;
        tmpTypeStatus = lookupType(enumTy.asDeclType(), ignoreExport, lineNum, columnNum);
        if (!tmpTypeStatus.isPresent()) {
          return Optional.empty();
        }
        mainStatus = tmpTypeStatus.get();

        return Optional.of(mainStatus);
      case UNDETERMINED_USER_INSTANCE_TYPE:
        UndeterminedUserInstanceType userInstanceTy = (UndeterminedUserInstanceType) type;
        tmpTypeStatus = lookupType(userInstanceTy.asDeclType(), ignoreExport, lineNum, columnNum);
        if (!tmpTypeStatus.isPresent()) {
          return Optional.empty();
        }
        mainStatus = tmpTypeStatus.get();

        for (Type innerTy : userInstanceTy.innerTypes) {
          Optional<TypeStatus> tmp = lookupType(innerTy, ignoreExport, lineNum, columnNum);
          if (!tmp.isPresent()) {
            return Optional.empty();
          }
          if (tmp.get() != TypeStatus.COMPLETE) {
            return Optional.of(tmp.get().combine(mainStatus));
          }
        }
        return Optional.of(mainStatus);
      case GENERIC_TYPE:
        return Optional.of(TypeStatus.GENERIC);
      case MULTI_TYPE:
        mainStatus = TypeStatus.COMPLETE;
        for (Type innerTy : type.getInnerTypes()) {
          Optional<TypeStatus> tmp = lookupType(innerTy, ignoreExport, lineNum, columnNum);
          if (!tmp.isPresent()) {
            return Optional.empty();
          }
          if (tmp.get() != TypeStatus.COMPLETE) {
            return Optional.of(tmp.get().combine(mainStatus));
          }
        }
        return Optional.of(mainStatus);
      default:
        break;
    }
    
    if (parent.isNotNull()) {
      return parent.get().lookupType(type, ignoreExport, lineNum, columnNum);
    }
    
    return Optional.of(TypeStatus.UNDEFINED);
  }

  public boolean registerNonGeneric(ClassType classType, int lineNum, int columnNum)
      throws FatalMessageException {
    
    if (registeredGenerics.isNull()) {
      //not the first typecheck
      return true;
    }
    
    Optional<TypeStatus> optTyStatus = lookupType(classType, lineNum, columnNum);
    if (!optTyStatus.isPresent()) {
      return false;
    }
    TypeStatus tyStatus = optTyStatus.get();
    if (!tyStatus.isGeneric() && tyStatus != TypeStatus.UNDEFINED) {
      registeredGenerics.get().addRegistration(this, classType, Nullable.empty(),
          effectiveImports, lineNum, columnNum);
      return true;
    }
    
    return false;
  }
  
  public boolean canSafeCast(Type fromTy, Type toTy) throws FatalMessageException {
    //check to ensure that types are abs - might remove
    Optional<Nullable<Type>> fromAbs = fromTy.basicNormalize(false, ofDeclType.getModuleType(),
        effectiveImports, msgState);
    if (!fromAbs.isPresent()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "fromTy failed to be normalized");
    }
    Optional<Nullable<Type>> toAbs = toTy.basicNormalize(false, ofDeclType.getModuleType(),
        effectiveImports, msgState);
    if (!toAbs.isPresent()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "toTy failed to be normalized");
    }
    
    return canSafeCast(typeGraph, fromAbs.get().get(), toAbs.get().get());
  }
  
  public static boolean canSafeCast(TypeGraph typeGraph, Type fromTy, Type toTy)
      throws FatalMessageException {
    CastDirection direction = CastDirection.UP;
    
    //normalize both types to their abs values
    
    if (fromTy instanceof ReferenceType) {
      //assigning ref to parent (or itself)
      direction = CastDirection.UP;
    } else if (fromTy instanceof NullType) {
      //assigning null to a ref
      return typeGraph.canCastTo(toTy, direction, fromTy);
    } else {
      //bool can assign (cast safely) down
      direction = CastDirection.DOWN;
    }

    return typeGraph.canCastTo(fromTy, direction, toTy);
  }

  public Nullable<TypeEnvironment> getParentScope(ScopeType scopeType) {
    Nullable<TypeEnvironment> iter = Nullable.of(this);
    while (iter.isNotNull()) {
      if (iter.get().envType == scopeType) {
        return iter;
      }
      iter = iter.get().parent;
    }
    return Nullable.empty();
  }

  public void setRegisteredGenerics(Nullable<RegisteredGenerics> registeredGenerics) {
    this.registeredGenerics = registeredGenerics;
  }

  public void activateSandboxMode(int lineNum, int columnNum) {
    sandboxMode  = Nullable.of(new SandboxModeInfo(lineNum, columnNum));
  }
  
  public Nullable<SandboxModeInfo> inSandboxMode() {
    if (sandboxMode.isNotNull()) {
      return sandboxMode;
    }
    if (parent.isNotNull()) {
      return parent.get().inSandboxMode();
    }
    return Nullable.empty();
  }

  public boolean copyFromTenvIntoTenv(UserDeclType classType, TypeEnvironment newTenv)
      throws FatalMessageException {
    //lookup the item
    Optional<UserDeclTypeBox> typeBoxOpt = lookupUserDeclType(classType, -1, -1);
    if (!typeBoxOpt.isPresent()) {
      return false;
    }
    // now define the path to this class
    return newTenv.defineUserType(classType, classType,
        Nullable.of(typeBoxOpt.get())).isPresent();
  }

  public boolean copyNonLambdaFromTenvIntoTenv(int lineNum, int columnNum,
      TypeEnvironment newTenv, Expression fname)
      throws FatalMessageException {
    if (!(fname instanceof IdentifierExpression || fname instanceof DotExpression)) {
      throw new IllegalArgumentException();
    }
    /*
     * If a dot expression, must be module path, then function name: a.b.c.fn
     * If identifier expression, must be function in this module
     */
    TypeEnvironment functionTenv = this;
    UserDeclType currentModType = ofDeclType.asUserDeclType();
    UserDeclType withinModule = currentModType;
    String id = "";
    if (fname instanceof DotExpression) {
      DotExpression dotExpr = (DotExpression) fname;
      Optional<TypecheckRet> leftTypeOpt = dotExpr.typecheck(this, Nullable.empty());
      if (!leftTypeOpt.isPresent()) {
        return false;
      }
      Type leftType = leftTypeOpt.get().type;
      if (!(leftType instanceof ModuleType)) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Left side of function name was not a module, but rather: " + leftType,
            lineNum, columnNum);
        return false;
      }
      
      Optional<TypeEnvironment> tenvOpt = ((ModuleType) leftType).getTenv(this, lineNum, columnNum);
      if (!tenvOpt.isPresent()) {
        return false;
      }
      functionTenv = tenvOpt.get();
      withinModule = functionTenv.ofDeclType.asUserDeclType();
      id = dotExpr.right.id;
    } else {
      id = ((IdentifierExpression) fname).id;
    }
    
    Nullable<IdToType> tmpIdToType = functionTenv.internalIdentifierLookup(id);
    if (tmpIdToType.isNull()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "No such function: " + fname,
          lineNum, columnNum);
      return false;
    }
    if (!(tmpIdToType.get() instanceof MapFunctionToType)) {
      msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Found id, but not a non-lambda function: " + fname,
          lineNum, columnNum);
      return false;
    }
    MapFunctionToType fnToType = (MapFunctionToType) tmpIdToType.get();
    
    if (currentModType.equals(withinModule)) {
      newTenv.identifiers.put(id, fnToType);
    }
    Optional<TypeEnvironment> targetTenv = newTenv.defineUserType(withinModule, withinModule,
        Nullable.empty());
    if (!targetTenv.isPresent()) {
      return false;
    }
    targetTenv.get().defineVar(id, fnToType);
    
    return true;
  }

  public Set<String> getSiblingClasses() {
    Set<String> siblingClasses = new HashSet<>();
    if (parent.isNotNull()) {
      siblingClasses = parent.get().getSiblingClasses();
    }
    for (Entry<String, IdToType> entry : identifiers.entrySet()) {
      if (!(entry.getValue() instanceof MapIdToType)) {
        continue;
      }
      MapIdToType details = (MapIdToType) entry.getValue();
      TypeBox tyBox = details.read();
      if (tyBox instanceof ClassTypeBox) {
        ClassTypeBox classTyBox = (ClassTypeBox) tyBox;
        if (classTyBox.classHeader.absName.outerType.equals(Nullable.of(ofDeclType))) {
          siblingClasses.add(entry.getKey());
        }
      } else if (tyBox instanceof EnumTypeBox) {
        EnumTypeBox classTyBox = (EnumTypeBox) tyBox;
        if (classTyBox.enumHeader.absName.outerType.equals(Nullable.of(ofDeclType))) {
          siblingClasses.add(entry.getKey());
        }
      }
    }
    return siblingClasses;
  }

}
