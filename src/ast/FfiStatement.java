package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.SandboxModeInfo;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class FfiStatement extends AbstractStatement {

  public final StringExpression literal;

  public FfiStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, StringExpression literal) {
    super(lineNum, columnNum, AbstractAst.AstType.FFI_STATEMENT, preComments, postComments);
    this.literal = literal;

    determinedType = VoidType.Create();
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + "@" + literal + "@" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Nullable<SandboxModeInfo> sandboxModeInfo = tenv.inSandboxMode();
    if (sandboxModeInfo.isNotNull()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.SANDBOX,
          "FFI statements are not allowed in sandbox mode (" + sandboxModeInfo + ")", lineNum,
          columnNum);
      return Optional.empty();
    }
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    throw new InterpError("Cannot interp ffi", lineNum, columnNum);
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.FfiStatement.Builder subDoc = AstProto.FfiStatement.newBuilder();

    subDoc.setLiteral(literal.serialize());

    document.setFfiStatement(subDoc);
    return document;
  }

  public static FfiStatement deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.FfiStatement document) throws SerializationError {
    return new FfiStatement(lineNum, columnNum, preComments, postComments,
        (StringExpression) AbstractExpression.deserialize(document.getLiteral()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof FfiStatement)) {
      return false;
    }
    return ((FfiStatement) other).literal.compare(literal);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<>(new LinkedList<>(), this);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    return this;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(new LinkedList<>(), Nullable.of(this)));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) {
    return new InsertDestructorsRet(this);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
