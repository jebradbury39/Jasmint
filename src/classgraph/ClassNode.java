package classgraph;

import import_mgmt.EffectiveImport.ImportUsage;
import java.util.HashSet;
import java.util.Set;
import typecheck.ClassDeclType;
import typecheck.EnumDeclType;
import typecheck.UserDeclType;

/*
 * Can have an uplink to a module or class. Cannot uplink to our own module (implied, so not needed).
 */
public class ClassNode extends Node {
  private Set<ClassUsageLink> uplinks = new HashSet<>();
  private Set<ClassUsageLink> downlinks = new HashSet<>(); //usage not useful, but same pattern
  
  public final UserDeclType classType; //abs module type (class or enum)
  public final int order; // the order in which the user declared the class
  
  public ClassNode(UserDeclType classType, int order) {
    this.classType = classType;
    this.order = order;
    
    if (!(classType instanceof ClassDeclType || classType instanceof EnumDeclType)) {
      throw new IllegalArgumentException();
    }
  }
  
  @Override
  public String toString() {
    String upStr = "";
    String downStr = "";
    for (ClassUsageLink up : uplinks) {
      upStr += up.node.getDeclType() + "<" + up.getUsage() + "> ";
    }
    for (ClassUsageLink down : downlinks) {
      downStr += down.node.getDeclType() + "<" + down.getUsage() + "> ";
    }
    return "(ClassNode " + getDeclType() + "(uplinks: " + upStr + ") (downlinks: " + downStr + "))\n";
  }
  
  @Override
  public int hashCode() {
    return classType.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof ClassNode)) {
      return false;
    }
    ClassNode otherClass = (ClassNode) other;
    return classType.equals(otherClass.classType);
  }

  @Override
  public UserDeclType getDeclType() {
    return classType;
  }
  
  @Override
  public boolean allUplinksFound(Set<UserDeclType> classTypes) {
    for (ClassUsageLink link : uplinks) {
      if (link.getUsage() == ImportUsage.HARD) {
        if (!classTypes.contains(link.node.getDeclType())) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  protected boolean addUplink(ClassUsageLink link) {
    if (link.node == this) {
      return false;
    }
    
    // never add an uplink to our own class
    if (link.node.getDeclType().equals(classType)) {
      return false;
    }
    
    // check for duplicate link
    for (ClassUsageLink uplink : getUplinks()) {
      if (uplink.node.getDeclType().equals(link.node.getDeclType())) {
        //increase the usage
        if (uplink.getUsage().ordinal() < link.getUsage().ordinal()) {
          uplink.setUsage(link.getUsage());
        }
        return false;
      }
    }
    
    uplinks.add(link);
    return true;
  }

  @Override
  protected boolean addDownlink(ClassUsageLink link) {
    if (link.node == this) {
      return false;
    }
    
    // never add a downlink to our own class
    if (link.node.getDeclType().equals(classType)) {
      return false;
    }
    downlinks.add(link);
    return true;
  }

  @Override
  public Set<ClassUsageLink> getUplinks() {
    return uplinks;
  }

  @Override
  public Set<ClassUsageLink> getDownlinks() {
    return downlinks;
  }
}
