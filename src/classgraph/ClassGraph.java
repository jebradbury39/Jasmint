package classgraph;

import ast.Program;
import audit.AuditTrailLite;
import classgraph.Node.ClassUsageLink;
import classgraph.Ordering.CompleteOrdering;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ProgHeader;
import import_mgmt.EffectiveImport.ImportUsage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import multifile.ModuleEndpoint;
import multifile.SourceManager;
import typecheck.ClassDeclType;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;
import typecheck.UserDeclType;
import util.DotGen;

/*
 * Stores class declarations as nodes linked up to other nodes based on stack fields,
 * including return types and parameter types in methods.
 * 
 * Also stores module nodes (linked by classes as well as by imports)
 */
public class ClassGraph {

  private Map<UserDeclType, Node> nodes = new HashMap<>();
  private int order = 0;
  public final Nullable<ModuleType> currentModule; //if null, not for a specific module
  public final MsgState msgState;

  public ClassGraph(Nullable<ModuleType> currentModule, MsgState msgState) {
    this.currentModule = currentModule;
    this.msgState = msgState;
  }
  
  public DotGen toDotFormat() {
    DotGen dotGraph = new DotGen();
    for (Node node : nodes.values()) {
      DotGen.Node graphNode = new DotGen.Node(node.getDeclType().toString());
      if (node instanceof ClassNode) {
        graphNode.color = DotGen.Color.PURPLE;
      }
      dotGraph.addNode(graphNode);
      node.updateDotGraph(dotGraph);
    }
    return dotGraph;
  }

  @Override
  public String toString() {
    return toDotFormat().toString();
  }

  // add a class
  public Node addClassNode(UserDeclType classType) {
    Nullable<Node> node = findNode(classType);
    if (node.isNotNull()) {
      return node.get();
    }
    node = Nullable.of(new ClassNode(classType, order++));
    nodes.put(classType, node.get());
    
    // add the module for this class
    if (classType.outerType.isNotNull()) {
      addModuleNode(classType.outerType.get());
    }
    
    return node.get();
  }
  
  // add a module
  public Node addModuleNode(ResolvedImportType moduleType) {
    return addModuleNode(moduleType.asModuleType());
  }
  
  public Node addModuleNode(ModuleType moduleType) {
    Nullable<Node> node = findNode(moduleType);
    if (node.isNotNull()) {
      return node.get();
    }
    node = Nullable.of(new ModuleNode(moduleType));
    nodes.put(moduleType, node.get());
    return node.get();
  }

  //find either a module or class
  public Nullable<Node> findNode(UserDeclType type) {
    Node node = nodes.get(type);
    if (node == null) {
      return Nullable.empty();
    }
    return Nullable.of(node);
  }

  //determine module order (in order of dependency - with zero dependency first)
  public Optional<CompleteOrdering> determineAllModsOrder(MsgState msgState, boolean noAnyLinks)
      throws FatalMessageException {
    List<Node> cycle = findCycle();
    if (!cycle.isEmpty()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Found cyclic class dependency involving: " + cycle);
      return Optional.empty();
    }

    //no more cycles, so determine the ideal class ordering based on uplinks
    //and apply class declaration reordering to program
    //start by collecting all the nodes with no dependencies
    List<ClassNode> nolinksClasses = new ArrayList<>();
    List<ModuleNode> nolinksModules = new ArrayList<>();
    for (Node node : nodes.values()) {
      boolean noHardUplink = true;
      for (ClassUsageLink uplink : node.getUplinks()) {
        if (noAnyLinks) {
          noHardUplink = false;
          break;
        }
        if (uplink.getUsage() == ImportUsage.HARD) {
          noHardUplink = false;
          break;
        }
      }
      
      if (noHardUplink) {
        if (node instanceof ClassNode) {
          nolinksClasses.add((ClassNode) node);
        } else {
          nolinksModules.add((ModuleNode) node);
        }
      }
    }
    nolinksClasses.sort(new SortByClassOrder());

    //add all nolinks to the write order
    //work our way down each one, setting visited to true once a node has been written
    //only write a node once ALL of its uplinks have been written
    clearVisited();
    
    Set<UserDeclType> foundTypes = new HashSet<>();
    CompleteOrdering classWriteOrder = new CompleteOrdering(msgState);
    
    // resolve modules FIRST, so that all classes will be able to find their modules
    List<ModuleNode> moduleQueue = new LinkedList<>();
    List<ModuleNode> moduleQueue2 = new LinkedList<>();
    for (ModuleNode root : nolinksModules) {
      classWriteOrder.addModule(root.moduleType, new HashSet<>());
      foundTypes.add(root.moduleType);
      
      List<Node> tmp = root.traverseDown();
      tmp.remove(0);
      for (Node node : tmp) {
        ModuleNode modNode = (ModuleNode) node;
        moduleQueue.add(modNode);
      }
    }
    
    while (!moduleQueue.isEmpty()) {
      int resolved = 0;
      while (!moduleQueue.isEmpty()) {
        ModuleNode modNode = moduleQueue.remove(0);
        
        if (modNode.allUplinksFound(foundTypes)) {
          resolved += 1;
          //remove from queue and add to write order
          classWriteOrder.addModule(modNode.moduleType, modNode.getUplinks());
          foundTypes.add(modNode.moduleType);
        } else {
          //under multiple roots
          moduleQueue2.add(modNode); //put the item at the end of the queue
        }
      }
      // swap queues
      List<ModuleNode> tmp = moduleQueue;
      moduleQueue = moduleQueue2; // has rejected items
      moduleQueue2 = tmp; // empty
      
      if (resolved == 0) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Stuck in determineOrder loop on modules: " + moduleQueue);
      }
    }
    
    List<ClassNode> classQueue = new LinkedList<>();
    List<ClassNode> classQueue2 = new LinkedList<>();
    for (ClassNode root : nolinksClasses) {
      classWriteOrder.addClass(root.classType, new HashSet<>());
      foundTypes.add(root.classType);
      
      List<Node> tmp = root.traverseDown();
      tmp.remove(0);
      for (Node node : tmp) {
        ClassNode classNode = (ClassNode) node;
        classQueue.add(classNode);
      }
    }
    
    while (!classQueue.isEmpty()) {
      int resolved = 0;
      while (!classQueue.isEmpty()) {
        ClassNode classNode = classQueue.remove(0);
        
        if (classNode.allUplinksFound(foundTypes)) {
          resolved += 1;
          //remove from queue and add to write order
          classWriteOrder.addClass(classNode.classType, classNode.getUplinks());
          foundTypes.add(classNode.classType);
        } else {
          //under multiple roots
          classQueue2.add(classNode); //put the item at the end of the queue
        }
      }
      
      // swap queues
      List<ClassNode> tmp = classQueue;
      classQueue = classQueue2; // has rejected items
      classQueue2 = tmp; // empty
      
      if (resolved == 0) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Stuck in determineOrder loop on classes: " + classQueue);
      }
    }
    
    return Optional.of(classWriteOrder);
  }

  //Returns class declarations in order with no declaration issues, or errors on cycle
  public Optional<Program> reorder(Program program, MsgState msgState)
      throws FatalMessageException {
    Optional<CompleteOrdering> classWriteOrder = determineAllModsOrder(msgState, false);
    if (!classWriteOrder.isPresent()) {
      return Optional.empty();
    }

    //find our specific module's class order
    Nullable<List<UserDeclType>> classOrder = classWriteOrder.get().getModClassOrder(
        program.moduleStatement.fullType, msgState);
    if (classOrder.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "Unable to get class order for '"
          + program.moduleStatement.fullType + "'");
      return Optional.empty();
    }

    return Optional.of(program.reorderClassWriteOrder(classOrder.get(), msgState));
  }
  
  public Optional<ProgHeader> reorder(ProgHeader progHeader, MsgState msgState)
      throws FatalMessageException {
    Optional<CompleteOrdering> classWriteOrder = determineAllModsOrder(msgState, false);
    if (!classWriteOrder.isPresent()) {
      return Optional.empty();
    }

    //find our specific module's class order
    Nullable<List<UserDeclType>> classOrder = classWriteOrder.get().getModClassOrder(
        progHeader.moduleType, msgState);
    if (classOrder.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "Unable to get class order for '"
          + progHeader.moduleType + "'");
      return Optional.empty();
    }

    return Optional.of(progHeader.reorderClassWriteOrder(classOrder.get(), msgState));
  }

  class SortByClassOrder implements Comparator<ClassNode> {

    @Override
    public int compare(ClassNode a, ClassNode b) {
      return a.order - b.order;
    }

  }

  //detect cycles. If found a cycle, return it
  private List<Node> findCycle() {
    for (Node node : nodes.values()) {
      clearVisited();
      List<Node> path = node.findOneCycle(node, new LinkedList<Node>());
      if (!path.isEmpty()) {
        return path;
      }
    }
    return new LinkedList<Node>();
  }

  private void clearVisited() {
    for (Node node : nodes.values()) {
      node.visited = false;
    }
  }

  // Find only module nodes
  public List<ModuleNode> findModuleNodes(ModuleType moduleType) {
    List<ModuleNode> moduleNodes = new LinkedList<>();
    for (Node node : nodes.values()) {
      if (node instanceof ModuleNode) {
        moduleNodes.add((ModuleNode) node);
      }
    }
    return moduleNodes;
  }
  
  // Determine which modules need to be rebuilt based on dependency
  public void determineRebuilds(SourceManager sources, AuditTrailLite auditTrail)
      throws FatalMessageException {
    clearVisited();
    
    //find root nodes (only modes)
    for (Node node : nodes.values()) {
      if (!(node instanceof ModuleNode) || !node.getUplinks().isEmpty()) {
        continue;
      }
      ModuleNode modNode = (ModuleNode) node;
      //traverse down each, only counting the modules
      Nullable<ModuleEndpoint> modEp = sources.get(modNode.moduleType, auditTrail);
      if (modEp.isNull()) {
        //mod not in this project, skip it since handled by lib
        continue;
      }
      modNode.determineRebuilds(sources, auditTrail, modEp.get().getHandle().needRebuild);
    }
  }

  public List<ClassDeclType> dfsAllChildClasses(Node node) {
    clearVisited();
    return node.dfsAllChildClasses();
  }
  
}
