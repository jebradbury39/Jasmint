package interp;

import java.util.Map;
import type_env.VariableScoping;
import typecheck.Type;

public class ValueBox {
  private Value value;
  public final VariableScoping scoping;
  
  public ValueBox(Value value) throws InterpError {
    this.value = value;
    this.scoping = null;
    
    init();
  }
  
  public ValueBox(Value value, VariableScoping scoping) throws InterpError {
    this.value = value;
    this.scoping = scoping;
    
    init();
  }
  
  @Override
  public String toString() {
    return "<value=" + value + ", scoping=" + scoping + ">";
  }
  
  private void init() throws InterpError {
    if (value != null) {
      if (value.getBox() != null) {
        throw new InterpError("value cannot exist in 2 memory locations at the same time! make a copy!", -1, -1);
      }
      this.value.setBox(this);
    }
  }
  
  public Value getValue() {
    return value;
  }
  
  public void setValue(Value value) throws InterpError {
    this.value = value;
    if (value != null) {
      if (value.getBox() != null) {
        throw new InterpError("value cannot exist in 2 memory locations at the same time! make a copy!", -1, -1);
      }
      this.value.setBox(this);
    }
  }
  
  //deep copy
  public ValueBox copy(Environment copyEnv) throws InterpError {
    return new ValueBox(value == null ? null : value.copy(copyEnv), scoping);
  }

  public ValueBox replaceType(Map<Type, Type> mapFromTo, String moduleAppend) throws InterpError {
    return new ValueBox(value == null ? null : value.replaceType(mapFromTo, moduleAppend), scoping);
  }
}
