package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class ExpressionStatement extends AbstractStatement {

  public final Expression expression;

  public ExpressionStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression expression) {
    super(lineNum, columnNum, AbstractAst.AstType.EXPRESSION_STATEMENT, preComments, postComments);
    this.expression = expression;
    this.determinedType = VoidType.Create();
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    ExpressionStatement val = new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        expression.renameIds(renamings, msgState));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    ExpressionStatement val = new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        expression.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public String toString(String indent) {
    return preToString() + expression.toString(indent) + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> typeOpt = expression.typecheck(tenv, Nullable.empty());
    if (!typeOpt.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    expression.interp(env);
    return new VoidValue();
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.ExpressionStatement.Builder subDoc = AstProto.ExpressionStatement.newBuilder();

    subDoc.setExpression(expression.serialize());

    document.setExpressionStatement(subDoc);
    return document;
  }

  public static ExpressionStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.ExpressionStatement document) throws SerializationError {
    return new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getExpression()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return expression.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof ExpressionStatement)) {
      return false;
    }
    return expression.compare(((ExpressionStatement) other).expression);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    ExpressionStatement val = new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        (Expression) expression.replace(target, with));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return expression.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    Pair<List<DeclarationStatement>, Ast> exprPair = expression.toAnf(tuning);
    ExpressionStatement value = new ExpressionStatement(lineNum, columnNum, preComments,
        postComments, (Expression) exprPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(exprPair.a, value);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    ExpressionStatement val = new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        (Expression) expression.replaceType(mapFromTo));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    ExpressionStatement val = new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        (Expression) expression.replaceVtableAccess(project, msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    expression.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return expression.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return expression.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    Optional<ResolveExcRet> tmp = expression.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
        stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }

    ExpressionStatement value = new ExpressionStatement(lineNum, columnNum, preComments,
        postComments, (Expression) tmp.get().ast.get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(tmp.get().preStatements, Nullable.of(value)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) {
    return new InsertDestructorsRet(this);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    ExpressionStatement val = new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        expression.liftClassDeclarations(liftedClasses, replacements).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    ExpressionStatement val = new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        expression.resolveUserTypes(msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    ExpressionStatement val = new ExpressionStatement(lineNum, columnNum, preComments, postComments,
        expression.addThisExpression(classHeader));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
