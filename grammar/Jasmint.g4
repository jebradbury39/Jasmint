grammar Jasmint;

@header
{
   /* package declaration here */
   package antlrgen;
}

/*
   Parser Rules
*/

program
   :  projectHeader programLine* EOF
   ;

projectHeader
   :  any_comment* modname childmod* importmod*
   ;

modname
   : (PROVIDER | FFI? MODULE) ID parentmod? SEMI
   ;

parentmod
   : OF ID (DOT ID)*
   ;

childmod
   : pre_comments=nl_comment* SUBMOD ID SEMI post_comments=nl_comment*
   ;

importmod
   : IMPORT ID (DOT ID)* SEMI
   ;

comment
   : LINE_COMMENT
   | BLOCK_COMMENT+ LINE_COMMENT?
   ;

nl_comment
   : NL_LINE_COMMENT
   | NL_BLOCK_COMMENT BLOCK_COMMENT* LINE_COMMENT?
   ;

any_comment
   : comment
   | nl_comment
   ;

programLine
   :  EXPORT? classDeclaration
   |  EXPORT? function
   |  EXPORT? enumDeclaration
   //same as FfiStatement (usually a helper function, just gets pasted over)
   |  ffiBlock
   | nl_comment
   ;

classDeclaration
   :  FFI? STATIC? 'class' ID parameterizedList? ('extends' type)? '{' classDeclarationSection* '}'
   ;
// declare a class as a statement
innerClassDeclaration
   :  'class' ID parameterizedList? ('extends' type)? '{' innerClassDeclarationSection* '}'
   ;
classDeclarationSection
   :  visibility '{' classDeclarationLine* '}'
   |  nl_comment
   ;
innerClassDeclarationSection
   :  visibility '{' innerClassDeclarationLine* '}'
   |  nl_comment
   ;
classDeclarationLine
   :  declarationLine
   |  STATIC? function
   //for adding members (fields and/or functions) to the class. Same as FfiStatement (takes whole line)
   |  ffiBlock
   |  constructorFun
   |  destructorFun
   |  overrideFun
   |  nl_comment
   ;
innerClassDeclarationLine
   :  innerDeclarationLine
   |  STATIC? regularFun
   |  constructorFun
   |  destructorFun
   |  overrideFun
   |  nl_comment
   ;

declarationLine
   :  STATIC? declaration SEMI comment?
   |  REQUIRE STATIC declNoExpr SEMI comment?
   ;
innerDeclarationLine
   :  STATIC? declaration SEMI comment?
   ;

visibility
   :  'public'
   |  'protected'
   |  'private'
   ;

parameterizedList
   :  '<' (ID (',' ID)*) '>'
   ;

enumDeclaration
   :  'enum' ID '{' enumEntries '}'
   ;
enumEntries
   :  ID (',' ID)*
   ;

type
   : CONST? wrappedType
   ;
wrappedType
   :  INT_TYPE                                                  # IntType
   |  'bool'                                                    # BoolType
   |  'string'                                                  # StringType
   |  FLOAT_TYPE                                                # FloatType
   |  'char'                                                    # CharType
   |  '[' elementType=type ']' REFERENCE?                       # ArrayType
   |  '{' keyType=type ',' valueType=type '}' REFERENCE?        # MapType
   |  '(' fnWithin? typelist? '->' retType=returnType ')'       # FunctionType
   |  undeterminedUserTypeInstance REFERENCE?                   # UndeterminedUserType
   ;
   
undeterminedUserTypeInstance
   :  modType? name=ID ('<' typelist '>')?
   ;
undeterminedUserTypeDecl
   :  modType? name=ID
   ;
modType
   :  (ID DOT)+
   ;

fnWithin
   :  undeterminedUserTypeDecl? '::'
   ;
typelist
   :  type (',' type)*
   ;
function
   :  regularFun
   |  requiredFun
   ;
regularFun
   :  'fun' returnType ID parameters block
   ;
requiredFun
   :  REQUIRE 'fun' returnType ID parameters SEMI
   ;

overrideFun
  : OVERRIDE ID block                                        # OverrideFunShort
  | OVERRIDE function                                        # OverrideFunLong
  ;

constructorFun
   :  'init' parameters parentConstructorCall? block
   |  REQUIRE 'init' parameters parentConstructorCall? SEMI
   ;
parentConstructorCall
   :  'parent' '(' arguments ')'
   ;
destructorFun
   : 'end' block
   | REQUIRE 'end' SEMI
   ;

lambda
   :  'fun' returnType parameters block
   ;
parameters
   :  '(' (pdecl (',' pdecl)*)? ')'
   ;
pdecl
   : CONST? declNoExpr
   ;

returnType
   :  returnTypeReal
   |  returnTypeVoid
   ;
returnTypeReal
   :  type (',' type)*
   ;
returnTypeVoid
   :  'void'
   ; 

ifStatement
   :  'if' expression block ('else' ifElseStatement)?
   ;
ifElseStatement
   :  block
   |  ifStatement
   ;
switchCase
   :  'case' expression ':' statementList
   ;
switchDefaultCase
   :  'default:' statementList
   ;

statementLine
   :  block                                                                       # BlockStatement
   |  ifStatement                                                                 # ConditionalStatement
   |  'while' expression block                                                    # WhileStatement
   |  'for' init=statement SEMI expression SEMI action=statement block              # ForStatement
   |  'for' '(' init=statement? SEMI expression SEMI action=statement? ')' block    # ForStatement
   |  'switch' expression '{' switchCase* switchDefaultCase? '}'                  # SwitchStatement
   |  'break' SEMI                                                                 # BreakStatement
   |  'continue' SEMI                                                              # ContinueStatement
   |  THREAD expression SEMI                                                       # ThreadStatement
   |  chan=ID '<-'                                                                # PipeStatement
   |  ffiBlock ('#' expression)? SEMI?                                             # FfiLineStatement
   |  value=statement SEMI comment?                                                # LineStatement
   |  nl_comment                                                                  # CommentStatement
   ;

statement
   :  mod=CONST? declaration                                                      # DeclarationStatement
   |  multiLvalue '=' expression                                                  # AssignmentStatement
   |  lvalue assignmentOperator expression                                        # ModifierAssignmentStatement
   |  'delete' expression                                                         # DeleteStatement
   |  'return' (multiExpression)?                                                 # ReturnStatement
   |  regularFun                                                                  # FunctionStatement
   |  innerClassDeclaration                                                       # ClassDeclarationStatement
   |  ffiBlock                                                                    # FfiBlockStatement
   |  expression '(' arguments ')'                                                # InvocationStatement
   ;

assignmentOperator
    :   '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|='
    ;

declaration
   :  declNoExpr
   |  declExpr
   ;
declNoExpr
   : type ID
   ;
declExpr
   : type ID '=' expression
   ;
   
block
   :  '{' statementList '}'
   ;
statementList
   :  statementLine*
   ;

//Lvalues
multiLvalue
   :  lvalue (',' lvalue)*
   ;
lvalue
   :  ID                                                          # LvalueId
   |  REFERENCE expression                                        # LvalueDereference
   |  expression op=DOT ID                                        # LvalueDot
   |  lft=expression '[' index=expression ']'                     # LvalueBracket
   |  ffiBlock                                                    # LvalueFfi
   ;

//Expressions

boolExpression
   :  'true'
   |  'false'
   ;

// a by itself is an id or module, but a module cannot be an expr, so id
// a.b could be an id access or module path, but ^^ so id
// a.B must be a class id
// A must be a class id
// <expr>.b must be a class field access
// <expr>.B cannot exist
// A.x must be a static access (same as a.B.c)
primaryExpression
   :  IntegerConstant                                                       # IntegerExpr
   |  FloatingConstant                                                      # DoubleExpr
   |  CharacterConstant                                                     # CharExpr
   |  stringExpression                                                      # StringExpr
   |  boolExpression                                                        # BoolExpr
   |  'null'                                                                # NullExpr
   |  undeterminedUserTypeDecl '<' typelist '>' '(' arguments ')'           # NewClassInstanceExpr
   |  ID                                                                    # IdentifierExpr
   |  ffiBlock                                                              # FfiExpr
   |  lambda                                                                # LambdaExpr
   |  '(' multiExpression ')'                                               # NestedExpr
   ;

postfixExpression
   :  primaryExpression                                                     # BasePrimaryExpr
   |  lft=postfixExpression '[' index=expression ']'                        # BracketExpr
   |  value=postfixExpression op=DOT ID                                     # DotExpr
   |  expr=postfixExpression '(' arguments ')'                              # InvocationExpr
   |  'cast' '<' type '>' '(' expression ')'                                # CastExpr
   |  '[' elementType=type ']' '[' arguments ']'                            # ArrayInitExpr
   |  '{' keyType=type ',' valueType=type '}' '{' newMap '}'                # MapInitExpr
   |  'new' postfixExpression                                               # NewExpr
   |  sandboxExpression                                                     # SandboxExpr
   ;
   
fnameExpr
   :  ID                                                                    # FnameIdExpr
   |  fnameExpr DOT ID                                                      # FnameDotExpr
   ;   
sandboxExpression
   :  'sandbox' returnTypeReal sandboxTypes? '(' sandboxArgs ')' sandboxFuns? sandboxParam* block catchExc
   ;
sandboxTypes
   :  '<' undeterminedUserTypeDecl (',' undeterminedUserTypeDecl)? '>'
   ;
sandboxArgs
   :  (declExpr (',' declExpr)*)?
   ;
sandboxFuns
   :  'fun' '(' fnameExpr (',' fnameExpr)* ')'
   ;
sandboxParam
   :  'timeout' '(' expression ')'                                          # TimeoutParam
   |  'recursion' '(' expression ')'                                        # RecursionParam
   |  'memlimit' '(' expression ')'                                         # MemlimitParam
   ;
catchExc
   :  'catch' ID block
   ;

unaryExpression
   :  postfixExpression                                                     # BasePostfixExpr
   |  GET_REF unaryExpression                                               # RefExpr
   |  REFERENCE unaryExpression                                             # DerefExpr
   |  op=('-' | NOT_OP) unaryExpression                                     # LeftUnaryOpExpr
   |  sizeofExpression                                                      # SizeofExpr
   ;
   
sizeofExpression
   :  'sizeof' unaryExpression
   |  'sizeof' type
   ;

multiplyExpression
   :  base=unaryExpression
   |  lft=multiplyExpression op='#' rht=unaryExpression
   |  lft=multiplyExpression op='*' rht=unaryExpression
   |  lft=multiplyExpression op='/' rht=unaryExpression
   |  lft=multiplyExpression op='%' rht=unaryExpression
   ;

additionExpression
   :  base=multiplyExpression
   |  lft=additionExpression op='+' rht=multiplyExpression
   |  lft=additionExpression op='-' rht=multiplyExpression
   ;

shiftExpression
   :  base=additionExpression
   |  lft=shiftExpression op=shiftOp rht=additionExpression
   ;

shiftOp
   : '>''>'
   | '<''<'
   ;

compareExpression
   :  base=shiftExpression
   |  lft=compareExpression op='<' rht=shiftExpression
   |  lft=compareExpression op='>' rht=shiftExpression
   |  lft=compareExpression op='<=' rht=shiftExpression
   |  lft=compareExpression op='>=' rht=shiftExpression
   ;

equalsExpression
   :  base=compareExpression
   |  lft=equalsExpression op='==' rht=compareExpression
   |  lft=equalsExpression pre=NOT_OP op='=' rht=compareExpression
   ;

andExpression
    :   base=equalsExpression
    |   lft=andExpression op='&' rht=equalsExpression
    ;

exclusiveOrExpression
    :   base=andExpression
    |   lft=exclusiveOrExpression op='^' rht=andExpression
    ;

inclusiveOrExpression
    :   base=exclusiveOrExpression
    |   lft=inclusiveOrExpression op='|' rht=exclusiveOrExpression
    ;

logicalAndExpression
    :   base=inclusiveOrExpression
    |   lft=logicalAndExpression op='&&' rht=inclusiveOrExpression
    ;

logicalOrExpression
    :   base=logicalAndExpression
    |   lft=logicalOrExpression op='||' rht=logicalAndExpression
    ;
    
instanceofExpression
    :   base=logicalOrExpression
    |   lft=instanceofExpression op=INSTANCEOF rht=undeterminedUserTypeInstance
    |   lft=instanceofExpression pre=NOT_OP op=INSTANCEOF rht=undeterminedUserTypeInstance
    ;

multiExpression
   :  expression (',' expression)*
   ;
expression
   :  instanceofExpression
   ;

//Expression helpers
stringExpression
   :  StringLiteral+
   ;
arguments
   :  (expression (',' expression)*)?
   ;
   
keyValuePair
   :  key=expression ':' value=expression
   ;
newMap
   :  (keyValuePair (',' keyValuePair)*)?
   ;

//Foreign Function Interface Block
ffiBlock
   :  ANY_DELIM stringExpression ANY_DELIM
   ;

/*
   Lexer Rules
*/

FFI : 'ffi';
STATIC : 'static' ;
CONST : 'const' ;
OVERRIDE : 'override' ;

MODULE : 'module' ;
PROVIDER : 'provider' ;
SUBMOD : 'submod' ;
IMPORT : 'import' ;
EXPORT : 'export' ;
OF : 'of' ;
THREAD : 'thread' ;

INSTANCEOF : 'instanceof' ;

REQUIRE : 'req' ;

NOT_OP : '!' ;
SEMI : ';' ;
DOT : '.' ;

/* reference is a pointer */
REFERENCE : '*' ;
GET_REF : '&' ;

//types
INT_TYPE
    :  'u'? 'int' ('8' | '16' | '32' | '64')?
    ;

FLOAT_TYPE
    :  'float' ('32' | '64')?
    ;

ID       :  [a-zA-Z_][a-zA-Z0-9_]* ;

//integers and doubles

IntegerConstant
    :   DecimalConstant
    |   OctalConstant
    |   HexadecimalConstant
    |   BinaryConstant
    ;

fragment
Digit
    :   [0-9]
    ;

fragment
DecimalConstant
    :   NonzeroDigit Digit*
    ;

fragment
OctalConstant
    :   '0' OctalDigit*
    ;

fragment
HexadecimalConstant
    :   HexadecimalPrefix HexadecimalDigit+
    ;

fragment
HexadecimalPrefix
    :   '0' [xX]
    ;

fragment
OctalDigit
    :   [0-7]
    ;

fragment
HexadecimalDigit
    :   [0-9a-fA-F]
    ;

fragment
NonzeroDigit
    :   [1-9]
    ;

fragment
BinaryConstant
  : '0' [bB] [0-1]+
  ;

//Double
FloatingConstant
    :   DecimalFloatingConstant
    |   HexadecimalFloatingConstant
    ;

fragment
DecimalFloatingConstant
    :   FractionalConstant ExponentPart?
    |   DigitSequence ExponentPart
    ;

fragment
HexadecimalFloatingConstant
    :   HexadecimalPrefix HexadecimalFractionalConstant BinaryExponentPart?
    |   HexadecimalPrefix HexadecimalDigitSequence BinaryExponentPart
    ;

fragment
FractionalConstant
    :   DigitSequence? '.' DigitSequence
    |   DigitSequence '.'
    ;

fragment
ExponentPart
    :   'e' Sign? DigitSequence
    |   'E' Sign? DigitSequence
    ;

fragment
Sign
    :   '+' | '-'
    ;

fragment
HexadecimalFractionalConstant
    :   HexadecimalDigitSequence? '.' HexadecimalDigitSequence
    |   HexadecimalDigitSequence '.'
    ;

fragment
BinaryExponentPart
    :   'p' Sign? DigitSequence
    |   'P' Sign? DigitSequence
    ;

fragment
HexadecimalDigitSequence
    :   HexadecimalDigit+
    ;

fragment
DigitSequence
    :   Digit+
    ;

//strings and chars

//char
CharacterConstant
    :   '\'' CChar '\''
    ;

fragment
CChar
    :   ~['\\\r\n]
    |   EscapeSequence
    ;
fragment
EscapeSequence
    :   SimpleEscapeSequence
    |   OctalEscapeSequence
    |   HexadecimalEscapeSequence
    ;
fragment
SimpleEscapeSequence
    :   '\\' ['"?abfnrtv\\]
    ;
fragment
OctalEscapeSequence
    :   '\\' OctalDigit
    |   '\\' OctalDigit OctalDigit
    |   '\\' OctalDigit OctalDigit OctalDigit
    ;
fragment
HexadecimalEscapeSequence
    :   '\\x' HexadecimalDigit+
    ;

//string
StringLiteral
    :   '"' SCharSequence? '"'
    ;
fragment
SCharSequence
    :   SChar+
    ;
fragment
SChar
    :   ~["\\\r\n]
    |   EscapeSequence
    |   '\\\n'   // Added line
    |   '\\\r\n' // Added line
    ;

//preceded only by at least one newline and maybe whitespace
NL_LINE_COMMENT  :  [ \t\r\f]* '\n' [ \t\n\r\f]* '//' ~('\r' | '\n')*;

NL_BLOCK_COMMENT :  [ \t\r\f]* '\n' [ \t\n\r\f]*  '/*' .*? '*/';

//not preceded by newline e.g. on same line as statement
LINE_COMMENT  :  '//' ~('\r' | '\n')*;

BLOCK_COMMENT :  '/*' .*? '*/';

ANY_DELIM : '@' ;

WS       :  [ \t\n\r\f]+ -> skip;
