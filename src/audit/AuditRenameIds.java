package audit;

import ast.SerializationError;
import astproto.AstProto;
import astproto.AstProto.AuditEntry.Builder;
import java.util.HashMap;
import mbo.Renamings;
import multifile.Project;
import typecheck.ModuleType;

public class AuditRenameIds implements AuditEntry {
  public final Renamings renamings;
  
  public AuditRenameIds(Project project, ModuleType moduleType) {
    this.renamings = new Renamings(project, moduleType, new HashMap<>());
  }
  
  public AuditRenameIds(Renamings renamings) {
    this.renamings = renamings;
  }
  
  @Override
  public String toString() {
    return getType().toString();
  }

  @Override
  public AuditEntryType getType() {
    return AuditEntryType.RENAME_IDS;
  }

  public static AuditEntry deserialize(Project project, ModuleType moduleType,
      AstProto.AuditRenameIds document)
      throws SerializationError {
    return new AuditRenameIds(Renamings.deserialize(project, moduleType, document.getRenamings()));
  }
  
  @Override
  public Builder serialize() {
    AstProto.AuditEntry.Builder document = AstProto.AuditEntry.newBuilder();
    document.setAuditEntryType(getType().ordinal());
    
    AstProto.AuditRenameIds.Builder subDoc = AstProto.AuditRenameIds.newBuilder();
    
    subDoc.setRenamings(renamings.serialize());
    
    document.setAuditRenameIds(subDoc);
    return document;
  }

  @Override
  public AuditEntry combineWith(AuditEntry other) {
    if (other.getType() != getType()) {
      throw new IllegalArgumentException();
    }
    throw new UnsupportedOperationException();
  }

  @Override
  public AuditEntry withRelativeEntries() {
    return this;
  }
}
