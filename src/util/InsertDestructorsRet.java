package util;

import ast.Statement;
import java.util.LinkedList;
import java.util.List;

public class InsertDestructorsRet {
  public final List<Statement> statements;
  public final boolean mustReturn;
  
  public InsertDestructorsRet(List<Statement> statements, boolean mustReturn) {
    this.statements = statements;
    this.mustReturn = mustReturn;
  }
  
  public InsertDestructorsRet(List<Statement> statements) {
    this.statements = statements;
    this.mustReturn = false;
  }
  
  public InsertDestructorsRet(Statement statement) {
    this.statements = new LinkedList<>();
    this.statements.add(statement);
    this.mustReturn = false;
  }
  
  public InsertDestructorsRet(Statement statement, boolean mustReturn) {
    this.statements = new LinkedList<>();
    this.statements.add(statement);
    this.mustReturn = mustReturn;
  }
}
