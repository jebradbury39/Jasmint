package typecheck;

import ast.Expression;
import ast.FunctionCallExpression;
import ast.FunctionCallStatement;
import ast.IdentifierExpression;
import ast.Program.DestructorRenamings;
import ast.SerializationError;
import ast.Statement;
import ast.StaticClassIdentifierExpression;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.EnumHeader;
import header.PsuedoClassHeader;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.ClassTypeBox;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;
import typecheck.FunctionType.LambdaStatus;

/*
 * The static declaration of a class
 */

public class ClassDeclType extends UserDeclType {

  public ClassDeclType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.CLASS_DECL_TYPE, outerType, name);
  }

  @Override
  public List<Type> getInnerTypes() {
    //don't care about generics since we are accessing statics here, which cannot be generic
    return new LinkedList<>();
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type tmp = mapFromTo.get(this);
    if (tmp == null) {
      return this;
    }
    return tmp;
  }

  @Override
  public Builder serialize() {
    AstProto.Type.Builder document = preSerialize();
    
    AstProto.ClassDeclType.Builder subDoc = AstProto.ClassDeclType.newBuilder();
    if (outerType.isNotNull()) {
      subDoc.addOuterType(outerType.get().serialize());
    }
    subDoc.setName(name);
    document.setClassDeclType(subDoc);
    return document;
  }
  
  public static ClassDeclType deserialize(AstProto.ClassDeclType document)
      throws SerializationError {
    Nullable<ModuleType> outerType = Nullable.empty();
    if (document.getOuterTypeCount() != 0) {
      outerType = Nullable.of((ModuleType) AbstractType.deserialize(document.getOuterType(0)));
    }
    return new ClassDeclType(outerType, document.getName());
  }

  @Override
  public Type setConst(boolean isConst) {
    // TODO Auto-generated method stub
    return this;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    stackTypes.add(this);
    return stackTypes;
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(this, lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(tyBox.get() instanceof ClassTypeBox)) {
      return Optional.empty();
    }
    ClassTypeBox classTyBox = (ClassTypeBox) tyBox.get();
    return Optional.of(classTyBox.classHeader.absName);
  }
  
  
  /*
   * import a;
   * a.A -> a.A;
   * a.b.A -> a.b.A;
   * 
   * import a.A;
   * A -> a.A
   * a.A -> a.A
   * b.A -> b.A - not matched
   * 
   * import a.b.A;
   * b.A -> b.A - not matched;
   */
  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState)
          throws FatalMessageException {
    // first try to find our class name in the effectiveImports
    Nullable<EffectiveImport> tmp = effectiveImports.getImport(asImportType(),
        EffectiveImport.ImportUsage.INVALID);
    
    Nullable<ModuleType> normOuter = Nullable.empty();
    if (tmp.isNull()) {
      if (outerType.isNull()) {
        //must be in our module
        if (keepCurrentModRelative) {
          return Optional.of(Nullable.of(new ClassDeclType(Nullable.empty(), name)));
        }
        return Optional.of(Nullable.of(new ClassDeclType(Nullable.of(currentModule), name)));
      }
      
      // not found, so now just look for module type (not the class name)
      tmp = effectiveImports.getImport(outerType.get().asImportType(),
          EffectiveImport.ImportUsage.INVALID);
      if (tmp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to find module in effective imports: " + outerType);
        return Optional.empty();
      }
      normOuter = Nullable.of(tmp.get().absPath.asModuleType());
    } else {
      // matched the class (must have outerType/ModuleType)
      normOuter = Nullable.of(tmp.get().absPath.outerType.get());
    }
    
    if (keepCurrentModRelative) {
      //remove any part of the outer type that starts with current module
      Nullable<ModuleType> uniqueRemainder = normOuter.get().removeCommonStart(currentModule);
      //unique/relative part
      normOuter = uniqueRemainder;
    }
    return Optional.of(Nullable.of(new ClassDeclType(normOuter, name)));
  }

  @Override
  public Type renameIds(Renamings renamings,
      MsgState msgState) throws FatalMessageException {
    // TODO Auto-generated method stub
    return this;
  }

  @Override
  public List<String> toList() {
    List<String> namePath = new LinkedList<>();
    if (outerType.isNotNull()) {
      namePath.addAll(outerType.get().toList());
    }
    namePath.add(name);
    return namePath;
  }
  
  public static ClassDeclType staticFromList(List<String> path) {
    Nullable<ModuleType> moduleType = Nullable.empty();
    int numLeft = path.size();
    for (String item : path) {
      numLeft -= 1;
      if (numLeft == 0) {
        return new ClassDeclType(moduleType, item);
      }
      moduleType = Nullable.of(new ModuleType(moduleType, item));
    }
    throw new IllegalArgumentException("empty path");
  }
  
  public UserDeclType fromList(List<String> list) {
    return staticFromList(list);
  }

  /*
   * Returns the static tenv for this class, with no global tenv access
   * 
   * TODO caching optimization
   */
  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(this, lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return Optional.empty();
    }
    if (!(tyBox.get() instanceof ClassTypeBox)) {
      return Optional.empty();
    }
    ClassTypeBox classTyBox = (ClassTypeBox) tyBox.get();

    return classTyBox.classHeader.getTenv(tenv, true, asInstanceType());
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState, Set<ModuleType> exportedTypes, boolean skipNormalize)
          throws FatalMessageException {
    ClassDeclType normType = this;
    if (!skipNormalize) {
      //make sure to normalize all types first, then pull out their modules
      Optional<Nullable<Type>> optType = basicNormalize(false, currentModule, effectiveImports,
          msgState);
      if (!optType.isPresent()) {
        return false;
      }
      normType = (ClassDeclType) optType.get().get();
    }
    
    exportedTypes.add(normType.outerType.get());
    return true;
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new StaticClassIdentifierExpression(this);
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    if (!isTop) {
      return this;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(this);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    
    Optional<UserDeclTypeBox> tyBox = tenv.lookupUserDeclType(this, lineNum, columnNum);
    if (!tyBox.isPresent()) {
      return -1;
    }
    if (!(tyBox.get() instanceof ClassTypeBox)) {
      return -1;
    }
    ClassTypeBox classTyBox = (ClassTypeBox) tyBox.get();
    
    return classTyBox.classHeader.calculateSize(tenv, true, lineNum, columnNum);
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    //id.destructor()
    PsuedoClassHeader classHeader = destructorRenamings.project.lookupClassHeader(this,
        Nullable.empty(), true, destructorRenamings.project.getAuditTrail());
    if (classHeader instanceof EnumHeader) {
      return Optional.empty();
    }
    
    //lookup renaming for this type destructor
    final String destructorId = destructorRenamings.getRenaming(this);
    
    FunctionCallExpression fnCallExpr = new FunctionCallExpression(
        Nullable.of(new IdentifierExpression(id)), new IdentifierExpression(destructorId),
        new LinkedList<>(), "destructor");
    fnCallExpr.setCalledType(new FunctionType(this,
        VoidType.Create(), new LinkedList<>(), LambdaStatus.NOT_LAMBDA));
    fnCallExpr.setIsDestructorCall(true);
    return Optional.of(new FunctionCallStatement(fnCallExpr));
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return tenv.copyFromTenvIntoTenv(this, newTenv);
  }

  @Override
  public Type asClassDeclType() {
    return this;
  }

  @Override
  public UserInstanceType asInstanceType() {
    return new ClassType(outerType, name, new LinkedList<>());
  }

  @Override
  public UserDeclType asUserDeclType() {
    return this;
  }

  @Override
  public ImportType asImportType() {
    return new ClassImportType(outerType, name);
  }

}
