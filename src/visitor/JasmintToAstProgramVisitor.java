package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import ast.AbstractAst;
import ast.ClassDeclaration;
import ast.EnumDeclaration;
import ast.FfiStatement;
import ast.Function;
import ast.ImportStatement;
import ast.ModuleStatement;
import ast.Program;
import ast.SubmodStatement;
import errors.Nullable;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import multifile.JsmntGlobal;
import typecheck.ClassDeclType;
import typecheck.GenericType;
import typecheck.ModuleType;
import typecheck.UserDeclType;

public class JasmintToAstProgramVisitor extends JasmintBaseVisitor<Program> {
  
  public final String srcFileName;
  public final boolean isJsmntGlobal;
  private ModuleType moduleName;
  private int withinSandboxExpression = 0;
  
  public JasmintToAstProgramVisitor(String srcFileName, boolean isJsmntGlobal) {
    this.srcFileName = srcFileName;
    this.isJsmntGlobal = isJsmntGlobal;
  }
  
  //there can only be one instance of these per parser run
  public final JasmintToAstClassDeclarationVisitor classDeclarationVisitor =
      new JasmintToAstClassDeclarationVisitor(this);
  private final JasmintToAstEnumDeclarationVisitor enumDeclarationVisitor =
      new JasmintToAstEnumDeclarationVisitor(this);
  
  final JasmintToAstFunctionVisitor functionVisitor =
      new JasmintToAstFunctionVisitor(this, classDeclarationVisitor);
  private final JasmintToAstStatementVisitor statementVisitor =
      new JasmintToAstStatementVisitor(this,
          new JasmintToAstExpressionVisitor(this));
  private final JasmintToAstTypeVisitor typeVisitor =
      new JasmintToAstTypeVisitor(this);
  
  public boolean getWithinSandboxExpression() {
    return withinSandboxExpression > 0;
  }
  
  public void enterSandboxExpression() {
    withinSandboxExpression += 1;
  }
  
  public void exitSandboxExpression() {
    withinSandboxExpression -= 1;
  }
  
  //returns ModuleType or ClassDeclType (abs)
  public UserDeclType getWithinModuleOrClass() {
    Nullable<ClassDeclType> withinClass = getWithinClass();
    if (withinClass.isNull()) {
      return getModuleType();
    }
    return withinClass.get();
  }
  
  public ModuleType getModuleType() {
    return moduleName;
  }
  
  public Nullable<ClassDeclType> getWithinClass() {
    return classDeclarationVisitor.getWithinClass();
  }
  
  public Set<GenericType> getCurrentClassGenerics() {
    return classDeclarationVisitor.getCurrentClassGenerics();
  }
  
  @Override
  public Program visitProgram(JasmintParser.ProgramContext ctx) {
    AbstractAst.resetLabelGen(); //only one program ctx, so this starts things
    
    ModuleStatement moduleStatement = (ModuleStatement) statementVisitor.visitModname(
        ctx.projectHeader().modname());
    
    moduleName = moduleStatement.fullType;
    if (!isJsmntGlobal && moduleName.equals(JsmntGlobal.modType)) {
      throw new IllegalArgumentException(
          "User cannot create module " + moduleName + " (reserved)");
    }
    
    List<ClassDeclaration> classes = new LinkedList<>();
    List<Function> functions = new LinkedList<>();
    List<FfiStatement> ffi = new LinkedList<>();
    List<EnumDeclaration> enums = new LinkedList<>();
    gatherProgramLines(ctx, classes, functions, ffi, enums);
    
    return new Program(Nullable.of(srcFileName), moduleStatement,
        gatherSubmods(ctx.projectHeader()),
        gatherImports(ctx.projectHeader()),
        classes, functions, ffi, enums);
  }
  
  private List<SubmodStatement> gatherSubmods(JasmintParser.ProjectHeaderContext ctx) {
    List<SubmodStatement> submods = new LinkedList<>();
    for (JasmintParser.ChildmodContext sctx : ctx.childmod()) {
      submods.add((SubmodStatement) statementVisitor.visitChildmod(sctx));
    }
    return submods;
  }

  private List<ImportStatement> gatherImports(JasmintParser.ProjectHeaderContext ctx) {
    List<ImportStatement> statements = new LinkedList<>();
    for (JasmintParser.ImportmodContext ictx : ctx.importmod()) {
      statements.add((ImportStatement) statementVisitor.visitImportmod(ictx));
    }
    
    if (!isJsmntGlobal) {
      ImportStatement val = new ImportStatement(-1, -1,
          new LinkedList<>(), new LinkedList<>(),
          JsmntGlobal.importModType);
      val.setOriginalLabel(val.label);
      statements.add(val);
      
      // import Obj directly as well
      ImportStatement valObj = new ImportStatement(-1, -1,
          new LinkedList<>(), new LinkedList<>(),
          JsmntGlobal.objClassType.asDeclType().asImportType());
      valObj.setOriginalLabel(valObj.label);
      statements.add(valObj);
    }
    return statements;
  }

  private void gatherProgramLines(JasmintParser.ProgramContext ctx, List<ClassDeclaration> classes,
      List<Function> functions, List<FfiStatement> ffi, List<EnumDeclaration> enums) {
    
    int ordering = 0;
    for (JasmintParser.ProgramLineContext plctx : ctx.programLine()) {
      boolean export = plctx.EXPORT() != null;
      if (plctx.classDeclaration() != null) {
        ClassDeclaration cdecl = classDeclarationVisitor.visit(plctx.classDeclaration());
        long saveOL = cdecl.getOriginalLabel();
        cdecl = new ClassDeclaration(cdecl.lineNum, cdecl.columnNum,
            new LinkedList<>(), new LinkedList<>(),
            export,
            cdecl.isFfi,
            cdecl.isStatic,
            moduleName,
            cdecl.name,
            cdecl.originalName,
            cdecl.genericTypeNames,
            cdecl.parent,
            cdecl.sections);
        cdecl.setOriginalLabel(saveOL);
        cdecl.setOrdering(ordering++);
        classes.add(cdecl);
      } else if (plctx.function() != null) {
        Function fn = functionVisitor.visit(plctx.function());
        long saveOL = fn.getOriginalLabel();
        fn = new Function(fn.lineNum, fn.columnNum,
            new LinkedList<>(), new LinkedList<>(),
            export, fn.required, fn.isOverride,
            fn.name, fn.originalWithin,
            fn.fullType, fn.params, fn.getBody(), fn.getIsStatic(), 
            fn.parentCall, fn.metaType);
        fn.setOriginalLabel(saveOL);
        fn.setOrdering(ordering++);
        functions.add(fn);
      } else if (plctx.ffiBlock() != null) {
        FfiStatement f = (FfiStatement) statementVisitor.visit(plctx.ffiBlock());
        f.setOrdering(ordering++);
        ffi.add(f);
      } else if (plctx.enumDeclaration() != null) {
        EnumDeclaration en = enumDeclarationVisitor.visit(plctx.enumDeclaration());
        long saveOL = en.getOriginalLabel();
        en = new EnumDeclaration(en.lineNum, en.columnNum,
            new LinkedList<>(), new LinkedList<>(),
            export, en.enumType, en.enumIds);
        en.setOriginalLabel(saveOL);
        en.setOrdering(ordering++);
        enums.add(en);
      }
    }
  }

}
