package errors;

@SuppressWarnings("serial")
public class FatalMessageException extends Exception {

  public final MsgState msgState;
  
  public FatalMessageException(MsgState msgState) {
    this.msgState = msgState;
  }
  
  @Override
  public String toString() {
    return msgState.toString();
  }
}
