package import_mgmt;

import errors.Nullable;
import import_mgmt.EffectiveImport.EffectiveImportType;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import typecheck.ImportType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.ResolvedImportType;
import util.DotGen;
import util.Pair;

/*
 * Goes into import graph
 */
public class ImportNode {
  
  public static class ImportUsageLink {
    public final ImportNode node;
    public final EffectiveImportType importType;
    
    public ImportUsageLink(ImportNode node, EffectiveImportType importType) {
      this.node = node;
      this.importType = importType;
    }
    
    @Override
    public int hashCode() {
      return node.hashCode();
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof ImportUsageLink)) {
        return false;
      }
      ImportUsageLink otherLink = (ImportUsageLink) other;
      return node.equals(otherLink.node);
    }
  }
  
  private final ImportGraph graph;
  public final ImportType absImportType;
  public final Nullable<ProjectNameVersion> withinProject;
  public Map<ResolvedImportType, ImportUsageLink> imports = new HashMap<>(); //uplinks
  protected boolean visited = false;
  
  public ImportNode(ImportGraph graph, ImportType absImportType,
      Nullable<ProjectNameVersion> withinProject) {
    this.graph = graph;
    this.absImportType = absImportType;
    this.withinProject = withinProject;
  }
  
  @Override
  public int hashCode() {
    return absImportType.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof ImportNode)) {
      return false;
    }
    ImportNode otherNode = (ImportNode) other;
    return absImportType.equals(otherNode.absImportType);
  }
  
  public void updateDotGraph(DotGen dotGraph) {
    for (ImportUsageLink upNode : imports.values()) {
      DotGen.Edge edge = new DotGen.Edge(absImportType.toString(),
          upNode.node.absImportType.toString());
      edge.color = DotGen.Color.BLACK;
      if (upNode.importType == EffectiveImportType.IMPLIED_IMPORT) {
        edge.color = DotGen.Color.PURPLE;
      }
      dotGraph.addEdge(edge);
    }
  }

  public void addImport(ResolvedImportType imp, EffectiveImportType importType,
      Nullable<ModuleType.ProjectNameVersion> withinProject) {
    
    ImportNode impNode = graph.findNode(imp, withinProject, true).get();
    imports.put(imp, new ImportUsageLink(impNode, importType));
  }

  public void dfs(Set<Pair<ImportType, Nullable<ProjectNameVersion>>> modTypes) {
    visited = true;
    modTypes.add(new Pair<>(absImportType, withinProject));
    for (ImportUsageLink imp : imports.values()) {
      if (!imp.node.visited) {
        imp.node.dfs(modTypes);
      }
    }
  }
  
}
