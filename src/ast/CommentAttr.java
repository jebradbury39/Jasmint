package ast;

import astproto.AstProto;

/*
 * Kind of like a CommentExpression, except it is always attached to another ast node.
 */

public class CommentAttr {
  public final boolean isBlockComment;
  public final String comment;
  
  public CommentAttr(String comment, boolean isBlockComment) {
    this.isBlockComment = isBlockComment;
    this.comment = comment;
  }

  @Override
  public String toString() {
    return comment;
  }

  public static CommentAttr deserialize(AstProto.CommentAttr post) {
    return new CommentAttr(post.getComment(), post.getIsBlockComment());
  }

  public AstProto.CommentAttr.Builder serialize() {
    AstProto.CommentAttr.Builder document = AstProto.CommentAttr.newBuilder();
    
    document.setComment(comment);
    document.setIsBlockComment(isBlockComment);
    
    return document;
  }
}
