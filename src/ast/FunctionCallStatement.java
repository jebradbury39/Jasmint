package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class FunctionCallStatement extends AbstractStatement {
  public final Expression fnExpression;

  public FunctionCallStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression fnExpression) {
    super(lineNum, columnNum, AstType.FUNCTION_CALL_STATEMENT, preComments, postComments);
    this.fnExpression = fnExpression;

    fnExpression.setParentAst(this);
  }

  public FunctionCallStatement(int lineNum, int columnNum, Expression fnExpression) {
    super(lineNum, columnNum, AstType.FUNCTION_CALL_STATEMENT);
    this.fnExpression = fnExpression;

    fnExpression.setParentAst(this);
  }

  public FunctionCallStatement(Expression fnExpression) {
    super(AstType.FUNCTION_CALL_STATEMENT);
    this.fnExpression = fnExpression;

    fnExpression.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + fnExpression.toString(indent) + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> fnTy = fnExpression.typecheck(tenv, Nullable.empty());
    if (!fnTy.isPresent()) {
      return Optional.empty();
    }
    determinedType = fnTy.get().type;
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    return fnExpression.interp(env);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.FunctionCallStatement.Builder subDoc = AstProto.FunctionCallStatement.newBuilder();

    subDoc.setFnExpression(fnExpression.serialize());

    document.setFunctionCallStatement(subDoc);
    return document;
  }

  public static FunctionCallStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.FunctionCallStatement document) throws SerializationError {
    return new FunctionCallStatement(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getFnExpression()));
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments, fnExpression.renameIds(renamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return fnExpression.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof FunctionCallStatement)) {
      return false;
    }
    FunctionCallStatement ourOther = (FunctionCallStatement) other;
    return ourOther.fnExpression.compare(fnExpression);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments, (Expression) fnExpression.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return fnExpression.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    Pair<List<DeclarationStatement>, Ast> fnPair = fnExpression.toAnf(tuning);

    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments, (Expression) fnPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(fnPair.a, value);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments, (Expression) fnExpression.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments, (Expression) fnExpression.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments,
        fnExpression
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    fnExpression.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return fnExpression.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return fnExpression.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    Optional<ResolveExcRet> newFnExpr = fnExpression.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings,
        withinFn, true, enableResolution, hitExcId, stackSize, msgState);
    
    if (!newFnExpr.isPresent()) {
      return newFnExpr;
    }
    if (newFnExpr.get().ast.isNull()) {
      return newFnExpr;
    }
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum,
        preComments, postComments, (Expression) newFnExpr.get().ast.get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(newFnExpr.get().preStatements, value));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) {
    return new InsertDestructorsRet(this);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments, fnExpression.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments, fnExpression.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    FunctionCallStatement value = new FunctionCallStatement(lineNum, columnNum, preComments,
        postComments, fnExpression.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
