package ast;

import astproto.AstProto;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import mbo.Renamings;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.ModuleType;
import typecheck.SizeofTarget;
import typecheck.Type;
import util.Pair;

public abstract class AbstractExpression extends AbstractAst implements Expression {

  public AbstractExpression(int lineNum, int columnNum, AstType astType,
      List<CommentAttr> preComments, List<CommentAttr> postComments) {
    super(lineNum, columnNum, astType, preComments, postComments);
  }

  public AbstractExpression(int lineNum, int columnNum, AstType astType) {
    super(lineNum, columnNum, astType);
  }

  public AbstractExpression(AstType astType) {
    super(astType);
  }

  public static Expression deserialize(AstProto.Ast document) throws SerializationError {
    int deLineNum = document.getLineNum();
    int deColumnNum = document.getColNum();
    Type deDeterminedType = AbstractType.deserialize(document.getDeterminedType());
    final long originalLabel = document.getLabel();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();

    Expression value = null;
    Pair<AstType, Long> tmp = AbstractAst.preDeserialize(document, preComments, postComments);
    AstType astType = tmp.a;
    long ordering = tmp.b;
    switch (astType) {
      case ARRAY_INIT_EXPRESSION:
        value = ArrayInitExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getArrayInitExpression());
        break;
      case BINARY_EXPRESSION:
        value = BinaryExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getBinaryExpression());
        break;
      case BOOL_EXPRESSION:
        value = BoolExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getBoolExpression());
        break;
      case BRACKET_EXPRESSION:
        value = BracketExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getBracketExpression());
        break;
      case CAST_EXPRESSION:
        value = CastExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getCastExpression());
        break;
      case CHAR_EXPRESSION:
        value = CharExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getCharExpression());
        break;
      case DOT_EXPRESSION:
        value = DotExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getDotExpression());
        break;
      case DOUBLE_EXPRESSION:
        value = FloatExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getDoubleExpression());
        break;
      case FUNCTION_CALL_EXPRESSION:
        value = FunctionCallExpression.deserialize(deLineNum, deColumnNum, preComments,
            postComments, document.getFunctionCallExpression());
        break;
      case IDENTIFIER_EXPRESSION:
        value = IdentifierExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getIdentifierExpression());
        break;
      case STATIC_CLASS_IDENTIFIER_EXPRESSION:
        value = StaticClassIdentifierExpression.deserialize(deLineNum, deColumnNum, preComments,
            postComments, document.getStaticClassIdentifierExpression());
        break;
      case INTEGER_EXPRESSION:
        value = IntegerExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getIntegerExpression());
        break;
      case LAMBDA_EXPRESSION:
        value = LambdaExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getLambdaExpression());
        break;
      case LEFT_UNARY_OP_EXPRESSION:
        value = LeftUnaryOpExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getLeftUnaryOpExpression());
        break;
      case MAP_INIT_EXPRESSION:
        value = MapInitExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getMapInitExpression());
        break;
      case NEW_CLASS_INSTANCE_EXPRESSION:
        value = NewClassInstanceExpression.deserialize(deLineNum, deColumnNum, preComments,
            postComments, document.getNewClassInstanceExpression());
        break;
      case NEW_EXPRESSION:
        value = NewExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getNewExpression());
        break;
      case NULL_EXPRESSION:
        value = NullExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getNullExpression());
        break;
      case PARENT_CALL_EXPRESSION:
        value = ParentCallExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getParentCallExpression());
        break;
      case STRING_EXPRESSION:
        value = StringExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getStringExpression());
        break;
      case THIS_EXPRESSION:
        value = ThisExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getThisExpression());
        break;
      case VTABLE_ACCESS_EXPRESSION:
        value = VtableAccessExpression.deserialize(deLineNum, deColumnNum, preComments,
            postComments, document.getVtableAccessExpression());
        break;
      case REFERENCE_EXPRESSION:
        value = ReferenceExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getReferenceExpression());
        break;
      case DEREFERENCE_EXPRESSION:
        value = DereferenceExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getDereferenceExpression());
        break;
      case FFI_EXPRESSION:
        value = FfiExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getFfiExpression());
        break;
      case MULTI_EXPRESSION:
        value = MultiExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getMultiExpression());
        break;
      case SANDBOX_EXPRESSION:
        value = SandboxExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getSandboxExpression());
        break;
      case CATCH_EXC_SECTION:
        value = CatchExcSection.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getCatchExcSection());
        break;
      case TIMEOUT_EXPRESSION:
        value = TimeoutExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getTimeoutExpression());
        break;
      case RECURSION_EXPRESSION:
        value = RecursionExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getRecursionExpression());
        break;
      case SIZEOF_EXPRESSION:
        value = SizeofExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getSizeofExpression());
        break;
      case MEMLIMIT_EXPRESSION:
        value = MemlimitExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getMemlimitExpression());
        break;
      case INSTANCEOF_EXPRESSION:
        value = InstanceofExpression.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getInstanceofExpression());
        break;
      default:
        break;
    }

    if (value == null) {
      throw new SerializationError("Unable to deserialize expression astType[" + astType + "]",
          false);
    }

    ((AbstractAst) value).determinedType = deDeterminedType;
    ((AbstractAst) value).setOriginalLabel(originalLabel);
    return value;
  }

  public static Expression fromIdList(int lineNum, int columnNum, List<String> identifiers,
      MsgState msgState) throws FatalMessageException {
    if (identifiers.isEmpty()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "identifers is empty", lineNum,
          columnNum);
    }
    if (identifiers.size() == 1) {
      Expression expr = new IdentifierExpression(lineNum, columnNum, new LinkedList<>(),
          new LinkedList<>(), identifiers.get(0));
      expr.setOriginalLabel(expr.getLabel());
      return expr;
    }

    List<Expression> expressions = new LinkedList<>();
    for (String retDeclId : identifiers) {
      Expression expr = new IdentifierExpression(lineNum, columnNum, new LinkedList<>(),
          new LinkedList<>(), retDeclId);
      expr.setOriginalLabel(expr.getLabel());
      expressions.add(expr);
    }
    Expression val = new MultiExpression(lineNum, columnNum, new LinkedList<>(), new LinkedList<>(),
        expressions);
    val.setOriginalLabel(val.getLabel());
    return val;
  }

  @Override
  public SizeofTarget renameIdsSizeof(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    return renameIds(renamings, msgState);
  }

  @Override
  public SizeofTarget replaceTypeSizeof(Map<Type, Type> mapFromTo) {
    return replaceType(mapFromTo);
  }

  @Override
  public Optional<SizeofTarget> normalizeTypeSizeof(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Expression>> tmp = normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(tmp.get().get());
  }
  
  @Override
  public boolean compareSizeof(SizeofTarget target) {
    if (!(target instanceof Expression)) {
      return false;
    }
    return compare((Expression) target);
  }
  
  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      int lineNum, int columnNum) throws FatalMessageException {
    // we already have line/column info in ast
    addDependency(modDepGraph, ourModNode, tenv);
  }
  
}
