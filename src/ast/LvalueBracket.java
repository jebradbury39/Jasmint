package ast;

import ast.BinaryExpression.BinOp;
import ast.LeftUnaryOpExpression.LeftUnaryOp;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.ArrayValue;
import interp.Environment;
import interp.InterpError;
import interp.MapValue;
import interp.MemValue;
import interp.ReferenceValue;
import interp.Value;
import interp.ValueBox;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.ArrayType;
import typecheck.FunctionType;
import typecheck.IntType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typegraph.CastFilter;
import util.Pair;

public class LvalueBracket extends AbstractLvalue {

  // IdentifierExpression, DotExpression, BracketExpression, FunctionCall
  public final Expression left;

  public final Expression index;

  public LvalueBracket(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression left, Expression index) {
    super(lineNum, columnNum, AstType.BRACKET_LVALUE, preComments, postComments);
    this.left = left;
    this.index = index;

    left.setParentAst(this);
    index.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + left.toString(indent) + "[" + index.toString(indent) + "]"
        + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    if (!(left instanceof IdentifierExpression) && !(left instanceof DotExpression)
        && !(left instanceof BracketExpression) && !(left instanceof FunctionCallExpression)) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Invalid left expression for LvalueBracket: " + left, lineNum, columnNum);
    }

    Optional<TypecheckRet> leftTypeOpt = left.typecheck(tenv, Nullable.empty());
    Optional<TypecheckRet> indexTypeOpt = index.typecheck(tenv, Nullable.empty());
    if (!leftTypeOpt.isPresent() || !indexTypeOpt.isPresent()) {
      return Optional.empty();
    }
    Type leftType = leftTypeOpt.get().type;
    Type indexType = indexTypeOpt.get().type;

    if (leftType.getIsConst()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "left side of lvalue[idx] is const", lineNum, columnNum);
    }

    // If a valid reference, then we can automatically dereference here (sorta)
    if (leftType instanceof ReferenceType) {
      leftType = ((ReferenceType) leftType).innerType;
    }

    if (leftType instanceof ArrayType) {
      if (!tenv.typeGraph.canCastTo(indexType, CastFilter.UP_OR_DOWN,
          IntType.Create(false, 64))) {
        tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "array index type " + indexType + " cannot be converted to an int", lineNum, columnNum);
      }
      determinedType = ((ArrayType) leftType).getInnerTypes().get(0);
    } else if (leftType instanceof MapType) {
      MapType mapTy = (MapType) leftType;

      if (!tenv.typeGraph.canCastTo(indexType, CastFilter.UP_OR_DOWN, mapTy.keyType)) {
        tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "map key type " + indexType + " cannot be converted to expected type " + mapTy.keyType,
            lineNum, columnNum);
      }
      determinedType = mapTy.valueType;
    } else {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "bracket notation not defined for left type " + leftType, lineNum, columnNum);
    }

    if (determinedType.getIsConst()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "lvalue[idx] is const: " + determinedType, lineNum, columnNum);
    }

    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    if (!(left instanceof IdentifierExpression) && !(left instanceof DotExpression)
        && !(left instanceof BracketExpression) && !(left instanceof FunctionCallExpression)) {
      throw new InterpError("Invalid left expression for LvalueBracket: " + left, lineNum,
          columnNum);
    }

    Value leftValue = left.interp(env); // we need the memory ref here
    Value idxValue = index.interp(env);

    if (leftValue instanceof ReferenceValue) {
      leftValue = ((ReferenceValue) leftValue).referenced.getValue(); // auto deref
    }

    if (leftValue instanceof ArrayValue) {
      ArrayValue laVal = (ArrayValue) leftValue;
      ValueBox box = laVal.getElementByIndex((int) idxValue.toInt());
      return new MemValue(laVal.getType().getInnerTypes().get(0), box);
    }
    if (leftValue instanceof MapValue) {
      MapValue lmVal = (MapValue) leftValue;
      return new MemValue(leftValue.getType().getInnerTypes().get(1),
          lmVal.getValueByKey(idxValue));
    }
    throw new InterpError("bracket notation not defined for left value " + leftValue, lineNum,
        columnNum);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.LvalueBracket.Builder subDoc = AstProto.LvalueBracket.newBuilder();

    subDoc.setLeft(left.serialize());
    subDoc.setIdx(index.serialize());

    document.setLvalueBracket(subDoc);
    return document;
  }

  public static LvalueBracket deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.LvalueBracket document) throws SerializationError {
    return new LvalueBracket(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getLeft()),
        AbstractExpression.deserialize(document.getIdx()));
  }

  @Override
  public Expression toExpression() {
    return new BracketExpression(columnNum, columnNum, preComments, postComments, left, index);
  }

  @Override
  public Lvalue renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        left.renameIds(renamings, msgState), index.renameIds(renamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = left.findFreeVariables();
    freeVars.putAll(index.findFreeVariables());
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof LvalueBracket)) {
      return false;
    }
    LvalueBracket ourOther = (LvalueBracket) other;
    return ourOther.left.compare(left) && ourOther.index.compare(index);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        (Expression) left.replace(target, with), (Expression) index.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = left.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    return index.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> leftPair = left.toAnf(tuning);
    insert.addAll(leftPair.a);
    Pair<List<DeclarationStatement>, Ast> indexPair = index.toAnf(tuning);
    insert.addAll(indexPair.a);

    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        (Expression) leftPair.b, (Expression) indexPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Lvalue replaceType(Map<Type, Type> mapFromTo) {
    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        (Expression) left.replaceType(mapFromTo), (Expression) index.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        (Expression) left.replaceVtableAccess(project, msgState).get(),
        (Expression) index.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Lvalue normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        left.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get(),
        index.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    left.addDependency(modDepGraph, ourModNode, tenv);
    index.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return left.containsType(types) || index.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return left.containsAst(asts) || index.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = left.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution,
        hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newLeft = (Expression) tmp.get().ast.get();

    if (enableResolution) {
      if (left.getDeterminedType() instanceof ReferenceType) {
        // insert null check
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.nullPtrExc,
            new BinaryExpression(BinOp.EQUAL, newLeft, new NullExpression()), false, msgState));
      }
    }

    tmp = index.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newIndex = (Expression) tmp.get().ast.get();

    if (enableResolution) {
      // insert bounds check
      Type leftType = left.getDeterminedType();
      if (leftType instanceof ReferenceType) {
        leftType = ((ReferenceType) leftType).innerType;
      }
      if (leftType instanceof ArrayType) {
        // newIndex must be int value: (newIndex < 0 || newIndex >= newLeft.size())
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.outOfBoundsExc,
            new BinaryExpression(BinOp.OR,
                new BinaryExpression(BinOp.LT, newIndex, new IntegerExpression("0")),
                new BinaryExpression(BinOp.GTE, newIndex,
                    new FunctionCallExpression(Nullable.of(newLeft),
                        new IdentifierExpression("size"), new LinkedList<>(), "size"))), false,
            msgState));
      } else if (leftType instanceof MapType) {
        // newIndex must be a key value: (!newLeft.contains(newIndex))
        List<Expression> mapContainsArgs = new LinkedList<>();
        mapContainsArgs.add(newIndex);
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.outOfBoundsExc,
            new LeftUnaryOpExpression(LeftUnaryOp.NOT,
                new FunctionCallExpression(Nullable.of(newLeft),
                    new IdentifierExpression("contains"), mapContainsArgs, "contains")), false,
            msgState));
      } else {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
            "Left type of bracket must be either array or map (or reference to such)", lineNum,
            columnNum);
      }
    }

    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments, newLeft,
        newIndex);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public Nullable<Lvalue> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        left.liftClassDeclarations(liftedClasses, replacements).get(),
        index.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Lvalue> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        left.resolveUserTypes(msgState).get(),
        index.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Lvalue addThisExpression(ClassHeader classHeader) {
    LvalueBracket value = new LvalueBracket(lineNum, columnNum, preComments, postComments,
        left.addThisExpression(classHeader),
        index.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
