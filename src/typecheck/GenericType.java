package typecheck;

import ast.Expression;
import ast.Program.DestructorRenamings;
import ast.Statement;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;

public class GenericType extends UserInstanceType {

  public GenericType(String name) {
    super(TypeType.GENERIC_TYPE, Nullable.empty(), name);
  }

  @Override
  public List<Type> getInnerTypes() {
    return new LinkedList<>();
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type newTy = mapFromTo.get(this);
    if (newTy != null) {
      return newTy;
    }
    return this;
  }

  @Override
  public Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.GenericType.Builder subDoc = AstProto.GenericType.newBuilder();
    subDoc.setName(name);
    document.setGenericType(subDoc);
    return document;
  }

  public static GenericType deserialize(AstProto.GenericType document) {
    return new GenericType(document.getName());
  }

  @Override
  public Type setConst(boolean isConst) {
    return this;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(this);
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return new HashSet<>();
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return this;
  }

  @Override
  public List<String> toList() {
    List<String> namePath = new LinkedList<>();
    if (outerType.isNotNull()) {
      namePath.addAll(outerType.get().toList());
    }
    namePath.add(name);
    return namePath;
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "GenericType cannot have a tenv",
        lineNum, columnNum);
    return Optional.empty();
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    throw new IllegalArgumentException("Cannot calculate dummy value for generic type: " + this);
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    if (!isTop) {
      return this;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(this);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return 0; // nothing until we sub
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) {
    throw new IllegalArgumentException("must do this after resolving generics");
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return true;
  }

  @Override
  public Type asClassDeclType() {
    return this;
  }

  @Override
  public UserDeclType asDeclType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public ModuleType getModuleType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public UserDeclType asUserDeclType() {
    throw new UnsupportedOperationException();
  }
}
