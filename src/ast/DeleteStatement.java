package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.ArrayValue;
import interp.Environment;
import interp.InterpError;
import interp.MapValue;
import interp.ReferenceValue;
import interp.UserTypeInstanceValue;
import interp.Value;
import interp.VoidValue;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.ArrayType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class DeleteStatement extends AbstractStatement {

  public final Expression target;

  public DeleteStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression target) {
    super(lineNum, columnNum, AstType.DELETE_STATEMENT, preComments, postComments);
    this.target = target;

    target.setParentAst(this);
    determinedType = VoidType.Create();
  }
  
  public DeleteStatement(Expression target) {
    super(AstType.DELETE_STATEMENT);
    this.target = target;

    target.setParentAst(this);
    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    return preToString() + "delete " + target.toString(indent) + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> targetType = target.typecheck(tenv, Nullable.empty());
    if (!targetType.isPresent()) {
      return Optional.empty();
    }
    // check if deletable type (class, array, map, ect)

    if (!(targetType.get().type instanceof ReferenceType)) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Can only delete reference types, not: " + targetType, lineNum, columnNum);
    }
    ReferenceType refTy = (ReferenceType) targetType.get().type;
    Type innerTy = refTy.getInnerTypes().get(0);
    if (!(innerTy instanceof UserInstanceType || innerTy instanceof ArrayType
        || innerTy instanceof MapType)) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Can only delete references to class, array, or map, not reference to: " + innerTy,
          lineNum, columnNum);
    }

    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value targetValue = target.interp(env);

    if (!(targetValue instanceof ReferenceValue)) {
      throw new InterpError("Can only delete references, not: " + targetValue, lineNum, columnNum);
    }
    ReferenceValue refVal = (ReferenceValue) targetValue;
    if (!(refVal.referenced.getValue() instanceof UserTypeInstanceValue
        || refVal.referenced.getValue() instanceof ArrayValue
        || refVal.referenced.getValue() instanceof MapValue)) {
      throw new InterpError("Can only delete references to class, array, or map, not to: "
          + refVal.referenced.getValue(), lineNum, columnNum);
    }

    // TODO mark referenced value as deleted
    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.DeleteStatement.Builder subDoc = AstProto.DeleteStatement.newBuilder();

    subDoc.setTarget(target.serialize().build());

    document.setDeleteStatement(subDoc);
    return document;
  }

  public static DeleteStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.DeleteStatement document) throws SerializationError {
    return new DeleteStatement(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getTarget()));
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        (Expression) target.renameIds(renamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return target.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof DeleteStatement)) {
      return false;
    }
    DeleteStatement ourOther = (DeleteStatement) other;
    return ourOther.target.compare(target);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        (Expression) this.target.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return target.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    Pair<List<DeclarationStatement>, Ast> targetPair = target.toAnf(tuning);
    insert.addAll(targetPair.a);

    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        (Expression) targetPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        target.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        (Expression) this.target.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        this.target.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    target.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return target.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return target.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    
    List<Statement> preStatements = new LinkedList<>();
    
    if (enableResolution) {
      if (withinFn.isNotNull()) {
        ReferenceType refType = (ReferenceType) target.getDeterminedType();
        preStatements.addAll(ResolveRaiseException.sanityMemcheck(project,
            new SizeofExpression(refType.innerType), true,
            jsmntGlobalRenamings, hitExcId, withinFn.get(), msgState));
      }
    }

    Optional<ResolveExcRet> tmp = target.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution,
        hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);

    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        (Expression) tmp.get().ast.get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    /*
     * delete expr;
     * 
     * becomes
     * 
     * type* tmp_expr_0 = expr;
     * tmp_expr_0.destructor();
     * delete tmp_expr_0;
     */
    List<Statement> ourStatements = new LinkedList<>();
    final String tmpId = "tmp_delete_expr_" + getTemp();
    ourStatements.add(new DeclarationStatement(target.getDeterminedType(), tmpId,
        Nullable.of(target), false, false, 8));
    
    DotAccessType targetType = null;
    if (target.getDeterminedType() instanceof ReferenceType) {
      targetType = (DotAccessType) ((ReferenceType) target.getDeterminedType()).innerType;
    } else {
      targetType = (DotAccessType) target.getDeterminedType();
    }
    
    ourStatements.add(targetType.createDestructorCall(tmpId, destructorRenamings).get());
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        new IdentifierExpression(tmpId));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    ourStatements.add(value);
    return new InsertDestructorsRet(ourStatements);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        this.target.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        target.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    DeleteStatement value = new DeleteStatement(lineNum, columnNum, preComments, postComments,
        target.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }
}
