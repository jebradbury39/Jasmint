package interp;

import ast.BlockStatement;
import ast.DeclarationStatement;
import ast.FunctionCallExpression;
import errors.FatalMessageException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.Type;
import typecheck.UserInstanceType;

public class ClosureValue extends AbstractValue {

  public final List<DeclarationStatement> params;
  public final BlockStatement body;
  protected Environment env; //cloEnv, what the function was defined in
  public final FunctionCallExpression parentCall;
  public final UserInstanceType forClass; //this is the class type this closure is defined in
  
  public ClosureValue(Type fnType, List<DeclarationStatement> params, BlockStatement body, 
      Environment env, FunctionCallExpression parentCall, UserInstanceType forClass) throws InterpError {
    super(MetaValue.NORMAL, fnType);
    this.params = params;
    this.body = body;
    this.env = env;
    this.parentCall = parentCall;
    this.forClass = forClass;
  }
  
  public ClosureValue(MetaValue metaValue, Type fnType, List<DeclarationStatement> params,
      BlockStatement body, 
      Environment env, FunctionCallExpression parentCall, UserInstanceType forClass) throws InterpError {
    super(metaValue, fnType);
    this.params = params;
    this.body = body;
    this.env = env;
    this.parentCall = parentCall;
    this.forClass = forClass;
  }
  
  public Value interp(List<Value> paramValues) throws InterpError, FatalMessageException {
    if (paramValues.size() != params.size()) {
      throw new InterpError("mismatched # of args", -1, -1);
    }
    
    Environment newEnv = env.extend();
    
    Iterator<Value> iterValues = paramValues.iterator();
    Iterator<DeclarationStatement> iterNames = params.iterator();
    for (; iterValues.hasNext() && iterNames.hasNext();) {
      newEnv.defineVar(iterNames.next().name, VariableScoping.NORMAL, iterValues.next(), false);
    }
    
    //call the parentCall
    if (parentCall != null) {
      parentCall.interp(newEnv); //same argsEnv and bodyEnv since env is for the class instance
    }
    
    return body.interp(newEnv).setMetaValue(MetaValue.NORMAL);
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    return new StringValue("<closure::" + getType() + ">");
  }

  @Override
  public Value copy(Environment copyEnv) throws InterpError {
    if (copyEnv == null) {
      return new ClosureValue(metaValue, type, params, body, env, parentCall, forClass);
    }
    
    //when copying a class, need new link to INSTANCE (otherwise still pointing to old INSTANCE)
    //copyEnv must be the lowest INSTANCE (INSTANCE never has a NORMAL in their parents)
    //actually it could also be GLOBAL or STATIC
    //copyEnv is only non-null when copying a class closure
    
    //find our highest NORMAL
    //for example: GLOBAL -> STATIC -> INSTANCE -> NORMAL
    Environment bottomCC = null;
    Environment cc = null;
    Environment track = env;
    while (track != null) {
      if (track.envType == TypeEnvironment.ScopeType.NORMAL) {
        if (cc == null) {
          cc = track.lightCopy();
          bottomCC = cc;
        } else {
          cc.setParent(track.lightCopy());
          cc = cc.getParent();
        }
        track = track.getParent();
      } else {
        //hit ceiling
        if (cc == null) {
          cc = copyEnv;
          bottomCC = cc;
        } else {
          cc.setParent(copyEnv);
        }
        break;
      }
    }
    
    return new ClosureValue(metaValue, type, params, body, bottomCC, parentCall, forClass);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return this == other;
  }
  
  @Override
  public Value replaceType(Map<Type, Type> mapFromTo, String moduleAppend) throws InterpError {
    //but we also need to replace the types in our body
    //Host Functions have body == null
    return new ClosureValue(metaValue, type.replaceType(mapFromTo), params, 
        body == null ? null : (BlockStatement) body.replaceType(mapFromTo),
            env, parentCall, forClass);
  }

}
