package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.ReferenceValue;
import interp.Value;
import interp.ValueBox;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.ReferencedType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

//allocate heap memory
public class NewExpression extends AbstractExpression {

  public final Expression value; // NewClassInstance, InitArray, InitMap

  public NewExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression value) {
    super(lineNum, columnNum, AstType.NEW_EXPRESSION, preComments, postComments);
    this.value = value;

    value.setParentAst(this);
  }

  public NewExpression(int lineNum, int columnNum, Expression value) {
    super(lineNum, columnNum, AstType.NEW_EXPRESSION);
    this.value = value;

    value.setParentAst(this);
  }

  public NewExpression(Expression value) {
    super(AstType.NEW_EXPRESSION);
    this.value = value;

    value.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + "new " + value.toString(indent) + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> tyOpt = value.typecheck(tenv, Nullable.empty());
    if (!tyOpt.isPresent()) {
      return Optional.empty();
    }
    determinedType = new ReferenceType((ReferencedType) tyOpt.get().type);
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value val = value.interp(env);
    ValueBox heapMemoryBox = new ValueBox(val); // TODO record heap use
    return new ReferenceValue((ReferencedType) val.getType(), heapMemoryBox);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.NewExpression.Builder subDoc = AstProto.NewExpression.newBuilder();

    subDoc.setValue(value.serialize());

    document.setNewExpression(subDoc);
    return document;
  }

  public static NewExpression deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.NewExpression document) throws SerializationError {
    return new NewExpression(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getValue()));
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        value.renameIds(renamings, msgState));
    val.determinedType = determinedType.renameIds(renamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return value.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof NewExpression)) {
      return false;
    }
    NewExpression ourOther = (NewExpression) other;
    return value.compare(ourOther.value);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        (Expression) this.value.replace(target, with));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return value.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    Pair<List<DeclarationStatement>, Ast> valuePair = value.toAnf(tuning);

    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        (Expression) valuePair.b);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(valuePair.a, val);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        (Expression) value.replaceType(mapFromTo));
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        (Expression) value.replaceVtableAccess(project, msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments, value
        .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get());
    val.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(val));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    value.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return value.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return value.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    if (value instanceof NewClassInstanceExpression) {
      NewClassInstanceExpression newClass = (NewClassInstanceExpression) value;

      NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments, value);
      val.determinedType = determinedType;
      val.setOriginalLabel(getOriginalLabel());

      return newClass.constructorCall.resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, withinFnStatement, Nullable.of(val), enableResolution,
          hitExcId, msgState);
    }

    List<Statement> preStatements = new LinkedList<>();

    if (enableResolution) {
      if (withinFn.isNotNull()) {
        preStatements.addAll(
            ResolveRaiseException.sanityMemcheck(project,
                new SizeofExpression(value.getDeterminedType()),
                false, jsmntGlobalRenamings, hitExcId, withinFn.get(), msgState));
      }
    }

    Optional<ResolveExcRet> tmp = value.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize,
        msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);

    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        (Expression) tmp.get().ast.get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(val)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        value.insertDestructorCalls(scopedIds, destructorRenamings));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        value.liftClassDeclarations(liftedClasses, replacements).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        value.resolveUserTypes(msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    NewExpression val = new NewExpression(lineNum, columnNum, preComments, postComments,
        value.addThisExpression(classHeader));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
