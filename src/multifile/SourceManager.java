package multifile;

import audit.AuditEntry.AuditEntryType;
import audit.AuditEntryLite;
import audit.AuditRenameTypesLite;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.Nullable;
import java.util.Collection;
import java.util.ListIterator;
import java.util.Map;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;

/*
 * Sources may be renamed, so this class acts as a wrapper to keep things sane
 */
public class SourceManager {
  private Map<ModuleType, ModuleEndpoint> sources; //original names only (abs paths)
  
  public SourceManager(Map<ModuleType, ModuleEndpoint> sources) {
    this.sources = sources;
  }
  
  /*
   * Go backwards from new module type to original module type
   */
  private Nullable<ModuleType> realKey(ModuleType key, AuditTrailLite auditTrail) {
    ListIterator<AuditEntryLite> reverseIter = auditTrail.values().listIterator(
        auditTrail.values().size());
    while (reverseIter.hasPrevious()) {
      AuditEntryLite entry = reverseIter.previous();
      if (entry.getType() == AuditEntryType.RENAME_TYPES) {
        Nullable<ModuleType> tmp = ((AuditRenameTypesLite) entry).getOldType(key);
        if (tmp.isNull()) {
          return Nullable.empty();
        }
        key = tmp.get();
      }
    }
    return Nullable.of(key);
  }
  
  /*
   * Pass in the auditTrail so far (if replaying audit trail, only give subset seen so far)
   */
  public Nullable<ModuleEndpoint> get(ModuleType moduleType, AuditTrailLite auditTrail)
      throws FatalMessageException {
    Nullable<ModuleType> realType = realKey(moduleType, auditTrail);
    if (realType.isNull()) {
      return Nullable.empty();
    }
    ModuleEndpoint modEp = sources.get(realType.get());
    if (modEp == null) {
      return Nullable.empty();
    }
    return Nullable.of(modEp);
  }
  
  public Nullable<ModuleEndpoint> get(ResolvedImportType importType, AuditTrailLite auditTrail)
      throws FatalMessageException {
    return get(importType.asModuleType(), auditTrail);
  }

  public Collection<ModuleEndpoint> values(AuditTrailLite auditTrail) throws FatalMessageException {
    return sources.values();
  }

  public void put(ModuleType key, ModuleEndpoint value, AuditTrailLite auditTrail) {
    if (auditTrail.contains(AuditEntryType.RENAME_TYPES)) {
      throw new IllegalArgumentException();
    }
    sources.put(key, value);
  }

  public boolean containsKey(ModuleType moduleType, AuditTrailLite auditTrail) {
    Nullable<ModuleType> realType = realKey(moduleType, auditTrail);
    if (realType.isNull()) {
      return false;
    }
    return sources.containsKey(realType.get());
  }
}
