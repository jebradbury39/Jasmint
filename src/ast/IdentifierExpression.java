package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.ClassHeader.ClassMember;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.ValueBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.BasicTypeBox;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.SandboxModeInfo;
import type_env.VariableScoping;
import typecheck.AnyType;
import typecheck.ClassDeclType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class IdentifierExpression extends AbstractExpression {

  public final String id;

  private VariableScoping scoping = null; /* only determined by typechecker */

  public IdentifierExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, String id) {
    super(lineNum, columnNum, AstType.IDENTIFIER_EXPRESSION, preComments, postComments);
    this.id = id;
    if (id.isEmpty()) {
      throw new IllegalArgumentException();
    }
  }

  public IdentifierExpression(int lineNum, int columnNum, String id) {
    super(lineNum, columnNum, AstType.IDENTIFIER_EXPRESSION);
    this.id = id;
    if (id.isEmpty()) {
      throw new IllegalArgumentException();
    }
  }

  public IdentifierExpression(String id) {
    super(AstType.IDENTIFIER_EXPRESSION);
    this.id = id;
    if (id.isEmpty()) {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public String toString(String indent) {
    return preToString() + id + postToString();
  }

  public VariableScoping getScoping() {
    return scoping;
  }

  public void setScoping(VariableScoping scoping) {
    this.scoping = scoping;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    if (id.equals("this")) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Cannot use 'this' as an identifier", lineNum, columnNum);
    }

    Nullable<TypeBox> tyBox = tenv.lookup(id, lineNum, columnNum);
    if (tyBox.isNull()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, id + " is not defined", lineNum,
          columnNum);
      return Optional.empty();
    }
    if (!tyBox.get().getIsInit()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, id + " is not initialized",
          lineNum, columnNum);
      return Optional.empty();
    }
    if (tyBox.get() instanceof BasicTypeBox) {
      BasicTypeBox basicTyBox = (BasicTypeBox) tyBox.get();
      checkVisibilityLevel(tenv, id, basicTyBox, tenv.msgState);
    }

    scoping = tenv.lookupScoping(id, null); // not an overloaded function
    determinedType = tyBox.get().getType();
    
    if (determinedType instanceof FunctionType) {
      FunctionType fnType = (FunctionType) determinedType;
      if (fnType.lambdaStatus == LambdaStatus.SANDBOX_NOT_LAMBDA) {
        
        // sandbox check
        Nullable<SandboxModeInfo> sandboxModeInfo = tenv.inSandboxMode();
        if (sandboxModeInfo.isNotNull()) {
          tenv.msgState.addMessage(MsgType.ERROR, MsgClass.SANDBOX,
              "can only call this function, but not access it as a lambda, in sandbox mode ("
                  + sandboxModeInfo + ")", lineNum, columnNum);
          return Optional.empty();
        } else {
          tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "should not be able to access sandbox-defined function outside of sandbox",
              lineNum, columnNum);
          return Optional.empty();
        }
      }
    }

    return Optional.of(new TypecheckRet(determinedType));
  }

  public Optional<TypeBox> typecheckAsFunction(TypeEnvironment tenv, DotAccessType within,
      List<Type> argTypes) throws FatalMessageException {
    /*
     * All we have is the id, within, and argTypes. However, we don't know if we are
     * getting an anonymous or not (only if within is a ClassType/ClassDeclType)
     */
    FunctionType queryFnType = new FunctionType(within, AnyType.Create(), argTypes,
        LambdaStatus.UNKNOWN);

    Nullable<TypeBox> exprTyBox = tenv.lookup(id, queryFnType, lineNum, columnNum);
    if (exprTyBox.isNull()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          id + " does not have a matching overload: " + argTypes, lineNum, columnNum);
      return Optional.empty();
    }
    if (!exprTyBox.get().getIsInit()) {
      // throw new TypeError(id + " is not initialized in memory with overload: "
      // + argTypes, lineNum, columnNum);
    }
    if (exprTyBox.get() instanceof BasicTypeBox) {
      BasicTypeBox basicTyBox = (BasicTypeBox) exprTyBox.get();
      checkVisibilityLevel(tenv, id, basicTyBox, tenv.msgState);
    }

    scoping = tenv.lookupScoping(id, queryFnType);
    
    determinedType = exprTyBox.get().getType();
    return Optional.of(exprTyBox.get());
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    ValueBox valBox = env.lookup(id);
    if (valBox == null) {
      throw new InterpError(id + " is not defined", lineNum, columnNum);
    }
    if (valBox.getValue() == null) {
      throw new InterpError(id + " is not initialized", lineNum, columnNum);
    }
    return valBox.getValue(); // notice that this is NOT by copy
  }

  public Value interpAsFunction(Environment env, List<Value> argValues)
      throws InterpError, FatalMessageException {
    List<Type> argTypes = new ArrayList<Type>();
    for (Value argVal : argValues) {
      argTypes.add(argVal.getType());
    }

    ValueBox exprValBox = env.lookup(id, new FunctionType(Nullable.empty(), null, argTypes,
        LambdaStatus.UNKNOWN));
    if (exprValBox == null) {
      throw new InterpError(id + " does not have a matching overload: " + argValues, lineNum,
          columnNum);
    }
    if (exprValBox.getValue() == null) {
      throw new InterpError(id + " is not initialized in memory with overload: " + argValues,
          lineNum, columnNum);
    }
    return exprValBox.getValue(); // don't pass functions around by copy
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.IdentifierExpression.Builder subDoc = AstProto.IdentifierExpression.newBuilder();

    subDoc.setId(id);
    if (scoping == null) {
      scoping = VariableScoping.UNKNOWN; // only for ffi
    }
    subDoc.setScoping(scoping.serialize());

    document.setIdentifierExpression(subDoc);
    return document;
  }

  public static IdentifierExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.IdentifierExpression document) throws SerializationError {
    IdentifierExpression result = new IdentifierExpression(lineNum, columnNum, preComments,
        postComments, document.getId());
    result.scoping = VariableScoping.deserialize(document.getScoping());
    return result;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Nullable<DotAccessType> leftTy = Nullable.empty();
    if (scoping == VariableScoping.GLOBAL) {
      leftTy = Nullable.of(renamings.moduleType);
    }
    return renameIdsCustom(renamings, leftTy, msgState);
  }

  public Expression renameIdsCustom(Renamings renamings, Nullable<DotAccessType> leftTy,
      MsgState msgState) throws FatalMessageException {

    Nullable<String> newId = renamings.lookup(id, determinedType, leftTy);
    if (newId.isNotNull()) {
      // if a class field, or left is class
      // (so renaming in a class context) find out what class and check the vtable

      IdentifierExpression value = new IdentifierExpression(lineNum, columnNum, preComments,
          postComments, newId.get());
      value.determinedType = determinedType.renameIds(renamings, msgState);
      value.setOriginalLabel(getOriginalLabel());
      return value;
    }
    return this;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<String, Type>();
    if (!(determinedType instanceof ClassDeclType) && !(determinedType instanceof ModuleType)) {
      freeVars.put(id, determinedType);
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof IdentifierExpression)) {
      return false;
    }
    IdentifierExpression ourOther = (IdentifierExpression) other;
    return ourOther.id.equals(id);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<>(new LinkedList<>(), this);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    String newId = id;
    Type ourType = determinedType;
    /* TODO we should be able to remove this, since DotExpression will be converted to
     * StaticClassIdExpr, and we will never use a module type unless it is in a dot
    if (ourType instanceof ModuleType) {
      Type newType = mapFromTo.get(ourType);
      if (newType != null) {
        Expression val = DotExpression.fromStaticList(((ModuleType) newType).toList(), lineNum,
            columnNum);
        return val;
      }
    }
    */

    IdentifierExpression val = new IdentifierExpression(lineNum, columnNum, preComments,
        postComments, newId);
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.scoping = scoping;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    // can only do polymorphism in function call
    return Optional.of(this);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState) {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize,
      MsgState msgState)
      throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return this;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    Nullable<List<ClassMember>> members = classHeader.lookup(id);
    if (members.isNotNull()) {
      // make sure these members are not static
      for (ClassMember member : members.get()) {
        if (member.isStatic) {
          return this;
        }
      }
      // we are within the class as a member, so prepend with 'this.'
      ThisExpression thisExpr = new ThisExpression();
      thisExpr.setOriginalLabel(thisExpr.label);
      
      DotExpression dotExpr = new DotExpression(lineNum, columnNum, preComments, postComments,
          thisExpr, this);
      dotExpr.determinedType = determinedType;
      dotExpr.setOriginalLabel(dotExpr.label);
      return dotExpr;
    }
    return this;
  }

}
