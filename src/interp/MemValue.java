package interp;

import typecheck.Type;

/* Lvalues return these */
public class MemValue extends AbstractValue {

  public final ValueBox location; /* memory location */
  
  public MemValue(Type type, ValueBox location) throws InterpError {
    super(MetaValue.NORMAL, type);
    if (location == null) {
      throw new InterpError("MemValue location cannot be null", -1, -1);
    }
    this.location = location;
  }
  
  public MemValue(MetaValue metaValue, Type type, ValueBox location) throws InterpError {
    super(metaValue, type);
    if (location == null) {
      throw new InterpError("MemValue location cannot be null", -1, -1);
    }
    this.location = location;
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    return new StringValue("<memory location>");
  }

  @Override
  public Value copy(Environment copyEnv) {
    return this; //this is also immutable (altho the value in the box will change
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    if (this == other) {
      return true;
    }
    return location == ((MemValue) other).location;
  }

}
