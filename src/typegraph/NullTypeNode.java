package typegraph;

import java.util.Map;
import java.util.Set;
import typecheck.AbstractType.TypeType;
import typecheck.AnyType;
import typecheck.NullType;
import typecheck.Type;
import typegraph.TypeGraph.CastDirection;

public class NullTypeNode extends AbstractTypeNode {

  public NullTypeNode() {
    super();
  }
  
  @Override
  public int hashCode() {
    return TypeType.NULL_TYPE.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    return other instanceof NullTypeNode;
  }

  @Override
  public boolean typeEquals(Type type, boolean looseMatch) {
    return type instanceof NullType || type instanceof AnyType;
  }

  @Override
  public boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph, Set<TypeNode> visited) {
    if (checkVisited(visited)) {
      return false;
    }
    if (fromTy instanceof AnyType || toTy instanceof AnyType) {
      return true;
    }
    if (!typeEquals(fromTy, false)) {
      return false; //should be a redundant check
    }
    return typeEquals(toTy, false); //can't cast down, and nowhere to cast up to
  }

  @Override
  public Type getType() {
    return NullType.Create();
  }

}
