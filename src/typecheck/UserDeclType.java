package typecheck;

import errors.FatalMessageException;
import errors.Nullable;
import java.util.List;
import java.util.Optional;
import type_env.TypeEnvironment;

public abstract class UserDeclType extends AbstractType implements DotAccessType, ImportableType {

  public final Nullable<ModuleType> outerType; //parent module. If null, not normalized yet
  public final String name;
  
  public UserDeclType(TypeType typeType, Nullable<ModuleType> outerType, String name) {
    super(typeType);
    this.outerType = outerType;
    this.name = name;
  }
  
  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof UserDeclType)) {
      return false;
    }
    UserDeclType otherUt = (UserDeclType) other;
    return name.equals(otherUt.name) && outerType.equals(otherUt.outerType);
  }
  
  @Override
  public int hashCode() {
    return outerType.hashCode() + name.hashCode();
  }
  
  @Override
  public String toString() {
    String res = "";
    if (outerType.isNotNull()) {
      res += outerType.toString() + ".";
    }
    return res + name;
  }
  
  public abstract List<String> toList();
  
  public abstract UserDeclType fromList(List<String> list);
  
  public abstract Optional<TypeEnvironment> getTenv(TypeEnvironment tenv,
      int lineNum, int columnNum) throws FatalMessageException;

  public abstract UserInstanceType asInstanceType();

  public ModuleType getModuleType() {
    if (this instanceof ModuleType) {
      return (ModuleType) this;
    }
    if (this instanceof ImportType) {
      return ((ImportType) this).asModuleType();
    }
    return outerType.get();
  }
}
