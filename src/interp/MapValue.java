package interp;

import errors.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.IntType;
import typecheck.MapType;
import typecheck.Type;
import typecheck.VoidType;

public class MapValue extends AbstractValue {

  /* memory */
  public final Map<String, ValueBox> pairs = new HashMap<String, ValueBox>();

  public MapValue(Type keyType, Type valueType, Map<Value, Value> initialPairs) {
    super(MetaValue.NORMAL, new MapType(keyType, valueType));
    for (Entry<Value, Value> entry : initialPairs.entrySet()) {
      try {
        ValueBox box = new ValueBox(entry.getValue());
        pairs.put(entry.getKey().toString(), box);
      } catch (InterpError e) {
        throw new IllegalArgumentException(e.getMessage());
      }
    }
  }

  public MapValue(MetaValue metaValue, Type keyType, Type valueType,
      Map<Value, Value> initialPairs) {
    super(metaValue, new MapType(keyType, valueType));
    for (Entry<Value, Value> entry : initialPairs.entrySet()) {
      try {
        ValueBox box = new ValueBox(entry.getValue());
        pairs.put(entry.getKey().toString(), box);
      } catch (InterpError e) {
        throw new IllegalArgumentException(e.getMessage());
      }
    }
  }

  private MapValue(Type type, Map<String, ValueBox> pairs) {
    super(MetaValue.NORMAL, type);
    this.pairs.clear();
    this.pairs.putAll(pairs);
  }

  public ValueBox getValueByKey(Value idxValue) throws InterpError {
    if (!pairs.containsKey(idxValue.toString())) {
      // TODO this should add a default value and return that box
      throw new InterpError("no entry [" + idxValue + "] in map: " + toString(), -1, -1);
    }
    return pairs.get(idxValue.toString());
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    ArrayList<String> kvStr = new ArrayList<String>();
    for (Entry<String, ValueBox> entry : pairs.entrySet()) {
      String tmp = entry.getKey() + ": " + entry.getValue().getValue();
      kvStr.add(tmp);
    }
    Collections.sort(kvStr);

    String result = "{";
    boolean first = true;
    for (String kv : kvStr) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += kv;
    }
    return new StringValue(leftValue + result + "}");
  }

  @Override
  public Value copy(Environment copyEnv) throws InterpError {
    Map<Value, Value> newPairs = new HashMap<Value, Value>();

    for (Entry<String, ValueBox> pair : pairs.entrySet()) {
      newPairs.put(new StringValue(pair.getKey()), pair.getValue().getValue().copy(null));
    }

    return new MapValue(metaValue, type.getInnerTypes().get(0), type.getInnerTypes().get(1),
        newPairs);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    if (this == other) {
      return true;
    }
    MapValue otherMap = (MapValue) other;
    if (pairs.entrySet().size() != otherMap.pairs.entrySet().size()) {
      return false;
    }

    for (Entry<String, ValueBox> ourPair : pairs.entrySet()) {
      // find corresponding entry in other map, if exists, then compare values
      ValueBox otherPairValue = otherMap.pairs.get(ourPair.getKey());
      if (!ourPair.getValue().equals(otherPairValue)) {
        return false;
      }
    }

    return true;
  }

  public Environment getEnv(Environment env) throws InterpError {
    Map<String, IdToValue> topIds = new HashMap<String, IdToValue>();

    // put
    List<Type> addParam = new ArrayList<Type>();
    addParam.add(type.getInnerTypes().get(0));
    addParam.add(type.getInnerTypes().get(1));
    topIds.put("put", new MapFunctionToValue().write("put",
        new ValueBox(HostFunctionValue.createHostFunctionValue(new FunctionType(Nullable.empty(),
            VoidType.Create(), addParam, LambdaStatus.NOT_LAMBDA), (List<Value> paramValues) -> {
              pairs.put(paramValues.get(0).toString(), new ValueBox(paramValues.get(1).copy(null)));
              return new VoidValue();
            }), VariableScoping.CLASS_INSTANCE)));

    // remove
    List<Type> removeParam = new ArrayList<Type>();
    removeParam.add(type.getInnerTypes().get(0));
    topIds.put("remove", new MapFunctionToValue().write("remove",
        new ValueBox(HostFunctionValue.createHostFunctionValue(new FunctionType(Nullable.empty(),
            type.getInnerTypes().get(0), removeParam, LambdaStatus.NOT_LAMBDA),
            (List<Value> paramValues) -> {
              // remove at idx and return removed value
              String idx = paramValues.get(0).toString();
              if (!pairs.containsKey(idx)) {
                throw new InterpError("cannot remove non-existent entry from map: " + idx, -1, -1);
              }

              return pairs.remove(idx).getValue();
            }), VariableScoping.CLASS_INSTANCE)));

    // size
    List<Type> sizeParam = new ArrayList<Type>();
    topIds.put("size", new MapFunctionToValue().write("size",
        new ValueBox(HostFunctionValue.createHostFunctionValue(new FunctionType(Nullable.empty(),
            IntType.Create(false, 64), sizeParam, LambdaStatus.NOT_LAMBDA),
            (List<Value> paramValues) -> {
              return new IntValue(pairs.entrySet().size(), IntType.Create(false, 64));
            }), VariableScoping.CLASS_INSTANCE)));

    return new Environment(topIds, TypeEnvironment.ScopeType.INSTANCE, null, env.typeGraph,
        env.project);
  }

}
