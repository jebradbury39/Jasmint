package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.BasicTypeBox;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.Pair;
import util.ScopedIdentifiers;

public class CatchExcSection extends AbstractExpression {

  public final String id; // This will be of type Expression*, and will be the thrown expression
  public final BlockStatement body;

  public CatchExcSection(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, String id, BlockStatement body) {
    super(lineNum, columnNum, AstType.CATCH_EXC_SECTION, preComments, postComments);
    this.id = id;
    this.body = body;

    body.setParentAst(this);

    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    return "catch " + id + " " + body.toString(indent);
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    // extend tenv with our id and set it equal to the thrown expression (must be
    // Exception*)
    TypeEnvironment newTenv = tenv.extend();
    newTenv.defineVar(id, new BasicTypeBox(JsmntGlobal.exceptionRefType, true, true,
        VariableScoping.NORMAL));

    return body.typecheck(newTenv, expectedReturnType);
  }

  public Optional<Type> typecheckAsFunctionBody(TypeEnvironment tenv, Type returnType)
      throws FatalMessageException {
    TypeEnvironment newTenv = tenv.extend();
    newTenv.defineVar(id, new BasicTypeBox(JsmntGlobal.exceptionRefType, true, true,
        VariableScoping.NORMAL));

    TypeEnvironment bodyTenv = newTenv.extend();
    return body.typecheckAsFunctionBody(bodyTenv, returnType);
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.CatchExcSection.Builder subDoc = AstProto.CatchExcSection.newBuilder();

    subDoc.setId(id);
    subDoc.setBody(body.serialize());

    AstProto.Ast.Builder document = preSerialize();
    document.setCatchExcSection(subDoc);
    return document;
  }

  public static CatchExcSection deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.CatchExcSection document) throws SerializationError {

    return new CatchExcSection(lineNum, columnNum, preComments, postComments, document.getId(),
        (BlockStatement) AbstractStatement.deserialize(document.getBody()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = body.findFreeVariables();
    freeVars.remove(id);
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof CatchExcSection)) {
      return false;
    }
    CatchExcSection ourOther = (CatchExcSection) other;

    return id.equals(ourOther.id) && body.equals(ourOther.body);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        (BlockStatement) body.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return body.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    Pair<List<DeclarationStatement>, Ast> tmpPair = body.toAnf(tuning);
    insert.addAll(tmpPair.a);
    BlockStatement newBody = (BlockStatement) tmpPair.b;

    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        newBody);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        (BlockStatement) body.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        (BlockStatement) body.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    body.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return body.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return body.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    // if (tmp_fn_ctx.exc != null) {
    //   jsmnt_global.Exception* e = tmp_fn_ctx.exc;
    //   <catch body>
    // }
    
    Optional<ResolveExcRet> tmp = body.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, false, "", stackSize,
        msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    BlockStatement newBody = (BlockStatement) tmp.get().ast.get();

    /*
     * Copy the thrown exception to a stack variable, then free the throw exc from
     * the heap
     */
    List<Statement> newCatchStatements = new LinkedList<>();
    newCatchStatements.add(new DeclarationStatement(JsmntGlobal.exceptionClassType, id,
        Nullable.of(new DereferenceExpression(
            new DotExpression(
                new IdentifierExpression(hitExcId),
                new IdentifierExpression(jsmntGlobalRenamings.exc)))),
        false, false, 8));
    newCatchStatements.add(new DeleteStatement(
        new DotExpression(
            new IdentifierExpression(hitExcId),
            new IdentifierExpression(jsmntGlobalRenamings.exc))));
    newCatchStatements.addAll(newBody.statements);
    BlockStatement newCatchBody = new BlockStatement(newCatchStatements);
    
    ConditionalStatement ifExcStatement = new ConditionalStatement(
        new BinaryExpression(BinOp.NOT_EQUAL,
            new DotExpression(
                new IdentifierExpression(hitExcId),
                new IdentifierExpression(jsmntGlobalRenamings.exc)),
            new NullExpression()),
        newCatchBody, Nullable.empty());
    
    return Optional.of(new ResolveExcRet(ifExcStatement));
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Renamings scopedRenamings = renamings.extend();

    String newId = scopedRenamings.add(id, JsmntGlobal.exceptionRefType, Nullable.empty(), label);

    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments,
        newId, (BlockStatement) body.renameIds(scopedRenamings, msgState));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        (BlockStatement) body
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {

    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        (BlockStatement) body.insertDestructorCalls(scopedIds, destructorRenamings).statements
            .get(0));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        (BlockStatement) body.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        (BlockStatement) body.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    CatchExcSection value = new CatchExcSection(lineNum, columnNum, preComments, postComments, id,
        (BlockStatement) body.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
