package interp;

import typecheck.ReferenceType;
import typecheck.ReferencedType;

public class ReferenceValue extends AbstractValue {
  
  public final ValueBox referenced;

  public ReferenceValue(ReferencedType referencedType, ValueBox referenced) {
    super(MetaValue.NORMAL, new ReferenceType(referencedType));
    this.referenced = referenced;
    if (referenced == null) {
      throw new IllegalArgumentException("cannot reference an invalid memory location: null");
    }
    if (referenced.getValue() == null) {
      throw new IllegalArgumentException("cannot reference host lang null");
    }
  }
  
  public ReferenceValue(MetaValue metaValue, ReferencedType referencedType, ValueBox referenced) {
    super(metaValue, new ReferenceType(referencedType));
    this.referenced = referenced;
    if (referenced == null) {
      throw new IllegalArgumentException("cannot reference an invalid memory location: null");
    }
    if (referenced.getValue() == null) {
      throw new IllegalArgumentException("cannot reference host lang null");
    }
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    return new StringValue(leftValue + "<reference::" + referenced.getValue().getType() + ">");
  }

  @Override
  public Value copy(Environment copyEnv) {
    //same referenced memory
    return new ReferenceValue(metaValue, (ReferencedType) type.getInnerTypes().get(0), referenced);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    if (this == other) {
      return true;
    }
    if (other instanceof NullValue) {
      return false;
    }
    return referenced.getValue() == ((ReferenceValue)other).referenced.getValue();
  }

}
