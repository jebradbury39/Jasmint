package type_env;

import errors.FatalMessageException;
import header.ClassHeader;
import java.util.Map;
import java.util.Optional;
import typecheck.ImportType;
import typecheck.Type;

// class header creates this
public class ClassTypeBox extends UserDeclTypeBox {
  public final ClassHeader classHeader;
  
  public ClassTypeBox(ClassHeader classHeader, boolean export, VariableScoping scoping) {
    super(export, true, scoping);
    this.classHeader = classHeader;
  }
  
  @Override
  public String toString() {
    return "ClassDecl(" + classHeader.absName + ", export = " + export + ")";
  }

  @Override
  public TypeBox fullCopy() {
    return new ClassTypeBox(classHeader, export, scoping);
  }

  @Override
  public TypeBox replaceType(TypeEnvironment tenv, Map<Type, Type> mapFromTo, int lineNum,
      int columnNum) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  protected Optional<TypeEnvironment> getTenv(TypeEnvironment tenv) throws FatalMessageException {
    return classHeader.getTenv(tenv, true, classHeader.absName.asInstanceType());
  }

  @Override
  public Type getType() {
    return classHeader.absName;
  }

  @Override
  public ImportType getImportType(ImportType importType) {
    return classHeader.absName.asImportType();
  }
}
