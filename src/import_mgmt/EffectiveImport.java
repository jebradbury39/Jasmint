package import_mgmt;

import ast.SerializationError;
import astproto.AstProto;
import audit.AuditRenameTypesLite;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import java.util.Optional;
import multifile.Project;
import transpile.BuiltinFunctionOverloads.NamespaceName;
import typecheck.AbstractType;
import typecheck.ImportType;
import typecheck.ModuleImportType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.ResolvedImportType;

/*
 * These are stored in EffectiveImports
 */

public class EffectiveImport {
  public static enum EffectiveImportType {
    LITERAL_IMPORT, // this module has an ImportStatement for this
    IMPLIED_IMPORT, // we are able to access this module indirectly
    INHERITED_IMPORT, // some import of this module imports this, so we are able to see it, too
    PARENT_IMPORT, // not a real import, just our parent module (needed for type normalization)
  }
  
  public static enum ImportUsage {
    INVALID, //error if we attempt to use this. Meant for downlinks in ClassGraph
    NONE, //the user told us this import, but never used in the module
    SOFT, //The import is only used via pointers
    HARD  //The import is used as stack types (and pointers, maybe)
  }

  public final EffectiveImportType importType;
  public final ImportType absPath;
  //if null, project is unknown rn. Should only be null if we have not linked yet
  public Nullable<ProjectNameVersion> withinProject;
  private ImportUsage usage = ImportUsage.NONE;
  public final boolean isSubmode; //can only be true if importType is implied
  
  public EffectiveImport(EffectiveImportType importType, boolean isSubmode,
      ImportType absPath,
      Nullable<ProjectNameVersion> tmpWithinProj) {
    this.importType = importType;
    this.isSubmode = isSubmode;
    this.absPath = absPath;
    this.withinProject = tmpWithinProj;
  }
  
  public EffectiveImport(EffectiveImportType importType, boolean isSubmode, ModuleType absPath,
      Nullable<ProjectNameVersion> tmpWithinProj) {
    this.importType = importType;
    this.isSubmode = isSubmode;
    this.absPath = new ModuleImportType(absPath.outerType, absPath.name);
    this.withinProject = tmpWithinProj;
  }
  
  public void setUsage(ImportUsage usage) {
    this.usage = usage;
  }
  
  public ImportUsage getUsage() {
    return usage;
  }
  
  @Override
  public String toString() {
    return absPath + " ~ " + importType + " ~ " + usage + " withinProj = " + withinProject;
  }

  public AstProto.EffectiveImport.Builder serialize() throws SerializationError {
    AstProto.EffectiveImport.Builder document = AstProto.EffectiveImport.newBuilder();
    
    document.setImportType(importType.ordinal());
    document.setIsSubmode(isSubmode);
    document.setAbsPath(absPath.serialize());
    if (withinProject.isNull()) {
      throw new SerializationError("Should have found out withinProject by now", true);
    }
    document.setWithinProject(withinProject.get().serialize());
    document.setImportUsage(usage.ordinal());
    
    return document;
  }

  public static EffectiveImport deserialize(AstProto.EffectiveImport document)
      throws SerializationError {
    EffectiveImport imp = new EffectiveImport(EffectiveImportType.values()[document.getImportType()],
        document.getIsSubmode(), (ImportType) AbstractType.deserialize(document.getAbsPath()),
        Nullable.of(ModuleType.ProjectNameVersion.deserialize(document.getWithinProject())));
    imp.usage = ImportUsage.values()[document.getImportUsage()];
    return imp;
  }

  public Nullable<EffectiveImport> applyTypeRenamings(AuditRenameTypesLite renameTransform) {
    Nullable<ResolvedImportType> newType = renameTransform.getNewType((ResolvedImportType) absPath);
    EffectiveImport newImp = new EffectiveImport(importType, isSubmode, newType.get(),
        withinProject);
    newImp.usage = usage;
    return Nullable.of(newImp);
  }

  public NamespaceName getNamespaceName() {
    return new NamespaceName(withinProject.get(), absPath);
  }

  public Optional<EffectiveImport> resolveImports(Project project, MsgState msgState)
      throws FatalMessageException {
    Optional<ImportType> resolvedImpTy = project.resolveImportType(absPath, project.getAuditTrail(),
        msgState, -1, -1);
    if (!resolvedImpTy.isPresent()) {
      return Optional.empty();
    }
    EffectiveImport effImp = new EffectiveImport(importType, isSubmode, resolvedImpTy.get(), withinProject);
    effImp.usage = usage;
    return Optional.of(effImp);
  }
}
