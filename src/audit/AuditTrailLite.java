package audit;

import ast.SerializationError;
import astproto.AstProto;
import audit.AuditEntry.AuditEntryType;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import multifile.Project;

public class AuditTrailLite {
  private List<AuditEntryLite> entries;
  
  public AuditTrailLite() {
    this.entries = new LinkedList<>();
  }
  
  public AuditTrailLite(AuditTrailLite other) {
    this.entries = new LinkedList<>(other.entries);
  }

  @Override
  public String toString() {
    return entries.toString();
  }

  public Iterator<AuditEntryLite> iterator() {
    return entries.iterator();
  }

  public boolean contains(AuditEntryType type) {
    for (AuditEntryLite entry : entries) {
      if (entry.getType() == type) {
        return true;
      }
    }
    return false;
  }

  public void add(AuditEntryLite entry) {
    entries.add(entry);
  }
  
  public void pop() {
    if (entries.isEmpty()) {
      return;
    }
    entries.remove(entries.size() - 1);
  }

  public List<AuditEntryLite> values() {
    return entries;
  }
  
  public static AuditTrailLite deserialize(Project project, AstProto.AuditTrailLite document)
      throws SerializationError {
    AuditTrailLite auditTrail = new AuditTrailLite();
    
    for (AstProto.AuditEntryLite entry : document.getEntriesList()) {
      AuditEntryType type = AuditEntryType.values()[entry.getAuditEntryType()];
      AuditEntryLite newEntry = null;
      switch (type) {
        case RENAME_TYPES:
          newEntry = AuditRenameTypesLite.deserialize(entry.getAuditRenameTypesLite());
          break;
        default:
          newEntry = new AuditBasic(type);
          break;
      }
      auditTrail.add(newEntry);
    }
    
    return auditTrail;
  }

  public AstProto.AuditTrailLite.Builder serialize() {
    AstProto.AuditTrailLite.Builder document = AstProto.AuditTrailLite.newBuilder();
    
    for (AuditEntryLite entry : entries) {
      document.addEntries(entry.serializeLite());
    }
    
    return document;
  }

  public int size() {
    return entries.size();
  }

  public AuditEntryLite getLast() {
    return entries.get(entries.size() - 1);
  }
}
