package util;

import java.util.ArrayList;
import java.util.List;

public class StringFn {
  public static String ltrim(String s) {
    int i = 0;
    while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
      i++;
    }
    return s.substring(i);
  }

  public static String rtrim(String s) {
    int i = s.length()-1;
    while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
      i--;
    }
    return s.substring(0,i+1);
  }
  
  public static List<String> split(char delim, String s) {
    List<String> tokens = new ArrayList<>();
    String currentToken = "";
    for (int i = 0; i < s.length(); ++i) {
      if (s.charAt(i) == delim) {
        if (!currentToken.isEmpty()) {
          tokens.add(currentToken);
        }
        currentToken = "";
      } else {
        currentToken += s.charAt(i);
      }
    }
    if (!currentToken.isEmpty()) {
      tokens.add(currentToken);
    }
    return tokens;
  }
}
