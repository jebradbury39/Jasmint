package classgraph;

import audit.AuditTrailLite;
import errors.FatalMessageException;
import import_mgmt.EffectiveImport.ImportUsage;
import java.util.HashSet;
import java.util.Set;
import multifile.SourceManager;
import typecheck.ModuleType;
import typecheck.UserDeclType;

/*
 * Can only have an uplink to another module
 */
public class ModuleNode extends Node {
  private Set<ClassUsageLink> uplinks = new HashSet<>();
  private Set<ClassUsageLink> downlinks = new HashSet<>(); //usage not useful, but same pattern
  
  public final ModuleType moduleType; //abs module type
  
  public ModuleNode(ModuleType moduleType) {
    this.moduleType = moduleType;
  }
  
  @Override
  public String toString() {
    String upStr = "";
    String downStr = "";
    for (ClassUsageLink up : uplinks) {
      upStr += up.node.getDeclType() + "<" + up.getUsage() + "> ";
    }
    for (ClassUsageLink down : downlinks) {
      downStr += down.node.getDeclType() + "<" + down.getUsage() + "> ";
    }
    return "(ModuleNode " + getDeclType() + "(uplinks: " + upStr + ") (downlinks: " + downStr + "))\n";
  }
  
  @Override
  public int hashCode() {
    return moduleType.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof ModuleNode)) {
      return false;
    }
    ModuleNode otherMod = (ModuleNode) other;
    return moduleType.equals(otherMod.moduleType);
  }

  @Override
  public UserDeclType getDeclType() {
    return moduleType;
  }
  
  public void determineRebuilds(SourceManager sources, AuditTrailLite auditTrail,
      boolean rebuild) throws FatalMessageException {
    if (visited) {
      return;
    }
    visited = true;
    if (rebuild) {
      sources.get(moduleType, auditTrail).get().getHandle().needRebuild = rebuild;
    }
    
    for (ClassUsageLink downlink : downlinks) {
      if (downlink.node instanceof ModuleNode) {
        ModuleNode modNode = (ModuleNode) downlink.node;
        modNode.determineRebuilds(sources, auditTrail, rebuild);
      }
    }
  }

  @Override
  public boolean allUplinksFound(Set<UserDeclType> classTypes) {
    for (ClassUsageLink link : uplinks) {
      if (link.getUsage() == ImportUsage.HARD) {
        if (!classTypes.contains(link.node.getDeclType().getModuleType())) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  protected boolean addUplink(ClassUsageLink link) {
    if (link.node == this) {
      return false;
    }
    // never add an uplink to either our own module, or to a class in our module
    if (link.node.getDeclType().getModuleType().equals(moduleType)) {
      return false;
    }
    
    // check for duplicate link
    for (ClassUsageLink uplink : getUplinks()) {
      if (uplink.node.getDeclType().equals(link.node.getDeclType())) {
        //increase the usage
        if (uplink.getUsage().ordinal() < link.getUsage().ordinal()) {
          uplink.setUsage(link.getUsage());
        }
        return false;
      }
    }
    
    uplinks.add(link);
    return true;
  }

  @Override
  protected boolean addDownlink(ClassUsageLink link) {
    if (link.node == this) {
      return false;
    }
    
    // never add a downlink to either our own module, or to a class in our module
    if (link.node.getDeclType().getModuleType().equals(moduleType)) {
      return false;
    }
    downlinks.add(link);
    return true;
  }

  @Override
  public Set<ClassUsageLink> getUplinks() {
    return uplinks;
  }

  @Override
  public Set<ClassUsageLink> getDownlinks() {
    return downlinks;
  }
}
