package typecheck;

import ast.Expression;
import ast.Program.DestructorRenamings;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.ModuleTypeBox;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;

public class ModuleType extends UserDeclType {

  public ModuleType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.MODULE_TYPE, outerType, name);
  }
  
  public static class ProjectNameVersion {
    public final boolean isFfi;
    public final String name;
    public final int majorVersion;
    public final int minorVersion;
    
    public ProjectNameVersion(boolean isFfi, String name, int majorVersion, int minorVersion) {
      this.isFfi = isFfi;
      this.name = name;
      this.majorVersion = majorVersion;
      this.minorVersion = minorVersion;
    }
    
    @Override
    public String toString() {
      if (isFfi) {
        return name;
      }
      return name + "_" + majorVersion + "_" + minorVersion;
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof ProjectNameVersion)) {
        return false;
      }
      ProjectNameVersion ourOther = (ProjectNameVersion) other;
      
      return name.equals(ourOther.name) && majorVersion == ourOther.majorVersion
          && minorVersion == ourOther.minorVersion;
    }
    
    @Override
    public int hashCode() {
      return name.hashCode() + majorVersion + minorVersion;
    }

    public AstProto.ModuleProjectNameVersion.Builder serialize() {
      AstProto.ModuleProjectNameVersion.Builder document =
          AstProto.ModuleProjectNameVersion.newBuilder();
      
      document.setIsFfi(isFfi);
      document.setName(name);
      document.setMajorVersion(majorVersion);
      document.setMinorVersion(minorVersion);
      
      return document;
    }
    
    public static ProjectNameVersion deserialize(AstProto.ModuleProjectNameVersion document)
        throws SerializationError {
      return new ProjectNameVersion(document.getIsFfi(), document.getName(),
          document.getMajorVersion(),
          document.getMinorVersion());
    }
  }

  @Override
  public List<Type> getInnerTypes() {
    return new LinkedList<>();
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type tmp = mapFromTo.get(this);
    if (tmp == null) {
      return this;
    }
    return (ModuleType) tmp;
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.ModuleType.Builder subDoc = AstProto.ModuleType.newBuilder();
    if (outerType.isNotNull()) {
      subDoc.addOuterType(outerType.get().serialize());
    }
    subDoc.setName(name);
    document.setModType(subDoc);
    return document;
  }

  public static ModuleType deserialize(AstProto.ModuleType document) throws SerializationError {
    Nullable<ModuleType> outerType = Nullable.empty();
    if (document.getOuterTypeCount() != 0) {
      outerType = Nullable.of((ModuleType) AbstractType.deserialize(document.getOuterType(0)));
    }
    return new ModuleType(outerType, document.getName());
  }

  @Override
  public Type setConst(boolean isConst) {
    return this;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return new HashSet<>();
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return this;
  }

  public List<String> toList() {
    List<String> namePath = new LinkedList<>();
    if (outerType.isNotNull()) {
      namePath.addAll(outerType.get().toList());
    }
    namePath.add(name);
    return namePath;
  }
  
  public static ModuleType staticFromList(List<String> path) {
    Nullable<ModuleType> moduleType = Nullable.empty();
    for (String item : path) {
      moduleType = Nullable.of(new ModuleType(moduleType, item));
    }
    return moduleType.get();
  }
  
  @Override
  public UserDeclType fromList(List<String> list) {
    return staticFromList(list);
  }

  public ModuleType prepend(ModuleType impPrefix) {
    if (outerType.isNull()) {
      return new ModuleType(Nullable.of(impPrefix), name);
    }
    return new ModuleType(Nullable.of(outerType.get().prepend(impPrefix)), name);
  }

  /*
   * In a dot expression, if you use a function, you will need this. The idea is
   * to load on demand and only for this module, so we already have the import
   * table -> ProgHeader for this module. Get that, then construct a tenv with
   * only the submods as imports (since we are importing this mod (it can be
   * parent, import, or submod) then we cannot see that module's imports or parent
   * by default, only the submods.
   * 
   * e.g. import std;
   * 
   * we can still use std.list.ArrayList if we want, or std.list.qsort()
   * 
   * TODO to optimize, could cache this in the global tenv/module endpoint
   */
  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    Optional<UserDeclTypeBox> typeBox = tenv.lookupUserDeclType(this, lineNum, columnNum);
    if (!typeBox.isPresent()) {
      return Optional.empty();
    }
    if (!(typeBox.get() instanceof ModuleTypeBox)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          this + " expected to be a module but found: " + typeBox, lineNum, columnNum);
      return Optional.empty();
    }
    
    ModuleTypeBox moduleTypeBox = (ModuleTypeBox) typeBox.get();
    
    return moduleTypeBox.getTenv(tenv);
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Nullable<EffectiveImport> absPath = effectiveImports.getImport(
        new ModuleImportType(outerType, name),
        EffectiveImport.ImportUsage.INVALID);
    if (absPath.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Unable to find " + this + " in effective imports list");
      return Optional.empty();
    }
    ModuleType absModType = absPath.get().absPath.asModuleType();
    if (keepCurrentModRelative) {
      Nullable<ModuleType> tmp = absModType.removeCommonStart(currentModule);
      if (tmp.isNull()) {
        return Optional.of(Nullable.empty());
      }
      return Optional.of(Nullable.of(tmp.get()));
    }
    return Optional.of(Nullable.of(absModType));
  }
  
  /*
   * Return remainder (after removing other from us), so (a.b.A vs a.b.c.d) -> a.b
   * a.b.f is NOT under a.b.c (entire path of other module must be matched)
   */
  public Nullable<ModuleType> removeCommonStart(ModuleType other) {
    List<String> ourPath = toList();
    List<String> otherPath = other.toList();

    // check if we are under other
    if (ourPath.size() < otherPath.size()) {
      // other is longer than us, so no way for us to start with other
      return Nullable.of(this);
    }

    Iterator<String> ourIter = ourPath.iterator();
    Iterator<String> otherIter = otherPath.iterator();
    while (ourIter.hasNext() && otherIter.hasNext()) {
      if (!ourIter.next().equals(otherIter.next())) {
        // stop on first mismatch. We must match all of other to start with
        return Nullable.of(this);
      }
    }

    if (otherPath.size() == ourPath.size()) {
      // total match
      return Nullable.empty();
    }
    return Nullable.of((ModuleType) fromList(ourPath.subList(otherPath.size(), ourPath.size())));
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState, Set<ModuleType> exportedTypes, boolean skipNormalize)
      throws FatalMessageException {
    ModuleType normType = this;
    if (!skipNormalize) {
      Optional<Nullable<Type>> optType = basicNormalize(false, currentModule, effectiveImports,
          msgState);
      if (!optType.isPresent()) {
        return false;
      }
      normType = (ModuleType) optType.get().get();
    }
    exportedTypes.add(normType);
    return true;
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    throw new IllegalArgumentException("cannot calculate the dummy value for module: " + this);
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    throw new IllegalArgumentException("cannot prepend " + prependType + " to module " + this);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
        "Cannot calculate size of module type: " + this, lineNum, columnNum);
    return -1;
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) {
    return Optional.empty();
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
        "cannot copy module type into new tenv");
    return false;
  }

  @Override
  public Type asClassDeclType() {
    return this;
  }

  @Override
  public UserInstanceType asInstanceType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public UserDeclType asUserDeclType() {
    return this;
  }

  @Override
  public ImportType asImportType() {
    return new ModuleImportType(outerType, name);
  }

}
