package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.MultiValue;
import interp.Value;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.MultiType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class MultiExpression extends AbstractExpression {

  public final List<Expression> expressions;

  public MultiExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, List<Expression> expressions) {
    super(lineNum, columnNum, AstType.MULTI_EXPRESSION, preComments, postComments);
    this.expressions = expressions;

    for (Expression expr : expressions) {
      expr.setParentAst(this);
    }
  }

  public MultiExpression(int lineNum, int columnNum, List<Expression> expressions) {
    super(lineNum, columnNum, AstType.MULTI_EXPRESSION);
    this.expressions = expressions;

    for (Expression expr : expressions) {
      expr.setParentAst(this);
    }
  }

  public MultiExpression(List<Expression> expressions) {
    super(AstType.MULTI_EXPRESSION);
    this.expressions = expressions;

    for (Expression expr : expressions) {
      expr.setParentAst(this);
    }
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      newExprs.add(expression.renameIds(renamings, msgState));
    }
    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      newExprs.add(expression
          .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get());
    }
    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.setOriginalLabel(val.getOriginalLabel());
    return Optional.of(Nullable.of(val));
  }

  @Override
  public String toString(String indent) {
    String res = preToString() + "(";
    boolean first = true;
    for (Expression expression : expressions) {
      if (!first) {
        res += ", ";
      }
      first = false;
      res += expression.toString();
    }
    return res + ")" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {

    boolean hitErr = false;
    List<Type> types = new LinkedList<>();
    for (Expression expression : expressions) {
      Optional<TypecheckRet> tyOpt = expression.typecheck(tenv, Nullable.empty());
      if (!tyOpt.isPresent()) {
        hitErr = true;
        continue;
      }
      types.add(tyOpt.get().type);
    }
    if (hitErr) {
      return Optional.empty();
    }
    determinedType = new MultiType(types);
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    List<Value> values = new LinkedList<>();
    for (Expression expression : expressions) {
      values.add(expression.interp(env));
    }
    return new MultiValue(determinedType, values);
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.MultiExpression.Builder subDoc = AstProto.MultiExpression.newBuilder();

    for (Expression expression : expressions) {
      subDoc.addExpressions(expression.serialize());
    }

    document.setMultiExpression(subDoc);
    return document;
  }

  public static MultiExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.MultiExpression document) throws SerializationError {
    List<Expression> values = new LinkedList<>();
    for (AstProto.Ast bVal : document.getExpressionsList()) {
      values.add(AbstractExpression.deserialize(bVal));
    }
    return new MultiExpression(lineNum, columnNum, preComments, postComments, values);
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<>();
    for (Expression expression : expressions) {
      freeVars.putAll(expression.findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof MultiExpression)) {
      return false;
    }
    MultiExpression ourOther = (MultiExpression) other;
    if (expressions.size() != ourOther.expressions.size()) {
      return false;
    }
    Iterator<Expression> iterUs = expressions.iterator();
    Iterator<Expression> iterOther = ourOther.expressions.iterator();
    while (iterUs.hasNext() && iterOther.hasNext()) {
      if (!iterUs.next().compare(iterOther.next())) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      newExprs.add((Expression) expression.replace(target, with));
    }

    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (Expression expression : expressions) {
      Nullable<Ast> tmp = expression.findByOriginalLabel(findLabel);
      if (tmp != null) {
        return tmp;
      }
    }
    return null;
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      Pair<List<DeclarationStatement>, Ast> tmp = expression.toAnf(tuning);
      insert.addAll(tmp.a);
      newExprs.add((Expression) tmp.b);
    }

    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      newExprs.add((Expression) expression.replaceType(mapFromTo));
    }
    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      newExprs.add((Expression) expression.replaceVtableAccess(project, msgState).get());
    }
    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.setOriginalLabel(val.getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    for (Expression expr : expressions) {
      if (expr.containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (Expression expr : expressions) {
      if (expr.containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();
    List<Expression> newExprs = new LinkedList<>();

    for (Expression expr : expressions) {
      Optional<ResolveExcRet> tmp = expr.resolveExceptions(project, currentModule,
          auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution,
          hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newExprs.add((Expression) tmp.get().ast.get());
    }

    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(val)));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    for (Expression expr : expressions) {
      expr.addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    int size = 0;
    for (Expression expr : expressions) {
      int tmp = expr.calculateSize(tenv, lineNum, columnNum);
      if (tmp == -1) {
        return -1;
      }
      size += tmp;
    }
    return size;
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    List<Expression> newExprs = new LinkedList<>();

    for (Expression expr : expressions) {
      newExprs.add(expr.insertDestructorCalls(scopedIds, destructorRenamings));
    }

    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      newExprs.add(expression.liftClassDeclarations(liftedClasses, replacements).get());
    }
    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.setOriginalLabel(val.getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      newExprs.add(expression.resolveUserTypes(msgState).get());
    }
    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.setOriginalLabel(val.getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    List<Expression> newExprs = new LinkedList<>();
    for (Expression expression : expressions) {
      newExprs.add(expression.addThisExpression(classHeader));
    }
    MultiExpression val = new MultiExpression(lineNum, columnNum, preComments, postComments,
        newExprs);
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }

}
