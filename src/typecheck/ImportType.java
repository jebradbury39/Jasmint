package typecheck;

import ast.Expression;
import ast.Program.DestructorRenamings;
import ast.Statement;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;
import util.DotGen;

/*
 * Only used by import statements. Can be:
 * 
 * module
 * module..module
 * module..class
 * module..enum
 * module..function
 * module..* - TODO, maybe, but for now is not allowed
 */

public abstract class ImportType extends UserDeclType {

  public ImportType(TypeType typeType, Nullable<ModuleType> outerType, String name) {
    super(typeType, outerType, name);
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return Optional.empty();
  }

  @Override
  public List<Type> getInnerTypes() {
    return new LinkedList<>();
  }

  @Override
  public Type setConst(boolean isConst) {
    return this;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return new HashSet<>();
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return this;
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Nullable<EffectiveImport> absPath = effectiveImports.getImport(this,
        EffectiveImport.ImportUsage.INVALID);
    if (absPath.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Unable to find " + this + " in effective imports list");
      return Optional.empty();
    }
    ImportType absModType = absPath.get().absPath;
    if (keepCurrentModRelative) {
      Nullable<ImportType> tmp = absModType.removeCommonStart(currentModule);
      if (tmp.isNull()) {
        return Optional.of(Nullable.empty());
      }
      return Optional.of(Nullable.of(tmp.get()));
    }
    return Optional.of(Nullable.of(absModType));
  }
  
  /*
   * Return remainder (after removing other from us), so (a.b.A vs a.b.c.d) -> a.b
   * a.b.f is NOT under a.b.c (entire path of other module must be matched)
   */
  public Nullable<ImportType> removeCommonStart(ModuleType other) {
    List<String> ourPath = toList();
    List<String> otherPath = other.toList();

    // check if we are under other
    if (ourPath.size() < otherPath.size()) {
      // other is longer than us, so no way for us to start with other
      return Nullable.of(this);
    }

    Iterator<String> ourIter = ourPath.iterator();
    Iterator<String> otherIter = otherPath.iterator();
    while (ourIter.hasNext() && otherIter.hasNext()) {
      if (!ourIter.next().equals(otherIter.next())) {
        // stop on first mismatch. We must match all of other to start with
        return Nullable.of(this);
      }
    }

    if (otherPath.size() == ourPath.size()) {
      // total match
      return Nullable.empty();
    }
    return Nullable.of((ImportType) fromList(ourPath.subList(otherPath.size(), ourPath.size())));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState)
      throws FatalMessageException {
    throw new UnsupportedOperationException("cannot calculate the dummy value for import: " + this);
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    throw new UnsupportedOperationException("cannot prepend " + prependType + " to import " + this);
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
        "cannot copy import type into new tenv");
    return false;
  }

  @Override
  public Type asClassDeclType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
        "Cannot calculate size of import type: " + this, lineNum, columnNum);
    return -1;
  }

  @Override
  public List<String> toList() {
    List<String> namePath = new LinkedList<>();
    if (outerType.isNotNull()) {
      namePath.addAll(outerType.get().toList());
    }
    namePath.add(name);
    return namePath;
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public UserInstanceType asInstanceType() {
    throw new UnsupportedOperationException();
  }

  public abstract ModuleType asModuleType();

  @Override
  public ImportType asImportType() {
    return this;
  }

  public abstract ImportType onlyName();

  public abstract DotGen.Color getGraphColor();
  
}
