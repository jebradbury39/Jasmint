package interp;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import typecheck.Type;

public class MultiValue extends AbstractValue {

  public final List<Value> values;
  
  public MultiValue(Type multiType, List<Value> values) {
    super(MetaValue.NORMAL, multiType);
    this.values = values;
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    String res = "";
    boolean first = true;
    for (Value value : values) {
      if (!first) {
        res += ", ";
      }
      first = false;
      res += value;
    }
    return new StringValue(leftValue + res);
  }

  @Override
  public Value copy(Environment copyEnv) throws InterpError {
    List<Value> newValues = new LinkedList<>();
    for (Value value : values) {
      newValues.add(value);
    }
    return new MultiValue(type, newValues);
  }

  @Override
  public boolean equalsValue(Value other) {
    // x,1,[1,2,3] == x,1,[1,2,3]
    if (!other.getType().equals(type)) {
      return false;
    }
    //equal types ensures equal number of values
    if (this == other) {
      return true;
    }
    MultiValue otherVal = (MultiValue) other;
    Iterator<Value> iterUs = values.iterator();
    Iterator<Value> iterOther = otherVal.values.iterator();
    while (iterUs.hasNext() && iterOther.hasNext()) {
      if (!iterUs.next().equals(iterOther.next())) {
        return false;
      }
    }
    return true;
  }

}
