package interp;

import typecheck.AbstractType;
import typecheck.IntType;

public class IntValue extends AbstractValue {

  public final long value;
  
  public IntValue(long value, IntType itype) {
    super(MetaValue.NORMAL, itype);
    this.value = truncate(value, itype);
  }
  
  public IntValue(MetaValue metaValue, int value, IntType itype) {
    super(metaValue, itype);
    this.value = truncate(value, itype);
  }
  
  public static long truncate(long val, IntType itype) {
    //truncate int to fit our size
    long max = 0;
    long min = 0;
    if (itype.signed) {
      max = (1 << (itype.size - 1));
      min = -1 * max + 1;
    } else {
      max = (1 << itype.size);
    }

    if (val > max) {
      return max;
    }
    if (val < min) {
      return min;
    }
    return val;
  }

  @Override
  public Value add(Value rightValue) throws InterpError {
    return rightValue.add(value);
  }
  
  @Override
  public Value add(long leftValue, IntType leftType) {
    return new IntValue(leftValue + value, (IntType) AbstractType.makePromotion(leftType, type));
  }
  
  @Override
  public Value add(double leftValue) {
    return new Float32Value(leftValue + (double)value);
  }
  
  @Override
  public Value add(String leftValue) {
    return new StringValue(leftValue + value);
  }

  @Override
  public Value sub(Value rightValue) throws InterpError {
    return rightValue.sub(value);
  }
  
  @Override
  public Value sub(long leftValue, IntType leftType) {
    return new IntValue(leftValue - value, (IntType) AbstractType.makePromotion(leftType, type));
  }
  
  @Override
  public Value sub(double leftValue) {
    return new Float32Value(leftValue - (double)value);
  }

  @Override
  public long toInt() {
    return value;
  }

  @Override
  public boolean toBool() throws InterpError {
    return value != 0;
  }

  @Override
  public Value mul(Value rightValue) throws InterpError {
    return rightValue.mul(value);
  }
  
  @Override
  public Value mul(long leftValue, IntType leftType) throws InterpError {
    return new IntValue(leftValue * value, (IntType) AbstractType.makePromotion(leftType, type));
  }
  
  @Override
  public Value mul(double leftValue) throws InterpError {
    return new Float32Value(leftValue * (double)value);
  }

  @Override
  public Value div(Value rightValue) throws InterpError {
    return rightValue.div(value);
  }
  
  @Override
  public Value div(long leftValue, IntType leftType) throws InterpError {
    if (value == 0) {
      throw new InterpError("division by zero", -1, -1);
    }
    return new IntValue(leftValue / value, (IntType) AbstractType.makePromotion(leftType, type));
  }
  
  @Override
  public Value div(double leftValue) throws InterpError {
    if (value == 0) {
      throw new InterpError("division by zero", -1, -1);
    }
    return new Float32Value(leftValue / (double)value);
  }

  @Override
  public Value neg() throws InterpError {
    return new IntValue(-value, IntType.Create(true, ((IntType) type).size));
  }

  @Override
  public Value copy(Environment copyEnv) {
    return new IntValue(value, (IntType) type);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return ((IntValue) other).value == value;
  }
  
}
