package typegraph;

import errors.FatalMessageException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import typecheck.AnyType;
import typecheck.EnumType;
import typecheck.GenericType;
import typecheck.Type;
import typecheck.UserInstanceType;
import typegraph.TypeGraph.CastDirection;

//This class should never have ANY links
public class UserTypeNode extends AbstractTypeNode {
  public final UserInstanceType type; //abs path, all access must go through basicNormalize
  
  public UserTypeNode(UserInstanceType type) {
    this.type = type;
  }
  
  @Override
  public int hashCode() {
    return type.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof UserTypeNode)) {
      return false;
    }
    UserTypeNode node = (UserTypeNode) other;
    return type.equals(node.type);
  }
  
  @Override
  public String toString() {
    return type.toString();
  }

  @Override
  public boolean typeEquals(Type type, boolean looseMatch) {
    if (type instanceof AnyType) {
      return true;
    }
    if (!(type instanceof UserInstanceType)) {
      return false;
    }
    if (this.type.getTypeType() != type.getTypeType()) {
      return false;
    }

    UserInstanceType ty = this.type;
    UserInstanceType userType = (UserInstanceType) type;
    
    if (!looseMatch) {
      if (type.equals(ty)) {
        return true;
      }
      return false;
    }
    
    //if looseMatch, then don't bother with the inner types, except for size
    if (userType.getInnerTypes().size() != ty.getInnerTypes().size()) {
      return false;
    }
    if (userType.outerType == null || ty.outerType == null) {
      if (userType.outerType != ty.outerType) {
        return false;
      }
    } else {
      if (!userType.outerType.equals(ty.outerType)) {
        return false;
      }
    }
    if (userType.name.equals(this.type.name)) {
      return true;
    }
    return false;
  }

  @Override
  public boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph, Set<TypeNode> visited)
          throws FatalMessageException {
    if (checkVisited(visited)) {
      return false;
    }
    if (fromTy instanceof AnyType || toTy instanceof AnyType) {
      return true;
    }
    
    if (!typeEquals(fromTy, true)) {
      return false; //should be a redundant check
    }
    if (type instanceof EnumType) {
      return true;
    }
    
    if (!typeEquals(toTy, true)) {
      return false; //must be equal and not a reference
    }
    if (fromTy.getInnerTypes().size() != toTy.getInnerTypes().size()) {
      return false;
    }
    
    //check inner types
    Iterator<Type> iterFrom = fromTy.getInnerTypes().iterator();
    Iterator<Type> iterTo = toTy.getInnerTypes().iterator();
    while (iterFrom.hasNext() && iterTo.hasNext()) {
      Type iterFromTy = iterFrom.next();
      Type iterToTy = iterTo.next();
      //Consider casting from A to C{U, V}. Generics must be able to cast to any
      if (iterFromTy instanceof GenericType) {
        continue;
      }
      if (iterToTy instanceof GenericType) {
        continue;
      }
      if (!graph.canCastTo(iterFromTy, direction, iterToTy)) {
        return false;
      }
    }
    
    return true;
  }

  @Override
  public Type getType() {
    return type;
  }
}
