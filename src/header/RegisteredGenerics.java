package header;

import ast.ClassDeclaration;
import ast.SerializationError;
import astproto.AstProto;
import com.google.protobuf.util.JsonFormat;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import type_env.ClassTypeBox;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;
import typecheck.AbstractType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.ModuleType;
import typecheck.Type;

/*
 * Holds the generics for a module, e.g. List<int> ... all registered types must be non-generic
 */

public class RegisteredGenerics {
  public static class Registration {
    public final ClassType realType; // abs path with replacements/transforms
    // optimization, part of caching AFTER plugin starts doing
    // transforms...resolveGenerics_trans
    // see notebook #3
    public final ClassType resolvedType; // abs new path with no generics (no other transforms)
    Nullable<ClassDeclaration> resolvedDeclaration = Nullable.empty();

    public Registration(ClassType realType) {
      this.realType = realType;
      if (realType.innerTypes.isEmpty()) {
        throw new IllegalArgumentException();
      }

      // replace the class name
      String newClassName = realType.name;
      for (Type ty : realType.innerTypes) {
        newClassName += "_" + ty;
      }
      newClassName = newClassName.replace("<", "_S_");
      newClassName = newClassName.replace(">", "_E_");
      newClassName = newClassName.replace("*", "_ptr_");
      newClassName = newClassName.replace(".", "_mod_");
      this.resolvedType = new ClassType(realType.outerType, newClassName, new LinkedList<>());
    }

    public Nullable<ClassDeclaration> getResolvedDecl(Project project, MsgState msgState,
        Map<Type, Type> genericToRealMapping, Set<ClassDeclType> nonTemplateClasses)
        throws FatalMessageException {
      if (resolvedDeclaration.isNull()
          && !nonTemplateClasses.contains(new ClassDeclType(realType.outerType, realType.name))) {
        // don't do this too early, otherwise you won't get the other transforms
        // (renamings ect)

        // find the class decl
        Nullable<ModuleEndpoint> modEp = project.lookupModule(realType.outerType.get(),
            project.getAuditTrail());
        if (modEp.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
              "Failed to find module for: " + realType);
        }
        modEp.get().load();
        LoadedModuleHandle handle = (LoadedModuleHandle) modEp.get().getHandle();
        Nullable<ClassDeclaration> cdeclOpt = handle.moduleBuildObj.program
            .lookupClassDecl(realType.name);
        if (cdeclOpt.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
              "Failed to find class for: " + realType);
        }
        ClassDeclaration cdecl = cdeclOpt.get();

        // perform type replacements on whole decl, then save as resolvedDeclaration
        /*
         * Replace our generics with their real types, ofc (this pass) Replace usages of
         * other generic types e.g. A<T> becomes A<int> becomes A_int (second pass)
         */

        Map<Type, Type> genericToRealInnerMapping = AbstractType.zip(cdecl.typedInnerTypes,
            realType.innerTypes);
        
        // note that the target real types are like a.A<string> before putting them into the class
        // we want to avoid having types like a.B<A_String>, since that exponentially increases
        // replacements

        ClassDeclaration newCdecl = cdecl.filterStatics(true);

        ClassDeclaration newCdecl1 = (ClassDeclaration) newCdecl.replaceType(genericToRealInnerMapping,
            resolvedType.name, true);

        // now make all generic types resolved
        ClassDeclaration newCdecl2 = (ClassDeclaration) newCdecl1.replaceType(genericToRealMapping,
            newCdecl1.name, true);

        if (newCdecl2.parent.isNotNull()) {
          if (!newCdecl2.parent.get().innerTypes.isEmpty()) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
                "Generic class should have no generics after replacement, "
                    + "but parent type is still generic: " + newCdecl2);
          }
        }
        
        resolvedDeclaration = Nullable.of(newCdecl2);
      }
      return resolvedDeclaration;
    }

    @Override
    public boolean equals(Object other) {
      if (!(other instanceof Registration)) {
        return false;
      }
      return realType.equals(((Registration) other).realType);
    }

    @Override
    public int hashCode() {
      return realType.hashCode();
    }

    public AstProto.RegisteredGeneric.Builder serialize() throws SerializationError {
      AstProto.RegisteredGeneric.Builder document = AstProto.RegisteredGeneric.newBuilder();

      document.setClassType(realType.serialize());
      if (resolvedDeclaration.isNotNull()) {
        document.addResolvedClassDeclaration(resolvedDeclaration.get().serialize());
      }

      return document;
    }

    public static Registration deserialize(AstProto.RegisteredGeneric document)
        throws SerializationError {
      ClassType realType = (ClassType) AbstractType.deserialize(document.getClassType());
      Registration reg = new Registration(realType);
      if (realType.innerTypes.isEmpty()) {
        throw new IllegalArgumentException();
      }

      if (document.getResolvedClassDeclarationCount() > 0) {
        reg.resolvedDeclaration = Nullable
            .of(ClassDeclaration.deserialize(document.getResolvedClassDeclaration(0)));
      }
      return reg;
    }

    public Registration replaceType(Map<Type, Type> genericToRealMapping) {
      Registration reg = new Registration(realType);

      if (resolvedDeclaration.isNotNull()) {
        ClassDeclaration cdecl = resolvedDeclaration.get();
        reg.resolvedDeclaration = Nullable
            .of((ClassDeclaration) cdecl.replaceType(genericToRealMapping, cdecl.name, true));
      }

      return reg;
    }
  }

  private final Set<ClassType> originalRegistrations; // as read from build file
  private Map<ClassType, Registration> registrations = new HashMap<>();

  public RegisteredGenerics(Set<ClassType> originalRegistrations) {
    this.originalRegistrations = originalRegistrations;
  }

  // return false on error
  public boolean addRegistration(TypeEnvironment globalTenv, ClassType realType,
      Nullable<ClassHeader> classHeader, EffectiveImports effectiveImports, int lineNum,
      int columnNum) throws FatalMessageException {
    if (realType.innerTypes.isEmpty()) {
      // only add generic instances
      return true;
    }

    ClassType absRealType = (ClassType) realType.basicNormalize(false,
        globalTenv.ofDeclType.getModuleType(),
        effectiveImports, globalTenv.msgState).get().get();

    if (registrations.containsKey(absRealType)) {
      return true;
    }
    if (classHeader.isNull()) {
      
      Optional<UserDeclTypeBox> tyBoxOpt = globalTenv.lookupUserDeclType(realType.asDeclType(),
          lineNum, columnNum);
      if (!tyBoxOpt.isPresent()) {
        return false;
      }
      if (!(tyBoxOpt.get() instanceof ClassTypeBox)) {
        return false;
      }
      classHeader = Nullable.of(((ClassTypeBox) tyBoxOpt.get()).classHeader);
    }
    Optional<PsuedoClassHeader> repl = classHeader.get().replaceGenerics(
        globalTenv.project.getAuditTrail(), globalTenv.project, Nullable.of(globalTenv),
        realType.innerTypes, true, new HashMap<>(), globalTenv.msgState, lineNum, columnNum);
    if (!repl.isPresent()) {
      return false;
    }
    ClassHeader replHeader = (ClassHeader) repl.get();

    // do this all the way up the inheritance tree
    if (replHeader.getParent().isNotNull()) {
      if (!addRegistration(globalTenv, replHeader.getParent().get(), Nullable.empty(), effectiveImports,
          lineNum, columnNum)) {
        return false;
      }
    }

    // validate our members (not inherited) (aka typecheck)
    if (!replHeader.templateTypecheck(globalTenv, lineNum, columnNum)) {
      // error, give replacement
      return false;
    }

    Registration reg = new Registration(absRealType);
    registrations.put(absRealType, reg);
    return true;
  }

  /*
   * At the end of register, for all non-local check if we still equal it or if we
   * added some If the latter, create local version of generics file
   */
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof RegisteredGenerics)) {
      return false;
    }
    return registrations.equals(((RegisteredGenerics) other).registrations);
  }

  public boolean equalsOriginal() {
    return originalRegistrations.equals(registrations.keySet());
  }

  // apply renamings (renamings must apply to new class decls as well)
  public RegisteredGenerics renameIds(Renamings globalRenamings, MsgState msgState) {
    return null;
  }

  public void serializeToFile(String genericsBuildPath) throws IOException, SerializationError {
    AstProto.RegisteredGenerics.Builder document = AstProto.RegisteredGenerics.newBuilder();

    for (Registration reg : registrations.values()) {
      document.addRegistrations(reg.serialize());
    }

    FileOutputStream fout = Project.getOutputFile(genericsBuildPath);
    document.build().writeTo(fout);
    fout.close();

    fout = Project.getOutputFile(genericsBuildPath + ".json");
    JsonFormat.Printer jprinter = JsonFormat.printer();
    fout.write(jprinter.print(document.build()).getBytes());
    fout.close();
  }

  public static RegisteredGenerics deserializeFromFile(String buildFilename, MsgState msgState)
      throws IOException, SerializationError {
    FileInputStream fis = new FileInputStream(new File(buildFilename));

    AstProto.RegisteredGenerics document = AstProto.RegisteredGenerics.parseFrom(fis);
    fis.close();

    Set<ClassType> originalRegistrations = new HashSet<>();
    Map<ClassType, Registration> registrations = new HashMap<>();

    for (AstProto.RegisteredGeneric bvReg : document.getRegistrationsList()) {
      Registration decReg = Registration.deserialize(bvReg);
      originalRegistrations.add(decReg.realType);
      registrations.put(decReg.realType, decReg);
    }

    RegisteredGenerics registeredGenerics = new RegisteredGenerics(originalRegistrations);
    registeredGenerics.registrations = registrations;

    return registeredGenerics;
  }

  public void indexGenericToRealMapping(ModuleType currentModule,
      final EffectiveImports effectiveImports, Map<Type, Type> replacements, MsgState msgState)
      throws FatalMessageException {
    for (Registration reg : registrations.values()) {
      ClassType toTy = (ClassType) reg.resolvedType.replaceType(replacements);
      toTy = (ClassType) toTy.basicNormalize(false, currentModule, effectiveImports, msgState).get().get();

      // only ClassType (instance) not ClassDeclType since a class declaration cannot
      // be generic
      ClassType realFromTy = (ClassType) reg.realType.basicNormalize(false, currentModule,
          effectiveImports, msgState).get().get();
      
      // put basic mapping e.g. a.A<a.B<string>, X> = A_B_string_X
      replacements.put(realFromTy, toTy);
    }
  }

  public List<Registration> getRegistrations() {
    for (Registration reg : registrations.values()) {
      if (reg.realType.innerTypes.isEmpty()) {
        throw new IllegalArgumentException();
      }
    }
    return new LinkedList<>(registrations.values());
  }

  /*
   * After creating a resolvedDelclaration, we need a phase 2 type replacement
   * (after all other resolved class names have been done)
   */
  public RegisteredGenerics replaceType(Map<Type, Type> genericToRealMapping, String moduleAppend) {
    RegisteredGenerics newRegGen = new RegisteredGenerics(originalRegistrations);

    for (Registration reg : registrations.values()) {
      if (reg.realType.innerTypes.isEmpty()) {
        throw new IllegalArgumentException();
      }
      newRegGen.registrations.put(reg.realType, reg.replaceType(genericToRealMapping));
    }

    return newRegGen;
  }
}
