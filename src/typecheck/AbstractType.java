package typecheck;

import ast.Ast;
import ast.SerializationError;
import astproto.AstProto;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import type_env.TypeEnvironment;

public abstract class AbstractType implements Type {

  public static enum TypeType {
    ANY_TYPE, ARRAY_TYPE, BOOL_TYPE, CHAR_TYPE, FLOAT32_TYPE, FLOAT64_TYPE, FUNCTION_TYPE,
    GENERIC_TYPE, INT_TYPE, MAP_TYPE, MODULE_TYPE_ENTRY, STRING_TYPE, UNDETERMINED_TYPE, VOID_TYPE,
    NULL_TYPE, REFERENCE_TYPE, MULTI_TYPE, CLASS_TYPE, MODULE_TYPE, CLASS_DECL_TYPE,
    UNDETERMINED_USER_DECL_TYPE, UNDETERMINED_USER_INSTANCE_TYPE, ENUM_DECL_TYPE, ENUM_TYPE,
    UNDETERMINED_IMPORT_TYPE, CLASS_IMPORT_TYPE, MODULE_IMPORT_TYPE, FUNCTION_IMPORT_TYPE,
    ENUM_IMPORT_TYPE
  }

  public final TypeType typeType;
  protected boolean isConst;

  @Override
  public String toString() {
    return isConst ? "const " : "";
  }

  @Override
  public int hashCode() {
    return typeType.ordinal();
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof AbstractType)) {
      return false;
    }
    return ((AbstractType) other).typeType == typeType;
  }

  public AbstractType(TypeType typeType) {
    this.typeType = typeType;
    this.isConst = false;
  }

  public TypeType getTypeType() {
    return typeType;
  }

  public static Map<Type, Type> zip(List<Type> innerTypes, List<Type> values) {
    Map<Type, Type> map = new HashMap<Type, Type>();

    if (innerTypes.size() != values.size()) {
      throw new IllegalArgumentException("cannot zip uneven lists");
    }

    Iterator<Type> keyIter = innerTypes.iterator();
    Iterator<Type> valIter = values.iterator();
    while (keyIter.hasNext() && valIter.hasNext()) {
      map.put(keyIter.next(), valIter.next());
    }

    return map;
  }

  protected AstProto.Type.Builder preSerialize() {
    AstProto.Type.Builder document = AstProto.Type.newBuilder();

    document.setTypeType(typeType.ordinal());
    document.setIsConst(isConst);

    return document;
  }

  public static Type deserialize(AstProto.Type document) throws SerializationError {
    int typeTypeIndex = document.getTypeType();
    boolean isConst = document.getIsConst();
    TypeType typeType = TypeType.values()[typeTypeIndex];

    Type type = null;
    switch (typeType) {
      case ARRAY_TYPE:
        type = ArrayType.deserialize(document.getArrayType());
        break;
      case BOOL_TYPE:
        type = BoolType.deserialize(document.getBoolType());
        break;
      case CHAR_TYPE:
        type = CharType.deserialize(document.getCharType());
        break;
      case FLOAT32_TYPE:
        type = Float32Type.deserialize(document.getFloat32Type());
        break;
      case FLOAT64_TYPE:
        type = Float64Type.deserialize(document.getFloat64Type());
        break;
      case FUNCTION_TYPE:
        type = FunctionType.deserialize(document.getFunctionType());
        break;
      case INT_TYPE:
        type = IntType.deserialize(document.getIntType());
        break;
      case MAP_TYPE:
        type = MapType.deserialize(document.getMapType());
        break;
      case STRING_TYPE:
        type = StringType.deserialize(document.getStringType());
        break;
      case UNDETERMINED_TYPE:
        type = UndeterminedType.deserialize(document.getUndeterminedType());
        break;
      case CLASS_TYPE:
        type = ClassType.deserialize(document.getClassType());
        break;
      case MODULE_TYPE:
        type = ModuleType.deserialize(document.getModType());
        break;
      case CLASS_DECL_TYPE:
        type = ClassDeclType.deserialize(document.getClassDeclType());
        break;
      case VOID_TYPE:
        type = VoidType.deserialize(document.getVoidType());
        break;
      case REFERENCE_TYPE:
        type = ReferenceType.deserialize(document.getReferenceType());
        break;
      case NULL_TYPE:
        type = NullType.deserialize(document.getNullType());
        break;
      case ANY_TYPE:
        type = AnyType.deserialize(document.getAnyType());
        break;
      case MULTI_TYPE:
        type = MultiType.deserialize(document.getMultiType());
        break;
      case GENERIC_TYPE:
        type = GenericType.deserialize(document.getGenericType());
        break;
      case ENUM_TYPE:
        type = EnumType.deserialize(document.getEnumType());
        break;
      case ENUM_DECL_TYPE:
        type = EnumDeclType.deserialize(document.getEnumDeclType());
        break;
      case MODULE_IMPORT_TYPE:
        type = ModuleImportType.deserialize(document.getModuleImportType());
        break;
      case CLASS_IMPORT_TYPE:
        type = ClassImportType.deserialize(document.getClassImportType());
        break;
      case ENUM_IMPORT_TYPE:
        type = EnumImportType.deserialize(document.getEnumImportType());
        break;
      case FUNCTION_IMPORT_TYPE:
        type = FunctionImportType.deserialize(document.getFunctionImportType());
        break;
      default:
        break;
    }

    if (type == null) {
      throw new SerializationError("Unable to deserialize typeType[" + typeType + "]", false);
    }
    if (isConst) {
      type = type.setConst(isConst);
    }

    return type;
  }

  @Override
  public boolean getIsConst() {
    return isConst;
  }

  /* only applicable to primitives */
  public static Type makePromotion(Type leftType, Type rightType) {
    if (toScore(leftType) < toScore(rightType)) {
      return rightType;
    }
    return leftType;
  }

  /*
   * Award the higher score to the type which can store more data, generally If
   * between signed and unsigned, award signed
   */
  private static int toScore(Type ty) {
    switch (ty.getTypeType()) {
      case BOOL_TYPE:
        return 0;
      case CHAR_TYPE:
        return 1;
      case INT_TYPE:
        IntType tmp = (IntType) ty;
        int inc = tmp.signed ? 1 : 0;
        switch (tmp.size) {
          case 8:
            return inc + 2;
          case 16:
            return inc + 4;
          case 32:
            return inc + 6;
          case 64:
            return inc + 8;
          default:
            throw new IllegalArgumentException("Invalid size");
        }
      case FLOAT32_TYPE:
        return 10;
      default:
        return -1;
    }
  }

  @Override
  public boolean refEquals(Type rightType) {
    if (equals(rightType)) {
      return true;
    }
    if (rightType instanceof NullType) {
      return typeType == TypeType.REFERENCE_TYPE || typeType == TypeType.NULL_TYPE
          || typeType == TypeType.FUNCTION_TYPE;
    }
    if (rightType instanceof ReferenceType) {
      return typeType == TypeType.NULL_TYPE;
    }
    return false;
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException {
    // default is to not add any
  }
  
  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      int lineNum, int columnNum)
      throws FatalMessageException {
    this.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
  }

  @Override
  public Type trimRelativeType(UserInstanceType currentModuleFullType) {
    return this;
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState, Set<ModuleType> exportedTypes, boolean skipNormalize)
      throws FatalMessageException {
    return true;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    return Optional.of(new TypecheckRet(Nullable.empty(), this));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<>();
  }

  @Override
  public SizeofTarget renameIdsSizeof(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    return renameIds(renamings, msgState);
  }

  @Override
  public SizeofTarget replaceTypeSizeof(Map<Type, Type> mapFromTo) {
    return replaceType(mapFromTo);
  }

  @Override
  public Optional<SizeofTarget> normalizeTypeSizeof(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Type>> tmp = basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(tmp.get().get());
  }
  
  @Override
  public boolean compareSizeof(SizeofTarget target) {
    if (!(target instanceof Type)) {
      return false;
    }
    return equals((Type) target);
  }
  
  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    return Nullable.empty();
  }
  
  @Override
  public boolean containsAst(Set<Ast> asts) {
    return false;
  }

}
