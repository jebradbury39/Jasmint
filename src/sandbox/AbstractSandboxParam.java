package sandbox;

import ast.SerializationError;
import astproto.AstProto;

public abstract class AbstractSandboxParam implements SandboxParam {
  public static enum ParamType {
    TIMEOUT,
    RECURSION,
    MEMLIMIT
  }
  
  public final int lineNum;
  public final int columnNum;
  public final ParamType paramType;
  
  public AbstractSandboxParam(int lineNum, int columnNum, ParamType paramType) {
    this.lineNum = lineNum;
    this.columnNum = columnNum;
    this.paramType = paramType;
  }
  
  @Override
  public ParamType getParamType() {
    return paramType;
  }
  
  protected AstProto.SandboxParam.Builder preSerialize() {
    AstProto.SandboxParam.Builder document = AstProto.SandboxParam.newBuilder();
    document.setParamType(paramType.ordinal());
    document.setLineNum(lineNum);
    document.setColumnNum(columnNum);
    return document;
  }

  public static SandboxParam deserialize(AstProto.SandboxParam document) throws SerializationError {
    ParamType paramType = ParamType.values()[document.getParamType()];
    int lineNum = document.getLineNum();
    int columnNum = document.getColumnNum();
    
    switch (paramType) {
      case TIMEOUT:
        return TimeoutParam.deserialize(lineNum, columnNum, document.getTimeoutParam());
      case RECURSION:
        return RecursionParam.deserialize(lineNum, columnNum, document.getRecursionParam());
      case MEMLIMIT:
        return MemlimitParam.deserialize(lineNum, columnNum, document.getMemlimitParam());
    }
    
    throw new SerializationError("invalid paramType: " + paramType, false);
  }
}
