package header;

import ast.SerializationError;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import audit.AuditResolveExceptions.RenamingEntry;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import java.util.Map;
import java.util.Optional;
import mbo.Renamings;
import mbo.Renamings.RenamingIdKey;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.FunctionImportType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;
import typecheck.Type;
import typecheck.UserDeclType;
import util.Pair;

public class FunctionHeader extends ProgItemHeader {
  
  public final UserDeclType originalName; // abs module path as well here. Before renaming
  private FunctionType type;

  public FunctionHeader(int lineNum, int columnNum, long label,
      boolean export, ModuleType moduleType, String name,
      UserDeclType originalName, FunctionType type) {
    super(lineNum, columnNum, label, moduleType, export, name);
    this.originalName = originalName;
    this.type = type;
  }
  
  public FunctionType getType() {
    return type;
  }

  public AstProto.FunctionHeader.Builder serialize() {
    AstProto.FunctionHeader.Builder document = AstProto.FunctionHeader.newBuilder();

    document.setLineNum(lineNum);
    document.setColumnNum(columnNum);
    document.setLabel(label);
    document.setExport(export);
    document.setModuleType(moduleType.serialize());
    document.setName(name);
    document.setFunctionType(type.serialize());

    return document;
  }

  public static FunctionHeader deserialize(AstProto.FunctionHeader document)
      throws SerializationError {
    return new FunctionHeader(document.getLineNum(), document.getColumnNum(), document.getLabel(),
        document.getExport(),
        (ModuleType) AbstractType.deserialize(document.getModuleType()),
        document.getName(), null,
        (FunctionType) AbstractType.deserialize(document.getFunctionType()));
  }

  public void populateRenameIds(Renamings renamings) {
    // we don't care about const here
    renamings.renamings
        .put(
            new RenamingIdKey(Nullable.of(moduleType),
                Nullable.of((FunctionType) type.setConst(false)), name),
            name + "_" + label);
  }

  public FunctionHeader renameIds(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    Nullable<String> newName = renamings.lookup(name, type, Nullable.of(moduleType));
    if (newName.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
          "Function header failed renamings: " + name);
    }
    return new FunctionHeader(lineNum, columnNum, label, export, moduleType, newName.get(),
        originalName, type);
  }

  public FunctionHeader replaceTypes(Map<Type, Type> typeRenamings) {
    return new FunctionHeader(lineNum, columnNum, label, export,
        (ModuleType) moduleType.replaceType(typeRenamings), name,
        originalName, (FunctionType) type.replaceType(typeRenamings));
  }

  public Pair<Nullable<FunctionHeader>, FunctionHeader> resolveExceptions(
      AuditResolveExceptions auditEntry) {
    Nullable<RenamingEntry> entry = auditEntry.findFunction(name, (FunctionType) type);
    Nullable<FunctionHeader> copyFn = Nullable.empty();
    if (entry.isNotNull()) {
      copyFn = Nullable.of(new FunctionHeader(lineNum, columnNum, label, export, moduleType,
          entry.get().name, originalName, entry.get().functionType));
    }
    return new Pair<>(copyFn, this);
  }

  public void resolveUserTypes(TypeEnvironment tenv) throws FatalMessageException {
    Optional<Type> tmpTy = type.normalize(tenv, lineNum, columnNum);
    if (!tmpTy.isPresent()) {
      return;
    }
    type = (FunctionType) tmpTy.get();
  }

  @Override
  public ResolvedImportType getImportType() {
    return new FunctionImportType(Nullable.of(moduleType), name);
  }

  @Override
  public String getName() {
    return name;
  }

}
