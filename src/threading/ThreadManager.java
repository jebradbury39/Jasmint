package threading;

import errors.Nullable;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/*
 * Pass this to each project transformation.
 * This takes a queue of Jobs. Some Jobs will have a dependency on other Jobs, so they will
 * not be put in the active queue until the other Jobs are done.
 */
public class ThreadManager {
  private ConcurrentLinkedQueue<Job> queue = new ConcurrentLinkedQueue<>();
  private AtomicBoolean queueSealed = new AtomicBoolean(false);
  //when queue is sealed and empty, wipe this
  private ConcurrentHashMap<String, Job.Status> doneJobs = new ConcurrentHashMap<>();
  private ReentrantReadWriteLock rwlockDoneJobs = new ReentrantReadWriteLock();
  
  private List<Worker> workers = new LinkedList<>();

  public ThreadManager(int numWorkers) {
    for (int i = 0; i < numWorkers; ++i) {
      workers.add(new Worker(this));
      workers.get(i).start();
    }
  }

  synchronized Nullable<Job> getNextJob() {
    //check queue for next job. If the jobs it is waiting on are not done, then keep waiting
    Job nxt = queue.peek();
    if (nxt == null) {
      return Nullable.empty();
    }
    boolean ready = true;
    boolean noneActiveOrDone = true;
    for (String jid : nxt.waitingOnJobs) {
      rwlockDoneJobs.readLock().lock();
      Job.Status tmp = doneJobs.get(jid);
      rwlockDoneJobs.readLock().unlock();
      if (tmp == Job.Status.NONE) {
        ready = false;
      } else if (tmp == Job.Status.ACTIVE) {
        ready = false;
        noneActiveOrDone = false;
      } else {
        noneActiveOrDone = false;
      }
    }
    if (!nxt.waitingOnJobs.isEmpty() && noneActiveOrDone) {
      //seems to be a circular dependency?
      throw new IllegalArgumentException();
    }
    if (!ready) {
      return Nullable.empty();
    }
    return Nullable.of(queue.remove());
  }

  public void openQueue() {
    queueSealed.set(false);
    queue.clear();
    rwlockDoneJobs.writeLock().lock();
    doneJobs.clear();
    rwlockDoneJobs.writeLock().unlock();
  }

  public void addToQueue(Job newJob) {
    if (queueSealed.get()) {
      throw new IllegalArgumentException("Cannot add job to sealed queue");
    }
    queue.add(newJob);
    rwlockDoneJobs.writeLock().lock();
    doneJobs.put(newJob.id, Job.Status.NONE);
    rwlockDoneJobs.writeLock().unlock();
  }

  public void sealQueue() {
    queueSealed.set(true);
  }

  //return true if all jobs finished successfully, else there was at least one error
  public boolean waitForJobs() {
    //wait until queue is empty and all workers are idle
    //basically just check all entries of doneJobs
    //TODO maybe add statuses to each worker instead to avoid constant looping here?
    boolean running = true;
    boolean hitError = false;
    while (running) {
      running = false;
      rwlockDoneJobs.readLock().lock();
      if (doneJobs.isEmpty()) {
        throw new IllegalArgumentException();
      }
      for (Job.Status j : doneJobs.values()) {
        switch (j) {
          case FAILURE:
            hitError = true;
            break;
          case NONE:
          case ACTIVE:
            running = true;
            break;
          case SUCCESS:
            break;
          default:
            throw new IllegalArgumentException();
        }
      }
      rwlockDoneJobs.readLock().unlock();
    }
    return !hitError;
  }

  public void jobDone(String jid, Job.Status jobStatus) {
    rwlockDoneJobs.writeLock().lock();
    doneJobs.put(jid, jobStatus);
    rwlockDoneJobs.writeLock().unlock();
  }

  public void stop() throws InterruptedException {
    for (Worker worker : workers) {
      worker.stopWorker();
    }
    for (Worker worker : workers) {
      worker.join();
    }
  }
}
