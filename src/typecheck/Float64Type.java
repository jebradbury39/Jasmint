package typecheck;

import ast.Expression;
import ast.FloatExpression;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;

public class Float64Type extends AbstractType {
  
  private static Float64Type instance = null;
  private static Float64Type constInstance = null;
  
  public static Float64Type Create() {
    return Create(false);
  }
  
  public static Float64Type Create(boolean isConst) {
    if (instance == null) {
      instance = new Float64Type();
      constInstance = new Float64Type();
      constInstance.isConst = true;
    }
    if (isConst) {
      return constInstance;
    }
    return instance;
  }
  
  private Float64Type() {
    super(TypeType.FLOAT64_TYPE);
  }
  
  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    return other instanceof Float64Type;
  }

  @Override
  public List<Type> getInnerTypes() {
    return new ArrayList<Type>();
  }
  
  @Override
  public String toString() {
    return super.toString() + "float64";
  }
  
  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }
    
    return this;
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.Type.Builder document = preSerialize();
    document.setFloat64Type(AstProto.Float64Type.getDefaultInstance());
    return document;
  }
  
  public static Float64Type deserialize(AstProto.Float64Type document) {
    return Create();
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(this);
  }
  
  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return new HashSet<>();
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) {
    return Optional.of(this);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Type setConst(boolean isConst) {
    return Create(isConst);
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(Nullable.of(this));
  }
  
  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new FloatExpression("0.0");
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    if (!isTop) {
      return this;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(this);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return 8;
  }
  
  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return true;
  }

  @Override
  public Type asClassDeclType() {
    return this;
  }
}
