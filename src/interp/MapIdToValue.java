package interp;

import java.util.Map;

import typecheck.FunctionType;
import typecheck.Type;
import util.Pair;

public class MapIdToValue implements IdToValue {
  private ValueBox vBox;
  
  @Override
  public String toString() {
    return vBox.toString();
  }

  @Override
  public IdToValue write(String name, ValueBox vBox) {
    this.vBox = vBox;
    return this;
  }

  @Override
  public Pair<ValueBox, Integer> read(Environment env, String name, FunctionType functionType) throws InterpError {
    return new Pair<ValueBox, Integer>(vBox, -1);
  }

  @Override
  public IdToValue fullCopy(String name, Environment copyEnv) throws InterpError {
    MapIdToValue copy = new MapIdToValue();
    copy.write(name, vBox.copy(copyEnv));
    return copy;
  }

  @Override
  public IdToValue replaceType(String name, Map<Type, Type> mapFromTo,
      String moduleAppend) throws InterpError {
    MapIdToValue copy = new MapIdToValue();
    copy.write(name, vBox.replaceType(mapFromTo, moduleAppend));
    return copy;
  }
}
