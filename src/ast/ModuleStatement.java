package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditRenameTypes;
import audit.AuditRenameTypesLite;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.GenericTree;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class ModuleStatement extends AbstractStatement {

  public final ModuleType fullType; // parent is outerType
  public final ModuleType originalName; // full original name
  // not real module, just provide stuff for the actual module entry
  // link this provided stuff in to the real module for transpile time
  public final boolean isProvider;
  public final boolean isFfi;

  public ModuleStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, ModuleType fullType, ModuleType originalName,
      boolean isProvider, boolean isFfi) {
    super(lineNum, columnNum, AstType.MODULE_STATEMENT, preComments, postComments);
    this.fullType = fullType;
    this.originalName = originalName;
    this.isProvider = isProvider;
    this.isFfi = isFfi;
  }

  @Override
  public String toString(String indent) {
    String result = preToString() + "module " + fullType.name;
    if (isProvider) {
      result = "provider " + fullType.name;
    }

    if (fullType.outerType.isNotNull()) {
      result += " of " + fullType.outerType;
    }

    return result + postToString();
  }

  /*
   * e.g. our path is officially std.string.String, but we want to just be
   * std.String so redir std.String to std.string.String So one module can
   * actually expand to multiple pseudo-modules (AsciiString, UnicodeString)
   * 
   * Look up parent (std) and get parent tenv Add entry to std tenv: "String" ->
   * ModuleType{string, expand=String} Add entry to std tenv: "string" ->
   * ModuleType{string} Lookup on "String" returns UserTypeEntry, while lookup on
   * "string" returns ModuleType
   */

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    return Optional.of(new TypecheckRet(VoidType.Create()));
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.ModuleStatement.Builder subDoc = AstProto.ModuleStatement.newBuilder();

    subDoc.setTarget(fullType.serialize());
    subDoc.setOriginalName(originalName.serialize());
    subDoc.setIsProvider(isProvider);
    subDoc.setIsFfi(isFfi);

    AstProto.Ast.Builder document = preSerialize();
    document.setModuleStatement(subDoc);
    return document;
  }

  public static Statement deserialize(int dLineNum, int dColumnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.ModuleStatement document) throws SerializationError {
    return new ModuleStatement(dLineNum, dColumnNum, preComments, postComments,
        (ModuleType) AbstractType.deserialize(document.getTarget()),
        (ModuleType) AbstractType.deserialize(document.getOriginalName()),
        document.getIsProvider(), document.getIsFfi());
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    // TODO Auto-generated method stub
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    // TODO Auto-generated method stub
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    ModuleStatement value = new ModuleStatement(lineNum, columnNum, preComments, postComments,
        (ModuleType) fullType.replaceType(mapFromTo), originalName, isProvider, isFfi);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    // TODO Auto-generated method stub
    return Optional.of(this);
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "called wrong version of renameIds",
        lineNum, columnNum);
    return null;
  }

  public ModuleStatement renameIds(Project project, GenericTree<String, String> renamings,
      MsgState msgState) throws FatalMessageException {
    ModuleType newFullType = fullType;
    if (fullType.outerType != null) {
      // find our new module name (our renamings "import" our top parent)
      // newFullType = fullType.realRenameIds(renamings, false, msgState);
    } else {
      newFullType = new ModuleType(Nullable.empty(), fullType.name + "_" + label);
    }
    ModuleStatement val = new ModuleStatement(lineNum, columnNum, preComments, postComments,
        newFullType, originalName, isProvider, isFfi);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) {
    return Optional.of(this);
  }

  public void createRenaming(GenericTree<String, String> globalRenamings) {
    if (isFfi) {
      globalRenamings.value = Nullable.of(fullType.name);
    } else {
      globalRenamings.value = Nullable.of(fullType.name + "_" + label);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize,
      MsgState msgState)
      throws FatalMessageException {
    return Optional.of(new ResolveExcRet(new LinkedList<>(), Nullable.of(this)));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  /*
   * need access to parent module renamings in order to compute what our full new
   * name will be
   */
  public ModuleType populateTypeRenamings(AuditRenameTypes auditEntry,
      AuditRenameTypesLite moduleRenamings, MsgState msgState) throws FatalMessageException {
    
    //there should not be any renamings yet, unless we have another version of this module in the
    // build
    Nullable<ModuleType> tmpNewType = moduleRenamings.getNewType(fullType);
    if (tmpNewType.isNotNull()) {
      // TODO is it dangerous to ignore this? Real issues of dupe mods will be handled via linking
      // and typecheck
      //msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAME_TYPES,
      //    "Found duplicate renaming for: " + fullType);
      
      // already in moduleRenamings, but need to update our auditEntry
      auditEntry.typeRenamings.put(fullType, tmpNewType.get());
      
      return tmpNewType.get();
    }
    
    String newName = fullType.name + "_MT_" + getTemp();
    if (isFfi) {
      newName = fullType.name;
    }
    
    ModuleType newType = new ModuleType(fullType.outerType, newName);
    if (fullType.outerType.isNotNull()) {
      Nullable<ModuleType> tmpParent = moduleRenamings.getNewType(fullType.outerType.get());
      if (tmpParent.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAME_TYPES,
            "Failed to find renaming for parent module: " + fullType.outerType);
      }
      newType = new ModuleType(tmpParent, newName);
    }
    
    auditEntry.typeRenamings.put(fullType, newType);
    
    //also add the module renaming to the project-level renamings
    moduleRenamings.put(fullType, newType);
    
    return newType;
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) {
    return new InsertDestructorsRet(this);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
