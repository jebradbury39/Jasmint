package typecheck;

import ast.Program.DestructorRenamings;
import ast.Statement;
import errors.FatalMessageException;
import java.util.Optional;
import type_env.TypeEnvironment;

/*
 * This type supports dot-accesses
 */

public interface DotAccessType extends Type {
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) throws FatalMessageException;

  public ModuleType getModuleType();

  public UserDeclType asUserDeclType();

  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException;
}
