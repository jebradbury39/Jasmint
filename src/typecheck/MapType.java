package typecheck;

import ast.Expression;
import ast.MapInitExpression;
import ast.Program.DestructorRenamings;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.BasicTypeBox;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.FunctionType.LambdaStatus;

public class MapType extends AbstractType implements DotAccessType, ReferencedType {
  public final Type keyType;
  public final Type valueType;

  public MapType(Type keyType, Type valueType) {
    super(TypeType.MAP_TYPE);
    this.keyType = keyType;
    this.valueType = valueType;
  }

  @Override
  public int hashCode() {
    return super.hashCode() + keyType.hashCode() + valueType.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof MapType)) {
      return false;
    }
    MapType otherMT = (MapType) other;
    return keyType.equals(otherMT.keyType) && valueType.equals(otherMT.valueType);
  }

  @Override
  public List<Type> getInnerTypes() {
    List<Type> tyList = new ArrayList<Type>();
    tyList.add(keyType);
    tyList.add(valueType);
    return tyList;
  }

  @Override
  public String toString() {
    return super.toString() + "{" + keyType + ", " + valueType + "}";
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }

    return new MapType(keyType.replaceType(mapFromTo), valueType.replaceType(mapFromTo));
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.MapType.Builder subDoc = AstProto.MapType.newBuilder();

    subDoc.setKeyType(keyType.serialize().build());
    subDoc.setValType(valueType.serialize().build());

    document.setMapType(subDoc);
    return document;
  }

  public static MapType deserialize(AstProto.MapType document) throws SerializationError {
    return new MapType(AbstractType.deserialize(document.getKeyType()),
        AbstractType.deserialize(document.getValType()));
  }

  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    TypeEnvironment newTenv = new TypeEnvironment(TypeEnvironment.ScopeType.INSTANCE,
        tenv.ofDeclType, tenv.typeGraph, tenv.callGraph, tenv.project, tenv.effectiveImports,
        tenv.msgState);

    List<Type> addParam = new LinkedList<>();
    addParam.add(keyType);
    addParam.add(valueType);
    newTenv.defineVar("put", new BasicTypeBox(
        new FunctionType(this, VoidType.Create(), addParam, LambdaStatus.NOT_LAMBDA), true, true,
        VariableScoping.CLASS_INSTANCE));

    List<Type> removeParam = new LinkedList<>();
    removeParam.add(keyType);
    newTenv.defineVar("remove", new BasicTypeBox(
        new FunctionType(this, valueType, removeParam, LambdaStatus.NOT_LAMBDA), true, true,
        VariableScoping.CLASS_INSTANCE));

    List<Type> containsParam = new LinkedList<>();
    containsParam.add(keyType);
    newTenv.defineVar("contains", new BasicTypeBox(
        new FunctionType(this, BoolType.Create(), containsParam, LambdaStatus.NOT_LAMBDA), true, true,
        VariableScoping.CLASS_INSTANCE));

    List<Type> sizeParam = new LinkedList<>();
    newTenv.defineVar("size", new BasicTypeBox(
        new FunctionType(this, IntType.Create(false, 64), sizeParam, LambdaStatus.NOT_LAMBDA), true, true,
        VariableScoping.CLASS_INSTANCE));

    return Optional.of(newTenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (types.contains(this)) {
      return true;
    }
    return keyType.containsType(types) || valueType.containsType(types);
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    stackTypes.addAll(keyType.getUserDeclTypes(stackTypesOnly));
    stackTypes.addAll(valueType.getUserDeclTypes(stackTypesOnly));
    return stackTypes;
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    Optional<Type> optKey = keyType.normalize(tenv, lineNum, columnNum);
    if (!optKey.isPresent()) {
      return Optional.empty();
    }
    Optional<Type> optVal = valueType.normalize(tenv, lineNum, columnNum);
    if (!optVal.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new MapType(optKey.get(), optVal.get()));
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return new MapType(keyType.renameIds(renamings, msgState),
        valueType.renameIds(renamings, msgState));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException {
    keyType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    valueType.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
  }

  @Override
  public Type trimRelativeType(UserInstanceType currentModuleFullType) {
    return new MapType(keyType.trimRelativeType(currentModuleFullType),
        valueType.trimRelativeType(currentModuleFullType));
  }

  @Override
  public Type setConst(boolean isConst) {
    MapType tmp = new MapType(keyType, valueType);
    tmp.isConst = isConst;
    return tmp;
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Optional<Nullable<Type>> tmpKey = keyType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmpKey.isPresent()) {
      return Optional.empty();
    }
    Optional<Nullable<Type>> tmpVal = valueType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmpVal.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(Nullable.of(new MapType(tmpKey.get().get(), tmpVal.get().get())));
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState, Set<ModuleType> exportedTypes, boolean skipNormalize)
      throws FatalMessageException {
    return keyType.getNormalizedModType(currentModule, effectiveImports, msgState, exportedTypes,
        skipNormalize)
        && valueType.getNormalizedModType(currentModule, effectiveImports, msgState, exportedTypes,
            skipNormalize);
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new MapInitExpression(keyType, valueType, new HashMap<>());
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    MapType newType = new MapType(keyType.prependFnRetType(prependType, false),
        valueType.prependFnRetType(prependType, false));
    if (!isTop) {
      return newType;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(newType);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    int keySize = keyType.calculateSize(tenv, lineNum, columnNum);
    if (keySize == -1) {
      return -1;
    }
    int valueSize = valueType.calculateSize(tenv, lineNum, columnNum);
    if (valueSize == -1) {
      return -1;
    }
    // add 8 bytes for map info
    return 8 + keySize + valueSize;
  }

  @Override
  public Optional<Statement> createDestructorCall(String id,
      DestructorRenamings destructorRenamings) {
    // TODO, will want some kind of for-in loop. Don't want to call destructor on
    // copy of key or val
    return Optional.empty();
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return keyType.copyFromTenvIntoTenv(tenv, newTenv)
        && valueType.copyFromTenvIntoTenv(tenv, newTenv);
  }

  @Override
  public Type asClassDeclType() {
    return new MapType(keyType.asClassDeclType(), valueType.asClassDeclType());
  }

  @Override
  public ModuleType getModuleType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public UserDeclType asUserDeclType() {
    throw new UnsupportedOperationException();
  }
}
