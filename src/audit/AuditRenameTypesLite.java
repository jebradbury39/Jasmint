package audit;

import ast.SerializationError;
import astproto.AstProto;
import astproto.AstProto.AuditEntryLite.Builder;
import audit.AuditEntry.AuditEntryType;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import errors.Nullable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import typecheck.AbstractType;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;
import typecheck.Type;

public class AuditRenameTypesLite implements AuditEntryLite {
  //map old name to new name, invert to get new name to old name
  //only abs module names for this project
  private final BiMap<ModuleType, ModuleType> typeRenamings;
  
  public AuditRenameTypesLite(BiMap<ModuleType, ModuleType> typeRenamings) {
    this.typeRenamings = typeRenamings;
  }

  public AuditRenameTypesLite() {
    this.typeRenamings = HashBiMap.create();
  }
  
  @Override
  public String toString() {
    return getType().toString();
  }

  @Override
  public AuditEntryType getType() {
    return AuditEntryType.RENAME_TYPES;
  }

  public static AuditRenameTypesLite deserialize(AstProto.AuditRenameTypesLite document)
      throws SerializationError {
    AuditRenameTypesLite newRenamings = new AuditRenameTypesLite();
    
    Iterator<AstProto.Type> iterKeys = document.getKeysList().iterator();
    Iterator<AstProto.Type> iterVals = document.getValsList().iterator();
    while (iterKeys.hasNext()) {
      newRenamings.typeRenamings.put((ModuleType) AbstractType.deserialize(iterKeys.next()),
          (ModuleType) AbstractType.deserialize(iterVals.next()));
    }
    
    return newRenamings;
  }

  @Override
  public Builder serializeLite() {
    AstProto.AuditEntryLite.Builder document = AstProto.AuditEntryLite.newBuilder();
    document.setAuditEntryType(getType().ordinal());
    
    AstProto.AuditRenameTypesLite.Builder subDoc = AstProto.AuditRenameTypesLite.newBuilder();
    
    for (Entry<ModuleType, ModuleType> entry : typeRenamings.entrySet()) {
      subDoc.addKeys(entry.getKey().serialize());
      subDoc.addVals(entry.getValue().serialize());
    }
    
    document.setAuditRenameTypesLite(subDoc);
    return document;
  }

  public void put(ModuleType fromTy, ModuleType toTy) {
    typeRenamings.put(fromTy, toTy);
  }
  
  public Nullable<ModuleType> getOldType(ModuleType newType) {
    ModuleType tmp = typeRenamings.inverse().get(newType);
    if (tmp == null) {
      return Nullable.empty();
    }
    return Nullable.of(tmp);
  }

  public Nullable<ModuleType> getNewType(ModuleType oldType) {
    ModuleType tmp = typeRenamings.get(oldType);
    if (tmp == null) {
      return Nullable.empty();
    }
    return Nullable.of(tmp);
  }
  
  // replace entire type only if it is a module, otherwise only replace outerType if present
  public Nullable<ResolvedImportType> getNewType(ResolvedImportType oldType) {
    // check if this imports a module
    ModuleType oldModType = oldType.asModuleType();
    ModuleType tmp = typeRenamings.get(oldModType);
    if (tmp == null) {
      return Nullable.empty();
    }
    return Nullable.of(oldType.replaceModuleType(tmp));
  }

  public Map<Type, Type> getMap() {
    Map<Type, Type> entries = new HashMap<>();
    
    for (Entry<ModuleType, ModuleType> entry : typeRenamings.entrySet()) {
      entries.put(entry.getKey(), entry.getValue());
    }
    
    return entries;
  }

}
