package multifile;

import ast.Ast.AnfTune;
import ast.ImportStatement;
import ast.Program;
import ast.SerializationError;
import astproto.AstProto;
import audit.AuditBasic;
import audit.AuditEntry;
import audit.AuditEntry.AuditEntryType;
import audit.AuditLiftedClasses;
import audit.AuditRenameIds;
import audit.AuditRenameTypes;
import audit.AuditRenameTypesLite;
import audit.AuditResolveExceptions;
import audit.AuditTrailLite;
import callgraph.CallGraph;
import callgraph.CallNode;
import classgraph.ClassGraph;
import classgraph.Ordering.CompleteOrdering;
import classgraph.Ordering.ModuleOrdering;
import com.google.protobuf.util.JsonFormat;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.ProgHeader;
import header.ProgItemHeader;
import header.PsuedoClassHeader;
import import_mgmt.EffectiveImports;
import import_mgmt.ImportGraph;
import interp.InterpError;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import mbo.ModuleBuildObj;
import mbo.ModuleBuildObj.ProgressStatus;
import mbo.Renamings;
import multifile.BuildManifest.BuildEntry;
import multifile.BuildManifest.BuildManifestProvider;
import threading.Job;
import threading.ThreadManager;
import type_env.TypeEnvironment;
import typecheck.ClassDeclType;
import typecheck.ImportType;
import typecheck.ModuleImportType;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;
import typecheck.Type;
import typecheck.UndeterminedImportType;
import typecheck.UserDeclType;
import typegraph.TypeGraph;
import util.Pair;
import util.StringFn;

/*
 * This is one project with no dependencies. Basically if you have a project with N src/ folders, 
 * all those sources are collected and managed here.
 * However, the Project is also what manages multiple projects
 * and how they interact. So if you have third_party/ libs,
 * each of those will be its own project, and this
 * project must list those projects as dependencies.
 * 
 * > std.jsmnt within std project
 * library std
 * sub class String String //Find std.String library (sub appends std.).
 * Attach ONLY String class as std.String. Basically reroutes std.String to std.String.String.
 * Lib name and class name do not need to be the same
 * sub library Network   //Find std.Network library. Attach as std.Network
 * 
 * import ...
 * jasmint...
 * 
 * > std/string.jsmnt
 * library std.String
 * class String {...}
 * 
 * > std/network.jsmnt
 * library std.Network
 * sub library Ipv4
 * sub library Ipv6
 * sub class Socket SocketFoo //lookup into std.Network tenv has entry
 * (SocketFoo -> RedirLib{std.Network.Socket.SocketFoo}) and will return UserTypeEntry, not Library
 * sub class Socket SocketBar
 */
public class Project {

  public final List<String> authors;
  public final String description;
  public final  ProjectNameVersion projectNameVersion;
  //Your parsed sources (flattened out. library names are not scoped by directory.
  //OS should not matter)
  private SourceManager sources;
  //sources contains all the unbuilt files (or files which need to be rebuilt due to edits)
  //Also need the built files list (gotten by only parsing the module name, then checking the build
  //dir for the corresponding file. If found, compare hash).
  //This is all done by the ProjectAssembler

  //Just a list of third party projects which this project depends upon
  public final Set<ProjectNameVersion> desiredLibs;
  private List<Project> libs = new LinkedList<>(); //No cycle in libs. Use for lookup
  private List<Project> buildLibs = new LinkedList<>(); //only do transforms on these
  private Project rootProject = this;
  public final Path buildto; //absolute path
  //the transpiler must choose one key to import their providers
  public final Map<String, Map<ModuleType, ModuleEndpoint>> providers;

  //computed
  private List<ModuleType> moduleProcessOrder = new LinkedList<>();
  private AuditTrailLite auditTrail = new AuditTrailLite(); //expected progress, don't need data
  //Class name (abs path, with transforms) to attrs
  //clear this before each typecheck run
  private Set<UserDeclType> completeClasses = new HashSet<>();
  private Set<Pair<ClassDeclType, String>> initClassMembers = new HashSet<>();
  
  private Set<CallNode> sandboxCalledFunctions = new HashSet<>();
  public final CallGraph callGraph;

  private int cacheId = 0;
  final MsgState msgState; //overall messages
  final Map<String, MsgState> msgStates; //file-specific messages
  final BuildManifest manifest;
  
  public Project(List<String> authors, String description,
      ProjectNameVersion projectNameVersion,
      Map<ModuleType, ModuleEndpoint> sources, Set<ProjectNameVersion> desiredLibs, Path buildto,
      Map<String, Map<ModuleType, ModuleEndpoint>> providers,
      Map<String, MsgState> msgStates, BuildManifest manifest) {
    this.authors = authors;
    this.description = description;
    this.projectNameVersion = projectNameVersion;
    this.sources = new SourceManager(sources);
    this.desiredLibs = desiredLibs;
    this.buildto = buildto;
    this.providers = providers;
    this.msgState = new MsgState(Nullable.empty());
    this.msgStates = msgStates;
    this.manifest = manifest;
    this.callGraph = new CallGraph(msgState);
    
    for (ModuleEndpoint src : sources.values()) {
      src.project = this;
    }
    for (Map<ModuleType, ModuleEndpoint> provider : providers.values()) {
      for (ModuleEndpoint provModEp : provider.values()) {
        provModEp.project = this;
      }
    }
  }
  
  @Override
  public String toString() {
    return projectNameVersion.toString();
  }
  
  public void addAllSources(Map<ModuleType, ModuleEndpoint> newSources) {
    for (Entry<ModuleType, ModuleEndpoint> src : newSources.entrySet()) {
      sources.put(src.getKey(), src.getValue(), auditTrail);
      src.getValue().project = this;
    }
  }

  public int getCacheId() {
    return cacheId;
  }
  
  public AuditTrailLite getAuditTrail() {
    return auditTrail;
  }
  
  private void helpBuildImportGraph(ImportGraph graph) throws FatalMessageException {
    for (Project lib : buildLibs) {
      lib.helpBuildImportGraph(graph);
    }
    
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.populateImportGraph(auditTrail, graph);
    }
  }
  
  public ImportGraph buildImportGraph() throws FatalMessageException {
    ImportGraph graph = new ImportGraph();
    
    helpBuildImportGraph(graph);
    
    return graph;
  }
  
  public Set<ProjectNameVersion> findExportedProjects() throws FatalMessageException {
    Set<ModuleType> exportedTypes = new HashSet<>();
    
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      moduleEp.load();
      LoadedModuleHandle modHandle = (LoadedModuleHandle) moduleEp.getHandle();
      exportedTypes.addAll(modHandle.moduleBuildObj.program.findExportedTypes(
          modHandle.moduleType, modHandle.moduleBuildObj.program.getEffectiveImports(this, false),
          msgState).get());
    }
    
    //now for each of the module types, find which project they came from
    Set<ProjectNameVersion> exportedProjects = new HashSet<>();
    
    for (ModuleType modType : exportedTypes) {
      Nullable<Project> tmp = lookupDefiningProject(modType);
      if (tmp.isNull()) {
        //invalid
      }
      Project proj = tmp.get();
      if (proj == this) {
        continue;
      }
      exportedProjects.add(proj.projectNameVersion);
    }
    
    return exportedProjects;
  }
  
  public Nullable<Project> lookupDefiningProject(ModuleType modType) throws FatalMessageException {
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      if (moduleEp.getModuleType(auditTrail).equals(modType)) {
        return Nullable.of(this);
      }
    }
    
    for (Project lib : libs) {
      Nullable<Project> tmp = lib.lookupDefiningProject(modType);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    
    return Nullable.empty();
  }
  
  public Nullable<Project> lookupDefiningProject(ImportType importType) throws FatalMessageException {
    // first assume that this is importing a module fully
    ModuleType modImpType = new ModuleType(importType.outerType, importType.name);
    Nullable<Project> foundProj = lookupDefiningProject(modImpType);
    if (foundProj.isNotNull()) {
      if (!(importType instanceof ModuleImportType || importType instanceof UndeterminedImportType)) {
        return Nullable.empty();
      }
      return foundProj;
    }
    if (importType instanceof ModuleImportType) {
      return Nullable.empty();
    }
    if (importType.outerType.isNull()) {
      return Nullable.empty();
    }
    
    return lookupDefiningProject(importType.outerType.get());
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Linking
  /////////////////////////////////////////////////////////////////////////////////////////////////
  
  private boolean linkAndVerifySubmods() throws FatalMessageException {
    //modpath->(submod, isLinkedToParent)
    Map<ModuleType, Map<ModuleType, Boolean>> submodLinks = new HashMap<>();
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      ModuleHandle moduleHandle = moduleEp.getHandle();
      //if no parent, then nothing to do here
      if (moduleHandle.getModuleType(auditTrail, msgState).outerType.isNull()) {
        continue;
      }
      //find parent mod and ask to be their child

      ModuleEndpoint parentModuleEp = sources.get(moduleHandle.getModuleType(
          auditTrail, msgState).outerType.get(),
          auditTrail).get();
      if (parentModuleEp == null) {
        //don't search libs since you cannot submod something from another project
        //(since they can never claim you as a child)
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot find parent module '"
                + moduleHandle.getModuleType(auditTrail, msgState).outerType
                + "' of module '" + moduleHandle.getModuleType(auditTrail, msgState) + "'");
        continue;
      }
      ModuleHandle parentModuleHandle = parentModuleEp.getHandle();

      //make sure the parent agrees that this is their child
      if (!parentModuleHandle.submods.contains(moduleHandle.getModuleType(auditTrail, msgState))) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "Module '"
            + parentModuleHandle.getModuleType(auditTrail, msgState) + "' does not claim '"
            + moduleHandle.getModuleType(auditTrail, msgState) + "' as a submod");
        continue;
      }

      Map<ModuleType, Boolean> parentModuleSubmods = submodLinks.get(
          moduleHandle.getModuleType(auditTrail, msgState).outerType.get());
      if (parentModuleSubmods == null) {
        parentModuleSubmods = new HashMap<>();
        submodLinks.put(moduleHandle.getModuleType(auditTrail, msgState).outerType.get(),
            parentModuleSubmods);
      }
      parentModuleSubmods.put(moduleHandle.getModuleType(auditTrail, msgState), true);
    }
    //now verify that all submods were found
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      ModuleHandle moduleHandle = moduleEp.getHandle();
      //if no children, then nothing to do here
      if (moduleHandle.submods.isEmpty()) {
        continue;
      }
      Map<ModuleType, Boolean> moduleSubmods = submodLinks.get(moduleHandle.getModuleType(
          auditTrail, msgState));
      if (moduleSubmods == null) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Module '" + moduleHandle.getModuleType(auditTrail, msgState)
            + " has submods but they do not list their parent: " + moduleHandle.submods);
        continue;
      }
      //ensure all submods were filled in
      for (ModuleType submod : moduleHandle.submods) {
        if (!moduleSubmods.containsKey(submod)) {
          msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "Module '"
              + moduleHandle.getModuleType(auditTrail, msgState) + " has submod '"
              + submod + "' but it does not list it as"
              + " the parent");
          continue;
        }
      }
    }

    boolean err = msgState.hitError();
    for (Project lib : buildLibs) {
      lib.linkAndVerifySubmods();
      if (lib.hitError()) {
        err = true;
      }
    }
    return err;
  }
  
  // return true if success
  // Make sure to do this BEFORE linking modules
  private boolean resolveAndVerifyImports() throws FatalMessageException {
    // go through all our sources and resolve imports in program and progheader
    boolean err = false;
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      moduleEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) moduleEp.getHandle();
      
      Optional<Program> program = moduleHandle.moduleBuildObj.program.resolveImports(this,
          moduleEp.msgState);
      if (!program.isPresent()) {
        err = true;
        continue;
      }
      moduleHandle.moduleBuildObj.program = program.get();
      
      // now update effective imports
      Optional<EffectiveImports> effectiveImports = moduleHandle.moduleBuildObj.effectiveImports.resolveImports(this, moduleEp.msgState);
      if (!effectiveImports.isPresent()) {
        err = true;
        continue;
      }
      moduleHandle.moduleBuildObj.effectiveImports = effectiveImports.get();
      moduleHandle.effectiveImports = effectiveImports.get();
      Nullable<BuildEntry> buildEntry = moduleHandle.manifest.getEntry(moduleHandle.srcFilename);
      if (buildEntry.isNull()) {
        err = true;
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Failed to find manifest entry for: "
            + moduleHandle.srcFilename);
        continue;
      }
      buildEntry.get().effectiveImports = effectiveImports.get();
    }
    
    if (msgState.hitError()) {
      err = true;
    }
    for (Project lib : buildLibs) {
      lib.resolveAndVerifyImports();
      if (lib.hitError()) {
        err = true;
      }
    }
    return !err;
  }

  // inherit from parents as well as all mods we are dependent on
  private void inheritRebuildFromDependOn() throws FatalMessageException {
    
    ClassGraph classGraph = new ClassGraph(Nullable.empty(), msgState);
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      moduleEp.populateClassGraphPlus(this, classGraph, false);
    }
    
    classGraph.determineRebuilds(sources, auditTrail);

    for (Project lib : buildLibs) {
      lib.inheritRebuildFromDependOn();
    }
  }
  
  private class LinkJobContext {
    final ModuleType moduleType;
    final Project project;
    
    LinkJobContext(ModuleType moduleType, Project project) {
      this.moduleType = moduleType;
      this.project = project;
    }
  }

  public static Job.Status linkHeaderJob(Object ctx) throws FatalMessageException {
    LinkJobContext context = (LinkJobContext) ctx; //this is the module to link the progheader for
    
    //find our build object
    Nullable<ModuleEndpoint> modEpTmp = context.project.lookupModule(context.moduleType,
        context.project.auditTrail);
    if (modEpTmp.isNull()) {
      context.project.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Unable to find our module '" + context.moduleType + "' in: "
          + context.project.sources); //TODO show libs as well
      return Job.Status.FAILURE;
    }
    ModuleEndpoint modEp = modEpTmp.get();
    modEp.load(); //ensure we have loaded the build obj
    if (!(modEp.getHandle() instanceof LoadedModuleHandle)) {
      context.project.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Failed to load module '" + context.moduleType + "'");
      return Job.Status.FAILURE;
    }
    LoadedModuleHandle modHandle = (LoadedModuleHandle) modEp.getHandle();
    
    //find the ProgHeaders for all of our imports and create mappings
    Map<ModuleType, ProgHeader> importedMods = new HashMap<>();
    Map<String, ClassHeader> importedClasses = new HashMap<>();
    for (ImportStatement imp : modHandle.moduleBuildObj.program.imports) {
      modEpTmp = context.project.lookupModule(imp.target, context.project.auditTrail);
      if (modEpTmp.isNull()) {
        context.project.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Failed to locate module '" + imp.target + "'",
            imp.lineNum, imp.columnNum);
        return Job.Status.FAILURE;
      }
      
      //load the imported module handle so we can access the ProgHeader
      ModuleEndpoint impModEp = modEpTmp.get();
      impModEp.load();
      if (!(impModEp.getHandle() instanceof LoadedModuleHandle)) {
        context.project.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to load module '"
                + impModEp.getHandle().getModuleType(
                    context.project.auditTrail, context.project.msgState) + "'",
            imp.lineNum, imp.columnNum);
        return Job.Status.FAILURE;
      }
      LoadedModuleHandle impModHandle = (LoadedModuleHandle) impModEp.getHandle();
      
      importedMods.put(impModHandle.getModuleType(
          context.project.auditTrail, context.project.msgState),
          impModHandle.moduleBuildObj.progHeader);
    }
    
    //TODO probably do this with submods as well
    
    //add our own module as an import
    importedMods.put(modHandle.getModuleType(
        context.project.auditTrail, context.project.msgState), modHandle.moduleBuildObj.progHeader);
    
    //link our header
    modHandle.moduleBuildObj.progHeader.linkImports(importedMods, importedClasses,
        context.project.msgState);
    
    modHandle.moduleBuildObj.auditTrail.add(new AuditBasic(AuditEntryType.LINK_HEADERS));
    
    return Job.Status.SUCCESS;
  }

  private void linkHeaders(ThreadManager threadManager) throws FatalMessageException {
    
    for (Project lib : buildLibs) {
      //projects must always link first
      lib.linkHeaders(threadManager);
    }
    
    //build classgraph purely from class extends
    ClassGraph classGraph = new ClassGraph(Nullable.empty(), msgState);
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      moduleEp.populateClassGraphPlus(this, classGraph, false);
    }

    //abstract this up to module level, but once in a module, follow the class order (in case
    //of sibling extends)
    Optional<CompleteOrdering> completeOrderingOpt = classGraph.determineAllModsOrder(msgState,
        false);
    if (!completeOrderingOpt.isPresent()) {
      return;
    }
    CompleteOrdering completeOrdering = completeOrderingOpt.get();

    //based on parent class order, link module headers together
    //e.g. b.B extends a.A, so we must link b after a is done linking
    //we ensure this by adding jobs to the ThreadManager queue
    
    Map<UserDeclType, String> jobMap = new HashMap<>(); //module to job id
    for (ModuleType moduleType : completeOrdering.moduleOrdering.keySet()) {
      if (!sources.containsKey((ModuleType) moduleType, auditTrail)) {
        //only add modules from this Project, not any libs
        //this means we will only depend on modules in our own proj, all libs are already queued up
        continue;
      }
      Job newJob = new Job(Project::linkHeaderJob, new LinkJobContext(moduleType, this));
      Set<UserDeclType> modUplinks = completeOrdering.mapDirectMods.get(moduleType);
      for (UserDeclType uplink : modUplinks) {
        if (!sources.containsKey((ModuleType) uplink, auditTrail)) {
          //external mods already queued up
          continue;
        }
        if (uplink.equals(moduleType)) {
          continue;
        }
        newJob.addDependency(jobMap.get(uplink));
      }
      jobMap.put(moduleType, newJob.id);
      threadManager.addToQueue(newJob);
    }
    
  }
  
  
  private void collectInheritedImports(ImportGraph graph) throws FatalMessageException {
    
    for (Project lib : buildLibs) {
      lib.collectInheritedImports(graph);
    }
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      moduleEp.load();
      LoadedModuleHandle modHandle = (LoadedModuleHandle) moduleEp.getHandle();
      modHandle.effectiveImports.collectInheritedImports(graph, moduleEp.msgState);
    }
  }
  
  /*
   * The order of module for this project only. Based on hard/soft imports
   */
  private void determineModuleOrder() throws FatalMessageException {
    ClassGraph classGraph = new ClassGraph(Nullable.empty(), msgState);
    for (ModuleEndpoint moduleEp : sources.values(auditTrail)) {
      moduleEp.populateClassGraphPlus(this, classGraph, true);
    }
    Optional<CompleteOrdering> completeOrderingOpt = classGraph.determineAllModsOrder(msgState,
        false);
    if (!completeOrderingOpt.isPresent()) {
      return;
    }
    CompleteOrdering completeOrdering = completeOrderingOpt.get();
    List<ModuleType> ordering = new LinkedList<>();
    for (Entry<ModuleType, ModuleOrdering> entry : completeOrdering.moduleOrdering.entrySet()) {
      if (!sources.containsKey(entry.getKey(), auditTrail)) {
        //only this project's modules
        continue;
      }
      ordering.add(entry.getKey());
    }
    moduleProcessOrder = ordering;
    
    for (Project lib : buildLibs) {
      lib.determineModuleOrder();
    }
  }
  
  private void resetNeedRebuild() throws FatalMessageException {
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.getHandle().needRebuild = false;
    }
    
    for (Project lib : buildLibs) {
      lib.resetNeedRebuild();
    }
  }

  public boolean linkModules(ThreadManager threadManager) throws FatalMessageException {
    //link/verify submods
    if (linkAndVerifySubmods()) {
      return false;
    }
    
    //resolve/verify imports
    if (!resolveAndVerifyImports()) {
      return false;
    }

    //determine rebuild+recreate headers (dfs)
    inheritRebuildFromDependOn();

    //link headers
    threadManager.openQueue();
    linkHeaders(threadManager);
    threadManager.sealQueue(); //no more jobs to add.
    boolean allSuccess = threadManager.waitForJobs(); //wait for all jobs to finish
    if (!allSuccess) {
      //TODO check msgState for the error
      msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "failed to link");
    }
    
    for (Project lib : buildLibs) {
      lib.auditTrail.add(new AuditBasic(AuditEntryType.LINK_HEADERS));
    }
    auditTrail.add(new AuditBasic(AuditEntryType.LINK_HEADERS));
    
    //fill in inherited imports
    ImportGraph graph = buildImportGraph();
    collectInheritedImports(graph);
    
    //determine the module order for other activities later
    //renaming will need to show up in this ordering as well
    determineModuleOrder();
    
    //change all needRebuild to false
    resetNeedRebuild();
    
    //fill in all class headers with isOverrideOf (now that we have linked)
    determineIsOverrideOf();
    
    //fill in effectiveImports withinProject (now that we have linked)
    determineWithinProject();
    
    // resolve all user types within ProgHeaders
    progHeadersResolveUserTypes();
    
    return allSuccess && !hitError();
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Resolve user types for all prog headers
  /////////////////////////////////////////////////////////////////////////////////////////////////
  
  private void progHeadersResolveUserTypes() throws FatalMessageException {
    for (Project lib : buildLibs) {
      lib.progHeadersResolveUserTypes();
    }
    
    for (ModuleType modO : moduleProcessOrder) {
      //TODO multi-thread
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return;
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      
      EffectiveImports effectiveImp = moduleHandle.moduleBuildObj.effectiveImports.applyTransforms(
          getAuditTrail(), modEp.get().msgState);
      
      TypeGraph typeGraph = new TypeGraph(moduleHandle.moduleBuildObj.progHeader.moduleType,
          effectiveImp, msgState);
      TypeEnvironment tenv = moduleHandle.moduleBuildObj.progHeader.createTenv(this, typeGraph,
          modEp.get().msgState, false, true, true).get();
      
      moduleHandle.moduleBuildObj.progHeader.resolveUserTypes(tenv);
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Fill in withinProject of all effectiveImports
  /////////////////////////////////////////////////////////////////////////////////////////////////
  
  private void determineWithinProject() throws FatalMessageException {
    for (Project lib : buildLibs) {
      lib.determineWithinProject();
    }
    
    for (ModuleType modO : moduleProcessOrder) {
      //TODO multi-thread
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return;
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      moduleHandle.effectiveImports.determineWithinProject(this, modEp.get().msgState);
      
      manifest.getEntry(moduleHandle.srcFilename).get().effectiveImports.determineWithinProject(
          this, msgState);
    }
    
    for (BuildManifestProvider provider : manifest.getProviders()) {
      for (BuildEntry entry : provider.getEntries()) {
        entry.effectiveImports.determineWithinProject(this, msgState);
      }
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Fill in isOverrideOf in all ClassHeaders
  /////////////////////////////////////////////////////////////////////////////////////////////////
  
  protected Pair<ProgHeader, TypeEnvironment> createEmptyTopLevelTenv(ModuleEndpoint modEp,
      LoadedModuleHandle moduleHandle)
          throws FatalMessageException {
    ProgHeader progHeader = moduleHandle.moduleBuildObj.progHeader.applyTransforms(this,
        getAuditTrail(), moduleHandle.moduleBuildObj, moduleHandle.registeredGenerics,
        modEp.msgState);
    
    EffectiveImports effectiveImp = moduleHandle.moduleBuildObj.effectiveImports.applyTransforms(
        getAuditTrail(), modEp.msgState);
    
    TypeGraph typeGraph = new TypeGraph(progHeader.moduleType, effectiveImp, msgState);
    TypeEnvironment tenv = progHeader.createTenv(this, typeGraph,
        modEp.msgState).get();
    
    return new Pair<>(progHeader, tenv);
  }
  
  private void determineIsOverrideOf() throws FatalMessageException {
    for (Project lib : buildLibs) {
      lib.determineIsOverrideOf();
    }
    
    for (ModuleType modO : moduleProcessOrder) {
      //TODO multi-thread
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return;
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      
      Pair<ProgHeader, TypeEnvironment> progTenv = createEmptyTopLevelTenv(modEp.get(),
          moduleHandle);
      ProgHeader progHeader = progTenv.a;
      TypeEnvironment tenv = progTenv.b;
      
      progHeader.determineIsOverrideOf(this, tenv,
          modEp.get().msgState);
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Module Graph
  /////////////////////////////////////////////////////////////////////////////////////////////////
  
  /*
   * Takes an import type and figures out the target type.
   */
  public Optional<ImportType> resolveImportType(ImportType importType,
      AuditTrailLite projectAuditTrailSoFar, MsgState msgState, int lineNum, int columnNum)
          throws FatalMessageException {
    // first, lookup the module as best we can
    Nullable<ModuleEndpoint> modEp = lookupModule(importType, projectAuditTrailSoFar);
    if (modEp.isNull()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Failed to find any module associated with " + importType + " or " + importType.outerType,
          lineNum, columnNum);
      return Optional.empty();
    }
    
    // once we have the module, check to see if it was a full match
    ModuleType modType = modEp.get().getModuleType(projectAuditTrailSoFar);
    ImportType modImpType = modType.asImportType();
    if (modImpType.equals(importType)) {
      return Optional.of(modImpType);
    }
    
    // not a full match, which means that the last item of the import must come from within the
    // module itself (e.g. class, enum, function).
    modEp.get().load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
    ProgHeader progHeader = moduleHandle.moduleBuildObj.progHeader.applyTransforms(rootProject,
        projectAuditTrailSoFar, moduleHandle.moduleBuildObj, moduleHandle.registeredGenerics,
        modEp.get().msgState);
    
    Nullable<ProgItemHeader> progItem = progHeader.lookupItem(importType.name);
    if (progItem.isNull()) {
      // import does not actually exist
      msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "Found module " + modType
            + " for import " + importType + " but did not find item within module: "
            + importType.name, lineNum, columnNum);
      return Optional.empty();
    }
    ResolvedImportType impTargetType = progItem.get().getImportType();
    return Optional.of(impTargetType);
  }
  
  public Nullable<ModuleEndpoint> lookupModule(ModuleType moduleType,
      AuditTrailLite projectAuditTrailSoFar) throws FatalMessageException {
    Nullable<ModuleEndpoint> modEp = sources.get(moduleType, projectAuditTrailSoFar);
    if (modEp.isNull()) {
      //search parents
      for (Project lib : libs) {
        Nullable<ModuleEndpoint> tmp = lib.lookupModule(moduleType, projectAuditTrailSoFar);
        if (tmp.isNotNull()) {
          return tmp;
        }
      }
      
      return Nullable.empty();
    }
    return modEp;
  }
  
  public Nullable<ModuleEndpoint> lookupModule(ImportType importType,
      AuditTrailLite projectAuditTrailSoFar) throws FatalMessageException {
    if (importType instanceof ResolvedImportType) {
      return lookupModule(importType.asModuleType(), projectAuditTrailSoFar);
    }
    
    // first assume that this is importing a module fully
    ModuleType modImpType = new ModuleType(importType.outerType, importType.name);
    Nullable<ModuleEndpoint> modEp = lookupModule(modImpType, projectAuditTrailSoFar);
    if (modEp.isNotNull()) {
      return modEp;
    }
    if (importType.outerType.isNull()) {
      return Nullable.empty();
    }
    
    return lookupModule(importType.outerType.get(), projectAuditTrailSoFar);
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Typecheck
  /////////////////////////////////////////////////////////////////////////////////////////////////

  public void typecheck() throws FatalMessageException  {
    callGraph.clear();
    
    typecheck(true);
  }
  
  /*
   * The first typecheck without renamings will track usage of all generic types (register)
   * Then at the end of the typecheck, we will update the serialized generic files, creating
   * local versions of std ones if needed.
   * 
   * The module renamings can be used on these generics.
   */

  public void typecheck(boolean initialCaller) throws FatalMessageException  {
    
    //fully typecheck our libs since no cycles back (they don't need any info from us)
    for (Project lib : buildLibs) {
      // callgraph must map calls that go across projecs
      lib.callGraph.cloneFrom(callGraph); // nodes are exactly the same mem location
      lib.typecheck(false);
    }

    cacheId = 0;
    
    completeClasses.clear();
    initClassMembers.clear();
    
    determineModuleOrder();
    
    for (ModuleType modO : moduleProcessOrder) {
      //multi-thread
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return;
      }
      //TODO compare audit trails
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      
      Pair<ProgHeader, TypeEnvironment> progTenv = createEmptyTopLevelTenv(modEp.get(),
          moduleHandle);
      ProgHeader progHeader = progTenv.a;
      TypeEnvironment tenv = progTenv.b;
      
      if (!moduleHandle.moduleBuildObj.auditTrail.contains(AuditEntryType.TYPECHECK)) {
        //very first typecheck of this module, so track generic registrations
        tenv.setRegisteredGenerics(Nullable.of(moduleHandle.registeredGenerics));
      }
      
      moduleHandle.moduleBuildObj.progressStatus = ProgressStatus.START;
      moduleHandle.moduleBuildObj.program.typecheck(this, moduleHandle, tenv);
      moduleHandle.moduleBuildObj.progressStatus = ProgressStatus.END;
      moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(AuditEntryType.TYPECHECK));
    }
    
    //TODO validate main function
    
    auditTrail.add(new AuditBasic(AuditEntryType.TYPECHECK));
    
    if (initialCaller) {
      // resolve all sandbox functions and determine that no lambda expressions are in any of them
      // nor any calls to sandbox exprs
      // save this info so that resolveExceptions will be able to use it as well
      sandboxCalledFunctions = callGraph.determineSandboxedFns();
      sandboxValidate(sandboxCalledFunctions);
    }
  }
  
  private void sandboxValidate(Set<CallNode> sandboxCalledFunctions2)
      throws FatalMessageException {
    
    for (Project lib : buildLibs) {
      lib.sandboxValidate(sandboxCalledFunctions2);
    }
    
    for (ModuleType modO : moduleProcessOrder) {
      //multi-thread
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return;
      }
      //TODO compare audit trails
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      // ensure that sandboxed functions do not contain lambdas, nor do they contain lambda calls
      moduleHandle.moduleBuildObj.program.sandboxValidate(sandboxCalledFunctions2);
    }
  }
  
  /////////////////////////
  // Resolve User Types (right after first typecheck before serialize)
  /////////////////////////
  
  public Optional<Project> resolveUserTypes() throws FatalMessageException {
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      Optional<Project> tmp = lib.resolveUserTypes();
      if (!tmp.isPresent()) {
        continue;
      }
      newLibs.add(tmp.get());
    }
    
    if (moduleProcessOrder.isEmpty()) {
      determineModuleOrder();
    }
    
    //now update our module progs
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      //TODO compare audit trails
      
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
    
      Optional<Program> newProg = moduleHandle.moduleBuildObj.program.resolveUserTypes(
          modEp.msgState);
      if (!newProg.isPresent()) {
        continue;
      }
      moduleHandle.moduleBuildObj.program = newProg.get();
    }
    
    return Optional.of(this);
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Interp
  /////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Serialize
  /////////////////////////////////////////////////////////////////////////////////////////////////

  public static FileOutputStream getOutputFile(String fname) throws IOException {
    File file = new File(fname);
    FileOutputStream fout = null;
    try {
      fout = new FileOutputStream(file);
    } catch (FileNotFoundException e) {
      // if file doesn't exist, then create it
      if (!file.exists()) {
        try {
          file.createNewFile();
          fout = new FileOutputStream(file);
        } catch (IOException e1) {
          throw new IOException("fatal, unable to serialize: " + fname);
        }
      } else {
        throw new IOException("internal error: path exists but is not a file: " + e);
      }
    }
    return fout;
  }
  
  public void initBuildManifest()
      throws NoSuchAlgorithmException, IOException, FatalMessageException {
    String rootProjDist = buildto.toFile().getAbsolutePath().toString();
    initBuildManifest(rootProjDist);
    
    for (Project lib : buildLibs) {
      lib.initBuildManifest(rootProjDist);
    }
  }
  
  /*
   * If a lib, then generics go into root proj dist
   */
  private void initBuildManifest(String rootProjDist)
      throws NoSuchAlgorithmException, IOException, FatalMessageException {
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      if (manifest.hasEntry(moduleHandle.srcFilename)) {
        continue;
      }
      
      InputStream dataStream = null;
      if (moduleHandle.moduleBuildObj.moduleType.equals(JsmntGlobal.modType)) {
        dataStream = new ByteArrayInputStream(JsmntGlobal.src.getBytes(StandardCharsets.UTF_8));
      } else {
        dataStream = new FileInputStream(new File(moduleHandle.srcFilename));
      }
       
      byte[] checksum = BuildManifest.computeChecksum(dataStream);
      BuildEntry entry = new BuildEntry(manifest,
          checksum, moduleHandle.getModuleType(auditTrail, msgState), moduleHandle.submods,
          moduleHandle.moduleBuildObj.effectiveImports, moduleHandle.srcFilename,
          buildto.toFile().getAbsolutePath() + File.separatorChar
            + moduleHandle.getModuleType(auditTrail, msgState).toString() + "-build",
            rootProjDist + File.separatorChar
            + moduleHandle.getModuleType(auditTrail, msgState).toString() + "-generics");
      manifest.addEntry(entry);
    }
    
    //add providers
    for (Entry<String, Map<ModuleType, ModuleEndpoint>> provider : providers.entrySet()) {
      for (ModuleEndpoint provModEp : provider.getValue().values()) {
        provModEp.load();
        LoadedModuleHandle moduleHandle = (LoadedModuleHandle) provModEp.getHandle();
        if (manifest.hasEntry(moduleHandle.srcFilename)) {
          continue;
        }
        byte[] checksum = BuildManifest.computeChecksum(
            new FileInputStream(new File(moduleHandle.srcFilename)));
        BuildEntry entry = new BuildEntry(manifest,
            checksum, moduleHandle.getModuleType(auditTrail, msgState), moduleHandle.submods,
            moduleHandle.moduleBuildObj.effectiveImports, moduleHandle.srcFilename,
            buildto.toFile().getAbsolutePath() + File.separatorChar
              + moduleHandle.getModuleType(auditTrail, msgState).toString() + "-build_provider",
              rootProjDist + File.separatorChar
              + moduleHandle.getModuleType(auditTrail, msgState).toString() + "-generics_provider");
        manifest.addProviderEntry(provider.getKey(), entry);
      }
    }
  }

  public void serializeToFile(boolean toJson)
      throws IOException, SerializationError, NoSuchAlgorithmException, FatalMessageException {
    serializeToFile(toJson, buildto.toFile().getAbsolutePath().toString());
  }
  
  /*
   * Return abs path where we put the serialized Project file
   */
  private String serializeToFile(boolean toJson, String rootProjDist)
        throws IOException, SerializationError, NoSuchAlgorithmException, FatalMessageException {
    //this project will serialize all the sources as well as our build manifest.
    AstProto.Project.Builder document = AstProto.Project.newBuilder();

    String ext = ".gpb";
    if (toJson) {
      ext = ".json";
    }
    
    document.addAllAuthors(authors);
    document.setProjectNameVersion(projectNameVersion.serialize());
    document.setDescription(description);
    document.setBuildTo(buildto.toFile().getAbsolutePath().toString());

    if (!buildto.toFile().exists()) {
      if (!buildto.toFile().mkdirs()) {
        throw new IOException("Failed to create build directory: " + buildto);
      }
    }

    //add the libs
    for (ProjectNameVersion impLib : desiredLibs) {
      document.addDesiredLibs(impLib.serialize());
    }
    for (Project actualLib : libs) {
      document.addActualLibs(actualLib.projectNameVersion.serialize());
    }
    for (Project lib : buildLibs) {
      String libFname = lib.serializeToFile(toJson, rootProjDist);
      document.addBuildLibs(libFname);
    }
    
    AstProto.BuildManifest.Builder manifestDoc = manifest.serialize();
    FileOutputStream fout = getOutputFile(buildto.toFile().getAbsolutePath().toString()
        + File.separatorChar + BuildManifest.MANIFEST_FILENAME + ext);
    if (toJson) {
      JsonFormat.Printer jprinter = JsonFormat.printer();
      fout.write(jprinter.print(manifestDoc.build()).getBytes());
    } else {
      manifestDoc.build().writeTo(fout);
    }
    fout.close();

    String projAbsPath = buildto.toFile().getAbsolutePath().toString()
        + File.separatorChar + "project.jsmnt-project" + ext;
    System.out.println(projAbsPath);
    
    fout = getOutputFile(projAbsPath);
    if (toJson) {
      JsonFormat.Printer jprinter = JsonFormat.printer();
      fout.write(jprinter.print(document.build()).getBytes());
    } else {
      document.build().writeTo(fout);
    }
    fout.close();
    
    //now serialize the actual sources based on the manifest
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.store();
    }
    
    //serialize the providers
    for (Entry<String, Map<ModuleType, ModuleEndpoint>> provider : providers.entrySet()) {
      //Nullable<BuildManifestProvider> provManifest = manifest.getProvider(provider.getKey());
      for (ModuleEndpoint provModEp : provider.getValue().values()) {
        provModEp.store();
      }
    }
    
    return projAbsPath;
  }

  public static Project deserializeFromFile(String fname, String writeBuildDir)
      throws IOException, SerializationError, FatalMessageException {
    return deserializeFromFile(fname, Nullable.empty(), writeBuildDir, Nullable.empty(),
        Nullable.empty());
  }
  
  private static Project deserializeFromFile(String fname, Nullable<BuildManifest> rootManifest,
      String writeBuildDir, Nullable<Map<ProjectNameVersion, Project>> projectMap,
      Nullable<Project> rootProj)
        throws IOException, SerializationError, FatalMessageException {
    List<String> extTmp = StringFn.split('.', fname);
    if (extTmp.size() < 2) {
      throw new SerializationError("project fname split is too short: " + fname, false);
    }
    String ext = "." + extTmp.get(extTmp.size() - 1);
    if (!ext.equals(".gpb") && !ext.equals(".json")) {
      throw new SerializationError("invalid ext: " + ext, false);
    }
    
    FileInputStream fis = new FileInputStream(new File(fname));

    AstProto.Project document = AstProto.Project.parseFrom(fis);
    fis.close();
    
    //read in the build manifest
    String manifestFname = document.getBuildTo() + File.separatorChar
        + BuildManifest.MANIFEST_FILENAME + ext;
    BuildManifest manifest = BuildManifest.deserializeFromFile(new File(manifestFname),
        writeBuildDir);
    
    //pass this root manifest up to the build libs
    List<Project> buildLibs = new ArrayList<>();
    if (projectMap.isNull()) {
      projectMap = Nullable.of(new HashMap<>());
    }
    
    Set<ProjectNameVersion> desiredLibs = new HashSet<>();
    for (AstProto.ProjectNameVersion lib : document.getDesiredLibsList()) {
      desiredLibs.add(ProjectNameVersion.deserialize(lib));
    }

    Map<String, MsgState> msgStates = new HashMap<>();
    
    //from the build manifest, load all our providers
    Map<String, Map<ModuleType, ModuleEndpoint>> providers = new HashMap<>();
    for (BuildManifestProvider provider : manifest.getProviders()) {
      Map<ModuleType, ModuleEndpoint> provSources = new HashMap<>();
      for (BuildEntry provEntry : provider.getEntries()) {
        msgStates.put(provEntry.srcFilename,
            new MsgState(Nullable.of(provEntry.srcFilename)));
        provSources.put(provEntry.moduleType,
            new ModuleEndpoint(new StoredModuleHandle(provEntry.srcFilename,
                provEntry.moduleType,
                provEntry.submods, provEntry.effectiveImports, false,
                manifest,
                provEntry.getBuildFile(false),
                provEntry.getGenericsFile(false)),
                msgStates.get(provEntry.srcFilename)));
      }
      providers.put(provider.providerName, provSources);
    }
    
    //from the build manifest, load all our sources
    Map<ModuleType, ModuleEndpoint> sources = new HashMap<>();
    for (BuildEntry manifestEntry : manifest.getEntries()) {
      msgStates.put(manifestEntry.srcFilename,
          new MsgState(Nullable.of(manifestEntry.srcFilename)));
      sources.put(manifestEntry.moduleType,
          new ModuleEndpoint(new StoredModuleHandle(manifestEntry.srcFilename,
              manifestEntry.moduleType,
              manifestEntry.submods, manifestEntry.effectiveImports, false,
              manifest,
              manifestEntry.getBuildFile(false),
              manifestEntry.getGenericsFile(false)),
              msgStates.get(manifestEntry.srcFilename)));
    }
    
    Project proj = new Project(document.getAuthorsList(),
        document.getDescription(),
        ProjectNameVersion.deserialize(document.getProjectNameVersion()),
        sources, desiredLibs,
        new File(document.getBuildTo()).toPath(),
        providers, msgStates, manifest);
    if (rootProj.isNotNull()) {
      proj.setRootProject(rootProj.get());
    }
    
    for (String buildLibPath : document.getBuildLibsList()) {
      Project buildLib = Project.deserializeFromFile(buildLibPath,
          Nullable.of(manifest), writeBuildDir, projectMap,
          rootProj.isNull() ? Nullable.of(proj) : rootProj);
      
      buildLibs.add(buildLib);
      projectMap.get().put(buildLib.projectNameVersion, buildLib);
    }
    
    List<Project> actualLibs = new LinkedList<>();
    for (AstProto.ProjectNameVersion impLib : document.getActualLibsList()) {
      ProjectNameVersion impLibName = ProjectNameVersion.deserialize(impLib);
      Project impLibProj = projectMap.get().get(impLibName);
      if (impLibProj == null) {
        throw new SerializationError("did not find import lib: " + impLibName, false);
      }
      actualLibs.add(impLibProj);
    }
    
    proj.buildLibs = buildLibs;
    proj.libs = actualLibs;
    //TODO only load when needed, but keep this for now for debug
    for (ModuleEndpoint modEp : sources.values()) {
      modEp.load();
    }
    for (Map<ModuleType, ModuleEndpoint> provider : providers.values()) {
      for (ModuleEndpoint provModEp : provider.values()) {
        provModEp.load();
      }
    }
    
    return proj;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Renaming Variables/fields
  /////////////////////////////////////////////////////////////////////////////////////////////////

  public Optional<Project> renameIds() throws FatalMessageException {
    //TODO multithread
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      Optional<Project> tmp = lib.renameIds();
      if (!tmp.isPresent()) {
        continue;
      }
      newLibs.add(tmp.get());
    }
    
    if (moduleProcessOrder.isEmpty()) {
      determineModuleOrder();
    }
    
    //update our module headers with renamings of class fields and global functions
    for (ModuleType modO : moduleProcessOrder) {
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return Optional.empty();
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      
      ProgHeader progHeader = moduleHandle.moduleBuildObj.progHeader.applyTransforms(this,
          this.getAuditTrail(), moduleHandle.moduleBuildObj, moduleHandle.registeredGenerics,
          msgState);
      
      moduleHandle.moduleBuildObj.progressStatus = ProgressStatus.START;
      
      AuditRenameIds newAuditEntry = new AuditRenameIds(this,
          moduleHandle.getModuleType(auditTrail, msgState));
      moduleHandle.moduleBuildObj.auditTrail.add(newAuditEntry);
      
      progHeader.populateRenameIds(
          this, newAuditEntry.renamings, moduleHandle.registeredGenerics,
          modEp.get().msgState);
    }
    
    Map<ModuleType, ModuleEndpoint> newSources = new HashMap<>();
    
    //now update our module progs
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      //TODO compare audit trails
      
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      
      Pair<ProgHeader, TypeEnvironment> progTenv = createEmptyTopLevelTenv(modEp,
          moduleHandle);
      TypeEnvironment tenv = progTenv.b;
      
      
      Optional<Program> newProg = moduleHandle.moduleBuildObj.program.renameIds(this, moduleHandle,
          tenv, ((AuditRenameIds) moduleHandle.moduleBuildObj.auditTrail.getLast()).renamings,
          modEp.msgState);
      moduleHandle.moduleBuildObj.progressStatus = ProgressStatus.END;
      if (!newProg.isPresent()) {
        continue;
      }
      moduleHandle.moduleBuildObj.program = newProg.get();
    }
    
    auditTrail.add(new AuditBasic(AuditEntryType.RENAME_IDS));
    
    //Project newProj = new Project(authors, description, description, majorVersion,
    //    minorVersion, sources, newLibs, buildto, providers, msgStates, manifest);
    //newProj.moduleProcessOrder = moduleProcessOrder;
    //newProj.auditTrail = auditTrail;
    return Optional.of(this);
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Renaming Classes/Modules
  /////////////////////////////////////////////////////////////////////////////////////////////////

  private static class SortByModuleDepth implements Comparator<ModuleType> {
    @Override
    public int compare(ModuleType a, ModuleType b) {
      return a.toList().size() - b.toList().size();
    }
  }
  
  private void indexRenameTypes(AuditRenameTypesLite moduleRenamings) throws FatalMessageException {
    
    //The buildLibs is a flat list of projects to build that this project depends on
    //Issue here is that we need to account for having multiple versions of the same project
    // in the buildLibs. Is is dangerous to mash all the renamings together?
    for (Project lib : buildLibs) {
      lib.indexRenameTypes(moduleRenamings);
    }
    
    //generate module/class renamings (e.g. a.Test -> a_JSMNT.Test_1)
    //ensure we always process parent modules before submodules
    //however, if the parent module has a HARD uplink on a submodule, then we have to
    //process the submodule first!
    //so first compute the new name just for the modules, strictly in order of length.
    AuditRenameTypes newAuditEntry = new AuditRenameTypes();
    List<ModuleType> modOrderByDepth = new LinkedList<>(moduleProcessOrder);
    modOrderByDepth.sort(new SortByModuleDepth());
    
    for (ModuleType modO : modOrderByDepth) {
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      
      moduleHandle.moduleBuildObj.program.populateTypeRenamings(newAuditEntry,
          moduleRenamings, true, modEp.get().msgState);
    }
    
    // then we can compute the new enum/class names
    for (ModuleType modO : moduleProcessOrder) {
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      
      moduleHandle.moduleBuildObj.program.populateTypeRenamings(newAuditEntry,
          moduleRenamings, false, modEp.get().msgState);
      moduleHandle.moduleBuildObj.auditTrail.add(newAuditEntry);
    }
    auditTrail.add(moduleRenamings);
  }
  
  private void applyRenameTypes()
      throws FatalMessageException {
    
    for (Project lib : buildLibs) {
      lib.applyRenameTypes();
    }
    
    //apply renamings
    
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      
      //use renamings from imports as well (all types should be abs at this point)
      Program newProg = moduleHandle.moduleBuildObj.program.replaceType(
          moduleHandle.moduleBuildObj.fullTypeRenamings(this, modEp.msgState));
      moduleHandle.moduleBuildObj.program = newProg;
    }
  }
  
  //do after resolving generics
  public Project renameTypes() throws FatalMessageException {
    
    determineModuleOrder();
    
    AuditRenameTypesLite moduleRenamings = new AuditRenameTypesLite();
    indexRenameTypes(moduleRenamings);
    
    applyRenameTypes();
    
    determineModuleOrder();
    
    //Project newProj = new Project(authors, description, description, majorVersion,
    //    minorVersion, sources, libs, buildto, providers, msgStates, manifest);
    //newProj.determineModuleOrder();
    //newProj.auditTrail = auditTrail;
    return this;
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Resolve Generics
  /////////////////////////////////////////////////////////////////////////////////////////////////

  //resolve generics (during FIRST typecheck, register all uses of generic classes - abs path)
  //at every register, create class decl with replacements
  //now insert those replacements into their respective ASTs and do second round of replacements
  //e.g. A<int> used in class becomes A_int
  public Optional<Project> resolveGenerics() throws FatalMessageException  {
    //there must not be any missing resolutions
    //must call renameTypes after this
    
    for (Project lib : buildLibs) {
      lib.resolveGenerics();
    }
    
    if (moduleProcessOrder.isEmpty()) {
      determineModuleOrder();
    }
    
    //reg real type -> reg class decl type
    //do this for our module as well as all effective imported modules
    
    for (ModuleType modO : moduleProcessOrder) {
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return Optional.empty();
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(AuditEntryType.RESOLVE_GENERICS));
      Map<Type, Type> genericToRealMapping =
          moduleHandle.moduleBuildObj.indexGenericToRealMapping(this, getAuditTrail(),
              moduleHandle.registeredGenerics, modEp.get().msgState);
      
      //don't change module append on this run
      Optional<Program> resProg = moduleHandle.moduleBuildObj.program.insertResolvedGenerics(
          this, moduleHandle.registeredGenerics, genericToRealMapping,
          moduleHandle.effectiveImports, modEp.get().msgState);
      
      moduleHandle.moduleBuildObj.program = resProg.get();
    }
    
    auditTrail.add(new AuditBasic(AuditEntryType.RESOLVE_GENERICS));
    
    return Optional.of(this);
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Resolve Vtables
  /////////////////////////////////////////////////////////////////////////////////////////////////

  private Optional<Project> resolveVtablesHelper() throws InterpError, FatalMessageException {
    
    //create global class graph from ONLY parent-child relationships
    ClassGraph classGraph = new ClassGraph(Nullable.empty(), msgState);
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.populateClassGraph(this, classGraph);
    }
    
    //parent class must be processed first, and module with parent class must be processed first
    //now go through all classes and tell them about all the classes which list them as parent
    //Also list which functions have been overridden by the (recursive) child classes
    Optional<CompleteOrdering> completeOrderingOpt = classGraph.determineAllModsOrder(msgState,
        true);
    if (!completeOrderingOpt.isPresent()) {
      return Optional.empty();
    }
    CompleteOrdering completeOrdering = completeOrderingOpt.get();
    
    //this loop operates on class headers and mbo
    for (Entry<ModuleType, ModuleOrdering> modO : completeOrdering.moduleOrdering.entrySet()) {
      Nullable<ModuleEndpoint> modEp = lookupModule(modO.getKey(), auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
            "Failed to find module: " + modO.getKey());
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      
      //first generate vtables for each class (only functions which do not exist in parent and
      //which are overridden in immediate child classes)
      //then stitch vtables together e.g. A_vtables_list + B_vtables_list for class B
      //or generally: parent_vtable + our_vtable
      //updateAllVtables();
      ProgHeader newProg = moduleHandle.moduleBuildObj.progHeader.applyTransforms(this,
          this.getAuditTrail(), moduleHandle.moduleBuildObj, moduleHandle.registeredGenerics,
          msgState);
      newProg.populateVtables(this, classGraph,
          modO.getValue().classOrdering, moduleHandle.moduleBuildObj, msgState);
    }
    
    //this loop operates on program ast (to update with vtable access calls)
    //now replace overridden function calls with vtable calls
    for (Entry<ModuleType, ModuleOrdering> modO : completeOrdering.moduleOrdering.entrySet()) {
      Nullable<ModuleEndpoint> modEp = lookupModule(modO.getKey(), auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return Optional.empty();
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
    
      Optional<Program> newProg = moduleHandle.moduleBuildObj.program.replaceWithVtableAccess(
          this, modEp.get().msgState);
      moduleHandle.moduleBuildObj.program = newProg.get();
    }
    
    return Optional.of(this);
  }
  
  public Optional<Project> resolveVtables() throws InterpError, FatalMessageException {
    for (Project lib : buildLibs) {
      lib.resolveVtablesHelper();
    }
    
    return resolveVtablesHelper();
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  //A-Normal Form
  /////////////////////////////////////////////////////////////////////////////////////////////////

  public Optional<Project> toAnf(AnfTune tuning) throws FatalMessageException  {
    
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      newLibs.add(lib.toAnf(tuning).get());
    }

    for (ModuleType modO : moduleProcessOrder) {
      Nullable<ModuleEndpoint> modEp = lookupModule(modO, auditTrail);
      if (modEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Unable to find module '" + modO + "' in project.");
        return Optional.empty();
      }
      
      modEp.get().load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
      moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(AuditEntryType.ANF));
      moduleHandle.moduleBuildObj.progressStatus = ProgressStatus.START;
      Program newProg = moduleHandle.moduleBuildObj.program.toAnf(tuning);
      moduleHandle.moduleBuildObj.program = newProg;
      moduleHandle.moduleBuildObj.progressStatus = ProgressStatus.END;
    }
    
    auditTrail.add(new AuditBasic(AuditEntryType.ANF));
    
    return Optional.of(this);
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Inits to Constructors
  /////////////////////////////////////////////////////////////////////////////////////////////////

  public Project moveInitsToConstructorsHelper() throws FatalMessageException {
    
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      newLibs.add(lib.moveInitsToConstructorsHelper());
    }

    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      Program newProg = moduleHandle.moduleBuildObj.program.moveInitsToConstructors(this,
          modEp.msgState);
      moduleHandle.moduleBuildObj.program = newProg;
      moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(
          AuditEntryType.MOVE_INITS_TO_CONSTRUCTORS));
    }
    
    auditTrail.add(new AuditBasic(AuditEntryType.MOVE_INITS_TO_CONSTRUCTORS));
    
    return this;
  }
  
  public Project moveInitsToConstructors() throws FatalMessageException {
    
    moveInitsToConstructorsHelper();

    typecheck();
    
    return this;
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Normalize All types to abs module path
  /////////////////////////////////////////////////////////////////////////////////////////////////

  private Project helpNormalizeTypes(boolean keepCurrentModRelative) throws FatalMessageException {
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      newLibs.add(lib.helpNormalizeTypes(keepCurrentModRelative));
    }

    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      Optional<Program> newProg = moduleHandle.moduleBuildObj.program.normalizeTypes(
          keepCurrentModRelative, moduleHandle.getModuleType(auditTrail, msgState),
          moduleHandle.moduleBuildObj.effectiveImports.applyTransforms(getAuditTrail(),
              modEp.msgState),
          msgState);
      moduleHandle.moduleBuildObj.program = newProg.get();
    }
    
    return this;
  }
  
  /*
   * For some cases it is easier to keep current module types as relative rather than abs
   */
  public Project normalizeTypes(boolean keepCurrentModRelative) throws FatalMessageException {
    typecheck(); //ensure all tenv are updated

    return helpNormalizeTypes(keepCurrentModRelative);
  }

  //for transpiling

  public Optional<Project> lrpfApply(String providersKey) throws FatalMessageException  {
    // Loop through provided sources
    // -> Find matching module using lookupModule.
    //    Then have list of lines, and as lines are matched by prov (tenv.lookup maps to ast),
    //    remove from list.
    // -> Loop through our program lines
    //     -> If line is not exported, just add it in. Else, find matching function
    //        (if class, need to folow pathing and iterate functions and add ffiBlock members).
    //        Once match is found (and it is req) set the body. During lookup, make sure not
    //        to travel to imports or submods (stay in module).
    //        This can be verified by tenv.currentModule

    boolean foundUnlinked = false;
    boolean hitErr = false;
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      Optional<Project> opt = lib.lrpfApply(providersKey);
      if (!opt.isPresent()) {
        hitErr = true;
        continue;
      }
      newLibs.add(opt.get());
    }
    if (hitErr) {
      return Optional.empty();
    }

    Map<ModuleType, ModuleEndpoint> newSources = new HashMap<>();
    if (providers.containsKey(providersKey)) {
      //convert providers to dict of module of UserType (module) -> Program
      Map<ModuleType, Program> provDict = new HashMap<>();
      for (ModuleEndpoint modEp : providers.get(providersKey).values()) {
        modEp.load();
        LoadedModuleHandle modHandle = (LoadedModuleHandle) modEp.getHandle();
        if (!modHandle.moduleBuildObj.program.moduleStatement.isProvider) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
              "Only providers may be put under provider directories", -1, -1);
        }
        provDict.put(modHandle.moduleBuildObj.program.moduleStatement.fullType,
            modHandle.moduleBuildObj.program);
        modHandle.moduleBuildObj.auditTrail.add(new AuditBasic(AuditEntryType.LINK_PROVIDERS));
      }

      for (ModuleEndpoint modEp : sources.values(auditTrail)) {
        modEp.load();
        LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
        Program provMod = provDict.get(modEp.getHandle().getModuleType(auditTrail, msgState));
        if (provMod == null) {
          if (moduleHandle.moduleBuildObj.program.findUnlinkedReqFun(modEp.msgState)) {
            foundUnlinked = true;
          }
          //newSources.add(mod.program);
        } else {
          Optional<Program> newProg =
              moduleHandle.moduleBuildObj.program.linkRequiredProvidedFunctions(
              provMod, modEp.msgState);
          if (!newProg.isPresent()) {
            return Optional.empty();
          }
          moduleHandle.moduleBuildObj.program = newProg.get();
        }
        moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(AuditEntryType.LINK_PROVIDERS));
      }
      
    } else {
      for (ModuleEndpoint modEp : sources.values(auditTrail)) {
        modEp.load();
        LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
        
        // no provider, so make sure we were not expecting one
        if (moduleHandle.moduleBuildObj.program.findUnlinkedReqFun(modEp.msgState)) {
          foundUnlinked = true;
        }
        
        moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(AuditEntryType.LINK_PROVIDERS));
      }
    }
    
    auditTrail.add(new AuditBasic(AuditEntryType.LINK_PROVIDERS));
    
    if (foundUnlinked) {
      return Optional.empty();
    }

    //return new Project(authors, name, description, majorVersion, minorVersion, 
    //    newSources, newLibs, buildto, providers, msgStates, manifest);
    return Optional.of(this);
  }

  /*
   * Must be used BEFORE any renaming takes place. After this function is called, classes
   * will be reordered, lifted, renamed, ect...
   */
  public Optional<Project> linkRequiredProvidedFunctions(String providersKey)
      throws FatalMessageException  {

    //generate globalTenv's (and moduleGraph)
    //this typechecks libs too
    typecheck();
    
    Optional<Project> newProject = lrpfApply(providersKey);
    if (!newProject.isPresent()) {
      return Optional.empty();
    }

    //set up scoping and Function.callType properly in ffi functions
    newProject.get().typecheck();

    return newProject;
  }
  
  ////////////////////////////////////////////////////////////////////
  // Remove all non-module imports (after type normalization and import resolution).
  // This makes it simpler for transpilers, since no need for the relative class/function imports
  // once all types are abs (or relative to current module)
  ////////////////////////////////////////////////////////////////////
  
  public Project removeNonModuleImports() throws FatalMessageException {
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      newLibs.add(lib.removeNonModuleImports());
    }
    
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      Program newProg = moduleHandle.moduleBuildObj.program.removeNonModuleImports();
      moduleHandle.moduleBuildObj.program = newProg;
      
      EffectiveImports effectiveImports = moduleHandle.moduleBuildObj.effectiveImports.removeNonModuleImports();
      
      moduleHandle.moduleBuildObj.effectiveImports = effectiveImports;
      moduleHandle.effectiveImports = effectiveImports;
      Nullable<BuildEntry> buildEntry = moduleHandle.manifest.getEntry(moduleHandle.srcFilename);
      if (buildEntry.isNull()) {
        throw new IllegalArgumentException();
      }
      buildEntry.get().effectiveImports = effectiveImports;
      
      moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(
          AuditEntryType.REMOVE_NON_MODULE_IMPORTS));
    }
    
    auditTrail.add(new AuditBasic(AuditEntryType.REMOVE_NON_MODULE_IMPORTS));
    
    return this;
  }
  
  ////////////////////////////////////////////////////////////////////
  // add 'this' expression where needed
  ////////////////////////////////////////////////////////////////////
  
  private Project addThisExpressionHelper() throws FatalMessageException {
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      newLibs.add(lib.addThisExpressionHelper());
    }
    
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      ProgHeader progHeader = moduleHandle.moduleBuildObj.progHeader.applyTransforms(this,
          getAuditTrail(), moduleHandle.moduleBuildObj, moduleHandle.registeredGenerics, msgState);
      Program newProg = moduleHandle.moduleBuildObj.program.addThisExpression(progHeader,
          modEp.msgState);
      moduleHandle.moduleBuildObj.program = newProg;
    }
    
    return this;
  }
  
  public Project addThisExpression() throws FatalMessageException {
    Project newProj = addThisExpressionHelper();
    
    // to fill in determined type
    newProj.typecheck();
    
    return newProj;
  }
  
  ////////////////////////////////////////////////////////////////////
  // Add the start() function
  ////////////////////////////////////////////////////////////////////
  
  private Project insertStartFunctionHelper() throws FatalMessageException {
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      newLibs.add(lib.insertStartFunctionHelper());
    }
    
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      Program newProg = moduleHandle.moduleBuildObj.program.insertStartFunction();
      moduleHandle.moduleBuildObj.program = newProg;
      moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(
          AuditEntryType.INSERT_START_FUNCTION));
    }
    
    auditTrail.add(new AuditBasic(AuditEntryType.INSERT_START_FUNCTION));
    
    return this;
  }
  
  /*
   * start(argv) wraps main(argv), but start will never take exc as an arg, and will be the final
   * handler for all exceptions. start cannot take extra args, it will only ever take argv.
   */
  public Project insertStartFunction() throws FatalMessageException {
    Project newProject = insertStartFunctionHelper();
    
    newProject.typecheck();

    return newProject;
  }
  
  ////////////////////////////////////////////////////////////////////
  // Resolve Exceptions
  ////////////////////////////////////////////////////////////////////

  private Optional<Project> resolveExceptionsHelper(Renamings jsmntGlobalRenamings)
      throws FatalMessageException {
    boolean hitErr = false;
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      Optional<Project> opt = lib.resolveExceptionsHelper(jsmntGlobalRenamings);
      if (!opt.isPresent()) {
        hitErr = true;
        continue;
      }
      newLibs.add(opt.get());
    }
    if (hitErr) {
      return Optional.empty();
    }
    
    Map<ModuleType, ModuleEndpoint> newSources = new HashMap<>();
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      AuditResolveExceptions auditEntry = new AuditResolveExceptions();
      Optional<Program> newProg = moduleHandle.moduleBuildObj.program.resolveExceptions(
          this, auditEntry, jsmntGlobalRenamings, modEp.msgState);
      moduleHandle.moduleBuildObj.auditTrail.add(auditEntry);
      if (!newProg.isPresent()) {
        hitErr = true;
        continue;
      }
      moduleHandle.moduleBuildObj.program = newProg.get();
    }
    auditTrail.add(new AuditBasic(AuditEntryType.RESOLVE_EXCEPTIONS));
    if (hitErr) {
      return Optional.empty();
    }
    
    return Optional.of(this);
  }
  
  // return true on success
  private boolean linkResolvedExceptionsAuditEntries() throws FatalMessageException {
    boolean hitErr = false;
    for (Project lib : buildLibs) {
      if (!lib.linkResolvedExceptionsAuditEntries()) {
        hitErr = true;
        continue;
      }
    }
    if (hitErr) {
      return false;
    }
    
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      AuditEntry tmpEntry = moduleHandle.moduleBuildObj.auditTrail.getLast();
      if (!(tmpEntry instanceof AuditResolveExceptions)) {
        hitErr = true;
        continue;
      }
      AuditResolveExceptions auditEntry = (AuditResolveExceptions) tmpEntry;
      
      ProgHeader progHeader = moduleHandle.moduleBuildObj.progHeader.applyTransforms(this,
          getAuditTrail(), moduleHandle.moduleBuildObj, moduleHandle.registeredGenerics,
          modEp.msgState);
      if (!progHeader.linkResolvedExceptionsAuditEntries(this,
          auditEntry, modEp.msgState)) {
        hitErr = true;
        continue;
      }
    }
    return !hitErr;
  }
  
  public Renamings getJsmntGlobalRenamings(AuditTrailLite projectAuditTrailSoFar)
      throws FatalMessageException {
    Nullable<ModuleEndpoint> globalModEp = lookupModule(JsmntGlobal.modType,
        projectAuditTrailSoFar);
    if (globalModEp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Failed to find Jasmint Global lib: " + JsmntGlobal.modType);
    }
    globalModEp.get().load();
    LoadedModuleHandle globalModHandle = (LoadedModuleHandle) globalModEp.get().getHandle();
    Nullable<AuditEntry> globalRenames = globalModHandle.moduleBuildObj.auditTrail.getCombined(
        AuditEntryType.RENAME_IDS);
    if (globalRenames.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "Have not done any renaming yet");
    }
    AuditRenameIds globalRenaming = (AuditRenameIds) globalRenames.get();
    return globalRenaming.renamings;
  }
  
  public Optional<Project> resolveExceptions() throws FatalMessageException {
    typecheck();
    
    /*
     * Find out renamings for jsmnt_global exception classes
     */
    Renamings globalRenamings = getJsmntGlobalRenamings(getAuditTrail());
    
    /*
     * For every sandbox fn call, we need to copy the corresponding function decl and give it a
     * new name, along with adding fn_ctx. Then all function calls within sandbox mode need to use
     * the copy (with the added argument), while all function calls outside of sandbox mode
     * will continue to use the original.
     */
    
    Optional<Project> newProject = resolveExceptionsHelper(globalRenamings);
    if (!newProject.isPresent()) {
      return Optional.empty();
    }
    
    /*
     * Now link the audit entries we just made.
     * 
     * If Obj.hash() is sandboxed, then every class that inherits from obj (even recursively), needs
     * to know about it (this is just one example)
     */
    if (!newProject.get().linkResolvedExceptionsAuditEntries()) {
      return Optional.empty();
    }
    
    newProject.get().typecheck();

    return newProject;
  }
  
  ////////////////////////////////////////////////////////////////////
  // Insert Destructors
  ////////////////////////////////////////////////////////////////////
  
  public Project insertDestructorCallsHelper() throws FatalMessageException {
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      Project opt = lib.insertDestructorCallsHelper();
      newLibs.add(opt);
    }
    
    Map<ModuleType, ModuleEndpoint> newSources = new HashMap<>();
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      Program newProg = moduleHandle.moduleBuildObj.program.insertDestructorCalls(this,
          modEp.msgState);
      moduleHandle.moduleBuildObj.auditTrail.add(new AuditBasic(
          AuditEntryType.INSERT_DESTRUCTOR_CALLS));
      moduleHandle.moduleBuildObj.program = newProg;
    }
    auditTrail.add(new AuditBasic(AuditEntryType.INSERT_DESTRUCTOR_CALLS));
    
    return this;
  }
  
  public Project insertDestructorCalls() throws FatalMessageException {
    return insertDestructorCallsHelper();
  }
  
  ////////////////////////////////////////////////////////////////////
  // Lift Class Declarations
  ////////////////////////////////////////////////////////////////////
  
  private Project liftClassDeclarationsHelper() throws FatalMessageException {
    List<Project> newLibs = new LinkedList<>();
    for (Project lib : buildLibs) {
      Project opt = lib.liftClassDeclarationsHelper();
      newLibs.add(opt);
    }
    
    Map<ModuleType, ModuleEndpoint> newSources = new HashMap<>();
    for (ModuleEndpoint modEp : sources.values(auditTrail)) {
      modEp.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.getHandle();
      AuditLiftedClasses auditEntry = new AuditLiftedClasses();
      Program newProg = moduleHandle.moduleBuildObj.program.liftClassDeclarations(this,
          auditEntry, modEp.msgState);
      moduleHandle.moduleBuildObj.auditTrail.add(auditEntry);
      moduleHandle.moduleBuildObj.program = newProg;
    }
    auditTrail.add(new AuditBasic(AuditEntryType.LIFT_CLASSES));
    
    return this;
  }
  
  public Project liftClassDeclarations() throws FatalMessageException {
    return liftClassDeclarationsHelper();
  }
  
  ////////////////////////////////////////////////////////////////////
  // Error Reporting
  ////////////////////////////////////////////////////////////////////
  
  /*
   * Creates the error message to display
   */
  public String errorReport() {
    String report = "";
    for (Project lib : buildLibs) {
      report += lib.errorReport();
    }
    
    for (MsgState ms : msgStates.values()) {
      report += ms.toString();
    }
    
    report += msgState.toString();
    if (!report.isEmpty()) {
      report = "No-can-do'sville, baby doll!\n" + report;
    }
    return report;
  }

  /*
   * Check if we hit any errors
   */
  public boolean hitError() {
    for (Project lib : buildLibs) {
      if (lib.hitError()) {
        return true;
      }
    }
    
    for (MsgState ms : msgStates.values()) {
      if (ms.hitError()) {
        return true;
      }
    }

    return msgState.hitError();
  }

  //takes abs path to class
  public PsuedoClassHeader lookupClassHeader(UserDeclType classTy, Nullable<ModuleBuildObj> ourMbo,
      boolean applyTransforms, AuditTrailLite projectAuditTrailSoFar)
      throws FatalMessageException {
    
    if (classTy.outerType.isNull()) {
      //actually it could, but we want to normalize stuff asap
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Project cannot lookup relative class: " + classTy);
    }
    
    //TODO could optimize
    Nullable<ModuleEndpoint> modEp = lookupModule(classTy.outerType.get(), projectAuditTrailSoFar);
    if (modEp.isNull()) {
      msgState.addMessage(MsgType.FATAL, MsgClass.TYPECHECK,
          "Failed to lookup module for class: " + classTy);
    }
    
    modEp.get().load();
    LoadedModuleHandle handle = (LoadedModuleHandle) modEp.get().getHandle();
    ModuleBuildObj mbo = handle.moduleBuildObj;
    if (ourMbo.isNotNull()) {
      mbo = ourMbo.get();
      if (!classTy.outerType.get().equals(ourMbo.get().moduleType)) {
        //class header not in mbo, so find correct one
        mbo = handle.moduleBuildObj;
      }
    }
    
    //now find the class header from the mbo
    ProgHeader progHeader = mbo.progHeader;
    if (applyTransforms) {
      progHeader = progHeader.applyTransforms(this, projectAuditTrailSoFar, mbo,
          handle.registeredGenerics, msgState);
    }
    Nullable<PsuedoClassHeader> ch = progHeader.getClassHeader(classTy.name);
    if (ch.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Did not find class '" + classTy + "' in prog header: "
              + mbo.progHeader.getClassNames());
    }
    return ch.get();
  }
  
  public boolean lookupClassIsComplete(UserDeclType classDeclType) {
    boolean complete = false;
    
    for (Project lib : libs) {
      complete = lib.lookupClassIsComplete(classDeclType);
      if (complete) {
        return true;
      }
    }
    
    return completeClasses.contains(classDeclType);
  }

  public Collection<ModuleEndpoint> getSources() throws FatalMessageException {
    return sources.values(auditTrail);
  }

  public void addCompletedClass(UserDeclType classDeclType) {
    completeClasses.add(classDeclType);
  }
  
  public boolean lookupClassMemberIsInit(UserDeclType classDeclType, String memberId) {
    boolean complete = false;
    
    for (Project lib : libs) {
      complete = lib.lookupClassMemberIsInit(classDeclType, memberId);
      if (complete) {
        return true;
      }
    }
    
    return initClassMembers.contains(new Pair<>(classDeclType, memberId));
  }
  
  public void addInitClassMember(ClassDeclType classDeclType, String memberId) {
    initClassMembers.add(new Pair<>(classDeclType, memberId));
  }

  public void removeInitClassMember(UserDeclType classDeclType, String memberId) {
    initClassMembers.remove(new Pair<>(classDeclType, memberId));
  }

  public void setBuildLibs(List<Project> newLibs, MsgState msgState)
      throws FatalMessageException {
    if (!this.buildLibs.isEmpty()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.PROJECT_ASSEMBLE,
          "Already set build libs for " + this);
    }
    
    //only the rootProj should have libs
    for (Project lib : newLibs) {
      if (!lib.buildLibs.isEmpty()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.PROJECT_ASSEMBLE, "Project lib "
            + lib.projectNameVersion + " cannot have its own build libs: " + lib.buildLibs);
      }
    }
    this.buildLibs  = newLibs;
  }

  public void addLib(Project newLib, MsgState msgState) throws FatalMessageException {
    for (Project lib : libs) {
      if (newLib.projectNameVersion.equals(lib.projectNameVersion)) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.PROJECT_ASSEMBLE,
            "Project " + this + " already has lib " + newLib);
      }
    }
    this.libs.add(newLib);
  }

  public List<Project> getBuildLibs() {
    return buildLibs;
  }

  public void setRootProject(Project project) {
    rootProject = project;
  }

  public String dirPath() {
    return projectNameVersion.toFilePath();
  }

  public ModuleType.ProjectNameVersion getModProjectNameVersion() {
    return projectNameVersion.toModProjectNameVersion();
  }

  public Set<CallNode> getSandboxedFunctions() {
    return rootProject.sandboxCalledFunctions;
  }
  
}
