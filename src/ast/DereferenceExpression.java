package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.ReferenceValue;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;
import util.ScopedIdentifiers;

public class DereferenceExpression extends AbstractExpression {

  public final Expression dereferenced; // must return a reference value

  public DereferenceExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression dereferenced) {
    super(lineNum, columnNum, AstType.DEREFERENCE_EXPRESSION, preComments, postComments);
    this.dereferenced = dereferenced;
  }

  public DereferenceExpression(int lineNum, int columnNum, Expression dereferenced) {
    super(lineNum, columnNum, AstType.DEREFERENCE_EXPRESSION);
    this.dereferenced = dereferenced;
  }

  public DereferenceExpression(Expression dereferenced) {
    super(AstType.DEREFERENCE_EXPRESSION);
    this.dereferenced = dereferenced;
  }

  @Override
  public String toString(String indent) {
    return preToString() + "*" + dereferenced.toString(indent) + postToString();
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    DereferenceExpression val = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, dereferenced.renameIds(renamings, msgState));
    val.determinedType = determinedType.renameIds(renamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> derefTy = dereferenced.typecheck(tenv, Nullable.empty());
    if (!derefTy.isPresent()) {
      return Optional.empty();
    }

    if (!(derefTy.get().type instanceof ReferenceType)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot dereference type: " + derefTy, lineNum, columnNum);
      return Optional.empty();
    }

    determinedType = ((ReferenceType) derefTy.get().type).innerType;
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value derefVal = dereferenced.interp(env);

    if (!(derefVal instanceof ReferenceValue)) {
      throw new InterpError("Cannot dereference value that is not a reference: " + derefVal,
          lineNum, columnNum);
    }

    return ((ReferenceValue) derefVal).referenced.getValue().copy(null);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.DereferenceExpression.Builder subDoc = AstProto.DereferenceExpression.newBuilder();

    subDoc.setDereferenced(dereferenced.serialize().build());

    document.setDereferenceExpression(subDoc);
    return document;
  }

  public static DereferenceExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.DereferenceExpression document) throws SerializationError {
    return new DereferenceExpression(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getDereferenced()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return dereferenced.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof DereferenceExpression)) {
      return false;
    }
    DereferenceExpression ourOther = (DereferenceExpression) other;
    return ourOther.dereferenced.compare(dereferenced);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, (Expression) dereferenced.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return dereferenced.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    Pair<List<DeclarationStatement>, Ast> dereferencedPair = dereferenced.toAnf(tuning);
    insert.addAll(dereferencedPair.a);

    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, (Expression) dereferencedPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, dereferenced.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, (Expression) dereferenced.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments,
        dereferenced
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    dereferenced.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return dereferenced.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return dereferenced.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = dereferenced.resolveExceptions(project,
        currentModule, auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement,
        enableResolution,
        hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    Expression newDereferenced = (Expression) tmp.get().ast.get();

    if (enableResolution) {
      // before dereferencing, must ensure that the target is not null
      // if it is, then throw an exception
      preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
          jsmntGlobalRenamings,
          hitExcId, withinFn.get(), jsmntGlobalRenamings.nullPtrExc,
          new BinaryExpression(BinOp.EQUAL, newDereferenced, new NullExpression()), false,
          msgState));
    }

    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, newDereferenced);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, dereferenced.insertDestructorCalls(scopedIds, destructorRenamings));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, dereferenced.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, dereferenced.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    DereferenceExpression value = new DereferenceExpression(lineNum, columnNum, preComments,
        postComments, dereferenced.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
