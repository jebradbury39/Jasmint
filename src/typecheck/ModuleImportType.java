package typecheck;

import ast.SerializationError;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import errors.Nullable;
import java.util.List;
import java.util.Map;
import util.DotGen;

public class ModuleImportType extends ResolvedImportType {

  public ModuleImportType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.MODULE_IMPORT_TYPE, outerType, name);
  }
  
  @Override
  public Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.ModuleImportType.Builder subDoc = AstProto.ModuleImportType.newBuilder();
    if (outerType.isNotNull()) {
      subDoc.addOuterType(outerType.get().serialize());
    }
    subDoc.setName(name);
    
    document.setModuleImportType(subDoc);
    return document;
  }
  
  public static ModuleImportType deserialize(AstProto.ModuleImportType document)
      throws SerializationError {
    Nullable<ModuleType> outerType = Nullable.empty();
    if (document.getOuterTypeCount() > 0) {
      outerType = Nullable.of((ModuleType) AbstractType.deserialize(document.getOuterType(0)));
    }
    return new ModuleImportType(outerType, document.getName());
  }

  @Override
  public UserDeclType asUserDeclType() {
    return new ModuleType(outerType, name);
  }

  @Override
  public ModuleType asModuleType() {
    return new ModuleType(outerType, name);
  }

  @Override
  public ImportType onlyName() {
    return new ModuleImportType(Nullable.empty(), name);
  }

  @Override
  public ResolvedImportType replaceModuleType(ModuleType newModType) {
    return (ResolvedImportType) newModType.asImportType();
  }
  
  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type tmp = mapFromTo.get(this);
    if (tmp == null) {
      return this;
    }
    if (tmp instanceof ModuleType) {
      return ((ModuleType) tmp).asImportType();
    }
    if (tmp instanceof ModuleImportType) {
      return tmp;
    }
    throw new IllegalArgumentException("expected ModuleType or ModuleImportType");
  }
  
  @Override
  public UserDeclType fromList(List<String> path) {
    if (path.isEmpty()) {
      throw new IllegalArgumentException();
    }
    final String name = path.get(path.size() - 1);
    List<String> subPath = path.subList(0, path.size() - 1);
    
    Nullable<ModuleType> moduleType = Nullable.empty();
    for (String item : subPath) {
      moduleType = Nullable.of(new ModuleType(moduleType, item));
    }
    return new ModuleImportType(moduleType, name);
  }

  @Override
  public DotGen.Color getGraphColor() {
    return DotGen.Color.BLACK;
  }
}
