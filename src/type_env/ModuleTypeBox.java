package type_env;

import errors.FatalMessageException;
import java.util.Map;
import java.util.Optional;
import typecheck.ImportType;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;
import typecheck.Type;

public class ModuleTypeBox extends UserDeclTypeBox {
  
  public final ModuleType moduleType;
  public final TypeEnvironment tenv;

  public ModuleTypeBox(ModuleType moduleType, boolean export, TypeEnvironment tenv) {
    super(export, true, VariableScoping.GLOBAL);
    this.moduleType = moduleType;
    this.tenv = tenv;
  }
  
  @Override
  public String toString() {
    return "Module(" + moduleType + ", export = " + export + ")";
  }

  @Override
  public TypeBox fullCopy() {
    return new ModuleTypeBox(moduleType, export, tenv);
  }

  @Override
  public TypeBox replaceType(TypeEnvironment tenv, Map<Type, Type> mapFromTo, int lineNum,
      int columnNum) throws FatalMessageException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv) {
    return Optional.of(this.tenv);
  }

  @Override
  public Type getType() {
    return moduleType;
  }

  @Override
  public ImportType getImportType(ImportType importType) {
    return (ResolvedImportType) moduleType.asImportType();
  }

}
