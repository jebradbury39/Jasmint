package multifile;

import ast.SerializationError;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import header.RegisteredGenerics;
import import_mgmt.EffectiveImports;
import java.io.IOException;
import java.util.Set;
import mbo.ModuleBuildObj;
import typecheck.ModuleType;

/*
 * This module is stored in a build directory
 */
public class StoredModuleHandle extends ModuleHandle {
  
  public final String progHeaderFilename;
  public final String genericsFilename;
  
  public StoredModuleHandle(String srcFilename, ModuleType moduleType, Set<ModuleType> submods,
      EffectiveImports effectiveImports, boolean needRebuild, BuildManifest manifest,
      String progHeaderFilename, String genericsFilename) {
    super(srcFilename, moduleType, submods, effectiveImports, needRebuild, manifest);
    this.progHeaderFilename = progHeaderFilename;
    this.genericsFilename = genericsFilename;
    
    if (progHeaderFilename.equals(genericsFilename)) {
      throw new IllegalArgumentException(progHeaderFilename + " == " + genericsFilename);
    }
  }
  
  @Override
  public synchronized ModuleHandle load(Project project, MsgState msgState)
      throws FatalMessageException, IOException, SerializationError {
    
    //TODO handle generics to load in RegisteredGenerics
    RegisteredGenerics registeredGenerics = RegisteredGenerics.deserializeFromFile(
        genericsFilename, msgState);
    
    ModuleBuildObj mbo = null;
    try {
      mbo = ModuleBuildObj.deserializeFromFile(project, progHeaderFilename, msgState);
    } catch (IOException e) {
      msgState.addMessage(MsgType.FATAL, MsgClass.DESERIALIZE,
          "Failed to load module: " + getModuleType(project.getAuditTrail(), msgState)
          + " with err: " + e.getMessage());
      return null; //fatal exn thrown
    }
    
    return new LoadedModuleHandle(srcFilename, needRebuild, manifest, mbo,
        registeredGenerics);
  }
  
  @Override
  public ModuleHandle store(MsgState msgState) {
    return this;
  }
}
