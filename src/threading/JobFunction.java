package threading;

import errors.FatalMessageException;

public interface JobFunction {
  Job.Status function(Object ctx) throws FatalMessageException;
}
